<?php
/*
 * The template for displaying the footer.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

if ($zipprich_meta) {
	$zipprich_hide_footer  = $zipprich_meta['hide_footer'];
} else { $zipprich_hide_footer = ''; }
if (!$zipprich_hide_footer) { // Hide Footer Metabox
?>

	<!-- Footer -->
	<footer class="ziph-footer_area">

		<?php
			$footer_widget_block = cs_get_option('footer_widget_block');
			if (isset($footer_widget_block)) {
	      // Footer Widget Block
	      get_template_part( 'layouts/footer/footer', 'widgets' );
	    }

	    $need_copyright = cs_get_option('need_copyright');
			if (isset($need_copyright)) {
	      // Copyright Block
      	get_template_part( 'layouts/footer/footer', 'copyright' );
	    }
    ?>

	</footer>
	<!-- Footer -->

<?php } // Hide Footer Metabox ?>

</div><!-- #vtheme-wrapper -->

<?php wp_footer(); ?>

</body>
</html>
