<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php
// if the `wp_site_icon` function does not exist (ie we're on < WP 4.3)
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
  if (cs_get_option('brand_fav_icon')) {
    echo '<link rel="shortcut icon" href="'. esc_url(wp_get_attachment_url(cs_get_option('brand_fav_icon'))) .'" />';
  }
  if (cs_get_option('iphone_icon')) {
    echo '<link rel="apple-touch-icon" sizes="57x57" href="'. esc_url(wp_get_attachment_url(cs_get_option('iphone_icon'))) .'" >';
  }
  if (cs_get_option('iphone_retina_icon')) {
    echo '<link rel="apple-touch-icon" sizes="114x114" href="'. esc_url(wp_get_attachment_url(cs_get_option('iphone_retina_icon'))) .'" >';
    echo '<link name="msapplication-TileImage" href="'. esc_url(wp_get_attachment_url(cs_get_option('iphone_retina_icon'))) .'" >';
  }
  if (cs_get_option('ipad_icon')) {
    echo '<link rel="apple-touch-icon" sizes="72x72" href="'. esc_url(wp_get_attachment_url(cs_get_option('ipad_icon'))) .'" >';
  }
  if (cs_get_option('ipad_retina_icon')) {
    echo '<link rel="apple-touch-icon" sizes="144x144" href="'. esc_url(wp_get_attachment_url(cs_get_option('ipad_retina_icon'))) .'" >';
  }
}
$zipprich_all_element_color  = cs_get_customize_option( 'all_element_colors' );
?>
<meta name="msapplication-TileColor" content="<?php echo esc_attr($zipprich_all_element_color); ?>">
<meta name="theme-color" content="<?php echo esc_attr($zipprich_all_element_color); ?>">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
// Metabox
global $post;
$zipprich_id    = ( isset( $post ) ) ? $post->ID : false;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $zipprich_id : false;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

// Header Style
if ($zipprich_meta) {
  $zipprich_header_design   = $zipprich_meta['select_header_design'];
  $zipprich_cart_widget     = $zipprich_meta['cart_widget'];
  $zipprich_search_icon     = $zipprich_meta['search_icon'];
} else {
  $zipprich_header_design   = cs_get_option('select_header_design');
  $zipprich_cart_widget     = cs_get_option('cart_widget');
  $zipprich_search_icon     = cs_get_option('search_icon');
}
if ($zipprich_header_design === 'default') {
  $zipprich_header_design_actual  = cs_get_option('select_header_design');
} else {
  $zipprich_header_design_actual = ( $zipprich_header_design ) ? $zipprich_header_design : cs_get_option('select_header_design');
}
if ($zipprich_meta && $zipprich_header_design !== 'default') {
  $zipprich_cart_widget     = $zipprich_meta['cart_widget'];
  $zipprich_search_icon     = $zipprich_meta['search_icon'];
} else {
  $zipprich_cart_widget     = cs_get_option('cart_widget');
  $zipprich_search_icon     = cs_get_option('search_icon');
}
if ($zipprich_header_design_actual === 'style_two') {
  $zipprich_header_class = 'ziph-header_styl-2 ';
  $zipprich_nav_row_class = 'ziph-header_nav-row-padding ';
  $zipprich_nav_wrap_class = '';
  $zipprich_nav_wrap_class_2 = 'ziph-header-main-menu';
} else {
  $zipprich_nav_row_class = '';
  $zipprich_header_class = '';
  $zipprich_nav_wrap_class = 'ziph-header-main-menu';
  $zipprich_nav_wrap_class_2 = '';
}

if ($zipprich_meta) {
  $zipprich_hide_header = $zipprich_meta['hide_header'];
} else { $zipprich_hide_header = ''; }

// Header Style
if ($zipprich_meta && $zipprich_header_design !== 'default') {
  $zipprich_address_info  = $zipprich_meta['header_address_info'];
} else {
  $zipprich_address_info  = cs_get_option('header_address_info');
}

wp_head();
?>
</head>
<body <?php body_class(); ?>>

<div id="zipprich-wrapper" class="ziph_page"> <!-- #vtheme-wrapper -->
  <!-- Header -->
  <header class="ziph-header_area <?php echo esc_attr( $zipprich_header_class ); ?>">
    <?php get_template_part( 'layouts/header/top', 'bar' ); // Topbar ?>
    <?php if(!$zipprich_hide_header) { ?>
    <!-- Navigation -->
    <div class="ziph-header_navigation <?php echo esc_attr( $zipprich_nav_wrap_class ); ?>">
      <div class="container">
        <div class="row <?php echo esc_attr( $zipprich_nav_row_class ); ?>">
          <!-- logo start-->
          <div class="col-md-3 col-xs-6 col-sm-4">
            <?php get_template_part( 'layouts/header/logo' ); // Logo ?>
          </div><!-- logo end-->
          <div class="col-md-9 col-xs-6 col-sm-8">
            <?php if ($zipprich_header_design_actual === 'style_two') { ?>
            <div class="text-right">
              <?php echo do_shortcode($zipprich_address_info); ?>
            </div>
            <?php } else {
              get_template_part( 'layouts/header/menu', 'bar' ); // Navigation
            } ?>
          </div>
        </div> <!-- Row -->
      </div> <!-- Container -->
    </div> <!-- ziph-header_navigation -->

    <?php if ($zipprich_header_design_actual === 'style_two') { ?>
    <div class="ziph-header_navigation ziph-main-navigation-area <?php echo esc_attr( $zipprich_nav_wrap_class_2 ); ?>">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="visible-sm visible-xs">
              <?php get_template_part( 'layouts/header/menu', 'bar' ); // Samll screen Navigation ?>
            </div>
            <!-- mainmenu  start -->
            <div class="ziph-flt_left ziph-mainmenu_warp">
              <div class="hidden-sm hidden-xs ziph-inline">
                <?php get_template_part( 'layouts/header/menu', 'bar' ); // Large screen Navigation ?>
              </div>
              <div class="ziph-hm2hdr_icons">
                <?php if($zipprich_search_icon) { ?>
                <div class="ziph-hm2hdr_icon ziph-hdricon-search">
                  <a href="#">
                    <img src="<?php echo esc_url(ZIPPRICH_IMAGES); ?>/search-icon.png" alt="<?php esc_attr_e( 'Search', 'zipprich' ); ?>">
                  </a>
                  <form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" class="ziph-wigtsrch_form ziph-menu_form" >
                    <input type="text" name="s" id="s" class="ziph-wigtsrch_field" placeholder="<?php esc_attr_e('Search','zipprich'); ?>" />
                    <button type="submit" id="searchsubmit" class="ziph-wigtsrch_submit"></button>
                  </form>
                </div>
                <?php } ?>
                <?php if($zipprich_cart_widget) { ?>
                <div class="ziph-hm2hdr_icon ziph-hdricon-cart">
                  <a href="<?php echo esc_url(wc_get_cart_url()); ?>">
                    <img src="<?php echo esc_url(ZIPPRICH_IMAGES); ?>/cart_icon.png" alt="<?php esc_attr_e( 'Cart', 'zipprich' ); ?>">
                  </a>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php } // Style Two?>
    <?php } // Hide Header ?>
  </header>

  <?php
  // Title Area
  $zipprich_need_title_bar = cs_get_option('need_title_bar');
  if (isset($zipprich_need_title_bar)) {
    get_template_part( 'layouts/header/title', 'bar' );
  }