<?php
/*
 * The template for displaying archive pages.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();

// Theme Options
$zipprich_blog_style = cs_get_option('blog_listing_style');
$zipprich_sidebar_position = cs_get_option('blog_sidebar_position');

// Style
if ($zipprich_blog_style === 'style-two') {
	$zipprich_blog_style = ' ziph-fix ziph-blogGrid_warp masonry-loop ';
} else {
	$zipprich_blog_style = ' ziph-blogList_warp ';
}

// Sidebar Position
if ($zipprich_sidebar_position === 'sidebar-hide') {
	$layout_class = 'row';
	$zipprich_sidebar_class = 'ziph-page_content ';
} else {
	if(is_active_sidebar( 'sidebar-1' )) {
		$layout_class = 'ziph-fix ziph-flt_left ziph-leftContent_warp ';
		$zipprich_sidebar_class = 'ziph-pageWite_sidebar ';		
	} else {
		$layout_class = 'row';
		$zipprich_sidebar_class = 'ziph-page_content ';		
	}
}
?>

<!-- ==== |Page content start|==== \-->
<div class="<?php echo esc_attr( $zipprich_sidebar_class ); ?>">
	<div class="container">
		<!-- ====Page content Left column start==== \-->
		<div class="<?php echo esc_attr($layout_class); ?>">
			<!-- Left content here start\-->
			<div class="<?php echo esc_attr($zipprich_blog_style);?>">
				<?php
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							get_template_part( 'layouts/post/content' );
						endwhile;
					else :
						get_template_part( 'layouts/post/content', 'none' );
					endif; 
				?>
			</div><!-- Blog Div -->
			<?php
				zipprich_paging_nav();
				wp_reset_postdata();  // avoid errors further down the page
			?>
		</div><!-- Content Area -->
		<?php
			if ($zipprich_sidebar_position !== 'sidebar-hide') {
				get_sidebar(); // Sidebar
			}
		?>
	</div>
</div>

<?php
get_footer();