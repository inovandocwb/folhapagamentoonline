<?php
/*
 * Codestar Framework - Custom Style
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/* All Dynamic CSS Styles */
if ( ! function_exists( 'zipprich_dynamic_styles' ) ) {
  function zipprich_dynamic_styles() {

    // Typography
    $zipprich_vt_get_typography  = zipprich_vt_get_typography();

    $all_element_color  = cs_get_customize_option( 'all_element_colors' );
    $secondary_element_color  = cs_get_customize_option( 'secondary_element_colors' );

    // Logo
    $brand_logo_top     = cs_get_option( 'brand_logo_top' );
    $brand_logo_bottom  = cs_get_option( 'brand_logo_bottom' );

    // Layout
    $bg_type = cs_get_option('theme_layout_bg_type');
    $bg_pattern = cs_get_option('theme_layout_bg_pattern');
    $bg_image = cs_get_option('theme_layout_bg');
    $bg_overlay_color = cs_get_option('theme_bg_overlay_color');

    // Footer
    $footer_bg_color  = cs_get_customize_option( 'footer_bg_color' );
    $footer_heading_color  = cs_get_customize_option( 'footer_heading_color' );
    $footer_text_color  = cs_get_customize_option( 'footer_text_color' );
    $footer_link_color  = cs_get_customize_option( 'footer_link_color' );
    $footer_link_hover_color  = cs_get_customize_option( 'footer_link_hover_color' );

  ob_start();

global $post;
$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

/* Top Bar - Customizer - Background */
$topbar_bg_color  = cs_get_customize_option( 'topbar_bg_color' );
if ($topbar_bg_color) {
echo <<<CSS
  .no-class {}
  .ziph-headerVery_top, .ziph-header_top, .ziph-header_styl-2 .ziph-header_top {
    background-color: {$topbar_bg_color};
  }
CSS;
}
if ($zipprich_meta) {
  $topbar_border_color  = $zipprich_meta['topbar_border'];
} else {
  $topbar_border_color  = cs_get_customize_option( 'topbar_border_color' );
}
$topbar_border_color = ( $topbar_border_color ) ? $zipprich_meta['topbar_border'] : cs_get_customize_option( 'topbar_border_color' );
if ($topbar_border_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_top {
    border-bottom: 1px solid {$topbar_border_color};
  }
CSS;
}
$topbar_text_color  = cs_get_customize_option( 'topbar_text_color' );
if ($topbar_text_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_top, .ziph-header_top i {
    color: {$topbar_text_color};
  }
CSS;
}
$topbar_link_color  = cs_get_customize_option( 'topbar_link_color' );
if ($topbar_link_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_top a {
    color: {$topbar_link_color};
  }
CSS;
}
$topbar_link_hover_color  = cs_get_customize_option( 'topbar_link_hover_color' );
if ($topbar_link_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_top a:hover, .ziph-header_top a:focus, .ziph-header_top a:hover i, .ziph-header_top a:focus i {
    color: {$topbar_link_hover_color};
  }
CSS;
}
$topbar_social_color  = cs_get_customize_option( 'topbar_social_color' );
if ($topbar_social_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_top .ziph-headV_social a, .ziph-header_top .ziph-headV_social i {
    color: {$topbar_social_color};
  }
CSS;
}
$topbar_social_hover_color  = cs_get_customize_option( 'topbar_social_hover_color' );
if ($topbar_social_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_top .ziph-headV_social a:hover, .ziph-header_top .ziph-headV_social a i:hover {
    color: {$topbar_social_hover_color};
  }
CSS;
}

/* Header - Customizer */
$header_bg_color  = cs_get_customize_option( 'header_bg_color' );
if ($header_bg_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_navigation.ziph-main-navigation-area.ziph-header-main-menu {
    background-color: {$header_bg_color};
  }
CSS;
}
$header_link_color  = cs_get_customize_option( 'header_link_color' );
$header_link_hover_color  = cs_get_customize_option( 'header_link_hover_color' );
if($header_link_color || $header_link_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-header_navigation .ziph-mainmenu > li > a,
  .ziph-header_navigation .ziph-hm2hdrcontct_txt h6,
  .ziph-header_navigation .ziph-hm2hdrcontct_txt h5 a,
  .ziph-header_styl-2 .ziph-mainmenu > ul > li > a {
    color: {$header_link_color} !important;
  }

  .no-class {}
  .ziph-header_styl-2 .ziph-mainmenu > ul li.current-menu-ancestor > a, .ziph-header_styl-2 .ziph-mainmenu > ul li.current_page_item > a,
  .ziph-header_styl-2 .ziph-mainmenu > ul > li > a:hover, .ziph-header_styl-2 .ziph-mainmenu > ul > li > a:focus {
    color: {$header_link_hover_color} !important;
  }
CSS;
}

$submenu_bg_color  = cs_get_customize_option( 'submenu_bg_color' );
$submenu_border_color  = cs_get_customize_option( 'submenu_border_color' );
$submenu_link_color  = cs_get_customize_option( 'submenu_link_color' );
$submenu_link_hover_color  = cs_get_customize_option( 'submenu_link_hover_color' );
if ($submenu_bg_color || $submenu_border_color || $submenu_link_color || $submenu_link_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-mainmenu .dropdown-menu > li > a,
  .ziph-header_styl-2 .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a,
  .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a {
    border-color: {$submenu_border_color};
    color: {$submenu_link_color};
  }
  .ziph-mainmenu .dropdown-menu > li > a:focus,
  .ziph-mainmenu .dropdown-menu > li > a:hover,
  .ziph-mainmenu .dropdown-menu > .active > a,
  .ziph-mainmenu .dropdown-menu > .active > a:focus,
  .ziph-mainmenu .dropdown-menu > .active > a:hover,
  .ziph-mainmenu ul.sub-menu > li:hover,
  .ziph-mainmenu ul.sub-menu > li.current-menu-item,
  .ziph-mainmenu ul.sub-menu > li a:hover,
  .ziph-header_styl-2 .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a:hover,
  .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a:hover, 
  .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a:focus {
    color: {$submenu_link_hover_color};
  }
  .dropdown-menu,
  .ziph-mainmenu ul.sub-menu li a,
  .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu {
    background-color: {$submenu_bg_color};
  }
  .ziph-mainmenu ul.sub-menu li a {
    color: {$submenu_link_color};
  }
  .ziph-mainmenu ul li li a,
  .ziph-mainmenu ul li li li a,
  .ziph-mainmenu ul li li li li a,
  .ziph-mainmenu ul li li li li li a,
  .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a {
    border-top-color: {$submenu_border_color} !important;
  }
CSS;
}

/* Title Area - Theme Options - Background */
$titlebar_bg = cs_get_option('titlebar_bg');
$title_heading_color  = cs_get_customize_option( 'titlebar_title_color' );
if ($titlebar_bg) {

  $title_area = ( ! empty( $titlebar_bg['image'] ) ) ? 'background-image: url('. $titlebar_bg['image'] .');' : '';
  $title_area .= ( ! empty( $titlebar_bg['repeat'] ) ) ? 'background-repeat: '. $titlebar_bg['repeat'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['position'] ) ) ? 'background-position: '. $titlebar_bg['position'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['attachment'] ) ) ? 'background-attachment: '. $titlebar_bg['attachment'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['size'] ) ) ? 'background-size: '. $titlebar_bg['size'] .';' : '';
  $title_area .= ( ! empty( $titlebar_bg['color'] ) ) ? 'background-color: '. $titlebar_bg['color'] .';' : '';

echo <<<CSS
  .no-class {}
  .ziph-pagebanner_height-200 {
    {$title_area}
  }
CSS;
}
if ($title_heading_color) {
echo <<<CSS
  .no-class {}
  .ziph-pagebanner_height-200 .ziph-page_title {
    color: {$title_heading_color};
  }
CSS;
}

// Breadcrubms
$breadcrumbs_text_color  = cs_get_customize_option( 'breadcrumbs_text_color' );
$breadcrumbs_link_color  = cs_get_customize_option( 'breadcrumbs_link_color' );
$breadcrumbs_link_hover_color  = cs_get_customize_option( 'breadcrumbs_link_hover_color' );
$breadcrumbs_bg_color  = cs_get_customize_option( 'breadcrumbs_bg_color' );
if ($breadcrumbs_text_color) {
echo <<<CSS
  .no-class {}
  .ziph-page_breadcrumb li {
    color: {$breadcrumbs_text_color};
  }
CSS;
}
if ($breadcrumbs_link_color) {
echo <<<CSS
  .no-class {}
  .ziph-page_breadcrumb li a {
    color: {$breadcrumbs_link_color};
  }
CSS;
}
if ($breadcrumbs_link_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-page_breadcrumb li a:hover {
    color: {$breadcrumbs_link_hover_color};
  }
CSS;
}

/* Footer */
if ($footer_bg_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_top {background: {$footer_bg_color};}
CSS;
}
if ($footer_heading_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_top .ziph-footrwidget_title {color: {$footer_heading_color} !important;}
CSS;
}
if ($footer_text_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_top,
  .ziph-footer_top p,
  .ziph-ftrform_warp .wpcf7 .wpcf7-form-control-wrap input,
  .ziph-ftrform_warp input,
  .ziph-ftrform_warp textarea,
  .ziph-footer_widget.vt-text-widget {color: {$footer_text_color} !important;}

  .ziph-ftrform_warp input,
  .ziph-ftrform_warp .wpcf7 textarea {border-color: {$footer_text_color} !important;}
CSS;
}
if ($footer_link_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_widget a,
  .ziph-footer_widget ul li,
  .ziph-footer_widget ul li a,
  .ziph-footer_widget .widget_list_style ul a,
  .ziph-footer_widget .widget_categories ul a,
  .ziph-footer_widget .widget_archive ul a,
  .ziph-footer_widget .widget_archive ul li,
  .ziph-footer_widget .widget_recent_comments ul a,
  .ziph-footer_widget .widget_recent_entries ul a,
  .ziph-footer_widget .widget_meta ul a,
  .ziph-footer_widget .widget_pages ul a,
  .ziph-footer_widget .widget_rss ul a,
  .ziph-footer_widget .widget_nav_menu ul a {color: {$footer_link_color};}
CSS;
}
if ($footer_link_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_widget a:hover,
  .ziph-footer_widget ul li a:hover,
  .ziph-footer_widget .widget_list_style ul a:hover,
  .ziph-footer_widget .widget_categories ul a:hover,
  .ziph-footer_widget .widget_archive ul a:hover,
  .ziph-footer_widget .widget_archive ul li,
  .ziph-footer_widget .widget_recent_comments ul a:hover,
  .ziph-footer_widget .widget_recent_entries ul a:hover,
  .ziph-footer_widget .widget_meta ul a:hover,
  .ziph-footer_widget .widget_pages ul a:hover,
  .ziph-footer_widget .widget_rss ul a:hover,
  .ziph-footer_widget .widget_nav_menu ul a:hover {color: {$footer_link_hover_color} !important;}
CSS;
}

/* Copyright */
$copyright_text_color  = cs_get_customize_option( 'copyright_text_color' );
$copyright_link_color  = cs_get_customize_option( 'copyright_link_color' );
$copyright_link_hover_color  = cs_get_customize_option( 'copyright_link_hover_color' );
$copyright_bg_color  = cs_get_customize_option( 'copyright_bg_color' );
if ($copyright_bg_color) {
echo <<<CSS
  .no-class {}
  footer.ziph-footer_area {background: {$copyright_bg_color};}
CSS;
}
if ($copyright_text_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_bottom,
  .ziph-footer_bottom p {color: {$copyright_text_color};}
CSS;
}
if ($copyright_link_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_bottom a {color: {$copyright_link_color} !important;}
CSS;
}
if ($copyright_link_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-footer_bottom a:hover {color: {$copyright_link_hover_color} !important;}
CSS;
}

/* Primary Colors */
if ($all_element_color) {
echo <<<CSS
  .no-class {}
.ziph-headlogin_btn a.btn:hover, .ziph-headlogin_btn a.btn:focus, .ziph-bdr_btn:hover, .ziph-bdr_btn:focus, .ziph-tblpricing, .ziph-crshap, .ziph-footrwidget_social a:hover, .ziph-footrwidget_social a:focus, .ziph-ftrform_warp button[type="submit"], .ziph-ftrform_warp input[type="submit"], .ziph-slidcap_txt3 .btn.ziph-btn:hover, .ziph-slidcap_txt3 .btn.ziph-btn:focus, .ziph-slidcap_txt.ziph-hm2slidcap_txt .ziph-slidcap_btns .ziph-bdr_btn.ziph-sldbtn_pri .ziph-hm2callout_area .ziph-callout_price .btn.ziph-btn:hover, .ziph-hm2callout_area .ziph-callout_price .btn.ziph-btn:focus, .ziph-about_us .btn.ziph-btn, .ziph-process_icon .ziph-badge, .ziph-process_price:hover, .ziph-process_price:focus, .ziph-shost_table .table > tfoot > tr > td .btn.ziph-btn, .ziph-affilte_table .table-bordered > thead > tr > th, #comments.comments-area #respond #commentform .form-submit input[type="submit"], .ziph-singlePost_content .post-password-form input[type="submit"]:hover, .ziph-singlePost_content .post-password-form input[type="submit"]:focus,#comments.comments-area .comment-content .post-password-form input[type="submit"]:hover,#comments.comments-area .comment-content .post-password-form input[type="submit"]:focus, .ziph-contactC_btn:hover, .ziph-contactC_btn:focus, .ziph-contactC_btn[aria-expanded="true"], .ziph-submit_btn, .ziph-contactForm_warp .wpcf7 input[type="submit"], .wpcf7 .ziph-file-upload .ziph-file-btn, .ziph-lrForm_social ul li a i, .woocommerce span.onsale, .woocommerce ul.products li.product .ajax_add_to_cart:hover, .woocommerce ul.products li.product .ajax_add_to_cart:focus, .woocommerce ul.products li.product .product_type_variable.add_to_cart_button:hover, .woocommerce ul.products li.product .product_type_variable.add_to_cart_button:focus,.woocommerce ul.products li.product .ziph-producDetails_btn:hover,.woocommerce ul.products li.product .ziph-producDetails_btn:focus, .woocommerce #respond input#submit.alt,.woocommerce a.button.alt,.woocommerce button.button.alt,.woocommerce input.button.alt, .woocommerce div.product .woocommerce-tabs ul.tabs li a::before, .woocommerce #review_form #respond input[type="submit"], .woocommerce a.button:not(.ajax_add_to_cart), .woocommerce form.login input[type="submit"],.woocommerce form.register input[type="submit"], .woocommerce-checkout .checkout_coupon input[type="submit"]:hover, .woocommerce-checkout .checkout_coupon input[type="submit"]:focus, .woocommerce-cart .cart_totals tr.shipping th .shipping-calculator-form .button:hover, .woocommerce-cart .cart_totals tr.shipping th .shipping-calculator-form .button:focus,.woocommerce-cart .cart_totals tr.shipping td .shipping-calculator-form .button:hover,.woocommerce-cart .cart_totals tr.shipping td .shipping-calculator-form .button:focus, .woocommerce-account .ziph-page_warp .woocommerce .woocommerce-MyAccount-content [type="submit"].button, .ziph-404Page_warp a.ziph-404_btn, .ziph-mainmenu > ul li.menu-item-has-children.megamenu-item > ul.sub-menu:before, div.ziph-domainType_single .btn.ziph-btn:hover, div.ziph-domainType_single .btn.ziph-btn:focus, ul.ziph-domainPrice_nav li a:before, #bridge .navbar-main, #bridge input.btn.btn-info, #bridge .announcement-single h3 .label, #bridge .announcement-single h2 .label, #bridge .list-group-item.active, #bridge .list-group-item:hover, #bridge .list-group-item:focus, #bridge .main-content input.btn:hover, #bridge .main-content input.btn:focus, #bridge .logincontainer input#login, #bridge .form-horizontal button.btn[type="submit"],#bridge .using-password-strength input.btn[type="submit"],#bridge .logincontainer button.btn[type="submit"], .whmpress_price_matrix table th, .whmpress_price_matrix_domain table th, .woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce .price_slider_amount .button, .woocommerce .price_slider_amount .button:hover, .ziph-contactForm_warp.contact-box-two .wpcf7 input[type="submit"], .ziph-contactForm_warp.contact-box-two .wpcf7 input[type="submit"]:hover, .ziph-about_us .ziph-bdr_btn, .woocommerce form.woocommerce-ResetPassword.lost_reset_password input[type="submit"], .woocommerce ul.products li.product .button.product_type_external:hover, .woocommerce button.button.alt.disabled, #bridge .btn,#bridge  #order-standard_cart .order-summary h2, #bridge #order-standard_cart .view-cart-items-header {background-color: {$all_element_color};}

a:hover, a:focus, .ziph-serv_txt h4 a:hover, .ziph-serv_txt h4 a:focus, .ziph-process_single h4 a:hover,.ziph-process_single h4 a:focus, .ziph-hmlblog_txt a.ziph-hmlblog_link:hover, .ziph-hmlblog_txt a.ziph-hmlblog_link:focus, .ziph-faq[data-show="true"] .panel-heading.ziph-faq_head, .ziph-faq[data-show="true"] .panel-heading.ziph-faq_head .panel-title, .ziph-faq[data-show="true"] .panel-heading.ziph-faq_head .panel-title a, .panel-heading.ziph-faq_head:hover, .panel-heading.ziph-faq_head:focus, .panel-heading.ziph-faq_head .panel-title:hover, .panel-heading.ziph-faq_head .panel-title:focus, .panel-heading.ziph-faq_head .panel-title a:hover, .panel-heading.ziph-faq_head .panel-title a:focus, .ziph-goodness_txt h4 a:hover, .ziph-goodness_txt h4 a:focus, .ziph-footer_widget ul li a:hover, .ziph-footer_widget ul li a:focus, .ziph-slidcap_txt2 p a:hover, .ziph-slidcap_txt2 p a:focus, .ziph-slidcap_txt3 p a:hover, .ziph-slidcap_txt3 p a:focus, .ziph-hm2hdrcontct_icon, .ziph-hm2hdrcontct_txt h5 a:hover, .ziph-hm2hdrcontct_txt h5 a:focus, .ziph-team_info .ziph-tm_name a:hover, .ziph-team_info .ziph-tm_name a:focus, .ziph-team_info .ziph-tm_intro, .ziph-team_socail ul li a:hover, .ziph-team_socail ul li a:focus,.ziph-readmore_link:hover,.ziph-readmore_link:focus,.ziph-commnt_link:hover,.ziph-commnt_link:focus, .ziph-readmore_link i, .ziph-page_breadcrumb li a, .ziph-about_activity h4 a:hover, .ziph-about_activity h4 a:focus, .ziph-revtestimonial2_warp .ziph-revtestimonial_info, .ziph-revtestimonial2_warp a, .ziph-shost_table .table-bordered > tbody > tr > td .fa-check-circle,.ziph-shost_table .table-bordered > tbody > tr > th .fa-check-circle,.ziph-shost_table .table-bordered > tfoot > tr > td .fa-check-circle,.ziph-shost_table .table-bordered > tfoot > tr > th .fa-check-circle,.ziph-shost_table .table-bordered > thead > tr > td .fa-check-circle,.ziph-shost_table .table-bordered > thead > tr > th .fa-check-circle, .ziph-sigldmn_feture h4 a:hover, .ziph-sigldmn_feture h4 a:focus, .ziph-submitbdr_btn:hover, .ziph-submitbdr_btn:focus, .ziph-side-widget.widget_archive ul, .ziph-side-widget.widget_categories ul, .ziph-side-widget.widget_pages ul, .ziph-side-widget.widget_meta ul, .ziph-side-widget.widget_recent_comments ul, .ziph-side-widget.widget_recent_entries ul, .ziph-side-widget.widget_rss ul, .ziph-side-widget.widget_nav_menu ul, .ziph-side-widget.widget_archive ul li a:hover, .ziph-side-widget.widget_archive ul li a:focus, .ziph-side-widget.widget_categories ul li a:hover, .ziph-side-widget.widget_categories ul li a:focus, .ziph-side-widget.widget_pages ul li a:hover, .ziph-side-widget.widget_pages ul li a:focus, .ziph-side-widget.widget_meta ul li a:hover, .ziph-side-widget.widget_meta ul li a:focus, .ziph-side-widget.widget_recent_comments ul li a:hover, .ziph-side-widget.widget_recent_comments ul li a:focus, .ziph-side-widget.widget_recent_entries ul li a:hover, .ziph-side-widget.widget_recent_entries ul li a:focus, .ziph-side-widget.widget_rss ul li a:hover, .ziph-side-widget.widget_rss ul li a:focus, .ziph-side-widget.widget_nav_menu ul li a:hover, .ziph-side-widget.widget_nav_menu ul li a:focus, .ziph-wigtResentPost_single .ziph-wigtRp_title:hover, .ziph-wigtResentPost_single .ziph-wigtRp_title:focus, .ziph-side-widget .tagcloud a:hover, .ziph-side-widget .tagcloud a:focus, .nav-links .page-numbers.current,.nav-links .page-numbers.prev i,.nav-links .page-numbers.next i, .nav-links .page-numbers:hover, .nav-links .page-numbers:focus, .ziph-singlePost_content ul.ziph-unordered_list li, .ziph-sigl_tags a:hover, .ziph-sigl_tags a:focus, .ziph-siglShare ul li a:hover, .ziph-siglShare ul li a:focus,#comments.comments-area .ziph-comments-meta h4 a:hover,#comments.comments-area .ziph-comments-meta h4 a:focus, #comments.comments-area a.comment-reply-link:hover, #comments.comments-area a.comment-reply-link:focus,#comments.comments-area a.comment-reply-link span:hover,#comments.comments-area a.comment-reply-link span:focus, .ziph-contactForm_title a:hover, .ziph-contactForm_title a:focus,.ziph-contactMInfo_title a:hover,.ziph-contactMInfo_title a:focus,.ziph-contactMI_name a:hover,.ziph-contactMI_name a:focus,.ziph-contactMInfo_txt h5 a:hover,.ziph-contactMInfo_txt h5 a:focus, .ziph-contactMInfo_txt h5, .ziph-contactMInfo_txt h5 a, .ziph-contactMInfo_txt ul li a:hover, .ziph-contactMInfo_txt ul li a:focus, .ziph-lrFormOptnlink a, .woocommerce ul.products li.product a .woocommerce-loop-product__title:hover, .woocommerce ul.products li.product a .woocommerce-loop-product__title:focus, .woocommerce ul.products li.product .price, .woocommerce ul.products li.product .added_to_cart.wc-forward:hover, .woocommerce ul.products li.product .added_to_cart.wc-forward:focus, .woocommerce ul.cart_list li .product-title:hover, .woocommerce ul.cart_list li .product-title:focus, .woocommerce ul.product_list_widget li .product-title:hover, .woocommerce ul.product_list_widget li .product-title:focus, .woocommerce ul.cart_list li .woocommerce-Price-amount,.woocommerce ul.product_list_widget li .woocommerce-Price-amount, .woocommerce div.product .woocommerce-review-link:hover, .woocommerce div.product .woocommerce-review-link:focus, .woocommerce div.product .entry-summary .product_meta > span a:hover, .woocommerce div.product .entry-summary .product_meta > span a:focus, .mc4wp-form-fields input[type="email"]:focus ~ button, .woocommerce-info a:hover, .woocommerce-info a:focus,.woocommerce-message a:hover,.woocommerce-message a:focus, .woocommerce-cart .woocommerce table.shop_table tbody td.product-name a:hover, .woocommerce-cart .woocommerce table.shop_table tbody td.product-name a:focus, .woocommerce-cart .cart_totals table.shop_table tr.shipping th a,.woocommerce-cart .cart_totals table.shop_table tr.shipping td a, .woocommerce-account .ziph-page_warp .woocommerce a:hover, .woocommerce-account .ziph-page_warp .woocommerce a:focus, .woocommerce-account .ziph-page_warp .woocommerce .woocommerce-MyAccount-navigation ul, .ziph-404Page_warp h2 span, .ziph-megmenuSingle_cont a, .ziph-domainType h5, ul.ziph-domainPrice_nav li.active a, ul.ziph-domainPrice_nav li:hover a, .ziph-domainAffilt_txt h2 span, #bridge #top-nav a:hover, #bridge #top-nav a:focus, #bridge .home-shortcuts ul li a i, #bridge .home-shortcuts ul li:hover p, #bridge .home-shortcuts ul li:focus p, #bridge .announcement-single h3 a:hover, #bridge .announcement-single h3 a:focus, #bridge #main-body .main-content > p a, #bridge div.header-lined .breadcrumb li a, #bridge .alert-info:before, #bridge .announcement-single h2 a:hover, #bridge .announcement-single h2 a:focus, .language-popover .popover-content li a:hover, .language-popover .popover-content li a:focus, .ziph-whmpressKnowl_single h3 a:hover, .ziph-whmpressKnowl_single h3 a:focus, .ziph-contactCI_txt a:hover, .ziph-contactCI_txt a:focus, .wp-pagenavi span.current, .ziph-footer_widget .tagcloud a:hover, .ziph-footer_widget .tagcloud a:focus,.ziph-side-widget.woocommerce.widget_layered_nav ul li:before,.ziph-side-widget.woocommerce.widget_layered_nav ul li a:hover, .ziph-brand-color {color: {$all_element_color};}

.ziph-bdr_btn, .ziph-ftrform_warp .wpcf7 .wpcf7-form-control-wrap input:focus, .ziph-ftrform_warp .wpcf7 .wpcf7-form-control-wrap textarea:focus, .ziph-hm2callout_area .ziph-callout_price .btn.ziph-btn, .ziph-submitbdr_btn, .ziph-side-widget .tagcloud a:hover, .ziph-side-widget .tagcloud a:focus, #comments.comments-area #respond #cancel-comment-reply-link:hover, #comments.comments-area #respond #cancel-comment-reply-link:focus,.woocommerce ul.cart_list li del:after, .woocommerce ul.product_list_widget li del:after, .ziph-contactForm_warp.contact-box-one .wpcf7 input[type="submit"], .ziph-contactForm_warp.contact-box-one .wpcf7 input[type="submit"]:hover,#bridge .btn, #bridge #order-standard_cart .view-cart-items {border-color: {$all_element_color};}

  .woocommerce .woocommerce-message {border-top-color: {$all_element_color};}
CSS;
}

/* Secondary Colors */
if ($secondary_element_color) {
echo <<<CSS
  .no-class {}
.ziph-headlogin_btn a.btn, .ziph-headerVery_top .ziph-headlogin_btn .btn.btn-info:hover, .ziph-headerVery_top .ziph-headlogin_btn .btn.btn-info:focus, .ziph-headV_social ul li a:hover, .ziph-headV_social ul li a:focus, .ziph-bdr_btn.ziph-sldbtn_sed:hover, .ziph-bdr_btn.ziph-sldbtn_sed:focus, .btn.ziph-btn, .ziph-testimonial_carousel .owl-dots .owl-dot.active, .ziph-ftrform_warp button[type="submit"]:hover, .ziph-ftrform_warp input[type="submit"]:hover, .ziph-ftrform_warp button[type="submit"]:focus, .ziph-domainsrch_form input[type="submit"],.whmpress_domain_search_ajax .search_btn, .ziph-slidcap_txt3 .ziph-slidcap_cms li a:hover, .ziph-slidcap_txt3 .ziph-slidcap_cms li a:focus, .ziph-slidcap_txt.ziph-hm2slidcap_txt .ziph-slidcap_btns .ziph-bdr_btn.ziph-sldbtn_pri:hover, .ziph-slidcap_txt.ziph-hm2slidcap_txt .ziph-slidcap_btns .ziph-bdr_btn.ziph-sldbtn_pri:focus, .ziph-slidcap_price:before,.ziph-slidcap_price, .ziph-about_us .btn.ziph-btn:hover, .ziph-about_us .btn.ziph-btn:focus, .ziph-vds_text .ziph-bdr_btn:hover, .ziph-vds_text .ziph-bdr_btn:focus, .ziph-process_price, .ziph-affilt_btn:hover, .ziph-affilt_btn:focus, .ziph-affilt_btn.active, .woocommerce-cart table.shop_table.cart .coupon input.button:hover, .woocommerce-cart table.shop_table.cart .coupon input.button:focus, .ziph-404Page_warp a.ziph-404_btn:hover, .ziph-404Page_warp a.ziph-404_btn:focus, .ziph-domainAffilt_txt .ziph-bdr_btn:hover, .ziph-domainAffilt_txt .ziph-bdr_btn:focus, #bridge input.btn.btn-warning, #bridge .main-content input.btn, #bridge .logincontainer input#login:hover, #bridge .logincontainer input#login:focus, #bridge .form-horizontal button.btn[type="submit"]:hover, #bridge .form-horizontal button.btn[type="submit"]:focus,#bridge .using-password-strength input.btn[type="submit"]:hover,#bridge .using-password-strength input.btn[type="submit"]:focus,#bridge .logincontainer button.btn[type="submit"]:hover,#bridge .logincontainer button.btn[type="submit"]:focus, .whmpress_domain_search_bulk button.search_btn:hover, .whmpress_domain_search_bulk button.search_btn:focus,.ziph-about_us .ziph-bdr_btn:hover,.ziph-about_us .ziph-bdr_btn:focus,.ziph-about_us .ziph-bdr_btn:active, .ziph-header_styl-2 .ziph-headlogin_btn a.btn:hover, #bridge #order-standard_cart .btn-checkout, #bridge #order-standard_cart .btn-checkout:hover {background-color: {$secondary_element_color};}

.ziph-slidcap_txt h2 span, .ziph-headerVery_top .ziph-head_info a:hover, .ziph-headerVery_top .ziph-head_info a:focus, .ziph-slidcap_list ul li a:hover, .ziph-slidcap_list ul li a:focus, .ziph-domainsrch_links a:hover, .ziph-domainsrch_links a:focus, .ziph-slidcap_txt2 h2 span, .ziph-slidcap_txt2 p span, .ziph-slidcap_txt2 p a, .ziph-slidcap_txt3 h2 span, .ziph-slidcap_txt3 p span, .ziph-slidcap_txt3 p a, .ziph-page_breadcrumb li a:hover, .ziph-page_breadcrumb li a:focus, .ziph-revtestimonial2_warp a:hover, .ziph-revtestimonial2_warp a:focus, .ziph-contactMInfo_txt h5 a:hover, .ziph-contactMInfo_txt h5 a:focus, .woocommerce-cart .cart_totals table.shop_table tr.shipping th a:hover, .woocommerce-cart .cart_totals table.shop_table tr.shipping th a:focus,.woocommerce-cart .cart_totals table.shop_table tr.shipping td a:hover,.woocommerce-cart .cart_totals table.shop_table tr.shipping td a:focus, .ziph-megmenuSingle_cont a:hover, .ziph-megmenuSingle_cont a:focus, #bridge #main-body .main-content > p a:hover, #bridge #main-body .main-content > p a:focus, .ziph-header_styl-2 .ziph-head_info a:hover {color: {$secondary_element_color};}

.ziph-bdr_btn.ziph-sldbtn_sed, .ziph-slidcap_txt3 .ziph-slidcap_cms li a:hover, .ziph-slidcap_txt3 .ziph-slidcap_cms li a:focus, .ziph-slidcap_txt.ziph-hm2slidcap_txt .ziph-slidcap_btns .ziph-bdr_btn.ziph-sldbtn_pri:hover, .ziph-slidcap_txt.ziph-hm2slidcap_txt .ziph-slidcap_btns .ziph-bdr_btn.ziph-sldbtn_pri:focus, .ziph-vds_text .ziph-bdr_btn, .woocommerce-cart table.shop_table.cart .coupon input.button, .ziph-domainAffilt_txt .ziph-bdr_btn, .whmpress_domain_search_bulk button.search_btn, #bridge #order-standard_cart .btn-checkout {border-color: {$secondary_element_color};}

CSS;
}

// Content Colors
$body_color  = cs_get_customize_option( 'body_color' );
if ($body_color) {
echo <<<CSS
  .no-class {}
  .ziph-post_header .ziph-post_excerpt p,
  .ziph-singlePost_content,
  .ziph-singlePost_content p, .comment-content p,
  .ziph-siglShare_txt,
  .ziph-post_excerpt p,
  .ziph-commnt_link a,
  .ziph-post_date, .ziph-post_date a,
  .ziph-blogList_post .ziph-post_excerpt p, .ziph-singlePost_warp .ziph-post_excerpt p,
  .ziph-page_warp .woocommerce {color: {$body_color};}
CSS;
}
$body_links_color  = cs_get_customize_option( 'body_links_color' );
if ($body_links_color) {
echo <<<CSS
  .no-class {}
  .ziph-post_header .ziph-post_excerpt p a,
  .ziph-singlePost_content a,
  .logged-in-as a,
  .ziph-siglShare ul li a,
  .ziph-post_title, .ziph-post_title a,
  .ziph-readmore_link,
  .ziph-page_warp .woocommerce a {color: {$body_links_color};}
CSS;
}
$body_link_hover_color  = cs_get_customize_option( 'body_link_hover_color' );
if ($body_link_hover_color) {
echo <<<CSS
  .no-class {}
  .ziph-post_header .ziph-post_excerpt p a:hover,
  .ziph-singlePost_content a:hover,
  .logged-in-as a:hover,
  .ziph-siglShare ul li a:hover,
  .ziph-post_title, .ziph-post_title a:hover,
  .ziph-readmore_link:hover,
  .ziph-commnt_link a:hover,
  .ziph-post_date a:hover,
  .ziph-page_warp .woocommerce a:hover {color: {$body_link_hover_color};}
CSS;
}
$sidebar_heading_color  = cs_get_customize_option( 'sidebar_heading_color' );
if ($sidebar_heading_color) {
echo <<<CSS
  .no-class {}
  .ziph-page_sidebar .ziph-widget-title {color: {$sidebar_heading_color};}
CSS;
}
$sidebar_content_color  = cs_get_customize_option( 'sidebar_content_color' );
if ($sidebar_content_color) {
echo <<<CSS
  .no-class {}
  .ziph-page_sidebar p,
  .widget_rss .rssSummary {color: {$sidebar_content_color};}
CSS;
}
$content_heading_color  = cs_get_customize_option( 'content_heading_color' );
if ($content_heading_color) {
echo <<<CSS
  .no-class {}
  .ziph-post_title,
  .ziph-singlePost_content h4, 
  .comment-content h4,
  .ziph-singlePost_content h1, 
  .ziph-singlePost_content h2, 
  .ziph-singlePost_content h4, 
  .ziph-singlePost_content h3, 
  .ziph-singlePost_content h5, 
  .ziph-singlePost_content h6, 
  .ziph-singlePost_content th, 
  .ziph-page_content th, 
  #comments.comments-area .comment-content h1, 
  #comments.comments-area .comment-content h2, 
  #comments.comments-area .comment-content h4, 
  #comments.comments-area .comment-content h3, 
  #comments.comments-area .comment-content h5, 
  #comments.comments-area .comment-content h6, 
  #comments.comments-area .comment-content th, 
  .woocommerce-account .ziph-page_warp .woocommerce h1, 
  .woocommerce-account .ziph-page_warp .woocommerce h2, 
  .woocommerce-account .ziph-page_warp .woocommerce h4, 
  .woocommerce-account .ziph-page_warp .woocommerce h3, 
  .woocommerce-account .ziph-page_warp .woocommerce h5, 
  .woocommerce-account .ziph-page_warp .woocommerce h6, 
  .woocommerce-account .ziph-page_warp .woocommerce th,
  #comments.comments-area #respond #reply-title,
  .ziph-post_title a {color: {$content_heading_color};}
CSS;
}

// Maintenance Mode
$maintenance_mode_bg  = cs_get_option( 'maintenance_mode_bg' );
if ($maintenance_mode_bg) {
  $maintenance_css = ( ! empty( $maintenance_mode_bg['image'] ) ) ? 'background-image: url('. $maintenance_mode_bg['image'] .');' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['repeat'] ) ) ? 'background-repeat: '. $maintenance_mode_bg['repeat'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['position'] ) ) ? 'background-position: '. $maintenance_mode_bg['position'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['attachment'] ) ) ? 'background-attachment: '. $maintenance_mode_bg['attachment'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['size'] ) ) ? 'background-size: '. $maintenance_mode_bg['size'] .';' : '';
  $maintenance_css .= ( ! empty( $maintenance_mode_bg['color'] ) ) ? 'background-color: '. $maintenance_mode_bg['color'] .';' : '';
echo <<<CSS
  .no-class {}
  .vt-maintenance-mode {
    {$maintenance_css}
  }
CSS;
}

  echo $zipprich_vt_get_typography;

  $output = ob_get_clean();
  return $output;

  }

}

/**
 * Custom Font Family
 */
if ( ! function_exists( 'zipprich_custom_font_load' ) ) {
  function zipprich_custom_font_load() {

    $font_family       = cs_get_option( 'font_family' );

    ob_start();

    if( ! empty( $font_family ) ) {

      foreach ( $font_family as $font ) {
        echo '@font-face{';

        echo 'font-family: "'. $font['name'] .'";';

        if( empty( $font['css'] ) ) {
          echo 'font-style: normal;';
          echo 'font-weight: normal;';
        } else {
          echo esc_url($font['css']);
        }

        echo ( ! empty( $font['ttf']  ) ) ? 'src: url('. esc_url($font['ttf']) .');' : '';
        echo ( ! empty( $font['eot']  ) ) ? 'src: url('. esc_url($font['eot']) .');' : '';
        echo ( ! empty( $font['woff'] ) ) ? 'src: url('. esc_url($font['woff']) .');' : '';
        echo ( ! empty( $font['otf']  ) ) ? 'src: url('. esc_url($font['otf']) .');' : '';

        echo '}';
      }

    }

    // Typography
    $output = ob_get_clean();
    return $output;
  }
}

/* Custom Styles */
if( ! function_exists( 'zipprich_vt_custom_css' ) ) {
  function zipprich_vt_custom_css() {
    wp_enqueue_style('zipprich-default-style', get_template_directory_uri() . '/style.css');
    $output  = zipprich_custom_font_load();
    $output .= zipprich_dynamic_styles();
    $output .= cs_get_option( 'theme_custom_css' );
    $custom_css = zipprich_compress_css_lines( $output );

    wp_add_inline_style( 'zipprich-default-style', $custom_css );
  }
}

/* Custom JS */
if( ! function_exists( 'zipprich_vt_custom_js' ) ) {
  function zipprich_vt_custom_js() {
    $output = cs_get_option( 'theme_custom_js' );
    wp_add_inline_script( 'jquery-migrate', $output );
  }
  add_action( 'wp_enqueue_scripts', 'zipprich_vt_custom_js' );
}