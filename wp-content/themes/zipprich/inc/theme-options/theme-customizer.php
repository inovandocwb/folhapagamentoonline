<?php
/*
 * All customizer related options for Zipprich theme.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

if( ! function_exists( 'zipprich_vt_customizer' ) ) {
  function zipprich_vt_customizer( $options ) {

	$options        = array(); // remove old options

	// Primary Color
	$options[]      = array(
	  'name'        => 'elemets_color_section',
	  'title'       => esc_html__('Primary Color', 'zipprich'),
	  'settings'    => array(

	    // Fields Start
		array(
			'name'      => 'all_element_colors',
			'control'   => array(
				'type'  => 'color',
				'label' => esc_html__('Elements Color', 'zipprich'),
				'description'    => esc_html__('This is theme primary color, means it\'ll affect all elements that have default color of our theme primary color.', 'zipprich'),
			),
		),
	    // Fields End

	  )
	);
	// Primary Color

	// Secondary Color
	$options[]      = array(
	  'name'        => 'secondary_color_section',
	  'title'       => esc_html__('Secondary Color', 'zipprich'),
	  'settings'    => array(

	    // Fields Start
		array(
			'name'      => 'secondary_element_colors',
			'control'   => array(
				'type'  => 'color',
				'label' => esc_html__('Secondary Color', 'zipprich'),
				'description'    => esc_html__('This is theme secondary color, means it\'ll affect all elements that have default color of our theme secondary color.', 'zipprich'),
			),
		),
	    // Fields End
	  )
	);
	// Secondary Color

	// Topbar Color
	$options[]      = array(
	  'name'        => 'topbar_color_section',
	  'title'       => esc_html__('01. Topbar Colors', 'zipprich'),
	  'settings'    => array(

	    // Fields Start
	    array(
				'name'          => 'topbar_bg_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Bar Color', 'zipprich'),
					),
				),
			),
			array(
				'name'      => 'topbar_bg_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Background Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'topbar_border_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Border Color', 'zipprich'),
				),
			),
			array(
				'name'          => 'topbar_text_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Common Color', 'zipprich'),
					),
				),
			),
			array(
				'name'      => 'topbar_text_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Text Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'topbar_link_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'topbar_link_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Hover Color', 'zipprich'),
				),
			),
			array(
				'name'          => 'topbar_social_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Social Icon Color', 'zipprich'),
					),
				),
			),
			array(
				'name'      => 'topbar_social_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Social Icon Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'topbar_social_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Social Icons Hover Color', 'zipprich'),
				),
			),
	    // Fields End

	  )
	);
	// Topbar Color

	// Header Color
	$options[]      = array(
	  'name'        => 'header_color_section',
	  'title'       => esc_html__('02. Header Colors', 'zipprich'),
	  'settings'    => array(

	    // Fields Start
			array(
				'name'          => 'header_main_menu_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Main Menu Colors', 'zipprich'),
					),
				),
			),
			array(
				'name'      => 'header_bg_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Background Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'header_link_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'header_link_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Hover Color', 'zipprich'),
				),
			),

			// Sub Menu Color
			array(
				'name'          => 'header_submenu_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Sub-Menu Colors', 'zipprich'),
					),
				),
			),
			array(
				'name'      => 'submenu_bg_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Background Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'submenu_border_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Border Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'submenu_link_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'submenu_link_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Hover Color', 'zipprich'),
				),
			),
	    // Fields End

	  )
	);
	// Header Color

	// Title Bar Color
	$options[]      = array(
	  'name'        => 'titlebar_section',
	  'title'       => esc_html__('03. Title Bar Colors', 'zipprich'),
      'settings'    => array(

    		// Fields Start
    		array(
				'name'          => 'titlebar_colors_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('<h2 style="margin: 0;text-align: center;">Title Colors</h2> <br /> This is common settings, if this settings not affect in your page. Please check your page metabox. You may set default settings there.', 'zipprich'),
					),
				),
			),
    		array(
				'name'      => 'titlebar_title_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Title Color', 'zipprich'),
				),
			),

			array(
				'name'          => 'titlebar_breadcrumbs_heading',
				'control'       => array(
					'type'        => 'cs_field',
					'options'     => array(
						'type'      => 'notice',
						'class'     => 'info',
						'content'   => esc_html__('Breadcrumbs Colors', 'zipprich'),
					),
				),
			),
    		array(
				'name'      => 'breadcrumbs_text_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Text Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'breadcrumbs_link_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Color', 'zipprich'),
				),
			),
			array(
				'name'      => 'breadcrumbs_link_hover_color',
				'control'   => array(
					'type'  => 'color',
					'label' => esc_html__('Link Hover Color', 'zipprich'),
				),
			),
	    // Fields End

	  )
	);
	// Title Bar Color

	// Content Color
	$options[]      = array(
	  'name'        => 'content_section',
	  'title'       => esc_html__('04. Blog Content Colors', 'zipprich'),
	  'description' => esc_html__('This is all about content area text and heading colors.', 'zipprich'),
	  'sections'    => array(

	  	array(
	      'name'          => 'content_text_section',
	      'title'         => esc_html__('Blog Content Text', 'zipprich'),
	      'settings'      => array(

			    // Fields Start
			    array(
						'name'      => 'body_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Blog Content Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'body_links_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Blog Content Links Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'body_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Blog Content Links Hover Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'sidebar_content_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Right Sidebar Content Color', 'zipprich'),
						),
					),
			    // Fields End
			  )
			),

			// Text Colors Section
			array(
				'name'          => 'content_heading_section',
				'title'         => esc_html__('Blog Headings', 'zipprich'),
				'settings'      => array(

	      		// Fields Start
				array(
					'name'      => 'content_heading_color',
					'control'   => array(
						'type'  => 'color',
						'label' => esc_html__('Content Heading Color', 'zipprich'),
					),
				),
      			array(
					'name'      => 'sidebar_heading_color',
					'control'   => array(
						'type'  => 'color',
						'label' => esc_html__('Sidebar Heading Color', 'zipprich'),
					),
				),
			    // Fields End

      	)
      ),

	  )
	);
	// Content Color

	// Footer Color
	$options[]      = array(
	  'name'        => 'footer_section',
	  'title'       => esc_html__('05. Footer Colors', 'zipprich'),
	  'description' => esc_html__('This is all about footer settings. Make sure you\'ve enabled your needed section at : Zipprich > Theme Options > Footer ', 'zipprich'),
	  'sections'    => array(

			// Footer Widgets Block
	  	array(
	      'name'          => 'footer_widget_section',
	      'title'         => esc_html__('Widget Block', 'zipprich'),
	      'settings'      => array(

			    // Fields Start
					array(
			      'name'          => 'footer_widget_color_notice',
			      'control'       => array(
			        'type'        => 'cs_field',
			        'options'     => array(
			          'type'      => 'notice',
			          'class'     => 'info',
			          'content'   => esc_html__('Content Colors', 'zipprich'),
			        ),
			      ),
			    ),
					array(
						'name'      => 'footer_heading_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Heading Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'footer_text_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Text Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'footer_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Link Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'footer_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Widget Link Hover Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'footer_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'zipprich'),
						),
					),
			    // Fields End
			  )
			),
			// Footer Widgets Block

			// Footer Copyright Block
	  	array(
	      'name'          => 'footer_copyright_section',
	      'title'         => esc_html__('Copyright Block', 'zipprich'),
	      'settings'      => array(

			    // Fields Start
			    array(
			      'name'          => 'footer_copyright_active',
			      'control'       => array(
			        'type'        => 'cs_field',
			        'options'     => array(
			          'type'      => 'notice',
			          'class'     => 'info',
			          'content'   => esc_html__('Make sure you\'ve enabled copyright block in : <br /> <strong>Zipprich > Theme Options > Footer > Copyright Bar : Enable Copyright Block</strong>', 'zipprich'),
			        ),
			      ),
			    ),
					array(
						'name'      => 'copyright_text_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Text Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'copyright_link_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'copyright_link_hover_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Link Hover Color', 'zipprich'),
						),
					),
					array(
						'name'      => 'copyright_bg_color',
						'control'   => array(
							'type'  => 'color',
							'label' => esc_html__('Background Color', 'zipprich'),
						),
					),

			  )
			),
			// Footer Copyright Block

	  )
	);
	// Footer Color

	return $options;

  }
  add_filter( 'cs_customize_options', 'zipprich_vt_customizer' );
}