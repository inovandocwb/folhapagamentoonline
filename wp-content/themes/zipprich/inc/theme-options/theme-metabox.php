<?php
/*
 * All Metabox related options for Zipprich theme.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

function zipprich_vt_metabox_options( $options ) {

  $options      = array();

  // -----------------------------------------
  // Page Metabox Options                    -
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'page_type_metabox',
    'title'     => esc_html__('Page Custom Options', 'zipprich'),
    'post_type' => array('post', 'page', 'product'),
    'context'   => 'normal',
    'priority'  => 'default',
    'sections'  => array(

      // Title Section
      array(
        'name'  => 'page_topbar_section',
        'title' => esc_html__('Top Bar', 'zipprich'),
        'icon'  => 'fa fa-minus',

        // Fields Start
        'fields' => array(

          array(
            'id'           => 'topbar_options',
            'type'         => 'image_select',
            'title'        => esc_html__('Topbar', 'zipprich'),
            'options'      => array(
              'default'     => ZIPPRICH_CS_IMAGES .'/topbar-default.png',
              'custom'      => ZIPPRICH_CS_IMAGES .'/topbar-custom.png',
              'hide_topbar' => ZIPPRICH_CS_IMAGES .'/topbar-hide.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'hide_topbar_select',
            ),
            'radio'     => true,
            'default'   => 'default',
          ),
          array(
            'id'          => 'top_left',
            'type'        => 'textarea',
            'title'       => esc_html__('Top Left', 'zipprich'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
            'shortcode'       => true,
          ),
          array(
            'id'          => 'top_right',
            'type'        => 'textarea',
            'title'       => esc_html__('Top Right', 'zipprich'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
            'shortcode'       => true,
          ),
          array(
            'id'         => 'topbar_left_width',
            'type'       => 'text',
            'title'      => esc_html__('Top Left Width in %', 'zipprich'),
            'attributes' => array(
              'placeholder' => '50%'
            ),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
          ),
          array(
            'id'         => 'topbar_right_width',
            'type'       => 'text',
            'title'      => esc_html__('Top Right Width in %', 'zipprich'),
            'attributes' => array(
              'placeholder' => '50%'
            ),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
          ),
          array(
            'id'    => 'topbar_bg',
            'type'  => 'color_picker',
            'title' => esc_html__('Topbar Background Color', 'zipprich'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
          ),
          array(
            'id'    => 'topbar_border',
            'type'  => 'color_picker',
            'title' => esc_html__('Topbar Border Color', 'zipprich'),
            'dependency'  => array('hide_topbar_select', '==', 'custom'),
          ),

        ), // End : Fields

      ), // Title Section

      // Header
      array(
        'name'  => 'header_section',
        'title' => esc_html__('Header', 'zipprich'),
        'icon'  => 'fa fa-bars',
        'fields' => array(

          array(
            'id'           => 'select_header_design',
            'type'         => 'image_select',
            'title'        => esc_html__('Select Header Design', 'zipprich'),
            'options'      => array(
              'default'     => ZIPPRICH_CS_IMAGES .'/hs-0.png',
              'style_one'   => ZIPPRICH_CS_IMAGES .'/hs-2.png',
              'style_two'   => ZIPPRICH_CS_IMAGES .'/hs-1.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'header_design',
            ),
            'radio'     => true,
            'default'   => 'default',
            'info'      => esc_html__('Select your header design, following options will may differ based on your selection of header design.', 'zipprich'),
          ),
          array(
            'id'              => 'header_address_info',
            'title'           => esc_html__('Header Content', 'zipprich'),
            'desc'            => esc_html__('Add your header content here. Example : Address Details', 'zipprich'),
            'type'            => 'textarea',
            'shortcode'       => true,
            'dependency' => array('header_design', '==', 'style_two'),
          ),
          array(
            'id'             => 'choose_menu',
            'type'           => 'select',
            'title'          => esc_html__('Choose Menu', 'zipprich'),
            'desc'          => esc_html__('Choose custom menus for this page.', 'zipprich'),
            'options'        => 'menus',
            'default_option' => esc_html__('Select your menu', 'zipprich'),
          ),

          // Enable & Disable
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Enable & Disable', 'zipprich'),
            'dependency' => array('header_design', '==', 'style_two'),
          ),
          array(
            'id'    => 'search_icon',
            'type'  => 'switcher',
            'title' => esc_html__('Search Icon', 'zipprich'),
            'info' => esc_html__('Turn On if you want to show search icon in navigation bar.', 'zipprich'),
            'default' => true,
            'dependency' => array('header_design', '==', 'style_two'),
          ),
          array(
            'id'    => 'cart_widget',
            'type'  => 'switcher',
            'title' => esc_html__('Cart Widget', 'zipprich'),
            'info' => esc_html__('Turn On if you want to show cart widget in header. Make sure about installation/activation of WooCommerce plugin.', 'zipprich'),
            'default' => false,
            'dependency' => array('header_design', '==', 'style_two'),
          ),

        ),
      ),
      // Header

      // Banner & Title Area
      array(
        'name'  => 'banner_title_section',
        'title' => esc_html__('Banner & Title Area', 'zipprich'),
        'icon'  => 'fa fa-bullhorn',
        'fields' => array(

          array(
            'id'        => 'banner_type',
            'type'      => 'select',
            'title'     => esc_html__('Choose Banner Type', 'zipprich'),
            'options'   => array(
              'default-title'    => 'Default Title',
              'revolution-slider' => 'Shortcode [Rev Slider]',
              'hide-title-area'   => 'Hide Title/Banner Area',
            ),
          ),
          array(
            'id'    => 'page_revslider',
            'type'  => 'textarea',
            'title' => esc_html__('Revolution Slider or Any Shortcodes', 'zipprich'),
            'desc' => esc_html__('Enter any shortcodes that you want to show in this page title area. <br />Eg : Revolution Slider shortcode.', 'zipprich'),
            'attributes' => array(
              'placeholder' => esc_html__('Enter your shortcode...', 'zipprich'),
            ),
            'dependency'   => array('banner_type', '==', 'revolution-slider'),
          ),
          array(
            'id'    => 'page_custom_title',
            'type'  => 'text',
            'title' => esc_html__('Custom Title', 'zipprich'),
            'attributes' => array(
              'placeholder' => esc_html__('Enter your custom title...', 'zipprich'),
            ),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),
          array(
            'id'        => 'title_area_spacings',
            'type'      => 'select',
            'title'     => esc_html__('Title Area Spacings', 'zipprich'),
            'options'   => array(
              'padding-none' => esc_html__('Default Spacing', 'zipprich'),
              'padding-custom' => esc_html__('Custom Padding', 'zipprich'),
            ),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),
          array(
            'id'    => 'title_top_spacings',
            'type'  => 'text',
            'title' => esc_html__('Top Spacing', 'zipprich'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('banner_type|title_area_spacings', '==|==', 'default-title|padding-custom'),
          ),
          array(
            'id'    => 'title_bottom_spacings',
            'type'  => 'text',
            'title' => esc_html__('Bottom Spacing', 'zipprich'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('banner_type|title_area_spacings', '==|==', 'default-title|padding-custom'),
          ),
          array(
            'id'    => 'title_area_bg',
            'type'  => 'background',
            'title' => esc_html__('Background', 'zipprich'),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),
          array(
            'id'    => 'titlebar_bg_overlay_color',
            'type'  => 'color_picker',
            'title' => esc_html__('Overlay Color', 'zipprich'),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),

        ),
      ),
      // Banner & Title Area

      // Content Section
      array(
        'name'  => 'page_content_options',
        'title' => esc_html__('Content Options', 'zipprich'),
        'icon'  => 'fa fa-file',

        'fields' => array(

          array(
            'id'        => 'content_spacings',
            'type'      => 'select',
            'title'     => esc_html__('Content Spacings', 'zipprich'),
            'options'   => array(
              'padding-default' => esc_html__('Default Spacing', 'zipprich'),
              'padding-none' => esc_html__('No Padding', 'zipprich'),
              'padding-custom' => esc_html__('Custom Padding', 'zipprich'),
            ),
            'desc' => esc_html__('Content area top and bottom spacings.', 'zipprich'),
          ),
          array(
            'id'    => 'content_top_spacings',
            'type'  => 'text',
            'title' => esc_html__('Top Spacing', 'zipprich'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('content_spacings', '==', 'padding-custom'),
          ),
          array(
            'id'    => 'content_bottom_spacings',
            'type'  => 'text',
            'title' => esc_html__('Bottom Spacing', 'zipprich'),
            'attributes'  => array( 'placeholder' => '100px' ),
            'dependency'  => array('content_spacings', '==', 'padding-custom'),
          ),

        ), // End Fields
      ), // Content Section

      // Enable & Disable
      array(
        'name'  => 'hide_show_section',
        'title' => esc_html__('Enable & Disable', 'zipprich'),
        'icon'  => 'fa fa-toggle-on',
        'fields' => array(

          array(
            'id'    => 'hide_header',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Header', 'zipprich'),
            'label' => esc_html__('Yes, Please do it.', 'zipprich'),
          ),
          array(
            'id'    => 'hide_breadcrumbs',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Breadcrumbs', 'zipprich'),
            'label' => esc_html__('Yes, Please do it.', 'zipprich'),
          ),
          array(
            'id'    => 'hide_footer',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Footer', 'zipprich'),
            'label' => esc_html__('Yes, Please do it.', 'zipprich'),
          ),

        ),
      ),
      // Enable & Disable

    ),
  );

  // -----------------------------------------
  // Page Layout
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'page_layout_options',
    'title'     => esc_html__('Page Layout', 'zipprich'),
    'post_type' => 'page',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(

      array(
        'name'   => 'page_layout_section',
        'fields' => array(

          array(
            'id'        => 'page_layout',
            'type'      => 'image_select',
            'options'   => array(
              'full-width'    => ZIPPRICH_CS_IMAGES . '/page-1.png',
              'right-sidebar' => ZIPPRICH_CS_IMAGES . '/page-4.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'page_layout',
            ),
            'default'    => 'full-width',
            'radio'      => true,
            'wrap_class' => 'text-center',
          ),
          array(
            'id'            => 'page_sidebar_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'zipprich'),
            'options'        => zipprich_vt_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'zipprich'),
            'dependency'   => array('page_layout', '==', 'right-sidebar'),
          ),

        ),
      ),

    ),
  );

  // -----------------------------------------
  // Testimonial
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'testimonial_options',
    'title'     => esc_html__('Testimonial Client', 'zipprich'),
    'post_type' => 'testimonial',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(

      array(
        'name'   => 'testimonial_option_section',
        'fields' => array(

          array(
            'id'      => 'testi_name',
            'type'    => 'text',
            'title'   => esc_html__('Name', 'zipprich'),
            'info'    => esc_html__('Enter client name', 'zipprich'),
          ),
          array(
            'id'      => 'testi_name_link',
            'type'    => 'text',
            'title'   => esc_html__('Name Link', 'zipprich'),
            'info'    => esc_html__('Enter client name link, if you want', 'zipprich'),
          ),
          array(
            'id'      => 'testi_pro',
            'type'    => 'text',
            'title'   => esc_html__('Profession', 'zipprich'),
            'info'    => esc_html__('Enter client profession', 'zipprich'),
          ),
          array(
            'id'      => 'testi_pro_link',
            'type'    => 'text',
            'title'   => esc_html__('Profession Link', 'zipprich'),
            'info'    => esc_html__('Enter client profession link', 'zipprich'),
          ),

        ),
      ),

    ),
  );

  // -----------------------------------------
  // Team
  // -----------------------------------------
  $options[]    = array(
    'id'        => 'team_options',
    'title'     => esc_html__('Job Position', 'zipprich'),
    'post_type' => 'team',
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(

      array(
        'name'   => 'team_option_section',
        'fields' => array(

          array(
            'id'      => 'team_job_position',
            'type'    => 'text',
            'attributes' => array(
              'placeholder' => esc_html__('Eg : Financial Manager', 'zipprich'),
            ),
            'info'    => esc_html__('Enter this employee job position, in your company.', 'zipprich'),
          ),
          array(
            'id'      => 'team_social',
            'type'            => 'group',
            'title'           => esc_html__('Social Groups', 'zipprich'),
            'button_title'    => esc_html__('Add New', 'zipprich'),
            'accordion_title' => esc_html__('Add New Social', 'zipprich'),
            'title'    => esc_html__('Social Links', 'zipprich'),
            'info'    => esc_html__('Enter team member social links.', 'zipprich'),
            'fields'          => array(
              array(
                'id'      => 'title',
                'type'    => 'text',
                'attributes' => array(
                  'placeholder' => esc_html__('Eg : Facebook', 'zipprich'),
                ),
                'info'    => esc_html__('Enter this socail profile name.', 'zipprich'),
              ),
              array(
                'id'      => 'icon',
                'type'    => 'icon',
                'title'    => esc_html__('Social Icon', 'zipprich'),
                'info'    => esc_html__('Enter team social icon.', 'zipprich'),
              ),
              array(
                'id'      => 'link',
                'type'    => 'text',
                'title'    => esc_html__('Social Link', 'zipprich'),
                'attributes' => array(
                  'placeholder' => esc_html__('http://', 'zipprich'),
                ),
                'info'    => esc_html__('Enter team social link.', 'zipprich'),
              ),
            )
          ),
          array(
            'id'      => 'open_link',
            'type'    => 'switcher',
            'title'    => esc_html__('Open in a new tab?', 'zipprich'),
            'info'    => esc_html__('Turn ON if you want to show social profile in a new tab.', 'zipprich'),
          ),
        ),
      ),

    ),
  );

  return $options;

}
add_filter( 'cs_metabox_options', 'zipprich_vt_metabox_options' );