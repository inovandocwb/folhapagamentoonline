<?php
/*
 * All Theme Options for Zipprich theme.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

function zipprich_vt_settings( $settings ) {

  $settings           = array(
    'menu_title'      => ZIPPRICH_NAME . esc_html__(' Options', 'zipprich'),
    'menu_slug'       => sanitize_title(ZIPPRICH_NAME) . '_options',
    'menu_type'       => 'menu',
    'menu_icon'       => 'dashicons-awards',
    'menu_position'   => '4',
    'ajax_save'       => false,
    'show_reset_all'  => true,
    'framework_title' => ZIPPRICH_NAME .' <small>V-'. ZIPPRICH_VERSION .' by <a href="'. ZIPPRICH_BRAND_URL .'" target="_blank">'. ZIPPRICH_BRAND_NAME .'</a></small>',
  );

  return $settings;

}
add_filter( 'cs_framework_settings', 'zipprich_vt_settings' );

// Theme Framework Options
function zipprich_vt_options( $options ) {

  $options      = array(); // remove old options

  // ------------------------------
  // Theme Brand
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_brand',
    'title'    => esc_html__('Brand', 'zipprich'),
    'icon'     => 'fa fa-bookmark',
    'sections' => array(

      // brand logo tab
      array(
        'name'     => 'brand_logo_title',
        'title'    => esc_html__('Logo', 'zipprich'),
        'icon'     => 'fa fa-star',
        'fields'   => array(

          // Site Logo
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Site Logo', 'zipprich')
          ),
          array(
            'id'    => 'brand_logo_default',
            'type'  => 'image',
            'title' => esc_html__('Default Logo', 'zipprich'),
            'info'  => esc_html__('Upload your default logo here. If you not upload, then site title will load in this logo location.', 'zipprich'),
            'add_title' => esc_html__('Add Logo', 'zipprich'),
          ),
          array(
            'id'    => 'brand_logo_retina',
            'type'  => 'image',
            'title' => esc_html__('Retina Logo', 'zipprich'),
            'info'  => esc_html__('Upload your retina logo here. Recommended size is 2x from default logo.', 'zipprich'),
            'add_title' => esc_html__('Add Retina Logo', 'zipprich'),
          ),
          array(
            'id'          => 'retina_width',
            'type'        => 'text',
            'title'       => esc_html__('Retina & Normal Logo Width', 'zipprich'),
            'unit'        => 'px',
          ),
          array(
            'id'          => 'retina_height',
            'type'        => 'text',
            'title'       => esc_html__('Retina & Normal Logo Height', 'zipprich'),
            'unit'        => 'px',
          ),
          array(
            'id'          => 'brand_logo_top',
            'type'        => 'number',
            'title'       => esc_html__('Logo Top Space', 'zipprich'),
            'attributes'  => array( 'placeholder' => 5 ),
            'unit'        => 'px',
          ),
          array(
            'id'          => 'brand_logo_bottom',
            'type'        => 'number',
            'title'       => esc_html__('Logo Bottom Space', 'zipprich'),
            'attributes'  => array( 'placeholder' => 5 ),
            'unit'        => 'px',
          ),

          // WordPress Admin Logo
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('WordPress Admin Logo', 'zipprich')
          ),
          array(
            'id'    => 'brand_logo_wp',
            'type'  => 'image',
            'title' => esc_html__('Login logo', 'zipprich'),
            'info'  => esc_html__('Upload your WordPress login page logo here.', 'zipprich'),
            'add_title' => esc_html__('Add Login Logo', 'zipprich'),
          ),
        ) // end: fields
      ), // end: section

      // Fav
      array(
        'name'     => 'brand_fav',
        'title'    => esc_html__('Fav Icon', 'zipprich'),
        'icon'     => 'fa fa-anchor',
        'fields'   => array(

            // -----------------------------
            // Begin: Fav
            // -----------------------------
            array(
              'id'    => 'brand_fav_icon',
              'type'  => 'image',
              'title' => esc_html__('Fav Icon', 'zipprich'),
              'info'  => esc_html__('Upload your site fav icon, size should be 16x16.', 'zipprich'),
              'add_title' => esc_html__('Add Fav Icon', 'zipprich'),
            ),
            array(
              'id'    => 'iphone_icon',
              'type'  => 'image',
              'title' => esc_html__('Apple iPhone icon', 'zipprich'),
              'info'  => esc_html__('Icon for Apple iPhone (57px x 57px). This icon is used for Bookmark on Home screen.', 'zipprich'),
              'add_title' => esc_html__('Add iPhone Icon', 'zipprich'),
            ),
            array(
              'id'    => 'iphone_retina_icon',
              'type'  => 'image',
              'title' => esc_html__('Apple iPhone retina icon', 'zipprich'),
              'info'  => esc_html__('Icon for Apple iPhone retina (114px x114px). This icon is used for Bookmark on Home screen.', 'zipprich'),
              'add_title' => esc_html__('Add iPhone Retina Icon', 'zipprich'),
            ),
            array(
              'id'    => 'ipad_icon',
              'type'  => 'image',
              'title' => esc_html__('Apple iPad icon', 'zipprich'),
              'info'  => esc_html__('Icon for Apple iPad (72px x 72px). This icon is used for Bookmark on Home screen.', 'zipprich'),
              'add_title' => esc_html__('Add iPad Icon', 'zipprich'),
            ),
            array(
              'id'    => 'ipad_retina_icon',
              'type'  => 'image',
              'title' => esc_html__('Apple iPad retina icon', 'zipprich'),
              'info'  => esc_html__('Icon for Apple iPad retina (144px x 144px). This icon is used for Bookmark on Home screen.', 'zipprich'),
              'add_title' => esc_html__('Add iPad Retina Icon', 'zipprich'),
            ),

        ) // end: fields
      ), // end: section

    ),
  );

  // ------------------------------
  // Layout
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_layout',
    'title'  => esc_html__('Layout', 'zipprich'),
    'icon'   => 'fa fa-file-text'
  );

  // ------------------------------
  // Header Sections
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_header_tab',
    'title'    => esc_html__('Header', 'zipprich'),
    'icon'     => 'fa fa-bars',
    'sections' => array(

      // header design tab
      array(
        'name'     => 'header_design_tab',
        'title'    => esc_html__('Design', 'zipprich'),
        'icon'     => 'fa fa-magic',
        'fields'   => array(

          // Header Select
          array(
            'id'           => 'select_header_design',
            'type'         => 'image_select',
            'title'        => esc_html__('Select Header Design', 'zipprich'),
            'options'      => array(
              'style_one'    => ZIPPRICH_CS_IMAGES .'/hs-2.png',
              'style_two'    => ZIPPRICH_CS_IMAGES .'/hs-1.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'header_design',
            ),
            'radio'        => true,
            'default'   => 'style_one',
            'info' => esc_html__('Select your header design, following options will may differ based on your selection of header design.', 'zipprich'),
          ),
          array(
            'id'              => 'header_address_info',
            'title'           => esc_html__('Header Content', 'zipprich'),
            'desc'            => esc_html__('Add your header content here. Example : Address Details', 'zipprich'),
            'type'            => 'textarea',
            'shortcode'       => true,
            'dependency' => array('header_design', '==', 'style_two'),
          ),
          // Header Select

          // Extra's
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Extra\'s', 'zipprich'),
          ),
          array(
            'id'          => 'mobile_breakpoint',
            'type'        => 'text',
            'title'       => esc_html__('Mobile Menu Starts from?', 'zipprich'),
            'attributes'  => array( 'placeholder' => '767' ),
            'info' => esc_html__('Just put numeric value only. Like : 767. Don\'t use px or any other units.', 'zipprich'),
          ),
          array(
            'id'    => 'search_icon',
            'type'  => 'switcher',
            'title' => esc_html__('Search Icon', 'zipprich'),
            'info' => esc_html__('Turn On if you want to show search icon in navigation bar.', 'zipprich'),
            'default' => true,
          ),
          array(
            'id'    => 'cart_widget',
            'type'  => 'switcher',
            'title' => esc_html__('Cart Widget', 'zipprich'),
            'info' => esc_html__('Turn On if you want to show cart widget in header. Make sure about installation/activation of WooCommerce plugin.', 'zipprich'),
            'default' => false,
            'dependency' => array('header_design', '==', 'style_two'),
          ),

        )
      ),

      // header top bar
      array(
        'name'     => 'header_top_bar_tab',
        'title'    => esc_html__('Top Bar', 'zipprich'),
        'icon'     => 'fa fa-minus',
        'fields'   => array(

          array(
            'id'          => 'top_bar',
            'type'        => 'switcher',
            'title'       => esc_html__('Hide Top Bar', 'zipprich'),
            'on_text'     => esc_html__('Yes', 'zipprich'),
            'off_text'    => esc_html__('No', 'zipprich'),
            'default'     => true,
          ),
          array(
            'id'          => 'top_left',
            'title'       => esc_html__('Top Left Block', 'zipprich'),
            'desc'        => esc_html__('Top bar left block.', 'zipprich'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),
          array(
            'id'          => 'top_right',
            'title'       => esc_html__('Top Right Block', 'zipprich'),
            'desc'        => esc_html__('Top bar right block.', 'zipprich'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),

          array(
            'id'          => 'topbar_left_width',
            'type'        => 'text',
            'title'       => esc_html__('Top Left Width in %', 'zipprich'),
            'attributes'  => array(
              'placeholder' => '50%'
            ),
            'dependency'  => array('top_bar', '==', false),
          ),
          array(
            'id'          => 'topbar_right_width',
            'type'        => 'text',
            'title'       => esc_html__('Top Right Width in %', 'zipprich'),
            'attributes'  => array(
              'placeholder' => '50%'
            ),
            'dependency'  => array('top_bar', '==', false),
          ),

        )
      ),

      // header banner
      array(
        'name'     => 'header_banner_tab',
        'title'    => esc_html__('Title Bar (or) Banner', 'zipprich'),
        'icon'     => 'fa fa-bullhorn',
        'fields'   => array(

          // Title Area
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Title Area', 'zipprich')
          ),
          array(
            'id'      => 'need_title_bar',
            'type'    => 'switcher',
            'title'   => esc_html__('Title Bar', 'zipprich'),
            'label'   => esc_html__('If you want title bar in your sub-pages, please turn this ON.', 'zipprich'),
            'default'    => true,
          ),
          array(
            'id'             => 'title_bar_padding',
            'type'           => 'select',
            'title'          => esc_html__('Padding Spaces Top & Bottom', 'zipprich'),
            'options'        => array(
              'padding-none' => esc_html__('Default Spacing', 'zipprich'),
              'padding-custom' => esc_html__('Custom Padding', 'zipprich'),
            ),
            'dependency'   => array( 'need_title_bar', '==', 'true' ),
          ),
          array(
            'id'             => 'titlebar_top_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Top', 'zipprich'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding', '==', 'padding-custom' ),
          ),
          array(
            'id'             => 'titlebar_bottom_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Bottom', 'zipprich'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding', '==', 'padding-custom' ),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Background Options', 'zipprich'),
            'dependency' => array( 'need_title_bar', '==', 'true' ),
          ),
          array(
            'id'      => 'titlebar_bg',
            'type'    => 'background',
            'title'   => esc_html__('Background', 'zipprich'),
            'dependency' => array( 'need_title_bar', '==', 'true' ),
          ),
          array(
            'id'      => 'titlebar_bg_overlay_color',
            'type'    => 'color_picker',
            'title'   => esc_html__('Overlay Color', 'zipprich'),
            'dependency' => array( 'need_title_bar', '==', 'true' ),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Breadcrumbs', 'zipprich'),
          ),
          array(
            'id'      => 'need_breadcrumbs',
            'type'    => 'switcher',
            'title'   => esc_html__('Breadcrumbs', 'zipprich'),
            'label'   => esc_html__('If you want Breadcrumbs in your banner, please turn this ON.', 'zipprich'),
            'default'    => true,
          ),

        )
      ),

    ),
  );

  // ------------------------------
  // Footer Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'footer_section',
    'title'    => esc_html__('Footer', 'zipprich'),
    'icon'     => 'fa fa-ellipsis-h',
    'sections' => array(

      // footer widgets
      array(
        'name'     => 'footer_widgets_tab',
        'title'    => esc_html__('Widget Area', 'zipprich'),
        'icon'     => 'fa fa-th',
        'fields'   => array(

          // Footer Widget Block
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Footer Widget Block', 'zipprich')
          ),
          array(
            'id'    => 'footer_widget_block',
            'type'  => 'switcher',
            'title' => esc_html__('Enable Widget Block', 'zipprich'),
            'info' => esc_html__('If you turn this ON, then Goto : Appearance > Widgets. There you can see <strong>Footer Widget 1,2,3 or 4</strong> Widget Area, add your widgets there.', 'zipprich'),
            'default' => true,
          ),
          array(
            'id'    => 'footer_widget_layout',
            'type'  => 'image_select',
            'title' => esc_html__('Widget Layouts', 'zipprich'),
            'info' => esc_html__('Choose your footer widget layouts.', 'zipprich'),
            'default' => 'theme-default',
            'options' => array(
              'theme-default'   => ZIPPRICH_CS_IMAGES . '/footer/footer-default.png',
              1   => ZIPPRICH_CS_IMAGES . '/footer/footer-1.png',
              2   => ZIPPRICH_CS_IMAGES . '/footer/footer-2.png',
              3   => ZIPPRICH_CS_IMAGES . '/footer/footer-3.png',
              4   => ZIPPRICH_CS_IMAGES . '/footer/footer-4.png',
              5   => ZIPPRICH_CS_IMAGES . '/footer/footer-5.png',
              6   => ZIPPRICH_CS_IMAGES . '/footer/footer-6.png',
              7   => ZIPPRICH_CS_IMAGES . '/footer/footer-7.png',
              8   => ZIPPRICH_CS_IMAGES . '/footer/footer-8.png',
              9   => ZIPPRICH_CS_IMAGES . '/footer/footer-9.png',
            ),
            'radio'       => true,
            'dependency'  => array('footer_widget_block', '==', true),
          ),

        )
      ),

      // footer copyright
      array(
        'name'     => 'footer_copyright_tab',
        'title'    => esc_html__('Copyright Bar', 'zipprich'),
        'icon'     => 'fa fa-copyright',
        'fields'   => array(

          // Copyright
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Copyright Layout', 'zipprich'),
          ),
          array(
            'id'    => 'need_copyright',
            'type'  => 'switcher',
            'title' => esc_html__('Enable Copyright Section', 'zipprich'),
            'default' => true,
          ),
          array(
            'id'    => 'footer_copyright_layout',
            'type'  => 'image_select',
            'title' => esc_html__('Select Copyright Layout', 'zipprich'),
            'info' => esc_html__('In above image, blue box is copyright text and yellow box is secondary text.', 'zipprich'),
            'default'      => 'copyright-1',
            'options'      => array(
              'copyright-1'    => ZIPPRICH_CS_IMAGES .'/footer/copyright-1.png',
              'copyright-2'    => ZIPPRICH_CS_IMAGES .'/footer/copyright-2.png',
              'copyright-3'    => ZIPPRICH_CS_IMAGES .'/footer/copyright-3.png',
              ),
            'radio'        => true,
            'dependency'     => array('need_copyright', '==', true),
          ),
          array(
            'id'    => 'copyright_text',
            'type'  => 'textarea',
            'title' => esc_html__('Copyright (Left)', 'zipprich'),
            'shortcode' => true,
            'dependency' => array('need_copyright', '==', true),
            'after'       => 'Helpful shortcodes: [ziph_current_year] [ziph_home_url] or any shortcode.',
          ),

          // Copyright Another Text
          array(
            'id'    => 'secondary_text',
            'type'  => 'textarea',
            'title' => esc_html__('Copyright (Right)', 'zipprich'),
            'shortcode' => true,
            'dependency' => array('need_copyright', '==', 'true'),
            'after'       => 'Helpful shortcodes: [ziph_current_year] [ziph_home_url] or any shortcode.',
          ),

        )
      ),

    ),
  );

  // ------------------------------
  // Design
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_design',
    'title'  => esc_html__('Design', 'zipprich'),
    'icon'   => 'fa fa-magic'
  );

  // ------------------------------
  // color section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_color_section',
    'title'    => esc_html__('Colors', 'zipprich'),
    'icon'     => 'fa fa-eyedropper',
    'fields' => array(

      array(
        'type'    => 'heading',
        'content' => esc_html__('Color Options', 'zipprich'),
      ),
      array(
        'type'    => 'subheading',
        'wrap_class' => 'color-tab-content',
        'content' => esc_html__('All color options are available in our theme customizer. The reason of we used customizer options for color section is because, you can choose each part of color from there and see the changes instantly using customizer.
          <br /><br />Highly customizable colors are in <strong>Appearance > Customize</strong>', 'zipprich'),
      ),

    ),
  );

  // ------------------------------
  // Typography section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_typo_section',
    'title'    => esc_html__('Typography', 'zipprich'),
    'icon'     => 'fa fa-header',
    'fields' => array(

      // Start fields
      array(
        'id'                  => 'typography',
        'type'                => 'group',
        'fields'              => array(
          array(
            'id'              => 'title',
            'type'            => 'text',
            'title'           => esc_html__('Title', 'zipprich'),
          ),
          array(
            'id'              => 'selector',
            'type'            => 'textarea',
            'title'           => esc_html__('Selector', 'zipprich'),
            'info'           => esc_html__('Enter css selectors like : <strong>body, .custom-class</strong>', 'zipprich'),
          ),
          array(
            'id'              => 'font',
            'type'            => 'typography',
            'title'           => esc_html__('Font Family', 'zipprich'),
          ),
          array(
            'id'              => 'size',
            'type'            => 'text',
            'title'           => esc_html__('Font Size', 'zipprich'),
          ),
          array(
            'id'              => 'line_height',
            'type'            => 'text',
            'title'           => esc_html__('Line-Height', 'zipprich'),
          ),
          array(
            'id'              => 'css',
            'type'            => 'textarea',
            'title'           => esc_html__('Custom CSS', 'zipprich'),
          ),
        ),
        'button_title'        => esc_html__('Add New Typography', 'zipprich'),
        'accordion_title'     => esc_html__('New Typography', 'zipprich'),
        'default'             => array(
          array(
            'title'           => esc_html__('Body Typography', 'zipprich'),
            'selector'        => 'body,.ziph-head_phnum,.ziph-head_info a,.ziph-headlogin_btn a.btn,.ziph-mainmenu > ul > li > a,.ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a,.ziph-slidcap_txt h2,.ziph-slidcap_list li,.ziph-section_txt h3,.ziph-htplt_priceform h3 span,.ziph-htplt_priceform h3 sub,.ziph-features-txt h3,.ziph-features-txt ul li,.ziph-serv_txt h4,.ziph-faq_head .panel-title,.panel-heading.ziph-faq_head .panel-title a.ziph-faq_btn:before,.ziph-sectn_title,.ziph-domainsrch_title,.ziph-domainsrch_form input[type="search"],.ziph-slidcap_txt2 h2 span,.ziph-slidcap_txt2 p,.ziph-slidcap_txt3 h2,.ziph-slidcap_txt3 p,.ziph-hm2hdrcontct_txt h6,.ziph-hm2hdrcontct_txt h5,.ziph-slidcap_txt.ziph-hm2slidcap_txt p,.ziph-hm2goodness_single .ziph-goodness_txt h4,.ziph-choose-text h3,.ziph-choose-list ul li,.ziph-team_info .ziph-tm_name,.ziph-revtestimonial_title,.ziph-post_title,.ziph-slidcap_price span,.ziph-choose_nav li a,.ziph-page_title,.ziph-about_us h3,.ziph-about_activity h4,.ziph-process_single h4,.ziph-process_info p,.ziph-shost_heading,.ziph-shost_table .table > thead > tr > th,.ziph-shost_table .table > tbody > tr > td,.ziph-shostfaq_heading,.ziph-affilte_table .table-bordered > tbody > tr > td h4,.ziph-affilte_table .table-bordered > tbody > tr > td p,.ziph-sigldmn_feture h4,.ziph-form-control,.wpcf7-form-control,.ziph-select-control,.ziph-wigtResentPost_single .ziph-wigtRp_title,.ziph-singlePost_content .ziph-cont_title,.comment-content .ziph-cont_title,.ziph-sigltags_warp > span,.ziph-sigltags_warp .ziph-sigl_tags,.ziph-siglShare_txt,#comments.comments-area a.comment-reply-link,#comments.comments-area #respond #commentform textarea,#comments.comments-area #respond #commentform input:not([type="submit"]),.ziph-contactC_btn,.ziph-contactForm_title,.ziph-contactMInfo_title,.ziph-contactMI_name,.ziph-contactMInfo_txt h5,.ziph-contactMInfo_txt ul li,.ziph-faqForm_heading,.ziph-inputRadio_warp > h5,.ziph-inputRadio_warp label,.ziph-lrFormOptnlink,.ziph-lrFormOptn_sprator span,.ziph-inputCheckbox label,.woocommerce .woocommerce-result-count span,.woocommerce-page .woocommerce-result-count span,.woocommerce .woocommerce-ordering select,.woocommerce ul.products li.product .woocommerce-loop-product__title,.woocommerce ul.products li.product .price,.woocommerce ul.products li.product .price ins,.woocommerce ul.products li.product .price del,.woocommerce ul.cart_list li .product-title,.woocommerce ul.product_list_widget li .product-title,.woocommerce ul.cart_list li .woocommerce-Price-amount,.woocommerce ul.product_list_widget li .woocommerce-Price-amount,.woocommerce div.product .entry-summary .quantity input[type="number"],.woocommerce div.product .entry-summary .product_meta > span a,.woocommerce div.product .woocommerce-Tabs-panel--description > h2,.woocommerce div.product .woocommerce-Tabs-panel--reviews #reviews #comments .woocommerce-Reviews-title,.woocommerce div.product .woocommerce-tabs ul.tabs li a,.woocommerce #reviews h3,.woocommerce #review_form #respond .comment-form-rating label,.woocommerce .related.products > h2,.woocommerce-error,.woocommerce-info,.woocommerce-message,.woocommerce .form-row label,.woocommerce .input-text,.woocommerce .select2-container .select2-choice,.woocommerce-checkout .woocommerce form.checkout_coupon .form-row label,.woocommerce-checkout .woocommerce form.login .form-row label,.woocommerce-checkout .woocommerce form.register .form-row label,.woocommerce-checkout .woocommerce form.checkout_coupon p.lost_password,.woocommerce-checkout .woocommerce form.login p.lost_password,.woocommerce-checkout .woocommerce form.register p.lost_password,.woocommerce-billing-fields > h3,.woocommerce-billing-fields > h3 label,.woocommerce-shipping-fields > h3,.woocommerce-shipping-fields > h3 label,.woocommerce-checkout h3#order_review_heading,.woocommerce-checkout .woocommerce table.shop_table th,.woocommerce-checkout .woocommerce table.shop_table th strong,.woocommerce-checkout .woocommerce table.shop_table td,.woocommerce-checkout .woocommerce table.shop_table td strong,.woocommerce-checkout #payment ul.payment_methods li label,.woocommerce-cart .woocommerce table.shop_table.cart thead th,.woocommerce-cart .woocommerce table.shop_table.cart tbody td.product-name,.woocommerce-cart .woocommerce table.shop_table.cart tbody td.product-price,.woocommerce-cart .woocommerce table.shop_table.cart tbody td.product-subtotal,.woocommerce-cart .quantity input[type="number"],.woocommerce-cart table.shop_table.cart .coupon input.input-text,.ziph-megmenuSingle_cont p,.ziph-megmenuSingle_cont a.ziph-megmRead_btn,.ziph-domainMainSrch_txt p,.ziph-domainType h5,.ziph-domainPrice-table thead > tr > th,.ziph-domainAffiltprice_Warp h3,.ziph-domainAffiltprice_Warp ul li,.ziph-domainAffiltprice_Warp p,.ziph-domainAffilt_txt p,#bridge .container h2,#bridge .form-control,#bridge .home-shortcuts .lead,#bridge .home-shortcuts ul li a,#bridge .home-shortcuts ul li a p,#bridge #main-body .main-content > p,#bridge .panel-title,#bridge .list-group-item,#bridge div.header-lined h1,#bridge .form-horizontal .form-group label.control-label,.language-popover .popover-content li,.language-popover .popover-content li a,.whmpress_price_matrix_domain label,#bridge.ziph-WhmpressAnnou .announcement-single h2,.ziph-whmpressKnowl_single h3,.ziph-singlePost_content,#comments.comments-area .comment-content,.woocommerce-account .ziph-page_warp .woocommerce,.ziph-singlePost_content h5,.ziph-singlePost_content h6,#comments.comments-area .comment-content h5,#comments.comments-area .comment-content h6,.woocommerce-account .ziph-page_warp .woocommerce h5,.woocommerce-account .ziph-page_warp .woocommerce h6,.ziph-singlePost_content strong,.ziph-singlePost_content dt,#comments.comments-area .comment-content strong,#comments.comments-area .comment-content dt,.woocommerce-account .ziph-page_warp .woocommerce strong,.woocommerce-account .ziph-page_warp .woocommerce dt,.woocommerce .select2-container--default .select2-selection--single,#bridge .btn, #bridge .main-content p a.btn',
            'font'            => array(
              'family'        => 'Roboto',
              'variant'       => 'regular',
            ),
            'size'            => '',
            'line_height'     => '',
          ),
          array(
            'title'           => esc_html__('Menu Typography', 'zipprich'),
            'selector'        => '.ziph-callout_price h4,.ziph-callout_price h4 span sub,.ziph-tblpricing q,.ziph-hmlblog_date,.ziph-footer_widget ul li,.ziph-footrwidget_loc,.ziph-ftrform_warp input,.ziph-ftrform_warp textarea,.ziph-ftrform_warp button[type="submit"],.ziph-ftrform_warp input[type="submit"], .ziph-footer_conform .wpcf7 input[type="submit"],.ziph-header_styl-2 .ziph-mainmenu > ul > li > a,.ziph-header_styl-2 .ziph-mainmenu > ul li.menu-item-has-children ul.sub-menu > li > a,.ziph-post_date,.ziph-readmore_link,.ziph-commnt_link a,.ziph-postPagination_warp .nav-links .page-numbers,.ziph-contactCI_label,.ziph-callout_price h4 span,.ziph-404Page_warp h1,#bridge blockquote p a.label',
            'font'            => array(
              'family'        => 'Open Sans',
              'variant'       => '600',
            ),
            'size'            => '',
          ),
          array(
            'title'           => esc_html__('Headings Typography', 'zipprich'),
            'selector'        => 'h1,h2,h3,.ziph-slidcap_txt h2 span,.ziph-hostpltfrm_single h4,.ziph-htplt_priceform h3,.ziph-callout_title,.btn.ziph-btn,.ziph-tblpric,.ziph-tstimnl_name,.ziph-footrwidget_title,.ziph-domainsrch_form input[type="submit"],.whmpress_domain_search_ajax .search_btn,.ziph-dsp_col strong,.ziph-slidcap_txt2 h5,.ziph-slidcap_txt2 h2,.ziph-slidcap_txt3 h5,.ziph-whmcsPage_title,.ziph-about_us h3 span,.ziph-process_icon .ziph-badge,.ziph-shost_table .table > tfoot > tr > td h2,#comments.comments-area #respond #commentform .form-submit input[type="submit"],.ziph-lrForm_title,.woocommerce div.product .entry-summary p.price,.woocommerce div.product .entry-summary p.price del,.woocommerce div.product .entry-summary p.price ins,.woocommerce #respond input#submit.alt,.woocommerce a.button.alt,.woocommerce button.button.alt,.woocommerce input.button.alt,.woocommerce #review_form #respond input[type="submit"],.woocommerce a.button:not(.add_to_cart_button),.woocommerce form.login input[type="submit"],.woocommerce form.register input[type="submit"],.woocommerce-checkout .place-order input[type="submit"],.woocommerce-cart table.shop_table.cart .coupon input.button,.woocommerce-account .u-column1 h2,.woocommerce-account .u-column2 h2,.ziph-404Page_warp a.ziph-404_btn,.ziph-domainType h2,.ziph-domainAffilt_txt h2,#bridge #main-body .main-content > h2,#bridge .announcement-single h2 .label,#bridge .announcement-single h3 .label,#bridge .logincontainer input#login,#bridge .logincontainer button.btn[type="submit"],#bridge.ziph-WhmpressAnnou .announcement-single h2 span,#bridge .form-horizontal button.btn[type="submit"],#bridge .using-password-strength input.btn[type="submit"],.ziph-singlePost_content h1,.ziph-singlePost_content h2,.ziph-page_content h2,#comments.comments-area .comment-content h1,#comments.comments-area .comment-content h2,.woocommerce-account .ziph-page_warp .woocommerce h1,.woocommerce-account .ziph-page_warp .woocommerce h2,.ziph-singlePost_content .post-password-form input[type="submit"],.ziph-page_content .post-password-form input[type="submit"],#comments.comments-area .comment-content .post-password-form input[type="submit"],.woocommerce-account .ziph-page_warp .woocommerce .post-password-form input[type="submit"],.woocommerce div.product .woocommerce-Price-amount.amount',
            'font'            => array(
              'family'        => 'Roboto',
              'variant'       => '700',
            ),
          ),
          array(
            'title'           => esc_html__('Sub Headings Typography', 'zipprich'),
            'selector'        => 'h4,h5,h6,.ziph-bdr_btn,.ziph-tblpric_list ul li,.ziph-counter_single h2,.ziph-counter_single p,.ziph-tstimnl_bio,.ziph-goodness_txt h4,.ziph-slidcap_txt3 .btn.ziph-btn,.ziph-slidcap_txt.ziph-hm2slidcap_txt h2,.ziph-revtestimonial_info,.ziph-slidcap_price,.ziph-page_breadcrumb li,.ziph-about_us h5,.ziph-vds_text p,.ziph-process_price sub,.ziph-shost_table .table > tbody > tr > td.ziph-sht_title,.ziph-affilt_btn,.ziph-affilte_table .table-bordered > thead > tr > th,.ziph-submitbdr_btn,.ziph-widget-title,.ziph-sigltags_warp > span,#comments.comments-area .comments-title,#comments.comments-area .ziph-comments-meta h4,#comments.comments-area #respond #reply-title,.ziph-submit_btn,.wpcf7 input[type="submit"],.wpcf7 button[type="submit"],.ziph-lrForm .ziph-submit_btn,.woocommerce ul.products li.product .ajax_add_to_cart,.woocommerce ul.products li.product .product_type_variable.add_to_cart_button,.woocommerce ul.products li.product .added_to_cart,.woocommerce ul.products li.product .ziph-producDetails_btn,.woocommerce div.product .entry-summary .product_title,.woocommerce div.product .entry-summary .product_meta > span,.woocommerce div.product .woocommerce-Tabs-panel--reviews #reviews #comments li .comment-text p.meta strong,.woocommerce-checkout .checkout_coupon input[type="submit"],.woocommerce-checkout .woocommerce table.shop_table th.product-total,.woocommerce-checkout #payment ul.payment_methods li.payment_method_paypal .about_paypal,.woocommerce-cart table.shop_table.cart .actions > input.button,.woocommerce-cart .cart_totals > h2,.woocommerce-cart .cart_totals table.shop_table tr.order-total th,.woocommerce-cart .cart_totals table.shop_table tr.order-total td,.woocommerce-cart .cart_totals table.shop_table tr.order-total th strong,.woocommerce-cart .cart_totals table.shop_table tr.order-total th .woocommerce-Price-amount,.woocommerce-cart .cart_totals table.shop_table tr.order-total td strong,.woocommerce-cart .cart_totals table.shop_table tr.order-total td .woocommerce-Price-amount,.woocommerce-cart .cart_totals table.shop_table tr.shipping td .shipping-calculator-form .button,.ziph-megmenuSingle_cont h4,ul.ziph-domainPrice_nav li a,.ziph-domainPrice-table tbody > tr > td.ziph-domainPrice_name,.ziph-domainAffilt_txt .ziph-bdr_btn,#bridge .navbar-main .navbar-nav > li > a,#bridge input.btn,#bridge .announcement-single h2,#bridge .announcement-single h3,#bridge .main-content input.btn,#bridge #main-body .main-content.pull-md-right h2,#bridge .logincontainer label,#bridge .using-password-strength label.control-label,.ziph-whmpresDms_txt h3,.whmpress_price_matrix table th,.whmpress_price_matrix_domain table th,.whmpress_price_matrix table td[data-content="Domain"],.whmpress_price_matrix_domain table td[data-content="Domain"],.ziph-bulkDomains_title,.whmpress_domain_search_bulk button.search_btn,#bridge.ziph-WhmpressAnnou > h1,.ziph-whmpressKnowledge_area .ziph-section_txt h3,.ziph-singlePost_content h4,.ziph-singlePost_content h3,.ziph-singlePost_content th,.ziph-page_content th,#comments.comments-area .comment-content h4,#comments.comments-area .comment-content h3,#comments.comments-area .comment-content th,.woocommerce-account .ziph-page_warp .woocommerce h4,.woocommerce-account .ziph-page_warp .woocommerce h3,.woocommerce-account .ziph-page_warp .woocommerce th, .ziph-contactForm_warp .wpcf7 input[type="submit"]',
            'font'            => array(
              'family'        => 'Roboto',
              'variant'       => '500',
            ),
          ),
          array(
            'title'           => esc_html__('Paragraph Typography', 'zipprich'),
            'selector'        => 'p,.ziph-section_txt p,.ziph-hostpltfrm_single p,.ziph-htplt_priceform h5,.ziph-serv_txt p,.ziph-hmlblog_title,.ziph-hmlfaq_title,.ziph-hmlblog_txt h4,.ziph-hmlblog_txt p,.ziph-hmlblog_txt a.ziph-hmlblog_link,.ziph-faq_body p,.ziph-tetimnal_txt p,.ziph-faq[data-show="true"] .panel-title a.ziph-faq_btn:before,.ziph-goodness_txt p,.ziph-copyright,.ziph-domainsrch_links,.ziph-choose-text p,.ziph-team_info .ziph-tm_intro,.ziph-revtestimonial_warp p,.ziph-revtestimonial2_warp p,.ziph-post_excerpt p,.ziph-about_us p,.ziph-about_activity p,.ziph-shost_table .table > tfoot > tr > td q,.ziph-sigldmn_feture p,.ziph-wigtsrch_field,.ziph-side-widget select,.ziph-side-widget .tagcloud a,.ziph-footer_widget .tagcloud a,.ziph-side-widget.widget_archive ul li,.ziph-side-widget.widget_categories ul li,.ziph-side-widget.widget_pages ul li,.ziph-side-widget.widget_meta ul li,.ziph-side-widget.widget_recent_comments ul li,.ziph-side-widget.widget_recent_entries ul li,.ziph-side-widget.widget_rss ul li,.ziph-side-widget.widget_nav_menu ul li,.ziph-wigtResentPost_single .ziph-wigtRp_date,.ziph-singlePost_content p,.comment-content p,.ziph-singlePost_content ul li,.ziph-page_content ul li,.ziph-singlePost_content ol li,.ziph-page_content ol li,.comment-content ul li,.comment-content ol li,.ziph-singlePost_content blockquote p,.ziph-page_content blockquote p,.comment-content blockquote p,.ziph_page blockquote p,#comments.comments-area .comment-content p,#comments.comments-area .comment-content blockquote p,.ziph-contactCI_txt address,.ziph-contactCI_txt p,.woocommerce div.product .entry-summary .woocommerce-review-link,.woocommerce div.product .entry-summary p,.woocommerce div.product .woocommerce-Tabs-panel--description p,.woocommerce div.product .woocommerce-Tabs-panel--reviews #reviews #comments li .comment-text p.meta,.woocommerce div.product .woocommerce-Tabs-panel--reviews #reviews #comments div[class="description"] p,.woocommerce #review_form #respond p.comment-notes,.woocommerce #review_form #respond input:not([type="submit"]),.woocommerce #review_form #respond textarea,.mc4wp-form-fields input[type="email"],.woocommerce-checkout .woocommerce form.checkout_coupon p:not(.form-row),.woocommerce-checkout .woocommerce form.login p:not(.form-row),.woocommerce-checkout .woocommerce form.register p:not(.form-row),.woocommerce-checkout .woocommerce form.checkout_coupon .form-row .input-text,.woocommerce-checkout .woocommerce form.login .form-row .input-text,.woocommerce-checkout .woocommerce form.register .form-row .input-text,.woocommerce-checkout #payment ul.payment_methods li div.payment_box p,.woocommerce-cart .cart_totals table.shop_table tr th,.woocommerce-cart .cart_totals table.shop_table tr.shipping td,.woocommerce-cart .cart_totals table.shop_table tr.shipping td .shipping-calculator-form select,.woocommerce-cart .cart_totals table.shop_table tr.shipping td .shipping-calculator-form input.input-text,.ziph-404Page_warp p,.ziph-domainType_single p,#bridge #top-nav a,#bridge .navbar-main #Secondary_Navbar-Account .dropdown-menu li a,#bridge blockquote p,#bridge .alert-info,#bridge .using-password-strength .alert-info,#bridge .using-password-strength .alert-info strong,#bridge #Primary_Sidebar-Already_Registered-Already_Registered_Heading.list-group-item,.whmpress_domain_search_ajax .whmp-domain-required,.ziph-whmpresDms_txt p,.whmpress_price_matrix_domain input[type="search"],.whmpress_domain_search_bulk form .bulk-options .extention-selection label,.whmpress_domain_search_bulk form .bulk-options .extentions > div,.ziph-whmpressKnowl_single p,.ziph-side-widget.woocommerce.widget_layered_nav ul li',
            'font'            => array(
              'family'        => 'Open Sans',
              'variant'       => 'regular',
            ),
          ),
          array(
            'title'           => esc_html__('Comments Typography', 'zipprich'),
            'selector'        => '#comments.comments-area .ziph-comments-meta span',
            'font'            => array(
              'family'        => 'Cabin',
              'variant'       => '400',
            ),
            'size'            => '',
          ),
          array(
            'title'           => esc_html__('Example Usage', 'zipprich'),
            'selector'        => '.your-custom-class',
            'font'            => array(
              'family'        => 'Roboto',
              'variant'       => 'regular',
            ),
          ),
        ),
      ),

      // Subset
      array(
        'id'                  => 'subsets',
        'type'                => 'select',
        'title'               => esc_html__('Subsets', 'zipprich'),
        'class'               => 'chosen',
        'options'             => array(
          'latin'             => 'latin',
          'latin-ext'         => 'latin-ext',
          'cyrillic'          => 'cyrillic',
          'cyrillic-ext'      => 'cyrillic-ext',
          'greek'             => 'greek',
          'greek-ext'         => 'greek-ext',
          'vietnamese'        => 'vietnamese',
          'devanagari'        => 'devanagari',
          'khmer'             => 'khmer',
        ),
        'attributes'         => array(
          'data-placeholder' => 'Subsets',
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( 'latin' ),
      ),

      array(
        'id'                  => 'font_weight',
        'type'                => 'select',
        'title'               => esc_html__('Font Weights', 'zipprich'),
        'class'               => 'chosen',
        'options'             => array(
          '100'   => 'Thin 100',
          '100i'  => 'Thin 100 Italic',
          '200'   => 'Extra Light 200',
          '200i'  => 'Extra Light 200 Italic',
          '300'   => 'Light 300',
          '300i'  => 'Light 300 Italic',
          '400'   => 'Regular 400',
          '400i'  => 'Regular 400 Italic',
          '500'   => 'Medium 500',
          '500i'  => 'Medium 500 Italic',
          '600'   => 'Semi Bold 600',
          '600i'  => 'Semi Bold 600 Italic',
          '700'   => 'Bold 700',
          '700i'  => 'Bold 700 Italic',
          '800'   => 'Extra Bold 800',
          '800i'  => 'Extra Bold 800 Italic',
          '900'   => 'Black 900',
          '900i'  => 'Black 900 Italic',
        ),
        'attributes'         => array(
          'data-placeholder' => 'Font Weight',
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( '300', '400', '500', '600', '700', '900' ),
      ),

      // Custom Fonts Upload
      array(
        'id'                 => 'font_family',
        'type'               => 'group',
        'title'              => 'Upload Custom Fonts',
        'button_title'       => 'Add New Custom Font',
        'accordion_title'    => 'Adding New Font',
        'accordion'          => true,
        'desc'               => 'It is simple. Only add your custom fonts and click to save. After you can check "Font Family" selector. Do not forget to Save!',
        'fields'             => array(

          array(
            'id'             => 'name',
            'type'           => 'text',
            'title'          => 'Font-Family Name',
            'attributes'     => array(
              'placeholder'  => 'for eg. Arial'
            ),
          ),

          array(
            'id'             => 'ttf',
            'type'           => 'upload',
            'title'          => 'Upload .ttf <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.ttf</i>',
            ),
          ),

          array(
            'id'             => 'eot',
            'type'           => 'upload',
            'title'          => 'Upload .eot <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.eot</i>',
            ),
          ),

          array(
            'id'             => 'otf',
            'type'           => 'upload',
            'title'          => 'Upload .otf <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.otf</i>',
            ),
          ),

          array(
            'id'             => 'woff',
            'type'           => 'upload',
            'title'          => 'Upload .woff <small><i>(optional)</i></small>',
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => 'Use this Font-Format',
              'button_title' => 'Upload <i>.woff</i>',
            ),
          ),

          array(
            'id'             => 'css',
            'type'           => 'textarea',
            'title'          => 'Extra CSS Style <small><i>(optional)</i></small>',
            'attributes'     => array(
              'placeholder'  => 'for eg. font-weight: normal;'
            ),
          ),

        ),
      ),
      // End All field

    ),
  );

  // ------------------------------
  // Pages
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_pages',
    'title'  => esc_html__('Pages', 'zipprich'),
    'icon'   => 'fa fa-files-o'
  );

  // ------------------------------
  // Blog Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'blog_section',
    'title'    => esc_html__('Blog', 'zipprich'),
    'icon'     => 'fa fa-edit',
    'sections' => array(

      // blog general section
      array(
        'name'     => 'blog_general_tab',
        'title'    => esc_html__('General', 'zipprich'),
        'icon'     => 'fa fa-cog',
        'fields'   => array(

          // Layout
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Layout', 'zipprich')
          ),
          array(
            'id'             => 'blog_listing_style',
            'type'           => 'select',
            'title'          => esc_html__('Blog Listing Style', 'zipprich'),
            'options'        => array(
              'style-one' => esc_html__('List (Default)', 'zipprich'),
              'style-two' => esc_html__('Grid', 'zipprich'),
            ),
            'default_option' => 'Select blog style',
            'help'          => esc_html__('This style will apply, default blog pages - Like : Archive, Category, Tags, Search & Author. If this settings will not apply your blog page, please set that page as a post page in Settings > Readings.', 'zipprich'),
          ),
          array(
            'id'             => 'blog_listing_columns',
            'type'           => 'select',
            'title'          => esc_html__('Blog Listing Columns', 'zipprich'),
            'options'        => array(
              'col-md-6' => esc_html__('Column Two', 'zipprich'),
              'col-md-4' => esc_html__('Column Three', 'zipprich'),
            ),
            'default_option' => esc_html__('Select blog column', 'zipprich'),
            'dependency'     => array('blog_listing_style', '==', 'style-two'),
            'info'          => esc_html__('Default option : Column Two', 'zipprich'),
          ),
          array(
            'id'             => 'blog_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'zipprich'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'zipprich'),
              'sidebar-hide' => esc_html__('Hide', 'zipprich'),
            ),
            'default_option' => 'Select sidebar position',
            'help'          => esc_html__('This style will apply, default blog pages - Like : Archive, Category, Tags, Search & Author.', 'zipprich'),
            'info'          => esc_html__('Default option : Right', 'zipprich'),
          ),
          array(
            'id'             => 'blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'zipprich'),
            'options'        => zipprich_vt_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'zipprich'),
            'dependency'     => array('blog_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'zipprich'),
          ),
          // Layout
          // Global Options
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Global Options', 'zipprich')
          ),
          array(
            'id'         => 'theme_exclude_categories',
            'type'       => 'checkbox',
            'title'      => esc_html__('Exclude Categories', 'zipprich'),
            'info'      => esc_html__('Select categories you want to exclude from blog page.', 'zipprich'),
            'options'    => 'categories',
          ),
          array(
            'id'      => 'theme_blog_excerpt',
            'type'    => 'text',
            'title'   => esc_html__('Excerpt Length', 'zipprich'),
            'info'   => esc_html__('Blog short content length, in blog listing pages.', 'zipprich'),
            'default' => '55',
          ),
          array(
            'id'      => 'theme_metas_hide',
            'type'    => 'checkbox',
            'title'   => esc_html__('Meta\'s to hide', 'zipprich'),
            'info'    => esc_html__('Check items you want to hide from blog/post meta field.', 'zipprich'),
            'class'      => 'horizontal',
            'options'    => array(
              'category'   => esc_html__('Category', 'zipprich'),
              'date'    => esc_html__('Date', 'zipprich'),
              'comments'      => esc_html__('Comments', 'zipprich'),
            ),
            // 'default' => '30',
          ),
          // End fields

        )
      ),

      // blog single section
      array(
        'name'     => 'blog_single_tab',
        'title'    => esc_html__('Single', 'zipprich'),
        'icon'     => 'fa fa-sticky-note',
        'fields'   => array(

          // Start fields
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Enable / Disable', 'zipprich')
          ),
          array(
            'id'    => 'single_featured_image',
            'type'  => 'switcher',
            'title' => esc_html__('Featured Image', 'zipprich'),
            'info' => esc_html__('If need to hide featured image from single blog post page, please turn this OFF.', 'zipprich'),
            'default' => true,
          ),
          array(
            'id'    => 'single_author_info',
            'type'  => 'switcher',
            'title' => esc_html__('Author Info', 'zipprich'),
            'info' => esc_html__('If need to hide author info on single blog page, please turn this OFF.', 'zipprich'),
            'default' => true,
          ),
          array(
            'id'    => 'single_share_option',
            'type'  => 'switcher',
            'title' => esc_html__('Share Option', 'zipprich'),
            'info' => esc_html__('If need to hide share option on single blog page, please turn this OFF.', 'zipprich'),
            'default' => true,
          ),
          array(
            'id'    => 'single_comment_form',
            'type'  => 'switcher',
            'title' => esc_html__('Comment Area/Form', 'zipprich'),
            'info' => esc_html__('If need to hide comment area and that form on single blog page, please turn this OFF.', 'zipprich'),
            'default' => true,
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Sidebar', 'zipprich')
          ),
          array(
            'id'             => 'single_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'zipprich'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'zipprich'),
              'sidebar-hide' => esc_html__('Hide', 'zipprich'),
            ),
            'default_option' => 'Select sidebar position',
            'info'          => esc_html__('Default option : Right', 'zipprich'),
          ),
          array(
            'id'             => 'single_blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'zipprich'),
            'options'        => zipprich_vt_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'zipprich'),
            'dependency'     => array('single_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'zipprich'),
          ),
          // End fields

        )
      ),

    ),
  );

if (class_exists( 'WooCommerce' )){
  // ------------------------------
  // WooCommerce Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'woocommerce_section',
    'title'    => esc_html__('WooCommerce', 'zipprich'),
    'icon'     => 'fa fa-shopping-cart',
    'fields' => array(

      // Start fields
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Layout', 'zipprich')
      ),
      array(
        'id'             => 'woo_sidebar_position',
        'type'           => 'select',
        'title'          => esc_html__('Sidebar Position', 'zipprich'),
        'options'        => array(
          'right-sidebar' => esc_html__('Right', 'zipprich'),
          'sidebar-hide' => esc_html__('Hide', 'zipprich'),
        ),
        'default_option' => esc_html__('Select sidebar position', 'zipprich'),
        'info'          => esc_html__('Default option : Right', 'zipprich'),
      ),
      array(
        'id'             => 'woo_widget',
        'type'           => 'select',
        'title'          => esc_html__('Sidebar Widget', 'zipprich'),
        'options'        => zipprich_vt_registered_sidebars(),
        'default_option' => esc_html__('Select Widget', 'zipprich'),
        'dependency'     => array('woo_sidebar_position', '!=', 'sidebar-hide'),
        'info'          => esc_html__('Default option : Shop Page', 'zipprich'),
      ),

      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Listing', 'zipprich')
      ),
      array(
        'id'      => 'theme_woo_limit',
        'type'    => 'text',
        'title'   => esc_html__('Product Limit', 'zipprich'),
        'info'   => esc_html__('Enter the number value for per page products limit.', 'zipprich'),
      ),
      array(
        'id'      => 'theme_align_height',
        'type'    => 'text',
        'title'   => esc_html__('Have Alignment Space?', 'zipprich'),
        'info'   => esc_html__('Set minimun height of each products here. Current minimum height is 100px', 'zipprich'),
      ),
      // End fields

      // Start fields
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => esc_html__('Single Product', 'zipprich')
      ),
      array(
        'id'             => 'woo_related_limit',
        'type'           => 'text',
        'title'          => esc_html__('Related Products Limit', 'zipprich'),
      ),
      array(
        'id'    => 'woo_single_upsell',
        'type'  => 'switcher',
        'title' => esc_html__('You May Also Like', 'zipprich'),
        'info' => esc_html__('If you don\'t want \'You May Also Like\' products in single product page, please turn this ON.', 'zipprich'),
        'default' => false,
      ),
      array(
        'id'    => 'woo_single_related',
        'type'  => 'switcher',
        'title' => esc_html__('Related Products', 'zipprich'),
        'info' => esc_html__('If you don\'t want \'Related Products\' in single product page, please turn this ON.', 'zipprich'),
        'default' => false,
      ),
      // End fields

    ),
  );
}

  // ------------------------------
  // WooCommerce Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'whmcs_section',
    'title'    => esc_html__('WHMCS', 'zipprich'),
    'icon'     => 'fa fa-globe',
    'fields' => array(

      // Start fields
      array(
        'type'    => 'notice',
        'class'   => 'info cs-vt-heading',
        'content' => __('Provide WHMCS credential Identifier and Secret. For more info <a href="https://docs.whmcs.com/API_Authentication_Credentials">visit here</a>', 'zipprich')
      ),
      array(
        'id'      => 'whmcs_identifier',
        'type'    => 'text',
        'title'   => esc_html__('WHMCS Identifier', 'zipprich'),
        'info'   => esc_html__('Enter the number value for per page products limit.', 'zipprich'),
      ),
      array(
        'id'      => 'whmcs_secret',
        'type'    => 'text',
        'title'   => esc_html__('WHMCS Secret', 'zipprich'),
        'info'   => esc_html__('Set minimun height of each products here. Current minimum height is 100px', 'zipprich'),
      ),
      // End fields

    ),
  );

  // ------------------------------
  // Extra Pages
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_extra_pages',
    'title'    => esc_html__('Extra Pages', 'zipprich'),
    'icon'     => 'fa fa-clone',
    'sections' => array(

      // error 404 page
      array(
        'name'     => 'error_page_section',
        'title'    => esc_html__('404 Page', 'zipprich'),
        'icon'     => 'fa fa-exclamation-triangle',
        'fields'   => array(

          // Start 404 Page
          array(
            'id'    => 'error_heading',
            'type'  => 'text',
            'title' => esc_html__('404 Page Heading', 'zipprich'),
            'info'  => esc_html__('Enter 404 page heading.', 'zipprich'),
          ),
          array(
            'id'    => 'error_subheading',
            'type'  => 'textarea',
            'title' => esc_html__('404 Page Sub Heading', 'zipprich'),
            'info'  => esc_html__('Enter 404 page sub heading.', 'zipprich'),
            'shortcode' => true,
          ),
          array(
            'id'    => 'error_page_content',
            'type'  => 'textarea',
            'title' => esc_html__('404 Page Content', 'zipprich'),
            'info'  => esc_html__('Enter 404 page content.', 'zipprich'),
            'shortcode' => true,
          ),
          array(
            'id'    => 'error_btn_text',
            'type'  => 'text',
            'title' => esc_html__('Button Text', 'zipprich'),
            'info'  => esc_html__('Enter BACK TO HOME button text. If you want to change it.', 'zipprich'),
          ),
          // End 404 Page

        ) // end: fields
      ), // end: fields section

      // maintenance mode page
      array(
        'name'     => 'maintenance_mode_section',
        'title'    => esc_html__('Maintenance Mode', 'zipprich'),
        'icon'     => 'fa fa-hourglass-half',
        'fields'   => array(

          // Start Maintenance Mode
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('If you turn this ON : Only Logged in users will see your pages. All other visiters will see, selected page of : <strong>Maintenance Mode Page</strong>', 'zipprich')
          ),
          array(
            'id'             => 'enable_maintenance_mode',
            'type'           => 'switcher',
            'title'          => esc_html__('Maintenance Mode', 'zipprich'),
            'default'        => false,
          ),
          array(
            'id'             => 'maintenance_mode_page',
            'type'           => 'select',
            'title'          => esc_html__('Maintenance Mode Page', 'zipprich'),
            'options'        => 'pages',
            'default_option' => esc_html__('Select a page', 'zipprich'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          array(
            'id'             => 'maintenance_mode_bg',
            'type'           => 'background',
            'title'          => esc_html__('Page Background', 'zipprich'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          // End Maintenance Mode

        ) // end: fields
      ), // end: fields section

    )
  );

  // ------------------------------
  // Advanced
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_advanced',
    'title'  => esc_html__('Advanced', 'zipprich'),
    'icon'   => 'fa fa-cog'
  );

  // ------------------------------
  // Misc Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'misc_section',
    'title'    => esc_html__('Misc', 'zipprich'),
    'icon'     => 'fa fa-recycle',
    'sections' => array(

      // custom sidebar section
      array(
        'name'     => 'custom_sidebar_section',
        'title'    => esc_html__('Custom Sidebar', 'zipprich'),
        'icon'     => 'fa fa-reorder',
        'fields'   => array(

          // start fields
          array(
            'id'              => 'custom_sidebar',
            'title'           => esc_html__('Sidebars', 'zipprich'),
            'desc'            => esc_html__('Go to Appearance -> Widgets after create sidebars', 'zipprich'),
            'type'            => 'group',
            'fields'          => array(
              array(
                'id'          => 'sidebar_name',
                'type'        => 'text',
                'title'       => esc_html__('Sidebar Name', 'zipprich'),
              ),
              array(
                'id'          => 'sidebar_desc',
                'type'        => 'text',
                'title'       => esc_html__('Custom Description', 'zipprich'),
              )
            ),
            'accordion'       => true,
            'button_title'    => esc_html__('Add New Sidebar', 'zipprich'),
            'accordion_title' => esc_html__('New Sidebar', 'zipprich'),
          ),
          // end fields

        )
      ),
      // custom sidebar section

      // Custom CSS/JS
      array(
        'name'        => 'custom_css_js_section',
        'title'       => esc_html__('Custom Codes', 'zipprich'),
        'icon'        => 'fa fa-code',

        // begin: fields
        'fields'      => array(

          // Start Custom CSS/JS
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Custom CSS', 'zipprich')
          ),
          array(
            'id'             => 'theme_custom_css',
            'type'           => 'textarea',
            'attributes' => array(
              'rows'     => 10,
              'placeholder'     => esc_html__('Enter your CSS code here...', 'zipprich'),
            ),
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Custom JS', 'zipprich')
          ),
          array(
            'id'             => 'theme_custom_js',
            'type'           => 'textarea',
            'attributes' => array(
              'rows'     => 10,
              'placeholder'     => esc_html__('Enter your JS code here...', 'zipprich'),
            ),
          ),
          // End Custom CSS/JS

        ) // end: fields
      ),

      // Translation
      array(
        'name'        => 'theme_translation_section',
        'title'       => esc_html__('Translation', 'zipprich'),
        'icon'        => 'fa fa-language',

        // begin: fields
        'fields'      => array(

          // Start Translation
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Common Texts', 'zipprich')
          ),
          array(
            'id'          => 'read_more_text',
            'type'        => 'text',
            'title'       => esc_html__('Read More Text', 'zipprich'),
          ),
          array(
            'id'          => 'share_text',
            'type'        => 'text',
            'title'       => esc_html__('Share Text', 'zipprich'),
          ),
          array(
            'id'          => 'post_comment_text',
            'type'        => 'text',
            'title'       => esc_html__('Post Comment Text [Submit Button]', 'zipprich'),
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('WooCommerce', 'zipprich')
          ),
          array(
            'id'          => 'add_to_cart_text',
            'type'        => 'text',
            'title'       => esc_html__('Add to Cart Text', 'zipprich'),
          ),
          array(
            'id'          => 'details_text',
            'type'        => 'text',
            'title'       => esc_html__('Details Text', 'zipprich'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-vt-heading',
            'content' => esc_html__('Pagination', 'zipprich')
          ),
          array(
            'id'          => 'older_post',
            'type'        => 'text',
            'title'       => esc_html__('Older Posts Text', 'zipprich'),
          ),
          array(
            'id'          => 'newer_post',
            'type'        => 'text',
            'title'       => esc_html__('Newer Posts Text', 'zipprich'),
          ),
          // End Translation

        ) // end: fields
      ),

    ),
  );

  // ------------------------------
  // envato account
  // ------------------------------
  $options[]   = array(
    'name'     => 'envato_account_section',
    'title'    => esc_html__('Envato Account', 'zipprich'),
    'icon'     => 'fa fa-link',
    'fields'   => array(

      array(
        'type'    => 'notice',
        'class'   => 'warning',
        'content' => esc_html__('Enter your Username and API key. You can get update our themes from WordPress admin itself.', 'zipprich'),
      ),
      array(
        'id'      => 'themeforest_username',
        'type'    => 'text',
        'title'   => esc_html__('Envato Username', 'zipprich'),
      ),
      array(
        'id'      => 'themeforest_api',
        'type'    => 'text',
        'title'   => esc_html__('Envato API Key', 'zipprich'),
        'class'   => 'text-security',
        'after'   => esc_html__('<p>This is not a password field. Enter your Envato API key, which is located in : <strong>http://themeforest.net/user/[YOUR-USER-NAME]/api_keys/edit</strong></p>', 'zipprich')
      ),

    )
  );

  // ------------------------------
  // backup                       -
  // ------------------------------
  $options[]   = array(
    'name'     => 'backup_section',
    'title'    => 'Backup',
    'icon'     => 'fa fa-shield',
    'fields'   => array(

      array(
        'type'    => 'notice',
        'class'   => 'warning',
        'content' => 'You can save your current options. Download a Backup and Import.',
      ),

      array(
        'type'    => 'backup',
      ),

    )
  );

  return $options;

}
add_filter( 'cs_framework_options', 'zipprich_vt_options' );