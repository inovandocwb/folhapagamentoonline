<?php
/*
 * All CSS and JS files are enqueued from this file
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/**
 * Enqueue Files for FrontEnd
 */
if ( ! function_exists( 'zipprich_vt_scripts_styles' ) ) {
  function zipprich_vt_scripts_styles() {

    // Styles
    wp_enqueue_style( 'masonry' );
    wp_enqueue_style( 'font-awesome', ZIPPRICH_THEMEROOT_URI . '/inc/theme-options/cs-framework/assets/css/font-awesome.min.css' );
    wp_enqueue_style( 'zipprich-plugins', ZIPPRICH_CSS .'/plugins.css', array(), ZIPPRICH_VERSION, 'all' );
    wp_enqueue_style( 'animated', ZIPPRICH_CSS .'/animated.css', array(), '3.5.1', 'all' );
    wp_enqueue_style( 'bootstrap', ZIPPRICH_CSS .'/bootstrap.min.css', array(), '3.3.6', 'all' );
    wp_enqueue_style( 'owl-carousel', ZIPPRICH_CSS .'/owl.carousel.min.css', array(), '2.2.1', 'all' );
    wp_enqueue_style( 'zipprich-style', ZIPPRICH_CSS .'/styles.css', array(), ZIPPRICH_VERSION, 'all' );
    wp_enqueue_style( 'zipprich-colors-css', ZIPPRICH_CSS .'/colors.css', array(), ZIPPRICH_VERSION, 'all' );

    // Scripts
    wp_enqueue_script( 'masonry' );
    wp_enqueue_script( 'bootstrap', ZIPPRICH_SCRIPTS . '/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );

    wp_enqueue_script( 'zipprich-plugins', ZIPPRICH_SCRIPTS . '/plugins.js', array( 'jquery' ), ZIPPRICH_VERSION, true );
    wp_enqueue_script( 'waypoints', ZIPPRICH_SCRIPTS . '/waypoints.min.js', array( 'jquery' ), ZIPPRICH_VERSION, true );
    wp_enqueue_script( 'counterup', ZIPPRICH_SCRIPTS . '/counterup.min.js', array( 'jquery' ), ZIPPRICH_VERSION, true );
    wp_enqueue_script( 'owl-carousel', ZIPPRICH_SCRIPTS . '/owl.carousel.min.js', array( 'jquery' ), '2.2.1', true );
    wp_enqueue_script( 'slimmenu', ZIPPRICH_SCRIPTS . '/slimmenu.min.js', array( 'jquery' ), ZIPPRICH_VERSION, true );
    wp_enqueue_script( 'fitvids', ZIPPRICH_SCRIPTS . '/fitvids.min.js', array( 'jquery' ), ZIPPRICH_VERSION, true );
    wp_enqueue_script( 'sticky-header', ZIPPRICH_SCRIPTS . '/sticky.header.min.js', array( 'jquery' ), ZIPPRICH_VERSION, true );
    wp_enqueue_script( 'match-height', ZIPPRICH_SCRIPTS . '/matchHeight.min.js', array( 'jquery' ), ZIPPRICH_VERSION, true );

    wp_enqueue_script( 'zipprich-scripts', ZIPPRICH_SCRIPTS . '/scripts.js', array( 'jquery' ), ZIPPRICH_VERSION, true );

    // Comments
    wp_enqueue_script( 'validate', ZIPPRICH_SCRIPTS . '/jquery.validate.min.js', array( 'jquery' ), '1.9.0', true );
    wp_add_inline_script( 'validate', 'jQuery(document).ready(function($) {$("#commentform").validate({rules: {author: {required: true,minlength: 2},email: {required: true,email: true},comment: {required: true,minlength: 10}}});});' );

    // Responsive
    wp_enqueue_style( 'zipprich-responsive', ZIPPRICH_CSS .'/responsive.css', array(), ZIPPRICH_VERSION, 'all' );

    // Adds support for pages with threaded comments
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }

  }
  add_action( 'wp_enqueue_scripts', 'zipprich_vt_scripts_styles' );
}

/**
 * Enqueue Files for BackEnd
 */
if ( ! function_exists( 'zipprich_vt_admin_scripts_styles' ) ) {
  function zipprich_vt_admin_scripts_styles() {
    wp_enqueue_style( 'jquery-ui-sortable' );
    wp_enqueue_style( 'zipprich-admin-main', ZIPPRICH_CSS . '/admin-styles.css', true );
    wp_enqueue_script( 'zipprich-admin-scripts', ZIPPRICH_SCRIPTS . '/admin-scripts.js', true );

  }
  add_action( 'admin_enqueue_scripts', 'zipprich_vt_admin_scripts_styles' );
}

/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function zipprich_add_editor_styles() {
 add_editor_style( get_stylesheet_uri() );
}
add_action( 'init', 'zipprich_add_editor_styles' );

/* Enqueue All Styles */
if ( ! function_exists( 'zipprich_vt_wp_enqueue_styles' ) ) {
  function zipprich_vt_wp_enqueue_styles() {
    zipprich_vt_google_fonts();
    add_action( 'wp_head', 'zipprich_vt_custom_css', 99 );
  }
  add_action( 'wp_enqueue_scripts', 'zipprich_vt_wp_enqueue_styles' );
}
