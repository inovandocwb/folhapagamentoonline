<?php
// Mega Menu Options
function zipprich_mega_menu_admin_options($fields) {
	$fields = array(
		'mmenu-active' => array(
			'label' => esc_html__('Enable Megamenu', 'zipprich'),
			'type' => 'checkbox',
		),
		'mmenu-item' => array(
			'label' => esc_html__('Megamenu Item', 'zipprich'),
			'type' => 'checkbox',
		),
		'mmenu-desc' => array(
			'label' => esc_html__('Description', 'zipprich'),
			'type' => 'textarea',
		),
		'mmenu-image' => array(
			'label' => esc_html__('Image', 'zipprich'),
			'type' => 'upload',
		),
		'mmenu-more' => array(
			'label' => esc_html__('More Text', 'zipprich'),
			'type' => 'text',
		),
	);
	return $fields;
}
add_filter( 'm_menu', 'zipprich_mega_menu_admin_options' );