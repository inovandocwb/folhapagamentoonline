<?php
/*
 * Zipprich Theme Widgets
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

if ( ! function_exists( 'zipprich_vt_widget_init' ) ) {
	function zipprich_vt_widget_init() {
		if ( function_exists( 'register_sidebar' ) ) {

			// Main Widget Area
			register_sidebar(
				array(
					'name' => esc_html__( 'Main Widget Area', 'zipprich' ),
					'id' => 'sidebar-1',
					'description' => esc_html__( 'Appears on posts and pages.', 'zipprich' ),
					'before_widget' => '<aside id="%1$s" class="ziph-side-widget %2$s">',
					'after_widget' => '</aside> <!-- end widget -->',
					'before_title' => '<h4 class="ziph-widget-title">',
					'after_title' => '</h4>',
				)
			);
			// Main Widget Area

			// Footer Widgets
			$footer_widgets = cs_get_option( 'footer_widget_layout' );
		    if( $footer_widgets != 'theme-default' ) {

		      switch ( $footer_widgets ) {
		        case 5:
		        case 6:
		        case 7:
		          $length = 3;
		        break;

		        case 8:
		        case 9:
		          $length = 4;
		        break;

		        default:
		          $length = $footer_widgets;
		        break;
		      }

		      for( $i = 0; $i < $length; $i++ ) {

		        $num = ( $i+1 );

		        register_sidebar( array(
					'id'            => 'footer-' . $num,
					'name'          => esc_html__( 'Footer Widget ', 'zipprich' ). $num,
					'description'   => esc_html__( 'Appears on footer section.', 'zipprich' ),
					'before_widget' => '<div class="ziph-footer_widget %2$s">',
					'after_widget'  => '<div class="clear"></div></div> <!-- end widget -->',
					'before_title'  => '<h4 class="ziph-footrwidget_title">',
					'after_title'   => '</h4>'
		        ) );

		      }

		    } else {
		    	// For Theme Default Layout Only
		    	$length = 4;
				for( $i = 0; $i < $length; $i++ ) {

					$num = ( $i+1 );

					register_sidebar( array(
						'id'            => 'footer-' . $num,
						'name'          => esc_html__( 'Footer Widget ', 'zipprich' ). $num,
						'description'   => esc_html__( 'Appears on footer section.', 'zipprich' ),
						'before_widget' => '<div class="ziph-footer_widget %2$s">',
						'after_widget'  => '<div class="clear"></div></div> <!-- end widget -->',
						'before_title'  => '<h4 class="ziph-footrwidget_title">',
						'after_title'   => '</h4>'
					) );

				}
		    }
		    // Footer Widgets

			/* Custom Sidebar */
			$custom_sidebars = cs_get_option('custom_sidebar');
			if ($custom_sidebars) {
				foreach($custom_sidebars as $custom_sidebar) :
				$heading = $custom_sidebar['sidebar_name'];
				$own_id = preg_replace('/[^a-z]/', "-", strtolower($heading));
				$desc = $custom_sidebar['sidebar_desc'];

				register_sidebar( array(
					'name' => esc_attr($heading),
					'id' => $own_id,
					'description' => esc_attr($desc),
					'before_widget' => '<aside id="%1$s" class="ziph-side-widget %2$s">',
					'after_widget' => '</aside> <!-- end widget -->',
					'before_title' => '<h4 class="ziph-widget-title">',
					'after_title' => '</h4>',
				) );
				endforeach;
			}
			/* Custom Sidebar */

		}
	}
	add_action( 'widgets_init', 'zipprich_vt_widget_init' );
}