<?php
/*
 * All Zipprich Theme Related Functions Files are Linked here
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/* Theme All Basic Setup */
require_once( ZIPPRICH_FRAMEWORK . '/theme-support.php' );
require_once( ZIPPRICH_FRAMEWORK . '/backend-functions.php' );
require_once( ZIPPRICH_FRAMEWORK . '/frontend-functions.php' );
require_once( ZIPPRICH_FRAMEWORK . '/enqueue-files.php' );
require_once( ZIPPRICH_CS_FRAMEWORK . '/custom-style.php' );
require_once( ZIPPRICH_CS_FRAMEWORK . '/config.php' );

/* Install Plugins */
require_once( ZIPPRICH_FRAMEWORK . '/plugins/notify/activation.php' );

/* Breadcrumbs */
require_once( ZIPPRICH_FRAMEWORK . '/plugins/breadcrumb-trail.php' );

/* Aqua Resizer */
require_once( ZIPPRICH_FRAMEWORK . '/plugins/aq_resizer.php' );

/* Sidebars */
require_once( ZIPPRICH_FRAMEWORK . '/core/sidebars.php' );

/* Menu Admin Options */
require_once( ZIPPRICH_FRAMEWORK . '/core/mmenu/admin/menu-item-custom-fields.php' );
require_once( ZIPPRICH_FRAMEWORK . '/core/mmenu/admin/walker-nav-menu-edit.php' );
require_once( ZIPPRICH_FRAMEWORK . '/core/mmenu/admin/menu-item-custom-fields-config.php' );

/* Mega Menu Options */
require_once( ZIPPRICH_FRAMEWORK . '/core/mmenu/ziph-navwalker.php' );
require_once( ZIPPRICH_FRAMEWORK . '/core/mmenu/ziph-mmenu-options.php' );