<?php
/*
 * All Front-End Helper Functions
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/* Exclude category from blog */
if( ! function_exists( 'zipprich_vt_excludeCat' ) ) {
  function zipprich_vt_excludeCat($query) {
  	if ( $query->is_home ) {
  		$exclude_cat_ids = cs_get_option('theme_exclude_categories');
  		if($exclude_cat_ids) {
  			foreach( $exclude_cat_ids as $exclude_cat_id ) {
  				$exclude_from_blog[] = '-'. $exclude_cat_id;
  			}
  			$query->set('cat', implode(',', $exclude_from_blog));
  		}
  	}
  	return $query;
  }
  add_filter('pre_get_posts', 'zipprich_vt_excludeCat');
}

/* Excerpt Length */
class ZipprichExcerpt {

  // Default length (by WordPress)
  public static $zipprich_excerpt_length = 55;

  // Output: zipprich_excerpt('short');
  public static $zipprich_excerpt_types = array(
    'short' => 20,
    'regular' => 55,
    'long' => 100
  );

  /**
   * Sets the length for the excerpt,
   * then it adds the WP filter
   * And automatically calls the_excerpt();
   *
   * @param string $new_length
   * @return void
   * @author Baylor Rae'
   */
  public static function zipprich_excerpt_length($new_length = 55) {
    ZipprichExcerpt::$zipprich_excerpt_length = $new_length;
    add_filter('excerpt_length', 'ZipprichExcerpt::new_length');
    ZipprichExcerpt::output();
  }

  // Tells WP the new length
  public static function new_length() {
    if( isset(ZipprichExcerpt::$zipprich_excerpt_types[ZipprichExcerpt::$zipprich_excerpt_length]) )
      return ZipprichExcerpt::$zipprich_excerpt_types[ZipprichExcerpt::$zipprich_excerpt_length];
    else
      return ZipprichExcerpt::$zipprich_excerpt_length;
  }

  // Echoes out the excerpt
  public static function output() {
    the_excerpt();
  }

}

// Custom Excerpt Length
if( ! function_exists( 'zipprich_excerpt' ) ) {
  function zipprich_excerpt($zipprich_excerpt_length = 55) {
    ZipprichExcerpt::zipprich_excerpt_length($zipprich_excerpt_length);
  }
}

if ( ! function_exists( 'zipprich_new_excerpt_more' ) ) {
  function zipprich_new_excerpt_more( $more ) {
    return ' [...]';
  }
  add_filter('excerpt_more', 'zipprich_new_excerpt_more');
}

/* Tag Cloud Widget - Remove Inline Font Size */
if( ! function_exists( 'zipprich_vt_tag_cloud' ) ) {
  function zipprich_vt_tag_cloud($tag_string){
    return preg_replace("/style='font-size:.+pt;'/", '', $tag_string);
  }
  add_filter('wp_generate_tag_cloud', 'zipprich_vt_tag_cloud', 10, 3);
}

/* Password Form */
if( ! function_exists( 'zipprich_vt_password_form' ) ) {
  function zipprich_vt_password_form( $output ) {
    $output = str_replace( 'type="submit"', 'type="submit" class=""', $output );
    return $output;
  }
  add_filter('the_password_form' , 'zipprich_vt_password_form');
}

/* Maintenance Mode */
if( ! function_exists( 'zipprich_vt_maintenance_mode' ) ) {
  function zipprich_vt_maintenance_mode(){
    $maintenance_mode_page = cs_get_option( 'maintenance_mode_page' );
    $enable_maintenance_mode = cs_get_option( 'enable_maintenance_mode' );

    if ( isset($enable_maintenance_mode) && ! empty( $maintenance_mode_page ) && ! is_user_logged_in() ) {
      get_template_part('layouts/post/content', 'maintenance');
      exit;
    }
  }
  add_action( 'wp', 'zipprich_vt_maintenance_mode', 1 );
}

/* Widget Layouts */
if ( ! function_exists( 'zipprich_vt_footer_widgets' ) ) {
  function zipprich_vt_footer_widgets() {

    $output = '';
    $footer_widget_layout = cs_get_option('footer_widget_layout');

    if( $footer_widget_layout ) {

      switch ( $footer_widget_layout ) {
        case 1: $widget = array('piece' => 1, 'class' => 'col-md-12'); break;
        case 2: $widget = array('piece' => 2, 'class' => 'col-md-6'); break;
        case 3: $widget = array('piece' => 3, 'class' => 'col-md-4'); break;
        case 4: $widget = array('piece' => 4, 'class' => 'col-md-3 col-sm-6'); break;
        case 5: $widget = array('piece' => 3, 'class' => 'col-md-3', 'layout' => 'col-md-6', 'queue' => 1); break;
        case 6: $widget = array('piece' => 3, 'class' => 'col-md-3', 'layout' => 'col-md-6', 'queue' => 2); break;
        case 7: $widget = array('piece' => 3, 'class' => 'col-md-3', 'layout' => 'col-md-6', 'queue' => 3); break;
        case 8: $widget = array('piece' => 4, 'class' => 'col-md-2', 'layout' => 'col-md-6', 'queue' => 1); break;
        case 9: $widget = array('piece' => 4, 'class' => 'col-md-2', 'layout' => 'col-md-6', 'queue' => 4); break;
        default : $widget = array('piece' => 4, 'class' => 'col-md-3'); break;
      }

      for( $i = 1; $i < $widget["piece"]+1; $i++ ) {

        $widget_class = ( isset( $widget["queue"] ) && $widget["queue"] == $i ) ? $widget["layout"] : $widget["class"];

        $output .= '<div class="'. esc_attr($widget_class) .'">';
        ob_start();
        if (is_active_sidebar('footer-'. $i)) {
          dynamic_sidebar( 'footer-'. $i );
        }
        $output .= ob_get_clean();
        $output .= '</div>';

      }
    }

    return $output;

  }
}

/* WP Link Pages */
if ( ! function_exists( 'zipprich_wp_link_pages' ) ) {
  function zipprich_wp_link_pages() {
    $defaults = array(
      'before'           => '<div class="wp-link-pages">' . esc_html__( 'Pages:', 'zipprich' ),
      'after'            => '</div>',
      'link_before'      => '<span>',
      'link_after'       => '</span>',
      'next_or_number'   => 'number',
      'separator'        => ' ',
      'pagelink'         => '%',
      'echo'             => 1
    );
    wp_link_pages( $defaults );
  }
}

/* Metas */
if ( ! function_exists( 'zipprich_post_metas' ) ) {
  function zipprich_post_metas() {
  $metas_hide = (array) cs_get_option( 'theme_metas_hide' );
  ?>
  <div class="ziph-post_date">
    <?php if ( !in_array( 'date', $metas_hide ) ) { // Date Hide
      echo get_the_date();
    } // Date Hides
    if ( !in_array( 'category', $metas_hide ) ) { // Category Hide
      if ( get_post_type() === 'post') {
        $category_list = get_the_category_list( ', ' );
        if ( $category_list ) {
          echo '&nbsp;<span class="ziph-crshap"></span>&nbsp;' . esc_html__( 'in', 'zipprich' ) . ' ' . '<span class="ziph-post_cat">'. $category_list .' </span>';
        }
      }
    } // Category Hides ?>
  </div>
  <?php
  }
}

/* Author Info */
if ( ! function_exists( 'zipprich_author_info' ) ) {
  function zipprich_author_info() {

    if (get_the_author_meta( 'url' )) {
      $author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );
      $website_url = get_the_author_meta( 'url' );
      $target = 'target="_blank"';
    } else {
      $author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );
      $website_url = get_author_posts_url( get_the_author_meta( 'ID' ) );
      $target = '';
    }

    // variables
    $author_text = cs_get_option('author_text');
    $author_text = $author_text ? $author_text : esc_html__( 'About author', 'zipprich' );
    $author_content = get_the_author_meta( 'description' );
if ($author_content) {
?>
  <div class="author-info-title"><?php echo esc_html($author_text); ?></div>
  <div class="ziph-author-info rounded-three">
    <div class="author-avatar">
      <a href="<?php echo esc_url($author_url); ?>" <?php echo esc_attr($target); ?>>
        <?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?>
      </a>
    </div>
     <div class="author-content">
      <a href="<?php echo esc_url($author_url); ?>" class="author-name"><?php echo get_the_author_meta('first_name').' '.get_the_author_meta('last_name'); ?></a>
      <p><?php echo get_the_author_meta( 'description' ); ?></p>
      <div class="ziph-social">
        
        <?php  if (get_the_author_meta( 'url' )): ?>
        <a href="<?php echo esc_url( get_the_author_meta( 'url' ) ); ?>" target="_blank" class="website">
          <i class="fa fa-globe"></i>
        </a><?php endif;

        if (get_the_author_meta( 'facebook' )): ?>
        <a href="<?php echo esc_url( get_the_author_meta( 'facebook' ) ); ?>" target="_blank" class="facebook">
          <i class="fa fa-facebook-official"></i>
        </a><?php endif;
        if (get_the_author_meta( 'twitter' )): ?>
        <a href="<?php echo esc_url( get_the_author_meta( 'twitter' ) ); ?>" target="_blank" class="twitter">
          <i class="fa fa-twitter-square"></i>
        </a><?php endif;
        if (get_the_author_meta( 'linkedin' )): ?>
        <a href="<?php echo esc_url( get_the_author_meta( 'linkedin' ) ); ?>" target="_blank" class="linkedin">
          <i class="fa fa-linkedin-square"></i>
        </a><?php endif;
        if (get_the_author_meta( 'google_plus' )): ?>
        <a href="<?php echo esc_url( get_the_author_meta( 'google_plus' ) ); ?>" target="_blank" class="google">
          <i class="fa fa-google-plus-square"></i>
        </a><?php endif; ?>
      </div>
    </div>
  </div>
<?php
} // if $author_content
  }
}

/* ==============================================
   Custom Comment Area Modification
=============================================== */
if ( ! function_exists( 'zipprich_comment_modification' ) ) {
  function zipprich_comment_modification($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    if ( 'div' == $args['style'] ) {
      $tag = 'div';
      $add_below = 'comment';
    } else {
      $tag = 'li';
      $add_below = 'div-comment';
    }
    $comment_class = empty( $args['has_children'] ) ? '' : 'parent';
  ?>

  <<?php echo esc_attr($tag); ?> <?php comment_class('item ' . $comment_class .' ' ); ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="">
    <?php endif; ?>
    <div class="comment-theme">
        <div class="comment-image">
          <?php if ( $args['avatar_size'] != 0 ) {
            echo get_avatar( $comment, 80 );
          } ?>
        </div>
    </div>
    <div class="comment-main-area">
      <div class="comment-wrapper">
        <div class="ziph-comments-meta">
          <h4><?php printf( '%s', get_comment_author() ); ?></h4>
          <span class="says"><?php esc_html_e('Says','zipprich') ?></span>
          <span class="comments-date">
            <?php echo get_comment_date('M d, Y'); echo ' ' . esc_html__('at','zipprich') . ' '; ?>
            <span class="caps"><?php echo get_comment_time(); ?></span>
          </span>
          <?php if ( $comment->comment_approved == '0' ) : ?>
          <em class="comment-awaiting-moderation"><?php echo esc_html__( 'Your comment is awaiting moderation.', 'zipprich' ); ?></em>
          <?php endif; ?>
          <div class="comment-content">
            <?php comment_text(); ?>
          </div>
          <div class="comments-reply">
          <?php
            comment_reply_link( array_merge( $args, array(
            'reply_text' => '<span class="comment-reply-link">'. esc_html__('Reply','zipprich') .'</span>',
            'before' => '',
            'class'  => '',
            'depth' => $depth,
            'max_depth' => $args['max_depth']
            ) ) );
          ?>
          </div>
        </div>
      </div>
    </div>
  <?php if ( 'div' != $args['style'] ) : ?>
  </div>
  <?php endif;
  }
}

/* Title Area */
if ( ! function_exists( 'zipprich_title_area' ) ) {
  function zipprich_title_area() {

    global $post, $wp_query;

    // Get post meta in all type of WP pages
    $zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
    $zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
    $zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
    $zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );
    if ($zipprich_meta && (!is_archive() || zipprich_is_woocommerce_shop())) {
      $custom_title = $zipprich_meta['page_custom_title'];
      if ($custom_title) {
        $custom_title = $custom_title;
      } elseif(post_type_archive_title()) {
        post_type_archive_title();
      } else {
        $custom_title = '';
      }
    } else { $custom_title = ''; }

    /**
     * For strings with necessary HTML, use the following:
     * Note that I'm only including the actual allowed HTML for this specific string.
     * More info: https://codex.wordpress.org/Function_Reference/wp_kses
     */
    $allowed_title_area_tags = array(
        'a' => array(
          'href' => array(),
        ),
        'span' => array(
          'class' => array(),
        )
    );

    if( $custom_title ) {
      echo esc_attr($custom_title);
    } elseif ( is_home() ) {
      bloginfo('description');
    } elseif ( is_search() ) {
      printf( esc_html__( 'Search Results for %s', 'zipprich' ), '<span>' . get_search_query() . '</span>' );
    } elseif ( is_category() || is_tax() ){
      single_cat_title();
    } elseif ( is_tag() ){
      single_tag_title(esc_html__('Posts Tagged: ', 'zipprich'));
    } elseif ( is_archive() ){
      if ( is_day() ) {
        printf( wp_kses( __( 'Archive for <span>%s</span>', 'zipprich' ), $allowed_title_area_tags ), get_the_date());
      } elseif ( is_month() ) {
        printf( wp_kses( __( 'Archive for <span>%s</span>', 'zipprich' ), $allowed_title_area_tags ), get_the_date( 'F, Y' ));
      } elseif ( is_year() ) {
        printf( wp_kses( __( 'Archive for <span>%s</span>', 'zipprich' ), $allowed_title_area_tags ), get_the_date( 'Y' ));
      } elseif ( is_author() ) {
        printf( wp_kses( __( 'Posts by: <span>%s</span>', 'zipprich' ), $allowed_title_area_tags ), get_the_author_meta( 'display_name', $wp_query->post->post_author ));
      } elseif( zipprich_is_woocommerce_shop() ) {
        echo esc_attr($custom_title);
      } elseif ( is_post_type_archive() ) {
        post_type_archive_title();
      } else {
        esc_html_e( 'Archives', 'zipprich' );
      }
    } elseif( is_404() ) {
      esc_html_e('404', 'zipprich');
    } else {
      the_title();
    }

  }
}

/**
 * Pagination Function
 */
if ( ! function_exists( 'zipprich_paging_nav' ) ) {
  function zipprich_paging_nav() {
    $zipprich_blog_style = cs_get_option('blog_listing_style');
    if ($zipprich_blog_style === 'style-two') {
      $zipprich_blog_style = 'm-24 ';
    } else { $zipprich_blog_style = ''; }
    echo '<div class="' . esc_attr( $zipprich_blog_style ) . 'text-center ziph-postPagination_warp"><nav class="navigation pagination"><div class="nav-links">';
    if ( function_exists('wp_pagenavi')) {
      wp_pagenavi();
    } else {
      $older_post = cs_get_option('older_post');
      $newer_post = cs_get_option('newer_post');
      $older_post = $older_post ? $older_post : esc_html__( 'Prev', 'zipprich' );
      $newer_post = $newer_post ? $newer_post : esc_html__( 'Next', 'zipprich' );
      global $wp_query;
      $big = 999999999;
      if($wp_query->max_num_pages == '1' ) {} else {echo '';}
      echo paginate_links( array(
        'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
        'format' => '?paged=%#%',
        'prev_text' => '<i class="fa fa-angle-left"></i>' . esc_attr($older_post),
        'next_text' => esc_attr($newer_post) . '<i class="fa fa-angle-right"></i>',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type' => 'plain'
      ));
      if($wp_query->max_num_pages == '1' ) {} else {echo '';}
    }
    echo '</div></nav></div>';
  }
}

/**
 * Default Menu Fallback
 */
if ( ! function_exists( 'zipprich_menu_fallback' ) ) {
  function zipprich_menu_fallback($args) {
    if ( ! current_user_can( 'manage_options' ) )
    {
        return;
    }

    // see wp-includes/nav-menu-template.php for available arguments
    extract( $args );

    $link = $link_before
        . '<a href="' .admin_url( 'nav-menus.php' ) . '">' . $before . esc_html__( 'Add a menu', 'zipprich' ) . $after . '</a>'
        . $link_after;

    // We have a list
    if ( FALSE !== stripos( $items_wrap, '<ul' )
        or FALSE !== stripos( $items_wrap, '<ol' )
    )
    {
        $link = "<li>".$link."</li>";
    }

    $output = sprintf( $items_wrap, $menu_id, $menu_class, $link );
    if ( ! empty ( $container ) )
    {
      $output  = '<'.$container.' class="'.esc_attr($container_class).'" id="'.esc_attr($container_id).'">'.$output.'</'.$container.'>';
    }

    if ( $echo ) {
      echo $output;
    }

    return $output;
  }
}

/**
 * Woocommerce Fixes
 */
// Dispaly product in shop page loop
if(!function_exists('zipprich_new_loop_shop_per_page')){
  function zipprich_new_loop_shop_per_page( $cols ) {
    $limit = cs_get_option('theme_woo_limit');
    if (empty($limit)) {
      $cols = 9;
    } else {
      $cols = $limit;
    }
    return $cols;
  }
  add_filter( 'loop_shop_per_page', 'zipprich_new_loop_shop_per_page', 20 );
}

// Remove the product rating display on product loops
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

// Added readmore link to display on product loops
if(!function_exists('zipprich_woocommerce_readmore_function')){
  function zipprich_woocommerce_readmore_function(){
    $details_text = cs_get_option('details_text');
    $details_text = $details_text ? $details_text : esc_html__( 'Details', 'zipprich' );
    echo '<a class="ziph-producDetails_btn" href="'. esc_url(get_the_permalink()) .'">'. esc_attr($details_text) .'</a>';
  }
  add_action('zipprich_woocommerce_readmore', 'zipprich_woocommerce_readmore_function');
}

// Change add to cart text (Theme Option)
if(!function_exists('zipprich_add_to_cart_text')){
  function zipprich_add_to_cart_text(){
    $add_to_cart_text = cs_get_option('add_to_cart_text');
    if ($add_to_cart_text) {
      add_filter( 'woocommerce_product_add_to_cart_text', function(){return cs_get_option('add_to_cart_text');} );    // 2.1 +
      add_filter( 'woocommerce_product_single_add_to_cart_text', function(){return cs_get_option('add_to_cart_text');} );    // 2.1 +
    }
  }
  add_action( 'init', 'zipprich_add_to_cart_text' );
}

// Single Product Image Lightbox Active
if(!function_exists('zipprich_single_product_gallery_image')){
  function zipprich_single_product_gallery_image() {
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
  }
  add_action( 'after_setup_theme', 'zipprich_single_product_gallery_image' );
}

// Price and Description Adjustment
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

// Disable Options
if(!function_exists('zipprich_woo_disable_options')){
  function zipprich_woo_disable_options(){

    $woo_single_upsell = cs_get_option('woo_single_upsell');
    if ($woo_single_upsell) {
      remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
    }

    $woo_single_related = cs_get_option('woo_single_related');
    if ($woo_single_related) {
      remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
    }

  }
  add_action( 'init', 'zipprich_woo_disable_options' );
}

/* Related Product Limit */
if(!function_exists('zipprich_related_product_limit')){
  function zipprich_related_product_limit(){

    $woo_related_limit = cs_get_option('woo_related_limit');
    $woo_related_limit = ($woo_related_limit) ? $woo_related_limit : 3;
    $args['posts_per_page'] = esc_attr($woo_related_limit);
    $args['columns'] = 3;
    return $args;

  }
  add_filter( 'woocommerce_output_related_products_args', 'zipprich_related_product_limit', 20 );
}

// Added Ex Tax text after price in single product page
if(!function_exists('zipprich_woocommerce_tax_text_function')){
  function zipprich_woocommerce_tax_text_function($price){
    if(is_product()) {
      $tax_display = get_post_meta( get_the_ID(), '_tax_status', true );
      $zipprich_calc_taxes = get_option( 'woocommerce_calc_taxes' );
      $zipprich_prices_include_tax = get_option( 'woocommerce_prices_include_tax' );
      if($zipprich_calc_taxes == 'yes' && 'taxable' === $tax_display ) {
        if($zipprich_prices_include_tax == 'yes') {
          return $price . ' <span class="ziph_tax_label">' . WC()->countries->ex_tax_or_vat() . '</span>';
        }
      }
      return $price;
    } else {
      return $price;
    }
  }
  add_filter('woocommerce_get_price_html', 'zipprich_woocommerce_tax_text_function');
}

// Define image sizes
add_theme_support( 'woocommerce', array(
  'thumbnail_image_width' => 515,
  'single_image_width' => 400,
  ) );

update_option( 'woocommerce_thumbnail_cropping', '1:1' );