<?php
/*
 * Zipprich Theme's Functions
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

/**
 * Define - Folder Paths
 */
define( 'ZIPPRICH_THEMEROOT_PATH', get_template_directory() );
define( 'ZIPPRICH_THEMEROOT_URI', get_template_directory_uri() );
define( 'ZIPPRICH_CSS', ZIPPRICH_THEMEROOT_URI . '/assets/css' );
define( 'ZIPPRICH_IMAGES', ZIPPRICH_THEMEROOT_URI . '/assets/images' );
define( 'ZIPPRICH_SCRIPTS', ZIPPRICH_THEMEROOT_URI . '/assets/js' );
define( 'ZIPPRICH_FRAMEWORK', get_template_directory() . '/inc' );
define( 'ZIPPRICH_LAYOUT', get_template_directory() . '/layouts' );
define( 'ZIPPRICH_CS_IMAGES', ZIPPRICH_THEMEROOT_URI . '/inc/theme-options/theme-extend/images' );
define( 'ZIPPRICH_CS_FRAMEWORK', get_template_directory() . '/inc/theme-options/theme-extend' ); // Called in Icons field *.json
define( 'ZIPPRICH_ADMIN_PATH', get_template_directory() . '/inc/theme-options/cs-framework' ); // Called in Icons field *.json

/**
 * Define - Global Theme Info's
 */
if (is_child_theme()) { // If Child Theme Active
	$zipprich_theme_child = wp_get_theme();
	$zipprich_get_parent = $zipprich_theme_child->Template;
	$zipprich_theme = wp_get_theme($zipprich_get_parent);
} else { // Parent Theme Active
	$zipprich_theme = wp_get_theme();
}
define('ZIPPRICH_NAME', $zipprich_theme->get( 'Name' ), true);
define('ZIPPRICH_VERSION', $zipprich_theme->get( 'Version' ), true);
define('ZIPPRICH_BRAND_URL', $zipprich_theme->get( 'AuthorURI' ), true);
define('ZIPPRICH_BRAND_NAME', $zipprich_theme->get( 'Author' ), true);

/**
 * All Main Files Include
 */
require_once( ZIPPRICH_FRAMEWORK . '/init.php' );

/*  Custom Field for Categories.
    ======================================== */
// Add new term page
function zipprich_add_meta_fields( $taxonomy ) { ?>
    <div class="form-field term-image-wrap">
        <label for="image_link"><?php esc_html_e( 'Image Link', 'zipprich' ) ?></label>
        <input type="text" id="image_link" name="image_link" value="" />
    </div>
    <div class="form-field term-fb-wrap">
        <label for="fb_link"><?php esc_html_e( 'Facebook Link', 'zipprich' ) ?></label>
        <input type="text" id="fb_link" name="fb_link" value="" />
    </div>
    <?php
}
add_action( 'category_add_form_fields', 'zipprich_add_meta_fields', 10, 2 );

// Edit term page
function zipprich_edit_meta_fields( $term, $taxonomy ) {
    $image_link = get_term_meta( $term->term_id, 'image_link', true );
    $fb_link = get_term_meta( $term->term_id, 'fb_link', true ); ?>

    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="image_link"><?php esc_html_e( 'Image Link', 'zipprich' ) ?></label>
        </th>
        <td>
            <input type="text" id="image_link" name="image_link" value="<?php echo esc_url($image_link); ?>">
        </td>
    </tr>

    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="fb_link"><?php esc_html_e( 'Facebook Link', 'zipprich' ) ?></label>
        </th>
        <td>
            <input type="text" id="fb_link" name="fb_link" value="<?php echo esc_url($fb_link); ?>">
        </td>
    </tr>
    <?php
}
add_action( 'category_edit_form_fields', 'zipprich_edit_meta_fields', 10, 2 );

// Save custom meta
function zipprich_save_taxonomy_meta( $term_id, $tag_id ) {
    if ( isset( $_POST[ 'image_link' ] ) ) {
        update_term_meta( $term_id, 'image_link', esc_url($_POST[ 'image_link' ]) );
        update_term_meta( $term_id, 'fb_link', esc_url($_POST[ 'fb_link' ]) );
    } else {
        update_term_meta( $term_id, 'image_link', '' );
        update_term_meta( $term_id, 'fb_link', '' );
    }
}
add_action( 'created_category', 'zipprich_save_taxonomy_meta', 10, 2 );
add_action( 'edited_category', 'zipprich_save_taxonomy_meta', 10, 2 );