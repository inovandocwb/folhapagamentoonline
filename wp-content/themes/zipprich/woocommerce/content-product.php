<?php
/**
 * VictorTheme Custom Changes
 * Added style if user set custom column height from theme options (line 40)
 * Added "div" tag with class "ziph-product_warp" (line 41)
 * Added "div" tag with class "ziph-productBtns_warp" (line 80)
 * Added custom hook "zipprich_woocommerce_readmore" (line 74). Function referance "zipprich_woocommerce_readmore_function" @frontend-functions.php
 */

/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
// Custom Changes
$woo_align_height = cs_get_option('theme_align_height');
$woo_align_height = $woo_align_height ? ' style="min-height: '.$woo_align_height.';"' : '';
?>
<li <?php wc_product_class(); ?>>
	<!-- Custom Changes - Div Added -->
	<div class="ziph-product_warp" <?php echo $woo_align_height; ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/** Custom Changes - Added 'woocommerce_after_shop_loop_item' within ziph-productBtns_warp. And Added entire "ziph-productBtns_warp" div
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	?>
		<div class="ziph-productBtns_warp">
			<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			<?php do_action( 'zipprich_woocommerce_readmore' ); ?>
		</div>
	</div>
</li>
