<?php
/**
 * VictorTheme Custom Changes 
 * Added custom theme option settings in pagination function args (line 48 - 50)
 */
/**
 * Pagination - Show numbered pagination for catalog pages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/pagination.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$total   = isset( $total ) ? $total : wc_get_loop_prop( 'total_pages' );
$current = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' );
$base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) );
$format  = isset( $format ) ? $format : '';

if ( $total <= 1 ) {
	return;
}
// Theme Option
$older_post = cs_get_option('older_post');
$newer_post = cs_get_option('newer_post');
$older_post = $older_post ? $older_post : esc_html__( 'Prev', 'zipprich' );
$newer_post = $newer_post ? $newer_post : esc_html__( 'Next', 'zipprich' );
?>
<div class="text-center ziph-postPagination_warp">
	<nav class="woocommerce-pagination navigation pagination">
		<div class="nav-links">
			<?php
				echo paginate_links( apply_filters( 'woocommerce_pagination_args', array(
					'base'         => $base,
					'format'       => $format,
					'add_args'     => false,
					'current'      => max( 1, get_query_var( 'paged' ) ),
					'total'        => $total,
		        	'prev_text' => '<i class="fa fa-angle-left"></i>' . $older_post,
		        	'next_text' => $newer_post . '<i class="fa fa-angle-right"></i>',
					'type'         => 'plain',
					'end_size'     => 3,
					'mid_size'     => 3,
				) ) );
			?>
		</div>
	</nav>
</div>