<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" class="ziph-wigtsrch_form" >
	<input type="text" name="s" id="s" class="ziph-wigtsrch_field" placeholder="<?php esc_attr_e('Search','zipprich'); ?>" />
	<button type="submit" id="searchsubmit" class="ziph-wigtsrch_submit"></button>
</form>