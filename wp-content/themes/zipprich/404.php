<?php
/*
 * The template for displaying 404 pages (not found).
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

// Theme Options
$zipprich_error_heading = cs_get_option('error_heading');
$zipprich_error_subheading = cs_get_option('error_subheading');
$zipprich_error_page_content = cs_get_option('error_page_content');
$zipprich_error_btn_text = cs_get_option('error_btn_text');

$zipprich_error_heading = ( $zipprich_error_heading ) ? $zipprich_error_heading : esc_html__( '404', 'zipprich' );
$zipprich_error_subheading = ( $zipprich_error_subheading ) ? $zipprich_error_subheading : '<span class="brand-color">'. esc_html__('Oops!', 'zipprich') .'</span>' . esc_html__( ' Page Not Found!', 'zipprich' );
$zipprich_error_page_content = ( $zipprich_error_page_content ) ? $zipprich_error_page_content : esc_html__( 'You may have typed the address incorrectly or you may have used an outdated link. Try search our site.', 'zipprich' );
$zipprich_error_btn_text = ( $zipprich_error_btn_text ) ? $zipprich_error_btn_text : esc_html__( 'Go to Home', 'zipprich' );

get_header(); ?>

	<!-- Content -->
	<div class="text-center ziph-404Page_warp">
		<h1><?php echo esc_attr($zipprich_error_heading); ?></h1>
		<h2><?php echo $zipprich_error_subheading; ?></h2>
		<p><?php echo $zipprich_error_page_content; ?></p>
		<a href="<?php echo esc_url(home_url( '/' )); ?>" class="btn ziph-btn ziph-404_btn"><?php echo esc_attr($zipprich_error_btn_text); ?></a>
	</div>
	<!-- Content -->

<?php
get_footer();