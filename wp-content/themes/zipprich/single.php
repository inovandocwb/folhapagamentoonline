<?php
/*
 * The template for displaying all single posts.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */
get_header();

// Metabox
$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

if ($zipprich_meta) {
	$zipprich_content_padding = $zipprich_meta['content_spacings'];
} else { $zipprich_content_padding = ''; }
// Padding - Metabox
if ($zipprich_content_padding && $zipprich_content_padding !== 'padding-none') {
	$zipprich_content_top_spacings = $zipprich_meta['content_top_spacings'];
	$zipprich_content_bottom_spacings = $zipprich_meta['content_bottom_spacings'];
	if ($zipprich_content_padding === 'padding-custom') {
		$zipprich_content_top_spacings = $zipprich_content_top_spacings ? 'padding-top:'. zipprich_check_px($zipprich_content_top_spacings) .';' : '';
		$zipprich_content_bottom_spacings = $zipprich_content_bottom_spacings ? 'padding-bottom:'. zipprich_check_px($zipprich_content_bottom_spacings) .';' : '';
		$zipprich_custom_padding = $zipprich_content_top_spacings . $zipprich_content_bottom_spacings;
	} else {
		$zipprich_custom_padding = '';
	}
} else {
	$zipprich_custom_padding = '';
}

// Theme Options
$zipprich_sidebar_position = cs_get_option('single_sidebar_position');
$zipprich_single_comment = cs_get_option('single_comment_form');

// Sidebar Position
if ($zipprich_sidebar_position === 'sidebar-hide') {
	$layout_class = 'row';
	$layout_col_class = 'ziph-col';
	$zipprich_sidebar_class = 'ziph-page_content ';
} else {
	if(is_active_sidebar( 'sidebar-1' )) {
		$layout_class = 'ziph-fix ziph-flt_left ziph-leftContent_warp ';
		$layout_col_class = 'ziph-col';
		$zipprich_sidebar_class = 'ziph-pageWite_sidebar ';
	} else {
		$layout_class = 'row';
		$layout_col_class = 'col-md-12';
		$zipprich_sidebar_class = 'ziph-page_content ';
	}
}
?>

<div class="<?php echo esc_attr($zipprich_content_padding .' '. $zipprich_sidebar_class); ?>" style="<?php echo esc_attr($zipprich_custom_padding); ?>">
	<div class="ziph-fix container">
		<div class="<?php echo esc_attr($layout_class); ?>">
			<div class="<?php echo esc_attr($layout_col_class); ?>">
			<?php
				if ( have_posts() ) :
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						get_template_part( 'layouts/post/content', 'single' );
						echo zipprich_author_info();
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					endwhile;
				else :
					get_template_part( 'layouts/post/content', 'none' );
				endif;

	    		wp_reset_postdata();  // avoid errors further down the page
			?>
			</div>
		</div><!-- Content Area -->

		<?php
			if ($zipprich_sidebar_position !== 'sidebar-hide') {
				get_sidebar(); // Sidebar
			}
		?>

	</div>
</div>

<?php
get_footer();