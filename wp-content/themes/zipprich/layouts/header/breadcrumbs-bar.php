<?php

	$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
	$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
	$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
	$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

	if ($zipprich_meta && is_page()) {
		$zipprich_hide_breadcrumbs  = $zipprich_meta['hide_breadcrumbs'];
	} else { $zipprich_hide_breadcrumbs = ''; }

	if (!$zipprich_hide_breadcrumbs) { // Hide Breadcrumbs
		if ( function_exists( 'breadcrumb_trail' ) ) breadcrumb_trail();
	} // Hide Breadcrumbs

?>