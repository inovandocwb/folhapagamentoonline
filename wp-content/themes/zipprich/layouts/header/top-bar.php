<?php
// Metabox
global $post;
$zipprich_id    = ( isset( $post ) ) ? $post->ID : false;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $zipprich_id : false;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

// Header Style
if ($zipprich_meta) {
  $zipprich_header_design  = $zipprich_meta['select_header_design'];
  $zipprich_search_icon    = $zipprich_meta['search_icon'];
} else {
  $zipprich_header_design  = cs_get_option('select_header_design');
  $zipprich_search_icon    = cs_get_option('search_icon');
}
if ($zipprich_header_design === 'default') {
  $zipprich_header_design_actual  = cs_get_option('select_header_design');
} else {
  $zipprich_header_design_actual = ( $zipprich_header_design ) ? $zipprich_header_design : cs_get_option('select_header_design');
}

// Top bar
if ($zipprich_meta) {
  $zipprich_topbar_options = $zipprich_meta['topbar_options'];
} else {
  $zipprich_topbar_options = '';
}
// Define Theme Options and Metabox varials in right way!
if ($zipprich_meta) {
  if ($zipprich_topbar_options === 'custom' && $zipprich_topbar_options !== 'default') {
    $zipprich_top_left           = $zipprich_meta['top_left'];
    $zipprich_top_right          = $zipprich_meta['top_right'];
    $zipprich_topbar_left_width  = $zipprich_meta['topbar_left_width'];
    $zipprich_topbar_right_width = $zipprich_meta['topbar_right_width'];
    $zipprich_left_width         = $zipprich_topbar_left_width ? 'width:'. $zipprich_topbar_left_width .';' : '';
    $zipprich_right_width        = $zipprich_topbar_right_width ? 'width:'. $zipprich_topbar_right_width .';' : '';
    $zipprich_hide_topbar        = $zipprich_topbar_options;
    $zipprich_topbar_bg          = $zipprich_meta['topbar_bg'];
    $zipprich_topbar_border      = $zipprich_meta['topbar_border'];
    if ($zipprich_topbar_bg) {
      $zipprich_topbar_bg = 'background-color: '. $zipprich_topbar_bg .';';
      $zipprich_topbar_border = 'border-bottom-color: '. $zipprich_topbar_border .';';
    } else {$zipprich_topbar_bg = '';$zipprich_topbar_border = '';}
  } else {
    $zipprich_top_left           = cs_get_option('top_left');
    $zipprich_top_right          = cs_get_option('top_right');
    $zipprich_topbar_left_width  = cs_get_option('topbar_left_width');
    $zipprich_topbar_right_width = cs_get_option('topbar_right_width');
    $zipprich_left_width         = $zipprich_topbar_left_width ? 'width:'. $zipprich_topbar_left_width .';' : '';
    $zipprich_right_width        = $zipprich_topbar_right_width ? 'width:'. $zipprich_topbar_right_width .';' : '';
    $zipprich_hide_topbar        = cs_get_option('top_bar');
    $zipprich_topbar_bg          = '';
    $zipprich_topbar_border      = '';
  }
} else {
  // Theme Options fields
  $zipprich_top_left           = cs_get_option('top_left');
  $zipprich_top_right          = cs_get_option('top_right');
  $zipprich_topbar_left_width  = cs_get_option('topbar_left_width');
  $zipprich_topbar_right_width = cs_get_option('topbar_right_width');
  $zipprich_left_width         = $zipprich_topbar_left_width ? 'width:'. $zipprich_topbar_left_width .';' : '';
  $zipprich_right_width        = $zipprich_topbar_right_width ? 'width:'. $zipprich_topbar_right_width .';' : '';
  $zipprich_hide_topbar        = cs_get_option('top_bar');
  $zipprich_topbar_bg          = '';
  $zipprich_topbar_border      = '';
}
// All options
if ($zipprich_meta && $zipprich_topbar_options === 'custom' && $zipprich_topbar_options !== 'default') {
  $zipprich_top_left = ( $zipprich_top_left ) ? $zipprich_meta['top_left'] : cs_get_option('top_left');
  $zipprich_top_right = ( $zipprich_top_right ) ? $zipprich_meta['top_right'] : cs_get_option('top_right');
} else {
  $zipprich_top_left = cs_get_option('top_left');
  $zipprich_top_right = cs_get_option('top_right');
}
if ($zipprich_meta && $zipprich_topbar_options !== 'default') {
  if ($zipprich_topbar_options === 'hide_topbar') {
    $zipprich_hide_topbar = 'hide';
  } else {
    $zipprich_hide_topbar = 'show';
  }
} else {
  $zipprich_hide_topbar_check = cs_get_option('top_bar');
  if ($zipprich_hide_topbar_check === true ) {
    $zipprich_hide_topbar = 'hide';
  } else {
    $zipprich_hide_topbar = 'show';
  }
}
if ($zipprich_meta) {
  $zipprich_topbar_bg = ( $zipprich_topbar_bg ) ? $zipprich_meta['topbar_bg'] : '';
  $zipprich_topbar_border = ( $zipprich_topbar_border ) ? $zipprich_meta['topbar_border'] : '';
} else {
  $zipprich_topbar_bg = '';
  $zipprich_topbar_border = '';
}

if ($zipprich_topbar_bg) {
  $zipprich_topbar_bg = 'background-color: '. $zipprich_topbar_bg .';';
  $zipprich_topbar_border = 'border-bottom-color: '. $zipprich_topbar_border .';';
} else {$zipprich_topbar_bg = ''; $zipprich_topbar_border = '';}

$zipprich_topbar_left_width = ( $zipprich_topbar_left_width ) ? $zipprich_meta['topbar_left_width'] : cs_get_option('topbar_left_width');
$zipprich_topbar_right_width = ( $zipprich_topbar_right_width ) ? $zipprich_meta['topbar_right_width'] : cs_get_option('topbar_right_width');
$zipprich_left_width   = $zipprich_topbar_left_width ? 'width:'. $zipprich_topbar_left_width .';' : '';
$zipprich_right_width  = $zipprich_topbar_right_width ? 'width:'. $zipprich_topbar_right_width .';' : '';
if($zipprich_hide_topbar === 'show') {
  if(!empty($zipprich_top_left) || !empty($zipprich_top_right)) {
?>
<!-- header top start\-->
<div class="ziph-header_top" style="<?php echo esc_attr($zipprich_topbar_bg . $zipprich_topbar_border); ?>">
  <div class="container">
    <div class="row">
      <div class="col-sm-4" style="<?php echo esc_attr($zipprich_left_width); ?>">
          <?php echo do_shortcode($zipprich_top_left); ?>
      </div>
      <div class="ziph-fix col-sm-8" style="<?php echo esc_attr($zipprich_right_width); ?>">
        <?php echo do_shortcode($zipprich_top_right); ?>
      </div>
    </div>
  </div>
</div><!--/ header top end-->
<?php } } ?>