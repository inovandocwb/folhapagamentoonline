<?php
// Logo Image
$zipprich_brand_logo_default = cs_get_option('brand_logo_default');
$zipprich_brand_logo_retina = cs_get_option('brand_logo_retina');

// Retina Size
$zipprich_retina_width = cs_get_option('retina_width');
$zipprich_retina_height = cs_get_option('retina_height');

// Logo Spacings
$zipprich_brand_logo_top = cs_get_option('brand_logo_top');
$zipprich_brand_logo_bottom = cs_get_option('brand_logo_bottom');
if ($zipprich_brand_logo_top !== '') {
	$zipprich_brand_logo_top = 'padding-top:'. zipprich_check_px($zipprich_brand_logo_top) .';';
} else { $zipprich_brand_logo_top = ''; }
if ($zipprich_brand_logo_bottom !== '') {
	$zipprich_brand_logo_bottom = 'padding-bottom:'. zipprich_check_px($zipprich_brand_logo_bottom) .';';
} else { $zipprich_brand_logo_bottom = ''; }

$width = $zipprich_retina_width ? 'width="' . $zipprich_retina_width .'"' : '';
$height = $zipprich_retina_height ? 'height="' . $zipprich_retina_height .'"' : '';
?>
<div class="ziph-logo" style="<?php echo esc_attr($zipprich_brand_logo_top); echo esc_attr($zipprich_brand_logo_bottom); ?>">
	<a href="<?php echo esc_url(home_url( '/' )); ?>">
	<?php
		if (isset($zipprich_brand_logo_default)){
			if ($zipprich_brand_logo_retina){
				$zipprich_brand_logo_retina_alt = get_post_meta($zipprich_brand_logo_retina, '_wp_attachment_image_alt', true);
				echo '<img src="'. esc_url(wp_get_attachment_url($zipprich_brand_logo_retina)) .'" '. $width .' '. $height .' alt="'. $zipprich_brand_logo_retina_alt .'" class="retina-logo">
					';
			}
			$zipprich_brand_logo_default_alt = get_post_meta($zipprich_brand_logo_default, '_wp_attachment_image_alt', true);
			echo '<img src="'. esc_url(wp_get_attachment_url($zipprich_brand_logo_default)) .'" alt="'. $zipprich_brand_logo_default_alt .'" class="default-logo" '. $width .' '. $height .'>';
		} else {
			echo '<div class="text-logo">'. esc_attr(get_bloginfo( 'name' )) . '</div>';
		}
	?>
	</a>
</div>