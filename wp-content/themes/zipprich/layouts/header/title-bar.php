<?php
// Metabox
$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );
if ($zipprich_meta && is_page()) {
	$zipprich_title_bar_padding = $zipprich_meta['title_area_spacings'];
} else { $zipprich_title_bar_padding = ''; }
// Padding - Theme Options
if ($zipprich_title_bar_padding && $zipprich_title_bar_padding !== 'padding-none') {
	$zipprich_title_top_spacings = $zipprich_meta['title_top_spacings'];
	$zipprich_title_bottom_spacings = $zipprich_meta['title_bottom_spacings'];
	if ($zipprich_title_bar_padding === 'padding-custom') {
		$zipprich_title_top_spacings = $zipprich_title_top_spacings ? 'padding-top:'. zipprich_check_px($zipprich_title_top_spacings) .';' : '';
		$zipprich_title_bottom_spacings = $zipprich_title_bottom_spacings ? 'padding-bottom:'. zipprich_check_px($zipprich_title_bottom_spacings) .';' : '';
		$zipprich_custom_padding = $zipprich_title_top_spacings . $zipprich_title_bottom_spacings;
	} else {
		$zipprich_custom_padding = '';
	}
} else {
	$zipprich_title_bar_padding = cs_get_option('title_bar_padding');
	$zipprich_titlebar_top_padding = cs_get_option('titlebar_top_padding');
	$zipprich_titlebar_bottom_padding = cs_get_option('titlebar_bottom_padding');
	if ($zipprich_title_bar_padding === 'padding-custom') {
		$zipprich_titlebar_top_padding = $zipprich_titlebar_top_padding ? 'padding-top:'. zipprich_check_px($zipprich_titlebar_top_padding) .';' : '';
		$zipprich_titlebar_bottom_padding = $zipprich_titlebar_bottom_padding ? 'padding-bottom:'. zipprich_check_px($zipprich_titlebar_bottom_padding) .';' : '';
		$zipprich_custom_padding = $zipprich_titlebar_top_padding . $zipprich_titlebar_bottom_padding;
	} else {
		$zipprich_custom_padding = '';
	}
}

// Banner Type - Meta Box
if ($zipprich_meta) {
	$zipprich_banner_type = $zipprich_meta['banner_type'];
} else { $zipprich_banner_type = ''; }

// Custom Title - Meta Box
if ($zipprich_meta && is_page()) {
	$zipprich_page_custom_title = $zipprich_meta['page_custom_title'];
} else { $zipprich_page_custom_title = ''; }

// Overlay Color - Theme Options
if ($zipprich_meta && is_page()) {
	$zipprich_bg_overlay_color = $zipprich_meta['titlebar_bg_overlay_color'];
} else { $zipprich_bg_overlay_color = ''; }
if ($zipprich_bg_overlay_color) {
	if ($zipprich_bg_overlay_color) {
		$zipprich_overlay_color = 'style="background-color: '. $zipprich_bg_overlay_color .'"';
	} else {
		$zipprich_overlay_color = '';
	}
} else {
	$zipprich_bg_overlay_color = cs_get_option('titlebar_bg_overlay_color');
	if ($zipprich_bg_overlay_color) {
		$zipprich_overlay_color = 'style="background-color: '. $zipprich_bg_overlay_color .'"';
	} else {
		$zipprich_overlay_color = '';
	}
}

// Background - Type
if( $zipprich_meta && count(array_filter($zipprich_meta['title_area_bg'])) != 0 ) {

  extract( $zipprich_meta['title_area_bg'] );

  $zipprich_background_image       = ( ! empty( $image ) ) ? 'background-image: url(' . $image . ');' : '';
  $zipprich_background_repeat      = ( ! empty( $image ) && ! empty( $repeat ) ) ? ' background-repeat: ' . $repeat . ';' : '';
  $zipprich_background_position    = ( ! empty( $image ) && ! empty( $position ) ) ? ' background-position: ' . $position . ';' : '';
  $zipprich_background_size    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-size: ' . $size . ';' : '';
  $zipprich_background_attachment    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-attachment: ' . $attachment . ';' : '';
  $zipprich_background_color       = ( ! empty( $color ) ) ? ' background-color: ' . $color . ';' : '';
  $zipprich_background_style       = ( ! empty( $image ) ) ? $zipprich_background_image . $zipprich_background_repeat . $zipprich_background_position . $zipprich_background_size . $zipprich_background_attachment : '';

  $zipprich_title_bg = ( ! empty( $zipprich_background_style ) || ! empty( $zipprich_background_color ) ) ? $zipprich_background_style . $zipprich_background_color : '';

  $titlebar_bg_overlay_color = $zipprich_meta['titlebar_bg_overlay_color'];
  $titlebar_bg_overlay_color = ($titlebar_bg_overlay_color) ? 'background-color:' . $titlebar_bg_overlay_color : '';

} else {

	$zipprich_titlebar_bg = cs_get_option('titlebar_bg');
	if(isset($zipprich_titlebar_bg)){
		extract( $zipprich_titlebar_bg );

		$zipprich_background_image       = ( ! empty( $image ) ) ? 'background-image: url(' . $image . ');' : '';
		$zipprich_background_repeat      = ( ! empty( $image ) && ! empty( $repeat ) ) ? ' background-repeat: ' . $repeat . ';' : '';
		$zipprich_background_position    = ( ! empty( $image ) && ! empty( $position ) ) ? ' background-position: ' . $position . ';' : '';
		$zipprich_background_size    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-size: ' . $size . ';' : '';
		$zipprich_background_attachment    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-attachment: ' . $attachment . ';' : '';
		$zipprich_background_color       = ( ! empty( $color ) ) ? ' background-color: ' . $color . ';' : '';
		$zipprich_background_style       = ( ! empty( $image ) ) ? $zipprich_background_image . $zipprich_background_repeat . $zipprich_background_position . $zipprich_background_size . $zipprich_background_attachment : '';

		$zipprich_title_bg = ( ! empty( $zipprich_background_style ) || ! empty( $zipprich_background_color ) ) ? $zipprich_background_style . $zipprich_background_color : '';

		$titlebar_bg_overlay_color = cs_get_option('titlebar_bg_overlay_color');
		$titlebar_bg_overlay_color = ($titlebar_bg_overlay_color) ? 'background-color:' . $titlebar_bg_overlay_color : '';
	} else {
		$titlebar_bg_overlay_color = '';
		$zipprich_title_bg = '';
	}

}

if($zipprich_banner_type === 'hide-title-area') { // Hide Title Area
} elseif($zipprich_meta && $zipprich_banner_type === 'revolution-slider') { // Hide Title Area
	echo do_shortcode($zipprich_meta['page_revslider']);
} else { ?>
<!-- ==== Page banner start==== \-->
<section class="ziph-pagebanner_height-200 <?php echo esc_attr($zipprich_title_bar_padding .' '. $zipprich_banner_type); ?>" style="<?php echo esc_attr($zipprich_custom_padding . $zipprich_title_bg); ?>">
    <div class="ziph-banneroverly_opc-80" style="<?php echo esc_attr($titlebar_bg_overlay_color); ?>"></div>
    <div class="ziph-vtl_mdl container">
        <div class="row">
            <div class="text-left col-sm-6 ">
                <?php if(!empty($zipprich_page_custom_title)){ ?>
				<h2 class="text-uppercase ziph-page_title"><?php echo esc_attr( $zipprich_page_custom_title ); ?></h2>
                <?php } else { ?>
                <h2 class="text-uppercase ziph-page_title"><?php echo zipprich_title_area(); ?></h2>
                <?php } ?>
            </div>
            <div class="text-right col-sm-6">
                <?php get_template_part( 'layouts/header/breadcrumbs', 'bar' ); // Breadcrumbs ?>
            </div>
        </div>
    </div>
</section>
<!--/ ==== Page banner end====-->
<?php } ?>