<?php
// Metabox
$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $zipprich_id : false;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

// Header Style - Theme Options & Metabox
if ($zipprich_meta) {
  $zipprich_header_design  = $zipprich_meta['select_header_design'];
} else {
  $zipprich_header_design  = cs_get_option('select_header_design');
}

$zipprich_mobile_breakpoint = cs_get_option('mobile_breakpoint');
$zipprich_breakpoint = $zipprich_mobile_breakpoint ? $zipprich_mobile_breakpoint : '767';

?>

    <!-- Navigation Start -->
    <?php
      if ($zipprich_meta) {
        $zipprich_choose_menu = $zipprich_meta['choose_menu'];
      } else { $zipprich_choose_menu = ''; }
      $zipprich_choose_menu = $zipprich_choose_menu ? $zipprich_choose_menu : '';

      // Mobil menu area start
      echo '<div class="hidden-md hidden-lg ziph-mobil_menu_warp" data-starts="'. esc_attr($zipprich_breakpoint) .'">';
      wp_nav_menu(
        array(
          'menu'              => 'primary',
          'theme_location'    => 'primary',
          'container'         => '',
          'container_class'   => '',
          'container_id'      => '',
          'menu'              => $zipprich_choose_menu,
          'menu_id'           => '',
          'menu_class'        => 'ziph-mobil_menu slimmenu',
          'walker'            => new Zipprich_Navwalker(),
          'fallback_cb'       => 'zipprich_menu_fallback'
        )
      );
      echo '</div>'; // Mobil menu area end

      // Mainmenu  start
      wp_nav_menu(
        array(
          'menu'              => 'primary',
          'theme_location'    => 'primary',
          'container'         => 'nav',
          'container_class'   => 'visible-md visible-lg text-right ziph-mainmenu',
          'container_id'      => '',
          'menu'              => $zipprich_choose_menu,
          'menu_class'        => 'list-inline',
          'walker'            => new Zipprich_Navwalker(),
          'fallback_cb'       => 'zipprich_menu_fallback'
        )
      ); // mainmenu  end
    ?>
    <!-- End -->