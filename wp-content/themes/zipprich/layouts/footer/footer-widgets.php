<?php if (is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' )) { ?>
<!-- Footer Widgets -->
<div class="ziph-footer_top">
	<div class="container">
		<div class="row ziph-footer_widgets">
			<?php
				$footer_widgets = cs_get_option( 'footer_widget_layout' );

				if($footer_widgets != 'theme-default') {
					echo zipprich_vt_footer_widgets();
				} else {
			?>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<?php
							if (is_active_sidebar( 'footer-1' )) {
								dynamic_sidebar( 'footer-1' );
							}
						?>
					</div>
					<div class="col-md-4 col-sm-6">
						<?php
							if (is_active_sidebar( 'footer-2' )) {
								dynamic_sidebar( 'footer-2' );
							}
						?>
					</div>
					<div class="col-md-4 col-sm-6">
						<?php
							if (is_active_sidebar( 'footer-3' )) {
								dynamic_sidebar( 'footer-3' );
							}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<?php
					if (is_active_sidebar( 'footer-4' )) {
						dynamic_sidebar( 'footer-4' );
					}
				?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- Footer Widgets -->
<?php } ?>