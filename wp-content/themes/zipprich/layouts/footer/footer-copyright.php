<?php
	// Main Text
	$zipprich_need_copyright = cs_get_option('need_copyright');
	$zipprich_footer_copyright_layout = cs_get_option('footer_copyright_layout');

	if ($zipprich_footer_copyright_layout === 'copyright-1') {
		$zipprich_copyright_layout_class = 'col-sm-4';
		$zipprich_copyright_seclayout_class = 'text-right col-sm-8';
	} elseif ($zipprich_footer_copyright_layout === 'copyright-2') {
		$zipprich_copyright_layout_class = 'col-sm-6';
		$zipprich_copyright_seclayout_class = 'text-right col-sm-6';
	} elseif ($zipprich_footer_copyright_layout === 'copyright-3') {
		$zipprich_copyright_layout_class = 'col-sm-12 text-center';
		$zipprich_copyright_seclayout_class = '';
	} else {
		$zipprich_copyright_layout_class = '';
		$zipprich_copyright_seclayout_class = '';
	}

	if (isset($zipprich_need_copyright)) {
?>
<!--footer bottom start\-->
<div class="ziph-footer_bottom">
	<div class="container">
		<div class="row">
			<div class="<?php echo esc_attr($zipprich_copyright_layout_class); ?>">
				<?php
					$zipprich_copyright_text = cs_get_option('copyright_text');
					echo do_shortcode($zipprich_copyright_text);
				?>
			</div>
			<?php if ($zipprich_footer_copyright_layout != 'copyright-3') { ?>
			<div class="<?php echo esc_attr($zipprich_copyright_seclayout_class); ?>">
				<p class="ziph-copyright">
				<?php
					$zipprich_secondary_text = cs_get_option('secondary_text');
					if($zipprich_secondary_text){
						echo do_shortcode($zipprich_secondary_text);
					} else { ?>
						&copy; <a href="https://victorthemes.com/">Victorthemes</a> - <?php esc_html_e('Elite ThemeForest Author', 'zipprich');
					}
				?>
				</p>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<!--/ footer bottom end-->
<?php }