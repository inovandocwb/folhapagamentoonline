<?php
/**
 * Single Post.
 */
$zipprich_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$zipprich_large_image = $zipprich_large_image[0];

$zipprich_post_type = get_post_meta( get_the_ID(), 'post_type_metabox', true );
$zipprich_blog_style = cs_get_option('blog_listing_style');

// Single Theme Option
$zipprich_single_featured_image = cs_get_option('single_featured_image');
$zipprich_single_author_info = cs_get_option('single_author_info');
$zipprich_single_share_option = cs_get_option('single_share_option');
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('ziph-singlePost_warp'); ?>>
	<!--Post header start\-->
	<header class="ziph-post_header">
		<?php
			if ($zipprich_single_featured_image) {
				if (has_post_thumbnail()) { ?>
					<div class="ziph-post_media">
						<?php the_post_thumbnail('zipprich-blog-list'); ?>
					</div>
				<?php } // Featured Image
			}
		?>

		<?php
			if(function_exists('zipprich_post_metas')){
				echo zipprich_post_metas();
			}
		?>
	</header><!--/ Post header  end-->

	<!-- Content -->
	<div class="ziph-singlePost_content">
		<?php
			the_content();
			if(function_exists('zipprich_wp_link_pages')){
				echo zipprich_wp_link_pages();
			}
		?>
	</div>
	<!-- Content -->

	<!-- Tags and Share -->
	<footer class="ziph-singlePost_footer">
		<?php
			$tag_list = get_the_tags();
	  		if($tag_list) { ?>
				<div class="ziph-sigltags_warp">
					<span><?php esc_html_e( 'Tags', 'zipprich' ); ?> :</span>
					<p class="ziph-sigl_tags">
						<?php echo the_tags(''); ?>
					</p>
				</div>
			<?php }
			if($zipprich_single_share_option) {
				if(function_exists('zipprich_wp_share_option')){
					echo zipprich_wp_share_option();
				} else {
					echo '<div class="ziph-sigltags_border"></div>';
				}
			} else {
				echo '<div class="ziph-sigltags_border"></div>';
			}
		?>
	</footer>
	<!-- Tags and Share -->

</div><!-- #post-## -->