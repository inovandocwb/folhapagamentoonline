<?php
/**
 * Template part for displaying posts.
 */
$zipprich_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$zipprich_large_image = $zipprich_large_image[0];

$zipprich_read_more_text = cs_get_option('read_more_text');
$zipprich_read_text = $zipprich_read_more_text ? $zipprich_read_more_text : esc_html__( 'Read more', 'zipprich' );
$zipprich_post_type = get_post_meta( get_the_ID(), 'post_type_metabox', true );
$zipprich_metas_hide = (array) cs_get_option( 'theme_metas_hide' );
$zipprich_blog_style = cs_get_option('blog_listing_style');
$zipprich_blog_columns = cs_get_option('blog_listing_columns');

// Columns
if ($zipprich_blog_style === 'style-two') {
	$zipprich_blog_columns = $zipprich_blog_columns ? $zipprich_blog_columns . ' ziph-fix col-sm-6 ziph-blog_post masonry-entry ' : 'ziph-fix col-sm-6 ziph-blog_post masonry-entry';
} else {
	$zipprich_blog_columns = 'ziph-blogList_post';
}

if(has_post_format('image')){
	$zipprich_post_type = 'ziph-image_post';
} else {
	$zipprich_post_type = 'ziph-regular_post';
}

if (is_sticky()) {
	$sticky_class = ' sticky';
} else {
	$sticky_class = '';
}

?>

<!--Single post start\-->
<article id="post-<?php the_ID(); ?>" <?php post_class( $zipprich_blog_columns . $sticky_class ); ?>>
	<!--Post header start\-->
	<header class="ziph-post_header <?php echo esc_attr( $zipprich_post_type ) ?>">
		<!-- Featured Image -->
		<?php if (has_post_thumbnail()) { ?>
		<div class="ziph-post_media">
			<?php 
				if ($zipprich_blog_style === 'style-two') {
					the_post_thumbnail( 'zipprich-blog-grid' );
				} else {
					the_post_thumbnail( 'zipprich-blog-list' );
				} 
			?>
		</div>
		<?php } // Featured Image ?>		
		<?php 
			if(function_exists('zipprich_post_metas')){
				echo zipprich_post_metas();
			}
		?>
		<h4 class="ziph-post_title">
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</h4>
	</header><!--Post header end\-->
	<!--Post excerpt start\-->
	<div class="ziph-post_excerpt">
		<p><?php
		$blog_excerpt = cs_get_option('theme_blog_excerpt');
		if ($blog_excerpt) {
			$blog_excerpt = $blog_excerpt;
		} else {
			$blog_excerpt = '20';
		}
		zipprich_excerpt($blog_excerpt);
		if(function_exists('zipprich_wp_link_pages')){
			echo zipprich_wp_link_pages();
		}
		?></p>
	</div><!--Post excerpt end\-->
	<!--Post footer start\-->
	<footer class="ziph-post_footer">
		<div class="row">
			<div class="text-left col-xs-6">
				<a href="<?php the_permalink(); ?>" class="ziph-readmore_link">
					<?php echo esc_attr($zipprich_read_text); ?><i class="fa fa-angle-right"></i>
				</a>				
			</div>
			<div class="text-right col-xs-6">
				<?php if ( !in_array( 'comments', $zipprich_metas_hide ) ) { // Comments Hide 
					echo '<span class="ziph-commnt_link">';
					comments_popup_link( esc_html__( 'No Comments', 'zipprich' ), esc_html__( 'One Comment', 'zipprich' ), esc_html__( '% Comments', 'zipprich' ), '', '' );
					echo '</span>';
				} // Comments Hide ?>
			</div>
		</div>
	</footer><!--Post footer end\-->
</article><!--Single post end\-->