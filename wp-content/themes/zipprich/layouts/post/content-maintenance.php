<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php
// if the `wp_site_icon` function does not exist (ie we're on < WP 4.3)
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
  if (cs_get_option('brand_fav_icon')) {
    echo '<link rel="shortcut icon" href="'. esc_url(wp_get_attachment_url(cs_get_option('brand_fav_icon'))) .'" />';
  } else { ?>
    <link rel="shortcut icon" href="<?php echo esc_url(ZIPPRICH_IMAGES); ?>/favicon.png" />
  <?php }
  if (cs_get_option('iphone_icon')) {
    echo '<link rel="apple-touch-icon" sizes="57x57" href="'. esc_url(wp_get_attachment_url(cs_get_option('iphone_icon'))) .'" >';
  }
  if (cs_get_option('iphone_retina_icon')) {
    echo '<link rel="apple-touch-icon" sizes="114x114" href="'. esc_url(wp_get_attachment_url(cs_get_option('iphone_retina_icon'))) .'" >';
    echo '<link name="msapplication-TileImage" href="'. esc_url(wp_get_attachment_url(cs_get_option('iphone_retina_icon'))) .'" >';
  }
  if (cs_get_option('ipad_icon')) {
    echo '<link rel="apple-touch-icon" sizes="72x72" href="'. esc_url(wp_get_attachment_url(cs_get_option('ipad_icon'))) .'" >';
  }
  if (cs_get_option('ipad_retina_icon')) {
    echo '<link rel="apple-touch-icon" sizes="144x144" href="'. esc_url(wp_get_attachment_url(cs_get_option('ipad_retina_icon'))) .'" >';
  }
}
$zipprich_all_element_color  = cs_get_customize_option( 'all_element_colors' );
?>
<meta name="msapplication-TileColor" content="<?php echo esc_attr($zipprich_all_element_color); ?>">
<meta name="theme-color" content="<?php echo esc_attr($zipprich_all_element_color); ?>">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
wp_head();

// Metabox
$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

if ($zipprich_meta) {
  $zipprich_content_padding = $zipprich_meta['content_spacings'];
} else { $zipprich_content_padding = ''; }
// Padding - Metabox
if ($zipprich_content_padding && $zipprich_content_padding !== 'padding-none') {
  $zipprich_content_top_spacings = $zipprich_meta['content_top_spacings'];
  $zipprich_content_bottom_spacings = $zipprich_meta['content_bottom_spacings'];
  if ($zipprich_content_padding === 'padding-custom') {
    $zipprich_content_top_spacings = $zipprich_content_top_spacings ? 'padding-top:'. zipprich_check_px($zipprich_content_top_spacings) .';' : '';
    $zipprich_content_bottom_spacings = $zipprich_content_bottom_spacings ? 'padding-bottom:'. zipprich_check_px($zipprich_content_bottom_spacings) .';' : '';
    $zipprich_custom_padding = $zipprich_content_top_spacings . $zipprich_content_bottom_spacings;
  } else {
    $zipprich_custom_padding = '';
  }
} else {
  $zipprich_custom_padding = '';
}
?>
</head>

<body <?php body_class(); ?>>
  <div class="vt-maintenance-mode ziph-page_content">
    <div class="container <?php echo esc_attr($zipprich_content_padding); ?> ziph-page_warp" style="<?php echo esc_attr($zipprich_custom_padding); ?>">

      <?php
        $zipprich_page = get_post( cs_get_option('maintenance_mode_page') );
        WPBMap::addAllMappedShortcodes();
        echo ( is_object( $zipprich_page ) ) ? do_shortcode( $zipprich_page->post_content ) : '';
      ?>

    </div>
  </div>
  <?php wp_footer(); ?>
</body>
</html>