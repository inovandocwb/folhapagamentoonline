<?php
/*
 * The sidebar containing the main widget area.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

$zipprich_blog_widget = cs_get_option('blog_widget');
$zipprich_single_blog_widget = cs_get_option('single_blog_widget');
$zipprich_team_widget = cs_get_option('team_widget');
$zipprich_woo_widget = cs_get_option('woo_widget');

if (is_page()) {
	// Page Layout Options
	$zipprich_page_layout = get_post_meta( get_the_ID(), 'page_layout_options', true );
}
?>

<!-- ==== sidebar start==== \-->
<div class="ziph-fix ziph-page_sidebar">
	<?php if (is_page() && $zipprich_page_layout['page_sidebar_widget']) {
		if (is_active_sidebar($zipprich_page_layout['page_sidebar_widget'])) {
			dynamic_sidebar($zipprich_page_layout['page_sidebar_widget']);
		}
	} elseif (!is_page() && $zipprich_blog_widget && !$zipprich_single_blog_widget) {
		if (is_active_sidebar($zipprich_blog_widget)) {
			dynamic_sidebar($zipprich_blog_widget);
		}
	} elseif ($zipprich_team_widget && is_singular('team')) {
		if (is_active_sidebar($zipprich_team_widget)) {
			dynamic_sidebar($zipprich_team_widget);
		}
	}  elseif ($zipprich_woo_widget && is_woocommerce()) {
		if (is_active_sidebar($zipprich_woo_widget)) {
			dynamic_sidebar($zipprich_woo_widget);
		}
	} elseif (is_single() && $zipprich_single_blog_widget) {
		if (is_active_sidebar($zipprich_single_blog_widget)) {
			dynamic_sidebar($zipprich_single_blog_widget);
		}
	} else {
		if (is_active_sidebar('sidebar-1')) {
			dynamic_sidebar( 'sidebar-1' );
		}
	} ?>
</div><!-- #secondary -->