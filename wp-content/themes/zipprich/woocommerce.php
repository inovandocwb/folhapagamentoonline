<?php
/*
 * The template for displaying all pages.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

// Metabox
$zipprich_id    = ( isset( $post ) ) ? $post->ID : 0;
$zipprich_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $zipprich_id;
$zipprich_id    = ( zipprich_is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $zipprich_id;
$zipprich_meta  = get_post_meta( $zipprich_id, 'page_type_metabox', true );

if ($zipprich_meta) {
	$zipprich_content_padding = $zipprich_meta['content_spacings'];
} else { $zipprich_content_padding = ''; }
// Padding - Metabox
if ($zipprich_content_padding && $zipprich_content_padding !== 'padding-none') {
	$zipprich_content_top_spacings = $zipprich_meta['content_top_spacings'];
	$zipprich_content_bottom_spacings = $zipprich_meta['content_bottom_spacings'];
	if ($zipprich_content_padding === 'padding-custom') {
		$zipprich_content_top_spacings = $zipprich_content_top_spacings ? 'padding-top:'. zipprich_check_px($zipprich_content_top_spacings) .';' : '';
		$zipprich_content_bottom_spacings = $zipprich_content_bottom_spacings ? 'padding-bottom:'. zipprich_check_px($zipprich_content_bottom_spacings) .';' : '';
		$zipprich_custom_padding = $zipprich_content_top_spacings . $zipprich_content_bottom_spacings;
	} else {
		$zipprich_custom_padding = '';
	}
} else {
	$zipprich_custom_padding = '';
}

// Page Layout Options
$zipprich_page_layout = cs_get_option('woo_sidebar_position');
if (!empty($zipprich_page_layout)) {

	if($zipprich_page_layout === 'right-sidebar') {
		$zipprich_layout_class = 'ziph-flt_left ziph-leftContent_warp ';
	} else {
		$zipprich_layout_class = 'ziph-page_warp';
	}

	// Page Layout Class
	if ($zipprich_page_layout === 'right-sidebar') {
		$zipprich_sidebar_class = 'ziph-pageWite_sidebar';
	} else {
		$zipprich_sidebar_class = 'ziph-page_content ';
	}
} else {
	$zipprich_layout_class = 'ziph-page_warp';
	$zipprich_sidebar_class = 'ziph-page_content ';
}

get_header(); ?>

<div class="<?php echo esc_attr($zipprich_content_padding .' '. $zipprich_sidebar_class); ?>" style="<?php echo esc_attr($zipprich_custom_padding); ?>">
	<div class="container">
		<div class="<?php echo esc_attr($zipprich_layout_class); ?> ziph-woocommercePage_warp">
			<?php
				woocommerce_content();
				// If comments are open or we have at least one comment, load up the comment template.
				if(is_page()):
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				endif;

			?>
		</div><!-- Content Area -->

		<?php
			if ($zipprich_page_layout === 'right-sidebar') {
				get_sidebar(); // Sidebar
			}
		?>

	</div>
</div>

<?php
get_footer();