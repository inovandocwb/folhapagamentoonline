<?php
/**
 * Domain Box - Shortcode Options
 */
add_action( 'init', 'ziph_domain_box_vc_map' );
if ( ! function_exists( 'ziph_domain_box_vc_map' ) ) {
	function ziph_domain_box_vc_map() {

		vc_map( array(
			"name" => __( "Domain Pricing Box", 'zipprich-core'),
			"base" => "ziph_domain_box",
			"description" => __( "Domain Pricing Box Style", 'zipprich-core'),
			"icon" => "fa fa-line-chart color-pink",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Domain Name', 'zipprich-core' ),
					'admin_label' => true,
					'description' => __( 'Enter domain name [Ex: .com].', 'zipprich-core' ),
					'param_name' => 'title',
					),

				array(
					'type' => 'textarea',
					'value' => '',
					'heading' => __( 'Description', 'zipprich-core' ),
					'description' => __( 'Enter domain description.', 'zipprich-core' ),
					'param_name' => 'info',
					),

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Price', 'zipprich-core' ),
					'admin_label' => true,
					'description' => __( 'Enter domain price.', 'zipprich-core' ),
					'param_name' => 'price',
          			'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Time', 'zipprich-core' ),
					'description' => __( 'Enter domain time [Ex: yr].', 'zipprich-core' ),
					'param_name' => 'time',
          			'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					"type" => "textfield",
					"heading" => __( "Button Text", 'zipprich-core' ),
					"param_name" => "btn_text",
					'value' => '',
					"description" => __( "Enter your button text.", 'zipprich-core'),
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					"type" => "switcher",
					"heading" => __( "Open New Tab?", 'zipprich-core' ),
					"param_name" => "open_link",
					"std" => false,
					'value' => '',
					"on_text" => __( "Yes", 'zipprich-core' ),
					"off_text" => __( "No", 'zipprich-core' ),
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					"type" => "textfield",
					"heading" => __( "Button Link", 'zipprich-core' ),
					"param_name" => "btn_link",
					'value' => '',
					"description" => __( "Enter your button link.", 'zipprich-core'),
					),

				ZipprichLib::vt_class_option(),

				array(
					'type' => 'colorpicker',
					'value' => '',
					'heading' => __( 'Button Text Color', 'zipprich-core' ),
					'description' => __( 'Pick text color for button.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'btn_color',
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					'type' => 'colorpicker',
					'value' => '',
					'heading' => __( 'Button Text Hover Color', 'zipprich-core' ),
					'description' => __( 'Pick text hover color for button.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'btn_hover_color',
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					'type' => 'colorpicker',
					'value' => '',
					'heading' => __( 'Button BG Color', 'zipprich-core' ),
					'description' => __( 'Pick button background color.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'btn_bg_color',
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					'type' => 'colorpicker',
					'value' => '',
					'heading' => __( 'Button BG Hover Color', 'zipprich-core' ),
					'description' => __( 'Pick button hover background color.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'btn_bg_hover_color',
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Button Text Size', 'zipprich-core' ),
					'description' => __( 'Enter text size for submit button.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'btn_size',
					),

      			), // Params
			)
		);
	}
}