<?php
/* ==========================================================
    Domain Box
=========================================================== */
if ( !function_exists('ziph_domain_box_function')) {
  function ziph_domain_box_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'info'   => '',
      'price'   => '',
      'time'   => '',
      'btn_text'   => '',
      'btn_link'   => '',
      'open_link'  => '',
      // Style
      'btn_size'  => '',
      'btn_color'  => '',
      'btn_hover_color'  => '',
      'btn_bg_color'  => '',
      'btn_bg_hover_color'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Button Text Color
    if ( $btn_color ) {
      $inline_style .= '.ziph_btn-'. $e_uniqid .' {';
      $inline_style .= ( $btn_color ) ? 'color:'. $btn_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button Text Hover Color
    if ( $btn_hover_color ) {
      $inline_style .= '.ziph_btn-'. $e_uniqid .':hover, .ziph_btn-'. $e_uniqid .':focus, .ziph_btn-'. $e_uniqid .':active {';
      $inline_style .= ( $btn_hover_color ) ? 'color:'. $btn_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button BG Color
    if ( $btn_bg_color ) {
      $inline_style .= '.ziph_btn-'. $e_uniqid .' {';
      $inline_style .= ( $btn_bg_color ) ? 'background:'. $btn_bg_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button BG Hover Color
    if ( $btn_bg_hover_color ) {
      $inline_style .= '.ziph_btn-'. $e_uniqid .':hover, .ziph_btn-'. $e_uniqid .':focus, .ziph_btn-'. $e_uniqid .':active {';
      $inline_style .= ( $btn_bg_hover_color ) ? 'background:'. $btn_bg_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Text Size
    if ( $btn_size ) {
      $inline_style .= '.ziph_btn-'. $e_uniqid .' {';
      $inline_style .= ( $btn_size ) ? 'font-size:'. $btn_size .' !important;' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph_btn-'. $e_uniqid;

    // Open Link
    $open_link = $open_link ? ' target="_blank"' : '';

    ob_start(); ?>

    <!-- single domain type start\-->
    <div class="ziph-domainType_single">
      <div class="ziph-domainType">
        <h2><?php echo esc_attr( $title ); ?></h2>
        <h5>
          <?php echo esc_attr( $price ); ?><?php if ($time) { ?>/<?php echo esc_attr( $time ); ?>*<?php } ?>
        </h5>
      </div>
      <p><?php echo esc_attr( $info ); ?></p>
      <?php if($btn_text){ ?>
      <a class="btn ziph-btn <?php echo esc_attr( $styled_class ); ?>" href="<?php echo esc_url( $btn_link ); ?>" <?php echo $open_link; ?>><?php echo esc_attr( $btn_text ); ?></a>
      <?php } ?>
    </div><!--/ single domain type end-->

    <?php
    $output = ob_get_clean();
    return $output;

  }
}
add_shortcode( 'ziph_domain_box', 'ziph_domain_box_function' );