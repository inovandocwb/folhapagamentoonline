<?php
/* About */
if ( !function_exists('ziph_about_function')) {
  function ziph_about_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'class'  => '',
    ), $atts));

    // Output
    $output   = '<div class="ziph-about_us '. $class .'">';
    $output  .= do_shortcode($content);
    $output  .= '</div>';
    return $output;

  }
}
add_shortcode( 'ziph_about', 'ziph_about_function' );