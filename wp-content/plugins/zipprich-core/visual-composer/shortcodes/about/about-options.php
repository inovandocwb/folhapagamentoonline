<?php
/**
 * About - Shortcode Options
 */
add_action( 'init', 'ziph_about_vc_map' );
if ( ! function_exists( 'ziph_about_vc_map' ) ) {
 function ziph_about_vc_map() {
   vc_map( array(
     "name" => __( "About", 'zipprich-core'),
     "base" => "ziph_about",
     "description" => __( "About Section", 'zipprich-core'),
     "as_parent" => array('only' => 'vc_column_text,ziph_button'),
     "content_element" => true,
     "show_settings_on_create" => false,
     "is_container" => true,
     "icon" => "fa fa-user color-grey",
     "category" => ZipprichLib::ziph_cat_name(),
     "params" => array(

        ZipprichLib::vt_class_option(),

     ),
     "js_view" => 'VcColumnView'
   ) );
 }
}