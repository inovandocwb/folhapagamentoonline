<?php
/**
 * Tabs - Shortcode Options
 */

add_action( 'init', 'ziph_tabs_vc_map' );
if ( ! function_exists( 'ziph_tabs_vc_map' ) ) {
  function ziph_tabs_vc_map() {

    $tab_one_id = time() . '-1-' . rand( 0, 100 );
    $tab_two_id = time() . '-2-' . rand( 0, 100 );
    $tab_three_id = time() . '-3-' . rand( 0, 100 );

    vc_map( array(
      "name"            => __( "Zipprich Tabs", 'zipprich-core'),
      'base'            => 'vc_tabs',
      'is_container'    => true,
      'icon'            => 'fa fa-list-alt color-blue',
      'description'     => __( "Tabs Style", 'zipprich-core'),
      'category'        => ZipprichLib::ziph_cat_name(),
      'params'          => array(
        array(
          'type'        => 'dropdown',
          'heading'     => __( "Tab Style", 'zipprich-core'),
          'param_name'  => 'tab_style',
          'value'       => array(
            __( "Select Tab Style", 'zipprich-core') => '',
            __( "Style One (Borderd Title)", 'zipprich-core')   => 'style-one',
            __( "Style Two (Background Title)", 'zipprich-core')   => 'style-two',
          ),
        ),
        array(
          'type'        => 'textfield',
          'heading'     => __( "Active Tab", 'zipprich-core'),
          'param_name'  => 'active',
          'description' => __( "Which tab you want to be active on load. [Eg. 1 or 2 or 3]", 'zipprich-core'),
        ),

      ),
      'custom_markup'   => '<div class="wpb_tabs_holder wpb_holder vc_container_for_children"><ul class="tabs_controls"></ul>%content%</div>',
      'default_content' => '
        [vc_tab title="Tab 1" tab_id="' . $tab_one_id . '"][/vc_tab]
        [vc_tab title="Tab 2" tab_id="' . $tab_two_id . '"][/vc_tab]
        [vc_tab title="Tab 3" tab_id="' . $tab_three_id . '"][/vc_tab]',
      'js_view'         => 'VcTabsView'
    ) );

    /* Tab */
    vc_map( array(
      'name'                      => __( "Tab", 'zipprich-core'),
      'base'                      => 'vc_tab',
      // 'allowed_container_element' => 'vc_row',
      'is_container'              => true,
      'content_element'           => false,
      'category'                  => ZipprichLib::ziph_cat_name(),
      'params'                    => array(
        array(
          'type'        => 'tab_id',
          'heading'     => __( "Tab Unique ID", 'zipprich-core'),
          'param_name'  => 'tab_id'
        ),
        array(
          'type'        => 'textfield',
          'heading'     => __( "Tab Title", 'zipprich-core'),
          'param_name'  => 'title',
        ),
      ),
      'js_view'         => 'VcTabView'
    ) );

  }
}