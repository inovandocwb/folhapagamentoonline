<?php
/* ==========================================================
  Tabs
=========================================================== */
if( ! function_exists( 'ziph_vt_tabs_function' ) ) {
  function ziph_vt_tabs_function( $atts, $content = '', $key = '' ) {

    global $vt_tabs;
    $vt_tabs = array();

    extract( shortcode_atts( array(
      'id'        => '',
      'tab_style' => '',
      'class'     => '',
      'active'    => 1,
    ), $atts ) );

    do_shortcode( $content );

    // is not empty
    if( empty( $vt_tabs ) ) { return; }

    $output       = '';
    $id           = ( $id ) ? ' id="'. $id .'"' : '';
    $active       = ( isset( $_REQUEST['tab'] ) ) ? $_REQUEST['tab'] : $active;
    $uniqtab      = uniqid();
    if ($tab_style === 'style-two') {
      $tab_style_class = 'ziph-choose_tabs ';
      $tab_list_wrap_class = 'ziph-fix ziph-choose_nav ';
      $tab_container_wrap_class = ' ziph-choosetab_content ';
      $tab_content_wrap_class = ' ziph-fix  ziph-choosetab-warp ';
    } else {
      $tab_style_class = 'ziph-domainPrice_warp ';
      $tab_list_wrap_class = 'ziph-domainPrice_nav ';
      $tab_container_wrap_class = '';
      $tab_content_wrap_class = ' ziph-domainPriceTable_warp ';
    }

    // begin output
    $output  .= '<div'. $id .' class="ziph-tabs '. $tab_style_class . $class .'">';

      // tab-navs
      $output  .= '<ul class="list-unstyled '.$tab_list_wrap_class.'" role="tablist">';
      foreach( $vt_tabs as $key => $tab ){
        $title      = $tab['atts']['title'];
        $active_nav = ( ( $key + 1 ) == $active ) ? ' class="active"' : '';
        $output    .= '<li'. $active_nav .'><a href="#'. $uniqtab .'-'. $key .'" data-toggle="tab"><span>' . $title . '</span></a></li>';
      }
      $output  .= '</ul>';

      // tab-contents
      $output  .= '<div class="tab-content '.$tab_container_wrap_class.'">';
      foreach( $vt_tabs as $key => $tab ){
        $title           = $tab['atts']['title'];
        $active_content  = ( ( $key + 1 ) == $active ) ? ' in active' : '';
        $output         .= '<div id="'. $uniqtab .'-'. $key .'" class="tab-pane fade'. $active_content .'"><div class="'.$tab_content_wrap_class.'">'. do_shortcode( $tab['content'] ) .'</div></div>';
      }
      $output  .= '</div>';

    $output  .= '</div>';
    // end output

    return $output;
  }
  add_shortcode( 'vt_tabs', 'ziph_vt_tabs_function' );
}

/* ==========================================================
  Tab
=========================================================== */
if( ! function_exists( 'ziph_vt_tab_function' ) ) {
  function ziph_vt_tab_function( $atts, $content = '', $key = '' ) {
    global $vt_tabs;
    $vt_tabs[]  = array( 'atts' => $atts, 'content' => $content );
    return;
  }
  add_shortcode('vt_tab', 'ziph_vt_tab_function');
}