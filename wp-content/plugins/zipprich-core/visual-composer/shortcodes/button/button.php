<?php
/* ===========================================================
    Button
=========================================================== */
if ( !function_exists('ziph_button_function')) {
  function ziph_button_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'button_style'  => '',
      'button_size'  => '',
      'button_text'  => '',
      'button_title'  => '',
      'button_time'  => '',
      'button_price'  => '',
      'button_link'  => '',
      'open_link'  => '',
      'class'  => '',
      // Styling
      'text_color'  => '',
      'text_hover_color'  => '',
      'btn_bg_color'  => '',
      'border_color'  => '',
      'bg_hover_color'  => '',
      'border_hover_color'  => '',
      'text_size'  => '',
      // Design
      'css' => ''
    ), $atts));

    // Design Tab
    $custom_css = ( function_exists( 'vc_shortcode_custom_css_class' ) ) ? vc_shortcode_custom_css_class( $css, ' ' ) : '';

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Button Text Color
    if ( $text_color ) {
      $inline_style .= '.ziph-bdr_btn-'. $e_uniqid .' .btn-text {';
      $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
      $inline_style .= '}';
    }

    // Button Text Hover Color
    if ( $text_hover_color ) {
      $inline_style .= '.ziph-bdr_btn-'. $e_uniqid .':hover .btn-text, .ziph-bdr_btn-'. $e_uniqid .':focus .btn-text, .ziph-bdr_btn-'. $e_uniqid .':active .btn-text {';
      $inline_style .= ( $text_hover_color ) ? 'color:'. $text_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Text Size
    if ( $text_size ) {
      $inline_style .= '.ziph-bdr_btn-'. $e_uniqid .' {';
      $inline_style .= ( $text_size ) ? 'font-size:'. zipprich_core_check_px($text_size) .';' : '';
      $inline_style .= '}';
    }

    // Button Color
    if ( $btn_bg_color || $border_color ) {
      $inline_style .= '.ziph-bdr_btn-'. $e_uniqid .' {';
      $inline_style .= ( $btn_bg_color ) ? 'background:'. $btn_bg_color .' !important;' : '';
      $inline_style .= ( $border_color ) ? 'border-color: '. $border_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button Hover Color
    if ( $bg_hover_color || $border_hover_color ) {
      $inline_style .= '.ziph-bdr_btn-'. $e_uniqid .':hover, .ziph-bdr_btn-'. $e_uniqid .':focus, .ziph-bdr_btn-'. $e_uniqid .':active {';
      $inline_style .= ( $bg_hover_color ) ? 'background:'. $bg_hover_color .' !important;' : '';
      $inline_style .= ( $border_hover_color ) ? 'border-color: '. $border_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-bdr_btn-'. $e_uniqid;

    // Styling
    $button_style = $button_style ? $button_style : '';
    $button_size = $button_size ? ' '. $button_size : '';
    $button_text = $button_text ? '<span class="btn-text">'. $button_text .'</span>' : '';
    $button_link = $button_link ? 'href="'. $button_link .'"' : '';
    $open_link = $open_link ? ' target="_blank"' : '';

    if($button_style == 'btn-rounded-price'){
      $button_title = $button_title ? '<p>'. $button_title .'</p>' : '';
      $button_text = $button_text ? '<sub>'. $button_text .'</sub>' : '';
      $button_price = $button_price ? ' '. $button_price : '';
      $button_time = $button_time ? '<sub>/'. $button_time.'</sub>' : '';
      $output = '<div class="text-center ziph-process_info">';
      $output .= $button_title;
      $output .= '<a class="ziph-process_price '. $custom_css .' '. $button_style . $styled_class . $class .'" '. $button_link . $open_link .'>'. $button_text . $button_price . $button_time .'</a>';
      $output .= '</div>';
    } else {
     $output = '<a class="ziph-bdr_btn '. $custom_css . $button_size .' '. $button_style . $styled_class . $class .'" '. $button_link . $open_link .'>'. $button_text .'</a>';
    }

    return $output;

  }
}
add_shortcode( 'ziph_button', 'ziph_button_function' );