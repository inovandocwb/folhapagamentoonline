<?php
/**
 * Button - Shortcode Options
 */
add_action( 'init', 'ziph_button_vc_map' );
if ( ! function_exists( 'ziph_button_vc_map' ) ) {
  function ziph_button_vc_map() {
    vc_map( array(
      "name" => __( "Button", 'zipprich-core'),
      "base" => "ziph_button",
      "description" => __( "Button Styles", 'zipprich-core'),
      "icon" => "fa fa-mouse-pointer color-orange",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          'type' => 'dropdown',
          'heading' => __( 'Button Size', 'zipprich-core' ),
          'value' => array(
            __( 'Select Button Size', 'zipprich-core' ) => '',
            __( 'Default', 'zipprich-core' ) => '',
            __( 'Large', 'zipprich-core' ) => 'btn-large',
          ),
          'admin_label' => true,
          'param_name' => 'button_size',
          'description' => __( 'Select button size', 'zipprich-core' ),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Button Style', 'zipprich-core' ),
          'value' => array(
            __( 'Select Button Style', 'zipprich-core' ) => '',
            __( 'Default', 'zipprich-core' ) => '',
            __( 'Rounded', 'zipprich-core' ) => 'btn-rounded',
            __( 'Rounded Price', 'zipprich-core' ) => 'btn-rounded-price',
          ),
          'param_name' => 'button_style',
          'description' => __( 'Select button style', 'zipprich-core' ),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Button Title", 'zipprich-core' ),
          "param_name" => "button_title",
          'value' => '',
          "description" => __( "Enter your button title.", 'zipprich-core'),
          'dependency' => array(
            'element' => 'button_style',
            'value' => 'btn-rounded-price',
          ),
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Button Text", 'zipprich-core' ),
          "param_name" => "button_text",
          'value' => '',
          'admin_label' => true,
          "description" => __( "Enter your button text.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Button Price", 'zipprich-core' ),
          "param_name" => "button_price",
          'value' => '',
          "description" => __( "Enter your button price.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
          'dependency' => array(
            'element' => 'button_style',
            'value' => 'btn-rounded-price',
          ),          
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Button Time", 'zipprich-core' ),
          "param_name" => "button_time",
          'value' => '',
          "description" => __( "Enter your button price time.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
          'dependency' => array(
            'element' => 'button_style',
            'value' => 'btn-rounded-price',
          ),          
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Button Link", 'zipprich-core' ),
          "param_name" => "button_link",
          'value' => '',
          "description" => __( "Enter your button link.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "switcher",
          "heading" => __( "Open New Tab?", 'zipprich-core' ),
          "param_name" => "open_link",
          "std" => false,
          'value' => '',
          "on_text" => __( "Yes", 'zipprich-core' ),
          "off_text" => __( "No", 'zipprich-core' ),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        ZipprichLib::vt_class_option(),

        // Styling
        array(
          "type" => "colorpicker",
          "heading" => __( "Text Color", 'zipprich-core' ),
          "param_name" => "text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Text Hover Color", 'zipprich-core' ),
          "param_name" => "text_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Background Color", 'zipprich-core' ),
          "param_name" => "btn_bg_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Background Hover Color", 'zipprich-core' ),
          "param_name" => "bg_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Border Color", 'zipprich-core' ),
          "param_name" => "border_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Border Hover Color", 'zipprich-core' ),
          "param_name" => "border_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Text Size", 'zipprich-core' ),
          "param_name" => "text_size",
          'value' => '',
          "description" => __( "Enter button text font size. [Eg: 14px]", 'zipprich-core'),
          "group" => __( "Styling", 'zipprich-core'),
        ),

        // Design Tab
        array(
          "type" => "css_editor",
          "heading" => __( "Text Size", 'zipprich-core' ),
          "param_name" => "css",
          "group" => __( "Design", 'zipprich-core'),
        ),

      )
    ) );
  }
}
