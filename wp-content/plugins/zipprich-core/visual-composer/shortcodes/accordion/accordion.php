<?php
/* ==========================================================
  Accordion
=========================================================== */
if( ! function_exists( 'ziph_vt_accordion_function' ) ) {
  function ziph_vt_accordion_function( $atts, $content = '', $key = '' ) {

    global $vt_accordion_tabs;
    $vt_accordion_tabs = array();

    extract( shortcode_atts( array(
      'accordion_style' => '',
      'id'            => '',
      'class'         => '',
      'one_active'    => '',
      'icon_color'    => '',
      'active_tab'    => 0,
    ), $atts ) );

    do_shortcode( $content );

    // is not empty clients
    if( empty( $vt_accordion_tabs ) ) { return; }

    $uniqtab     = uniqid();
    $id          = ( $id ) ? ' id="'. $id .'"' : '';
    $class       = ( $class ) ? ' '. $class : '';
    $one_active  = ( $one_active ) ? '#ziph-affilte_faq-' . $uniqtab : '';

    // Style
    if ($accordion_style === 'style-two') {
      $accordion_class = '';
      $panel_group_class = 'ziph-faq_group';
    } else if ($accordion_style === 'style-three') {
      $accordion_class = 'contactPRS_collapse';
      $panel_group_class = 'ziph-contactC_group';
    } else {
      $accordion_class = 'ziph-shostfaq_warp';
      $panel_group_class = 'ziph-faq_group';
    }

    // begin output
    ob_start(); ?>
      <div class="<?php echo $accordion_class ?>">
          <div class="panel-group  <?php echo $panel_group_class ?>" id="ziph-affilte_faq-<?php echo $uniqtab;?>" role="tablist" aria-multiselectable="true">
              <?php foreach ( $vt_accordion_tabs as $key => $tab ) { 
                $aria_opened = ( ( $key + 1 ) == $active_tab ) ? 'true' : 'false';
                $opened = ( ( $key + 1 ) == $active_tab ) ? ' in' : ''; ?>

              <?php if($accordion_style === 'style-three') { ?>
              <!--single contact start\-->
              <div class="panel panel-default ziph-contactC">
                <div class="panel-heading  ziph-contactC_head" role="tab" id="ziph-contactC-<?php echo $uniqtab . '-' . $key; ?>">
                  <h4 class="panel-title">
                  <a class="ziph-fix blo collapsed  ziph-contactC_btn" role="button" data-toggle="collapse" data-parent="<?php echo $one_active;?>" href="#ziph-contactCclps-<?php echo $uniqtab . '-' . $key; ?>" aria-expanded="<?php echo $aria_opened; ?>" aria-controls="ziph-contactCclps-<?php echo $uniqtab . '-' . $key; ?>">
                    <span class="ziph-flt_left ziph-contactC_title">
                      <?php echo $tab['atts']['title']; ?>
                    </span> 
                    <span class="ziph-flt_right fa fa-angle-down"></span>
                  </a>
                  </h4>
                </div>
                <div id="ziph-contactCclps-<?php echo $uniqtab . '-' . $key; ?>" class="panel-collapse collapse <?php echo $opened; ?>" role="tabpanel" aria-labelledby="ziph-contactC-<?php echo $uniqtab . '-' . $key; ?>">
                   <div class="panel-body  ziph-contactC_body">
                    <?php echo do_shortcode( $tab['content'] ); ?>
                   </div>
                </div>
              </div><!--/single contact end-->

              <?php } else { ?>
              <!--single Faq start\-->       
              <div class="panel panel-default  ziph-faq">
                <div class="panel-heading  ziph-faq_head" role="tab" id="ziph-faq-<?php echo $uniqtab . '-' . $key; ?>">
                  <h4 class="panel-title">
                    <a class="ziph-faq_btn" role="button" data-toggle="collapse" data-parent="<?php echo $one_active;?>" href="#ziph-faqclps-<?php echo $uniqtab . '-' . $key; ?>" aria-expanded="<?php echo $aria_opened; ?>" aria-controls="ziph-faqclps-<?php echo $uniqtab . '-' . $key; ?>">
                      <?php echo $tab['atts']['title']; ?>
                    </a>
                  </h4>
                </div>
                <div id="ziph-faqclps-<?php echo $uniqtab . '-' . $key; ?>" class="panel-collapse collapse <?php echo $opened; ?>" role="tabpanel" aria-labelledby="ziph-faq-<?php echo $uniqtab . '-' . $key; ?>">
                  <div class="panel-body   ziph-faq_body">
                    <?php echo do_shortcode( $tab['content'] ); ?>
                  </div>
                </div>
              </div><!--/single Faq end-->
              <?php } } ?>
              
          </div>
      </div><!--/Faq warp end xxx-->
    <?php
    return ob_get_clean();
  }
  add_shortcode( 'vc_accordion', 'ziph_vt_accordion_function' );
}


/**
 *
 * Accordion Shortcode
 * @since 1.0.0
 * @version 1.1.0
 *
 */
if( ! function_exists( 'ziph_vt_accordion_tab' ) ) {
  function ziph_vt_accordion_tab( $atts, $content = '', $key = '' ) {
    global $vt_accordion_tabs;
    $vt_accordion_tabs[]  = array( 'atts' => $atts, 'content' => $content );
    return;
  }
  add_shortcode( 'vc_accordion_tab', 'ziph_vt_accordion_tab' );
}