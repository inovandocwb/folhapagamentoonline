<?php
/**
 * Services - Shortcode Options
 */
add_action( 'init', 'ziph_services_vc_map' );
if ( ! function_exists( 'ziph_services_vc_map' ) ) {
  function ziph_services_vc_map() {
    vc_map( array(
      "name" => __( "Service", 'zipprich-core'),
      "base" => "ziph_services",
      "description" => __( "Service Shortcodes", 'zipprich-core'),
      "icon" => "fa fa-cog color-brown",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          'type' => 'dropdown',
          'heading' => __( 'Services Style', 'zipprich-core' ),
          'value' => array(
            __( 'Style One', 'zipprich-core' ) => 'ziph-service-one',
            __( 'Style Two', 'zipprich-core' ) => 'ziph-service-two',
            __( 'Style Three', 'zipprich-core' ) => 'ziph-service-three',
            __( 'Style Four', 'zipprich-core' ) => 'ziph-service-four',
            __( 'Style Five', 'zipprich-core' ) => 'ziph-service-five',
          ),
          'admin_label' => true,
          'param_name' => 'service_style',
          'description' => __( 'Select your service style.', 'zipprich-core' ),
        ),
        ZipprichLib::vt_open_link_tab(),
        array(
          "type"      => 'attach_image',
          "heading"   => __('Upload Service Image', 'zipprich-core'),
          "param_name" => "service_image",
          "value"      => "",
          "description" => __( "Set your service image.", 'zipprich-core'),
          'dependency' => array(
            'element' => 'service_style',
            'value' => 'ziph-service-one',
          ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"      => 'attach_image',
          "heading"   => __('Upload Icon', 'zipprich-core'),
          "param_name" => "service_icon_image",
          "value"      => "",
          "description" => __( "Set your service icon image.", 'zipprich-core'),
          'dependency' => array(
            'element' => 'service_style',
            'value' => 'ziph-service-five',
          ),
        ),
        array(
          "type"      => 'href',
          "heading"   => __('Image Link', 'zipprich-core'),
          "param_name" => "service_image_link",
          "value"      => "",
          "description" => __( "Enter your service image link.", 'zipprich-core'),
          'dependency' => array(
            'element' => 'service_style',
            'value' => 'ziph-service-one',
          ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"      => 'vt_icon',
          "heading"   => __('Set Icon', 'zipprich-core'),
          "param_name" => "service_icon",
          "value"      => "",
          "description" => __( "Set your service icon.", 'zipprich-core'),
          'dependency' => array(
            'element' => 'service_style',
            'value' => array('ziph-service-two','ziph-service-three','ziph-service-four'),
          ),
        ),
        ZipprichLib::vt_notice_field(__( "Content Area", 'zipprich-core' ),'cntara_opt','cs-warning', ''), // Notice
        array(
          "type"      => 'vc_link',
          "heading"   => __('Service Title', 'zipprich-core'),
          "param_name" => "service_title",
          "value"      => "",
          "description" => __( "Enter your service title and link.", 'zipprich-core')
        ),
        array(
          "type"      => 'textarea_html',
          "heading"   => __('Content', 'zipprich-core'),
          "param_name" => "content",
          "value"      => "",
          "description" => __( "Enter your service content here.", 'zipprich-core')
        ),

        // Read More
        array(
    			"type"        => "notice",
    			"heading"     => __( "Read More Link", 'zipprich-core' ),
    			"param_name"  => 'rml_opt',
    			'class'       => 'cs-warning',
    			'value'       => '',
          'dependency' => array(
            'element' => 'service_style',
            'value' => 'ziph-service-one',
          ),
    		),
        array(
          "type"      => 'href',
          "heading"   => __('Link', 'zipprich-core'),
          "param_name" => "read_more_link",
          "value"      => "",
          "description" => __( "Set your link for read more.", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'dependency' => array(
            'element' => 'service_style',
            'value' => 'ziph-service-one',
          ),
        ),
        array(
          "type"      => 'textfield',
          "heading"   => __('Title', 'zipprich-core'),
          "param_name" => "read_more_title",
          "value"      => "",
          "description" => __( "Enter your read more title.", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'dependency' => array(
            'element' => 'service_style',
            'value' => 'ziph-service-one',
          ),
        ),
        ZipprichLib::vt_class_option(),

        // Style
        ZipprichLib::vt_notice_field(__( "Title Styling", 'zipprich-core' ),'tle_opt','cs-warning', 'Style'), // Notice
        array(
          "type"      => 'colorpicker',
          "heading"   => __('Title Color', 'zipprich-core'),
          "param_name" => "title_color",
          "value"      => "",
          "description" => __( "Pick your heading color.", 'zipprich-core'),
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"      => 'textfield',
          "heading"   => __('Title Size', 'zipprich-core'),
          "param_name" => "title_size",
          "value"      => "",
          "description" => __( "Enter the numeric value for title size in px.", 'zipprich-core'),
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"      => 'textfield',
          "heading"   => __('Title Top Space', 'zipprich-core'),
          "param_name" => "title_top_space",
          "value"      => "",
          "description" => __( "Enter the value for title top space in px.", 'zipprich-core'),
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"      => 'textfield',
          "heading"   => __('Title Bottom Space', 'zipprich-core'),
          "param_name" => "title_bottom_space",
          "value"      => "",
          "description" => __( "Enter the value for title bottom space in px.", 'zipprich-core'),
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),

      )
    ) );
  }
}
