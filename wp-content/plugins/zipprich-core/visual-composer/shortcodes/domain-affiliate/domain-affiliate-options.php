<?php
/**
 * Call to Action - Shortcode Options
 */
add_action( 'init', 'ziph_domain_affiliate_vc_map' );
if ( ! function_exists( 'ziph_domain_affiliate_vc_map' ) ) {
 function ziph_domain_affiliate_vc_map() {
   vc_map( array(
     "name" => __( "Domain Affiliate", 'zipprich-core'),
     "base" => "ziph_domain_affiliate",
     "description" => __( "Domain Affiliate Area", 'zipprich-core'),
     "as_parent" => array('only' => 'ziph_domain_affiliate_content, ziph_domain_affiliate_list'),
     "content_element" => true,
     "show_settings_on_create" => false,
     "is_container" => true,
     "icon" => "fa fa-star color-brown",
     "category" => ZipprichLib::ziph_cat_name(),
     "params" => array(
        
        array(
          "type" => "colorpicker",
          "heading" => __( "Text Color", 'zipprich-core' ),
          "param_name" => "text_color",
          'value' => '',
        ),

        ZipprichLib::vt_class_option(),

     ),
     "js_view" => 'VcColumnView'
   ) );
 }
}

// Left Side Lists
add_action( 'init', 'ziph_domain_affiliate_list_vc_map' );
if ( ! function_exists( 'ziph_domain_affiliate_list_vc_map' ) ) {
  function ziph_domain_affiliate_list_vc_map() {
    vc_map( array(
      "name" => __( "Affiliate Offer - Price Lists", 'zipprich-core'),
      "base" => "ziph_domain_affiliate_list",
      "description" => __( "Affiliate Offer List Prices", 'zipprich-core'),
      "icon" => "fa fa-font color-slate-blue",
      "as_child" => array('only' => 'ziph_domain_affiliate'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        array(
          "type"        => 'textfield',
          "heading"     => __('Title', 'zipprich-core'),
          "param_name"  => "title",
          "admin_label" => true,
          "value"       => "",
        ),        
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Affiliate Pricing', 'zipprich-core' ),
          'param_name' => 'list_items',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              "type"        => 'textfield',
              "heading"     => __('Title', 'zipprich-core'),
              "param_name"  => "title",
              "value"       => "",
            ),
            array(
              "type"        => 'textfield',
              "heading"     => __('Sales Price', 'zipprich-core'),
              "param_name"  => "price",
              "value"       => "",
            ),
          ),
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Important Note', 'zipprich-core'),
          "param_name"  => "important_note",
          "value"       => "",
        ),
        ZipprichLib::vt_class_option(),
        
        // Styling
        array(
          "type" => "colorpicker",
          "heading" => __( "Heading Text Color", 'zipprich-core' ),
          "param_name" => "head_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-4 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "List Text Color", 'zipprich-core' ),
          "param_name" => "list_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-4 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Paragraph Text Color", 'zipprich-core' ),
          "param_name" => "para_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-4 vc_column vt_field_space',
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Heading Text Size", 'zipprich-core' ),
          "param_name" => "head_text_size",
          'value' => '',
          "description" => __( "Enter heading text font size. [Eg: 14px]", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-4 vc_column vt_field_space',
          "group" => __( "Styling", 'zipprich-core'),
        ),
        array(
          "type" => "textfield",
          "heading" => __( "List Text Size", 'zipprich-core' ),
          "param_name" => "list_text_size",
          'value' => '',
          "description" => __( "Enter list text font size. [Eg: 14px]", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-4 vc_column vt_field_space',
          "group" => __( "Styling", 'zipprich-core'),
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Paragraph Text Size", 'zipprich-core' ),
          "param_name" => "para_text_size",
          'value' => '',
          "description" => __( "Enter paragraph text font size. [Eg: 14px]", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-4 vc_column vt_field_space',
          "group" => __( "Styling", 'zipprich-core'),
        ),
      )
    ) );
  }
}

// Right Side Content
add_action( 'init', 'ziph_domain_affiliate_content_vc_map' );
if ( ! function_exists( 'ziph_domain_affiliate_content_vc_map' ) ) {
  function ziph_domain_affiliate_content_vc_map() {
    vc_map( array(
      "name" => __( "Affiliate Offer - Description", 'zipprich-core'),
      "base" => "ziph_domain_affiliate_content",
      "description" => __( "Affiliate Offer Content", 'zipprich-core'),
      "icon" => "fa fa-font color-slate-blue",
      "as_child" => array('only' => 'ziph_domain_affiliate'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          "type"        => 'textarea_html',
          "heading"     => __('Description', 'zipprich-core'),
          "param_name"  => "content",
          "value"       => "",
          "admin_label" => true,
          "description" => __( "Explain about your offer.", 'zipprich-core')
        ),

        array(
          "type" => "textfield",
          "heading" => __( "Button Text", 'zipprich-core' ),
          "param_name" => "button_text",
          'value' => '',
          "description" => __( "Enter your button text.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),

        array(
          "type" => "switcher",
          "heading" => __( "Open New Tab?", 'zipprich-core' ),
          "param_name" => "open_link",
          "std" => false,
          'value' => '',
          "on_text" => __( "Yes", 'zipprich-core' ),
          "off_text" => __( "No", 'zipprich-core' ),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),

        array(
          "type" => "textfield",
          "heading" => __( "Button Link", 'zipprich-core' ),
          "param_name" => "button_link",
          'value' => '',
          "description" => __( "Enter your button link.", 'zipprich-core'),
        ),

        ZipprichLib::vt_class_option(),
        
        // Styling
        array(
          "type"        => "notice",
          "heading"     => __( "Content Styles", 'zipprich-core' ),
          "param_name"  => 'content_style',
          'class'       => 'cs-info',
          'value'       => '',
          "group" => __( "Styling", 'zipprich-core'),
        ),        
        array(
          "type" => "colorpicker",
          "heading" => __( "Heading Text Color", 'zipprich-core' ),
          "param_name" => "head_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Paragraph Text Color", 'zipprich-core' ),
          "param_name" => "para_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Heading Text Size", 'zipprich-core' ),
          "param_name" => "head_text_size",
          'value' => '',
          "description" => __( "Enter heading text font size. [Eg: 14px]", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
          "group" => __( "Styling", 'zipprich-core'),
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Paragraph Text Size", 'zipprich-core' ),
          "param_name" => "para_text_size",
          'value' => '',
          "description" => __( "Enter paragraph text font size. [Eg: 14px]", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
          "group" => __( "Styling", 'zipprich-core'),
        ),
        array(
          "type"        => "notice",
          "heading"     => __( "Button Styles", 'zipprich-core' ),
          "param_name"  => 'button_style',
          'class'       => 'cs-info',
          'value'       => '',
          "group" => __( "Styling", 'zipprich-core'),
        ),          
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Color", 'zipprich-core' ),
          "param_name" => "button_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Hover Color", 'zipprich-core' ),
          "param_name" => "button_text_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Color", 'zipprich-core' ),
          "param_name" => "button_bg_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Hover Color", 'zipprich-core' ),
          "param_name" => "bg_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Border Color", 'zipprich-core' ),
          "param_name" => "border_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Border Hover Color", 'zipprich-core' ),
          "param_name" => "border_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Button Text Size", 'zipprich-core' ),
          "param_name" => "button_text_size",
          'value' => '',
          "description" => __( "Enter button text font size. [Eg: 14px]", 'zipprich-core'),
          "group" => __( "Styling", 'zipprich-core'),
        ),
      )
    ) );
  }
}
