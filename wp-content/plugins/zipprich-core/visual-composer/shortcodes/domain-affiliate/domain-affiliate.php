<?php
/* Info Grid */
if ( !function_exists('ziph_domain_affiliate_function')) {
  function ziph_domain_affiliate_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'text_color'  => '',
      'class'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Text Color
    if ( $text_color ) {
      $inline_style .= '.ziph-choose-text-'. $e_uniqid .' a.ziph-bdr_btn, .ziph-choose-text-'. $e_uniqid .' h3, .ziph-choose-text-'. $e_uniqid .' p, .ziph-choose-text-'. $e_uniqid .' .ziph-choose-list ul li {';
      $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-choose-text-'. $e_uniqid;

    // Output
    $output   = '<div class="ziph-domainAffiliate_area ' . esc_attr($class . $styled_class) .'"><div class="ziph-fix ziph-domainAffilt_container"><div class="ziph-fix ziph-domainAffiltCont_warp">';
    $output  .= do_shortcode($content);
    $output  .= '</div></div></div>';
    return $output;

  }
}
add_shortcode( 'ziph_domain_affiliate', 'ziph_domain_affiliate_function' );

/* Right Side Content Area */
if ( !function_exists('ziph_domain_affiliate_content_function')) {
  function ziph_domain_affiliate_content_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'info'  => '',
      'button_size'  => '',
      'button_text'  => '',
      'button_link'  => '',
      'open_link'  => '',
      'class'  => '',
      // Title Styling
      'head_text_color' => '',
      'head_text_size' => '',
      // Paragraph Styling
      'para_text_color' => '',
      'para_text_size' => '',
      // Button Styling
      'button_text_color'  => '',
      'button_text_hover_color'  => '',
      'button_bg_color'  => '',
      'border_color'  => '',
      'bg_hover_color'  => '',
      'border_hover_color'  => '',
      'button_text_size'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Head Text
    if ( $head_text_color || $head_text_size ) {
      $inline_style .= '.ziph-domainAffilt_txt-'. $e_uniqid .' h1, .ziph-domainAffilt_txt-'. $e_uniqid .' h2, .ziph-domainAffilt_txt-'. $e_uniqid .' h3, .ziph-domainAffilt_txt-'. $e_uniqid .' h4 {';
      $inline_style .= ( $head_text_color ) ? 'color:'. $head_text_color .';' : '';
      $inline_style .= ( $head_text_size ) ? 'font-size:'. zipprich_core_check_px($head_text_size) .';' : '';
      $inline_style .= '}';
    }

    // Paragraph Text
    if ( $para_text_color || $para_text_size ) {
      $inline_style .= '.ziph-domainAffilt_txt-'. $e_uniqid .' p {';
      $inline_style .= ( $para_text_color ) ? 'color:'. $para_text_color .';' : '';
      $inline_style .= ( $para_text_size ) ? 'font-size:'. zipprich_core_check_px($para_text_size) .' !important;' : '';
      $inline_style .= '}';
    }

    // Button Text Color
    if ( $button_text_color ) {
      $inline_style .= '.ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn {';
      $inline_style .= ( $button_text_color ) ? 'color:'. $button_text_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button Text Hover Color
    if ( $button_text_hover_color ) {
      $inline_style .= '.ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn:hover, .ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn:focus, .ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn:active {';
      $inline_style .= ( $button_text_hover_color ) ? 'color:'. $button_text_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Text Size
    if ( $button_text_size ) {
      $inline_style .= '.ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn {';
      $inline_style .= ( $button_text_size ) ? 'font-size:'. zipprich_core_check_px($button_text_size) .';' : '';
      $inline_style .= '}';
    }

    // Button Color
    if ( $button_bg_color || $border_color ) {
      $inline_style .= '.ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn {';
      $inline_style .= ( $button_bg_color ) ? 'background:'. $button_bg_color .' !important;' : '';
      $inline_style .= ( $border_color ) ? 'border-color: '. $border_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button Hover Color
    if ( $bg_hover_color || $border_hover_color ) {
      $inline_style .= '.ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn:hover, .ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn:focus, .ziph-domainAffilt_txt-'. $e_uniqid .' .ziph-bdr_btn:active {';
      $inline_style .= ( $bg_hover_color ) ? 'background:'. $bg_hover_color .' !important;' : '';
      $inline_style .= ( $border_hover_color ) ? 'border-color: '. $border_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-domainAffilt_txt-'. $e_uniqid;

    // Styling
    $button_text = $button_text ?  $button_text : '';
    $button_link = $button_link ? 'href="'. $button_link .'"' : '';
    $open_link = $open_link ? ' target="_blank"' : '';

    // fix unclosed/unwanted paragraph tags in $content
    $content = wpb_js_remove_wpautop($content, true);
    $content = $content ? do_shortcode($content) : '';

    // Output
    $output = '<div class="ziph-fix ziph-domainAffilt_txt '. $class . $styled_class .'">' . $content;
    if($button_text){
      $output .= '<a class="ziph-bdr_btn" '. $button_link . $open_link .'>'. $button_text .'</a>';
    }
    $output .= '</div>';

    return $output;

  }
}
add_shortcode( 'ziph_domain_affiliate_content', 'ziph_domain_affiliate_content_function' );

/* Left Side Content Area */
if ( !function_exists('ziph_domain_affiliate_list_function')) {
  function ziph_domain_affiliate_list_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'list_items'  => '',
      'important_note'  => '',
      'class'  => '',
      // Title Styling
      'head_text_color' => '',
      'head_text_size' => '',
      // List Styling
      'list_text_color' => '',
      'list_text_size' => '',
      // Paragraph Styling
      'para_text_color' => '',
      'para_text_size' => '',
    ), $atts));

    $title = ($title) ? '<h3>'.$title.'</h3>' : '';
    $important_note = ($important_note) ? '<p>'.esc_textarea($important_note).'</p>' : '';

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Head Text
    if ( $head_text_color || $head_text_size ) {
      $inline_style .= '.ziph-domainAffiltprice_Warp-'. $e_uniqid .' h3 {';
      $inline_style .= ( $head_text_color ) ? 'color:'. $head_text_color .';' : '';
      $inline_style .= ( $head_text_size ) ? 'font-size:'. zipprich_core_check_px($head_text_size) .';' : '';
      $inline_style .= '}';
    }

    // Para Text
    if ( $para_text_color || $para_text_size ) {
      $inline_style .= '.ziph-domainAffiltprice_Warp-'. $e_uniqid .' p {';
      $inline_style .= ( $para_text_color ) ? 'color:'. $para_text_color .';' : '';
      $inline_style .= ( $para_text_size ) ? 'font-size:'. zipprich_core_check_px($para_text_size) .';' : '';
      $inline_style .= '}';
    }

    // List Text
    if ( $list_text_color || $list_text_size ) {
      $inline_style .= '.ziph-domainAffiltprice_Warp-'. $e_uniqid .' ul li {';
      $inline_style .= ( $list_text_color ) ? 'color:'. $list_text_color .';' : '';
      $inline_style .= ( $list_text_size ) ? 'font-size:'. zipprich_core_check_px($list_text_size) .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-domainAffiltprice_Warp-'. $e_uniqid;

    // Group Field
    $list_items = (array) vc_param_group_parse_atts( $list_items );
    $get_each_list = array();
    foreach ( $list_items as $list_item ) {
      $each_list = $list_item;
      $each_list['title'] = isset( $list_item['title'] ) ? $list_item['title'] : '';
      $each_list['price'] = isset( $list_item['price'] ) ? $list_item['price'] : '';
      $get_each_list[] = $each_list;
    }

    $output = '<div class="ziph-domainAffiltprice_Warp '. esc_attr($class . $styled_class) .'"><div class="ziph-domainAffilt_price">' .$title;
    $output .= '<ul class="list-unstyled">';
    foreach ( $get_each_list as $each_list ) {
      $output .= '<li>'.$each_list['title'].'<span>'.$each_list['price'].'</span></li>';
    }
    $output .= '</ul></div>' . $important_note;
    $output .= '</div>';
    // Output
    return $output;

  }
}
add_shortcode( 'ziph_domain_affiliate_list', 'ziph_domain_affiliate_list_function' );