<?php
/* ==========================================================
    Contact Member
=========================================================== */
if ( !function_exists('ziph_contact_members_function')) {
  function ziph_contact_members_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'class'  => '',
    ), $atts));

    // Output
    $output   = '<div class="ziph-contactMembarInfo '. $class .'"><h3 class="ziph-contactMInfo_title">'. $title .'</h3><div class="ziph-contactMInfo_warp">';
    $output  .= do_shortcode($content);
    $output  .= '</div></div>';
    return $output;

  }
}
add_shortcode( 'ziph_contact_members', 'ziph_contact_members_function' );

/* Single Member */
if ( !function_exists('ziph_contact_member_function')) {
  function ziph_contact_member_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'image'  => '',
      'info'  => '',
      'list_items'  => '',
      'open_link'  => '',
      'class'  => '',
    ), $atts));

    // Group Field
    $list_items = (array) vc_param_group_parse_atts( $list_items );
    $get_each_list = array();
    foreach ( $list_items as $list_item ) {
      $each_list = $list_item;
      $each_list['icon'] = isset( $list_item['icon'] ) ? $list_item['icon'] : '';
      $each_list['link'] = isset( $list_item['link'] ) ? $list_item['link'] : '';
      $each_list['text'] = isset( $list_item['text'] ) ? $list_item['text'] : '';
      $get_each_list[] = $each_list;
    }    

    $open_link = $open_link ? ' target="_blank"' : ''; 
    
		$large_image =  wp_get_attachment_image_src( $image, 'fullsize', false, '' );
    $large_image = $large_image[0];
    $large_image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
		
		if(class_exists('Aq_Resize')) {
			$contact_member_img = aq_resize( $large_image, '110', '110', true );
		} else {$contact_member_img = $large_image;} 

		$image = ($contact_member_img) ? '<img src="'. esc_url($contact_member_img) .'" alt="'. esc_attr($large_image_alt) .'">' : '';

    $output = '<div class="ziph-contactMInfo_single '. $class .'"><div class="ziph-contactMInfo">';
    $output .= '<div class="ziph-contactMInfo_img">'. $image .'</div>';
    $output .= '<div class="ziph-contactMInfo_txt">';
    $output .= '<h4 class="ziph-contactMI_name">'. $title .'</h4><h5>'. $info .'</h5>';
    $output .= '<ul class="list-unstyled">';
    foreach ($get_each_list as $each_list) {
      $output .= '<li><a href="'. $each_list['link'] .'"><i class="'. $each_list['icon'] .'"></i>'. $each_list['text'] .'</a></li>';
    }
    $output .= '</ul></div>';
    $output .= '</div></div>';
    return $output;

  }
}
add_shortcode( 'ziph_contact_member', 'ziph_contact_member_function' );