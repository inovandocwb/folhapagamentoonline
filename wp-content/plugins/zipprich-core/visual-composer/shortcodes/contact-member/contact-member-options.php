<?php
/**
 * Contact Member - Shortcode Options
 */
add_action( 'init', 'ziph_contact_members_vc_map' );
if ( ! function_exists( 'ziph_contact_members_vc_map' ) ) {
	function ziph_contact_members_vc_map() {

		vc_map( array(
			"name" => __( "Contact Member", 'zipprich-core'),
			"base" => "ziph_contact_members",
			"description" => __( "Contact member info area", 'zipprich-core'),
			"as_parent" => array('only' => 'ziph_contact_member'),
			"content_element" => true,
			"show_settings_on_create" => true,
			"is_container" => true,			
			"icon" => "fa fa-male color-pink",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Title', 'zipprich-core' ),
					'description' => __( 'Member area title', 'zipprich-core' ),
					'param_name' => 'title',
					'admin_label' => true,
					),

				ZipprichLib::vt_class_option(),
      			), // Params
				"js_view" => 'VcColumnView'
			)
		);
	}
}

// Single Member
add_action( 'init', 'ziph_contact_member_vc_map' );
if ( ! function_exists( 'ziph_contact_member_vc_map' ) ) {
	function ziph_contact_member_vc_map() {

		vc_map( array(
			"name" => __( "Contact Member", 'zipprich-core'),
			"base" => "ziph_contact_member",
			"description" => __( "Contact member info area", 'zipprich-core'),
			"as_child" => array('only' => 'ziph_contact_members'),		
			"icon" => "fa fa-gift color-black",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Title', 'zipprich-core' ),
					'description' => __( 'Input a title', 'zipprich-core' ),
					'param_name' => 'title',
					'admin_label' => true,
					),			
				array(
					'type' => 'attach_image',
					'value' => '',
					'heading' => __( 'Image', 'zipprich-core' ),
					'description' => __( 'Upload Image', 'zipprich-core' ),
					'param_name' => 'image',
					),
				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Info', 'zipprich-core' ),
					'description' => __( 'Memeber Info', 'zipprich-core' ),
					'param_name' => 'info',
					),
					// List
					array(
						'type' => 'param_group',
						'value' => '',
						'heading' => __( 'Socail Links', 'zipprich-core' ),
						'param_name' => 'list_items',
						// Note params is mapped inside param-group:
						'params' => array(
							array(
								'type' => 'textarea',
								'value' => '',
								'heading' => __( 'Text', 'zipprich-core' ),
								'param_name' => 'text',
								),
							array(
								'type' => 'vt_icon',
								'value' => '',
								'heading' => __( 'Select Icon', 'zipprich-core' ),
								'param_name' => 'icon',
								),
							array(
								'type' => 'href',
								'value' => '',
								'heading' => __( 'Link', 'zipprich-core' ),
								'param_name' => 'link',
								),

							)
						),

				array(
					"type" => "switcher",
					"heading" => __( "Open New Tab?", 'zipprich-core' ),
					"param_name" => "open_link",
					"std" => false,
					'value' => '',
					"on_text" => __( "Yes", 'zipprich-core' ),
					"off_text" => __( "No", 'zipprich-core' ),
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				ZipprichLib::vt_class_option(),
      			), // Params
			)
		);
	}
}