<?php

/* ==========================================================
  WHMCS Announcement
=========================================================== */
if ( !function_exists('ziph_whmcs_announcements_function')) {
  function ziph_whmcs_announcements_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'limit'  => '',
      'read_more'  => '',
      'word_limit'  => '',
      'class'  => '',
    ), $atts));

    $read_more = ($read_more) ? $read_more : esc_html__('Read More', 'zipprich-core');
    $word_limit = ($word_limit) ? $word_limit : 60;

    // For WHMCS 7.2 and later, we recommend using an API Authentication Credential pair.
    // Learn more at http://docs.whmcs.com/API_Authentication_Credentials
    // Prior to WHMCS 7.2, an admin username and md5 hash of the admin password may be used.
    ob_start();
    
    $whmcsUrl = get_option('cc_whmcs_bridge_url');
    $username = cs_get_option('whmcs_identifier');
    $password = cs_get_option('whmcs_secret');

    // Set post values
    $postfields = array(
      'username' => $username,
      'password' => $password,
      'action' => 'GetAnnouncements',
      'responsetype' => 'json',
    );

    // Call the API
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    if (curl_error($ch)) {
      die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);

    // Decode response
    $jsonData = json_decode($response, true);

    if($jsonData['result'] === 'success') {
      $announcements = $jsonData['announcements']['announcement'];
       ?>
      <div id="bridge" class="ziph-WhmpressAnnou container">
      <?php 
        if($title){
          echo '<h1>'.$title.'</h1>';
        }

        if($limit){$count = 1;}
        foreach ($announcements as $key => $announcement) { 
        if($announcement['published']) {
      ?>
        <div class="announcement-single">
          <h2>
             <span class="label label-default">
             <?php echo (new DateTime($announcement['date']))->format('M d'); ?>
             </span>
             <?php echo $announcement['title']; ?>
          </h2>
          <blockquote>
            <?php  
              if(function_exists('cc_whmcs_bridge_mainpage')){
                $whmcs_page_link = get_permalink(cc_whmcs_bridge_mainpage());
              } else {
                $whmcs_page_link = '';
              }            
              $announcement_content = $announcement['announcement'];
              $read_more_html = '<a href="'. $whmcs_page_link .'?ccce=index&rp=/announcements/'. $announcement['id'] .'/" class="label label-warning">'. $read_more .' »</a>';
              $short_content = wp_trim_words( $announcement_content, $word_limit, ' ... ' . $read_more_html );
            ?>
            <p><?php echo $short_content; ?></p>
            
          </blockquote>
         </div>     
        <?php
        if($limit){if ($count++ == $limit) break; $count++;}
      }}
      ?>
     </div>
      <?php
    } else {
      echo esc_html__("An Error Occurred: ", 'zipprich-core') . $jsonData['result'];
    }
   
    return ob_get_clean();
    
  }
}
add_shortcode( 'ziph_whmcs_announcements', 'ziph_whmcs_announcements_function' );