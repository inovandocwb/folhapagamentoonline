<?php
/**
 * WHMCS Announcement - Shortcode Options
 */
add_action( 'init', 'ziph_whmcs_announcements_vc_map' );
if ( ! function_exists( 'ziph_whmcs_announcements_vc_map' ) ) {
  function ziph_whmcs_announcements_vc_map() {
    vc_map( array(
      "name" => __( "WHMCS - Announcement", 'zipprich-core'),
      "base" => "ziph_whmcs_announcements",
      "description" => __( "WHMCS Announcement Styles", 'zipprich-core'),
      "icon" => "fa fa-globe color-green",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(  
        array(
          'type' => 'notice',
          'value' => '',
          'class' => 'cs-warning',
          'heading' => __( 'To use this addons make sure you fillup the WHMCS API Authentication Credentials from theme options and add your ip from WHMCS admin area Genneral > Security tab', 'zipprich-core' ),
          'param_name' => 'vt_notice',
        ),  
        array(
          'type' => 'textfield',
          'value' => '',
          'admin_label' => true,
          'heading' => __( 'Section Title', 'zipprich-core' ),
          'param_name' => 'title',
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Announcement Article Limit', 'zipprich-core' ),
          'param_name' => 'limit',
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Announcement Article Word Limit', 'zipprich-core' ),
          'param_name' => 'word_limit',
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Read More Text', 'zipprich-core' ),
          'param_name' => 'read_more',
        ),
        ZipprichLib::vt_class_option(),
      )
    ) );
  }
}