<?php
/**
 * Contact - Shortcode Options
 */
add_action( 'init', 'contact_vc_map' );
if ( ! function_exists( 'contact_vc_map' ) ) {
  function contact_vc_map() {

    $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );
    $contact_forms = array();
    if ( $cf7 ) {
      foreach ( $cf7 as $cform ) {
        $contact_forms[ $cform->post_title ] = $cform->ID;
      }
    } else {
      $contact_forms[ __( 'No contact forms found', 'sixten-core' ) ] = 0;
    }

    vc_map( array(
    "name" => __( "Contact Form 7", 'zipprich-core'),
    "base" => "ziph_contact",
    "description" => __( "Contact Form 7 Style", 'zipprich-core'),
    "icon" => "icon-wpb-contactform7",
    "category" => ZipprichLib::ziph_cat_name(),
    "params" => array(

      array(
        'type' => 'dropdown',
        'heading' => __( 'Select contact form', 'js_composer' ),
        'param_name' => 'id',
        'value' => $contact_forms,
        'save_always' => true,
        'admin_label' => true,
        'description' => __( 'Choose previously created contact form from the drop down list.', 'js_composer' ),
      ),      
      array(
        'type' => 'dropdown',
        'value' => array(
          __( 'Default Styles', 'zipprich-core' ) => '',
          __( 'Style One (Bordered Button)', 'zipprich-core' ) => 'contact-box-one',
          __( 'Style Two (BG Button)', 'zipprich-core' ) => 'contact-box-two',
          __( 'Style Three (Button Right)', 'zipprich-core' ) => 'contact-box-one ziph-submit-right',
        ),
        'admin_label' => true,
        'heading' => __( 'Contact Form Box Style', 'zipprich-core' ),
        'param_name' => 'box_style',
        'description' => __( 'Select from box style.', 'zipprich-core' ),
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __( 'Title', 'zipprich-core' ),
        'description' => __( 'Enter Form title for your contact form.', 'zipprich-core' ),
        'param_name' => 'form_title',
      ),
      ZipprichLib::vt_class_option(),

      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __( 'Button Text Size', 'zipprich-core' ),
        'description' => __( 'Enter text size for submit button.', 'zipprich-core' ),
        'group' => __( 'Style', 'zipprich-core' ),
        'param_name' => 'submit_size',
      ),
      array(
        'type' => 'colorpicker',
        'value' => '',
        'heading' => __( 'Button Text Color', 'zipprich-core' ),
        'description' => __( 'Pick text color for submit button.', 'zipprich-core' ),
        'group' => __( 'Style', 'zipprich-core' ),
        'param_name' => 'submit_color',
      ),
      array(
        'type' => 'colorpicker',
        'value' => '',
        'heading' => __( 'Button BG Color', 'zipprich-core' ),
        'description' => __( 'Pick button background color.', 'zipprich-core' ),
        'group' => __( 'Style', 'zipprich-core' ),
        'param_name' => 'submit_bg_color',
      ),

      ), // Params
    ) );
  }
}