<?php
/* ==========================================================
    Contact
=========================================================== */
if ( !function_exists('ziph_contact_function')) {
  function ziph_contact_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'box_style'  => '',
      'id'  => '',
      'form_title'  => '',
      'class'  => '',
      // Style
      'submit_size'  => '',
      'submit_color'  => '',
      'submit_bg_color'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    // Own Styles
    if ( $submit_size || $submit_color || $submit_bg_color ) {
      $inline_style .= '.ziph-contact-'. $e_uniqid .' .wpcf7 input[type="submit"] {';
      $inline_style .= ( $submit_size ) ? 'font-size:'. zipprich_core_check_px($submit_size) .';' : '';
      $inline_style .= ( $submit_color ) ? 'color:'. $submit_color .';' : '';
      $inline_style .= ( $submit_bg_color ) ? 'background-color:'. $submit_bg_color .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-contact-'. $e_uniqid;

    // Atts If
    $id = ( $id ) ? $id : '';
    $form_title = ( $form_title ) ? '<h3 class="ziph-contactForm_title">'. $form_title .'</h3>' : '';
    $class = ( $class ) ? ' '. $class : '';

    // Starts
    $output  = '<div class="ziph-contactForm_warp '. $box_style . $styled_class . $class .'">';
    $output .= $form_title;
    $output .= zipprich_set_wpautop(do_shortcode( '[contact-form-7 id="'. $id .'"]' ));
    $output .= '</div>';

    return $output;

  }
}
add_shortcode( 'ziph_contact', 'ziph_contact_function' );
