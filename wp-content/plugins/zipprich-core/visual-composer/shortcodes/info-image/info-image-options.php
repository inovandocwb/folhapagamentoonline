<?php
// Grid Image - Options
add_action( 'init', 'ziph_info_grid_image_vc_map' );
if ( ! function_exists( 'ziph_info_grid_image_vc_map' ) ) {
  function ziph_info_grid_image_vc_map() {
    vc_map( array(
      "name" => __( "Features Image", 'zipprich-core'),
      "base" => "ziph_info_grid_image",
      "description" => __( "Features info image", 'zipprich-core'),
      "icon" => "fa fa-photo color-slate-blue",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          "type"        => 'attach_image',
          "heading"     => __('Image', 'zipprich-core'),
          "param_name"  => "image",
          "value"       => "",
          "admin_label" => true,
          "description" => __( "Upload an image or icon.", 'zipprich-core')
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Image Alignment', 'zipprich-core' ),
          'value' => array(
            __( 'Left', 'zipprich-core' ) => '',
            __( 'Right', 'zipprich-core' ) => 'right',
            ),
          'param_name' => 'image_align',
          'description' => __( 'Select image alignment (default: left)', 'zipprich-core' ),
        ),        
        ZipprichLib::vt_class_option(),
        // Style
        array(
          "type" => "colorpicker",
          "heading" => __( "Image Background Color", 'zipprich-core' ),
          "param_name" => "image_bg",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
        ),
      )
    ) );
  }
}