<?php
/* Grid Image */
if ( !function_exists('ziph_info_grid_image_function')) {
  function ziph_info_grid_image_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'image'  => '',
      'image_align'  => '',
      'image_bg'  => '',
      'class'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Button Text Color
    if ( $image_bg ) {
      $inline_style .= '.ziph-choose-icon-'. $e_uniqid .' {';
      $inline_style .= ( $image_bg ) ? 'background-color:'. $image_bg .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-choose-icon-'. $e_uniqid;

    $image_align = ($image_align == 'right') ? ' ziph-flt_right ' : '';

    $zipprich_large_image =  wp_get_attachment_image_src( $image, 'fullsize', false, '' );
    $zipprich_large_image = $zipprich_large_image[0];
    $zipprich_image_alt   = get_post_meta($image, '_wp_attachment_image_alt', true);
    if(class_exists('Aq_Resize')) {
      $zipprich_grid_img = aq_resize( $zipprich_large_image, '193', '212', true );
      $zipprich_grid_img    = ( $zipprich_grid_img ) ? '<img src="'.esc_url( $zipprich_grid_img ).'" alt="'.esc_attr( $zipprich_image_alt ).'">' : '<img src="'.esc_url( $zipprich_large_image ).'" alt="'.esc_attr( $zipprich_image_alt ).'">';
    } else {$zipprich_grid_img = $zipprich_large_image;}


    // Group Field
    $output = '<div class="hidden-xs ziph-choose-icon '. $styled_class . $image_align .' '. $class .'">'.$zipprich_grid_img.'</div>';

    // Output
    return $output;

  }
}
add_shortcode( 'ziph_info_grid_image', 'ziph_info_grid_image_function' );