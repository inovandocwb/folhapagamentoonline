<?php
/* ==========================================================
    Section Title
=========================================================== */
if ( !function_exists('ziph_section_title_function')) {
  function ziph_section_title_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'text_alignment'  => '',
      'subtitle'  => '',
      'class'  => '',
      // Design
      'css' => ''
    ), $atts));

    // Design Tab
    $custom_css = ( function_exists( 'vc_shortcode_custom_css_class' ) ) ? vc_shortcode_custom_css_class( $css, ' ' ) : '';

    $text_alignment = ($text_alignment) ? $text_alignment : 'text-center ';

    $output = '<div class="ziph-section_txt '.esc_attr( $text_alignment . $class ).'">';

    if ($title) {
    	$output .= '<h3 class="'.$custom_css.'">'.esc_attr( $title ).'</h3>';
    }
    if ($subtitle) {
      $subtext_class = ($text_alignment == 'left' || $text_alignment == 'right') ?  : 'ziph-section_subtxt';
    	$output .= '<p class="'.$subtext_class.'">'.esc_attr( $subtitle ).'</p>';
    }

    $output .= '</div>';
    return $output;

  }
}
add_shortcode( 'ziph_section_title', 'ziph_section_title_function' );