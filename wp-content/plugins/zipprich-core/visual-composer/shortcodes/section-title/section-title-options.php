<?php
/**
 * Section Title - Shortcode Options
 */
add_action( 'init', 'ziph_section_title_vc_map' );
if ( ! function_exists( 'ziph_section_title_vc_map' ) ) {
	function ziph_section_title_vc_map() {

		vc_map( array(
			"name" => __( "Section Title", 'zipprich-core'),
			"base" => "ziph_section_title",
			"description" => __( "Section Title Style", 'zipprich-core'),
			"icon" => "fa fa-exchange color-black",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Title', 'zipprich-core' ),
					'description' => __( 'Input a title', 'zipprich-core' ),
					'param_name' => 'title',
					'admin_label' => true,
					),
				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Sub Title', 'zipprich-core' ),
					'description' => __( 'Input a sub title', 'zipprich-core' ),
					'param_name' => 'subtitle',
					),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'zipprich-core' ),
					'value' => array(
						__( 'Center', 'zipprich-core' ) => '',
						__( 'Left', 'zipprich-core' ) => 'text-left',
						__( 'Right', 'zipprich-core' ) => 'text-right',
						),
					'param_name' => 'text_alignment',
					'description' => __( 'Select title text alignment (default: center)', 'zipprich-core' ),
					),

				ZipprichLib::vt_class_option(),				

				array(
					"type" => "css_editor",
					"heading" => __( "Title Margin/Padding", 'zipprich-core' ),
					"param_name" => "css",
					"group" => __( "Design", 'zipprich-core'),
					),	
      			), // Params
		
			));
	}
}