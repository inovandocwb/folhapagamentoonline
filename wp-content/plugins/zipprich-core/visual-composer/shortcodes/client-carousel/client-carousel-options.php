<?php
/**
 * Client Carousel - Shortcode Options
 */
add_action( 'init', 'client_carousel_vc_map' );
if ( ! function_exists( 'client_carousel_vc_map' ) ) {
  function client_carousel_vc_map() {
    vc_map( array(
      "name" => __( "Client", 'zipprich-core'),
      "base" => "ziph_client_carousel",
      "description" => __( "Client Carousel", 'zipprich-core'),
      "icon" => "fa fa-shield color-green",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        ZipprichLib::vt_open_link_tab(),

        // Client Logos
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Client Logos', 'zipprich-core' ),
          'param_name' => 'client_logos',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              'type'        => 'attach_image',
              'value'       => '',
              'admin_label' => true,
              'heading'     => __( 'Upload Image', 'zipprich-core' ),
              'param_name'  => 'client_logo',
            ),
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Client Link', 'zipprich-core' ),
              'param_name' => 'client_link',
            ),          
          )
        ),

        array(
          'type' => 'dropdown',
          'heading' => __( 'Border Top/Bottom ?', 'zipprich-core' ),
          'value' => array(
            __( 'Select Border Style', 'zipprich-core' ) => '',
            __( 'Border Top', 'zipprich-core' ) => 'top',
            __( 'Border Bottom', 'zipprich-core' ) => 'bottom'
          ),
          'param_name' => 'client_border',
        ),          
        ZipprichLib::vt_class_option(),

        // Carousel
        ZipprichLib::vt_notice_field(__( "Basic Options", 'zipprich-core' ),'bsic_opt','cs-warning', 'Carousel'), // Notice
        ZipprichLib::vt_carousel_loop(), // Loop
        ZipprichLib::vt_carousel_items(), // Items
        ZipprichLib::vt_carousel_margin(), // Margin
        ZipprichLib::vt_carousel_dots(), // Dots
        ZipprichLib::vt_carousel_nav(), // Nav
        ZipprichLib::vt_notice_field(__( "Auto Play & Interaction", 'zipprich-core' ),'apyi_opt','cs-warning', 'Carousel'), // Notice
        ZipprichLib::vt_carousel_autoplay_timeout(), // Autoplay Timeout
        ZipprichLib::vt_carousel_autoplay(), // Autoplay
        ZipprichLib::vt_carousel_animateout(), // Animate Out
        ZipprichLib::vt_carousel_mousedrag(), // Mouse Drag
        ZipprichLib::vt_notice_field(__( "Width & Height", 'zipprich-core' ),'wah_opt','cs-warning', 'Carousel'), // Notice
        ZipprichLib::vt_carousel_autowidth(), // Auto Width
        ZipprichLib::vt_carousel_autoheight(), // Auto Height
        ZipprichLib::vt_notice_field('Responsive Options','res_opt','cs-warning', 'Carousel'), // Notice
        ZipprichLib::vt_carousel_tablet(), // Tablet
        ZipprichLib::vt_carousel_mobile(), // Mobile
        ZipprichLib::vt_carousel_small_mobile(), // Small Mobile

      )
    ) );
  }
}
