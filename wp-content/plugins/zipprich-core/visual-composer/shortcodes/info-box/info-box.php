<?php
/* ==========================================================
    Info Box
=========================================================== */
if ( !function_exists('ziph_info_box_function')) {
  function ziph_info_box_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'style'  => '',
      'title'  => '',
      'image'  => '',
      'link'  => '',
      'open_link'  => '',
      'info'  => '',
    ), $atts));

    // Image
	$zipprich_large_image =  wp_get_attachment_image_src( $image, 'fullsize', false, '' );
	$zipprich_large_image = $zipprich_large_image[0];
	$zipprich_image_alt = get_post_meta($image, '_wp_attachment_image_alt', true);    

    ob_start();?>
    
    <?php if($style == 'style-one' || $style == 'style-two'){ ?>

    <?php  
      $wrap_class   = ($style == 'style-two') ? 'ziph-sigldmn_feture': 'ziph-goodness_single';
      $image_class  = ($style == 'style-two') ? 'ziph-sdmnftur_icon': 'ziph-goodness_icon';
      $text_class   = ($style == 'style-two') ? '': 'ziph-goodness_txt';
    ?>
    <!--Our goodness single item start\-->
    <div class="text-center <?php echo esc_attr( $wrap_class ); ?>">
      <!--icon start\-->
      <div class="<?php echo esc_attr( $image_class ); ?>">
  			<?php
          if(class_exists('Aq_Resize')) {
            if($style == 'style-two'){
              $zipprich_info_img = aq_resize( $zipprich_large_image, '64', '64', true );
            } else {
              $zipprich_info_img = aq_resize( $zipprich_large_image, '90', '90', true );
            }
          } else {$zipprich_info_img = $zipprich_large_image;}
  		  	$zipprich_info_img = ( $zipprich_info_img ) ? $zipprich_info_img : '';
  		  	if (!empty($zipprich_info_img)) {
  		  		echo '<img src="'.esc_url( $zipprich_info_img ).'" alt="'.esc_attr( $zipprich_image_alt ).'">';
  		  	}
  			?>
      </div><!--/icon end-->
      <!--txt start\-->
      <div class="<?php echo esc_attr( $text_class ); ?>">
	     <?php $open_link = $open_link ? 'target="_blank"' : '';?>
        <h4><a href="<?php echo esc_url( $link ); ?>" <?php echo $open_link; ?>><?php echo do_shortcode( $title ); ?></a></h4>
        <p><?php echo do_shortcode( esc_attr( $info ) ); ?></p>
      </div><!--/txt end-->
    </div><!--/Our goodness single item end-->
	
	<?php } else { ?>
    
    <!--single service start\-->
    <div class="ziph-single_service">
  		<?php
        if(class_exists('Aq_Resize')) {
          $zipprich_info_img = aq_resize( $zipprich_large_image, '65', '51', true );
        } else {$zipprich_info_img = $zipprich_large_image;}
  	  	$zipprich_info_img = ( $zipprich_info_img ) ? $zipprich_info_img : '';
  	  	if (!empty($zipprich_info_img)) {
  	  		echo '<div class="ziph-serv_icon"><img src="'.esc_url( $zipprich_info_img ).'" alt="'.esc_attr( $zipprich_image_alt ).'"></div>';
  	  	}
  		?>
      <div class="ziph-serv_txt">
	      <?php $open_link = $open_link ? 'target="_blank"' : '';?>        
        <h4><a href="<?php echo esc_url( $link ); ?>" <?php $open_link; ?>><?php echo do_shortcode( $title ); ?></a></h4>
        <p><?php echo do_shortcode( esc_attr( $info ) ); ?></p>
      </div>
    </div>
    <!--/single service end-->
    <?php } ?>
    
    <?php
    $output = ob_get_clean();
    return $output;

  }
}
add_shortcode( 'ziph_info_box', 'ziph_info_box_function' );