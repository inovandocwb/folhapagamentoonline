<?php
/**
 * Info Box - Shortcode Options
 */
add_action( 'init', 'ziph_info_box_vc_map' );
if ( ! function_exists( 'ziph_info_box_vc_map' ) ) {
	function ziph_info_box_vc_map() {

		vc_map( array(
			"name" => __( "Info Box", 'zipprich-core'),
			"base" => "ziph_info_box",
			"description" => __( "Info Box Style", 'zipprich-core'),
			"icon" => "fa fa-gift color-pink",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(
				
				array(
					'type' => 'dropdown',
					'heading' => __( 'Box Style', 'zipprich-core' ),
					'value' => array(
						__( 'Select Box Style', 'zipprich-core' ) => '',
						__( 'Style One(Center Text Large Icon)', 'zipprich-core' ) => 'style-one',
						__( 'Style Two(Center Text Small Icon)', 'zipprich-core' ) => 'style-two',
						__( 'Style Three(Left Alignment)', 'zipprich-core' ) => 'style-three'
						),
					'admin_label' => true,
					'param_name' => 'style',
					'description' => __( 'Select info box style', 'zipprich-core' ),
					),
				
				array(
					'type' => 'textarea',
					'value' => '',
					'heading' => __( 'Title', 'zipprich-core' ),
					'admin_label' => true,
					'description' => __( 'Enter box title.', 'zipprich-core' ),
					'param_name' => 'title',
					),

				array(
					'type' => 'attach_image',
					'value' => '',
					'heading' => __( 'Image/Icon', 'zipprich-core' ),
					'description' => __( 'Upload an image/icon.', 'zipprich-core' ),
					'param_name' => 'image',
					),

				array(
					'type' => 'href',
					'value' => '',
					'heading' => __( 'Link', 'zipprich-core' ),
					'description' => __( 'Input a link', 'zipprich-core' ),
					'param_name' => 'link',
					),

				array(
					"type" => "switcher",
					"heading" => __( "Open New Tab?", 'zipprich-core' ),
					"param_name" => "open_link",
					"std" => false,
					'value' => '',
					"on_text" => __( "Yes", 'zipprich-core' ),
					"off_text" => __( "No", 'zipprich-core' ),
					),

				array(
					'type' => 'textarea',
					'value' => '',
					'heading' => __( 'Description', 'zipprich-core' ),
					'description' => __( 'Enter box description.', 'zipprich-core' ),
					'param_name' => 'info',
					),

				ZipprichLib::vt_class_option(),

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Button Text Size', 'zipprich-core' ),
					'description' => __( 'Enter text size for submit button.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'submit_size',
					),
				array(
					'type' => 'colorpicker',
					'value' => '',
					'heading' => __( 'Button Text Color', 'zipprich-core' ),
					'description' => __( 'Pick text color for submit button.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'submit_color',
					),
				array(
					'type' => 'colorpicker',
					'value' => '',
					'heading' => __( 'Button BG Color', 'zipprich-core' ),
					'description' => __( 'Pick button background color.', 'zipprich-core' ),
					'group' => __( 'Style', 'zipprich-core' ),
					'param_name' => 'submit_bg_color',
					),

      			), // Params
			));
	}
}