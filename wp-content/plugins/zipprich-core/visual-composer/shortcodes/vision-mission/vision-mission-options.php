<?php
/**
 * Vision Mission - Shortcode Options
 */
add_action( 'init', 'ziph_vm_vc_map' );
if ( ! function_exists( 'ziph_vm_vc_map' ) ) {
	function ziph_vm_vc_map() {

		vc_map( array(
			"name" => __( "Vision or Mission", 'zipprich-core'),
			"base" => "ziph_vm",
			"description" => __( "Vision/Mission Style", 'zipprich-core'),
			"icon" => "fa fa-gift color-black",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Title', 'zipprich-core' ),
					'description' => __( 'Input a title', 'zipprich-core' ),
					'param_name' => 'title',
					'admin_label' => true,
					),
				array(
					'type' => 'attach_image',
					'value' => '',
					'heading' => __( 'Upload image', 'zipprich-core' ),
					'param_name' => 'mv_img',
					),
				array(
					'type' => 'textarea_html',
					'value' => '',
					'heading' => __( 'Content', 'zipprich-core' ),
					'description' => __( 'Input a description', 'zipprich-core' ),
					'param_name' => 'content',
					),

				ZipprichLib::vt_class_option(),
      			), // Params
			));
	}
}