<?php
/* ==========================================================
    Vision Mission
=========================================================== */
if ( !function_exists('ziph_vm_function')) {
  function ziph_vm_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'mv_img'  => '',
      'class'  => '',
    ), $atts));
    
    $zipprich_large_image =  wp_get_attachment_image_src( $mv_img, 'fullsize', false, '' );
    $zipprich_large_image = $zipprich_large_image[0];
    $zipprich_large_image_alt = get_post_meta($mv_img, '_wp_attachment_image_alt', true);

	if(class_exists('Aq_Resize')) {
		$post_img = aq_resize( $zipprich_large_image, '270', '200', true );
	} else {$post_img = $zipprich_large_image;}    

    $output = '<div class="ziph-about_activity '.esc_attr( $class ).'">';
    if($post_img){
    	$output .= '<div class="ziph-aboutact_media"><img src="'.esc_url( $post_img ).'" alt="'.esc_attr( $zipprich_large_image_alt ).'"></div>';
    }
    $output .= '<h4>'.esc_attr( $title ).'</h4>';
    $output .= '<p>' . wp_strip_all_tags( do_shortcode( $content ) ). '</p>';
    $output .= '</div>';
    return $output;

  }
}
add_shortcode( 'ziph_vm', 'ziph_vm_function' );