<?php
/**
 * Team - Shortcode Options
 */
add_action( 'init', 'ziph_team_vc_map' );
if ( ! function_exists( 'ziph_team_vc_map' ) ) {
  function ziph_team_vc_map() {
    vc_map( array(
    "name" => __( "Team", 'zipprich-core'),
    "base" => "ziph_team",
    "description" => __( "Team Style", 'zipprich-core'),
    "icon" => "fa fa-users color-grey",
    "category" => ZipprichLib::ziph_cat_name(),
    "params" => array(
      
      array(
        "type"        => "notice",
        "heading"     => __( "Addon to show team member profile", 'zipprich-core' ),
        "param_name"  => 'lsng_opt',
        'class'       => 'cs-warning',
        'value'       => '',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Testimonial ID', 'zipprich-core'),
        "param_name"  => "team_id",
        "value"       => "",
        "description" => __( "Enter single[Recommnded]/comma seperated testimonial id to show.", 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Limit', 'zipprich-core'),
        "param_name"  => "team_limit",
        "value"       => "4",
        "description" => __( "Enter the number of items to show.", 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),        
      array(
        'type' => 'dropdown',
        'heading' => __( 'Order', 'zipprich-core' ),
        'value' => array(
          __( 'Select Team Order', 'zipprich-core' ) => '',
          __('Asending', 'zipprich-core') => 'ASC',
          __('Desending', 'zipprich-core') => 'DESC',
        ),
        'param_name' => 'team_order',
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Order By', 'zipprich-core' ),
        'value' => array(
          __('None', 'zipprich-core') => 'none',
          __('ID', 'zipprich-core') => 'ID',
          __('Author', 'zipprich-core') => 'author',
          __('Title', 'zipprich-core') => 'title',
          __('Date', 'zipprich-core') => 'date',
        ),
        'param_name' => 'team_orderby',
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      
      ZipprichLib::vt_class_option(),

      // Style
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Name Color', 'zipprich-core'),
        "param_name"  => "name_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Profession Color', 'zipprich-core'),
        "param_name"  => "profession_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      // Size
      array(
        "type"        =>'textfield',
        "heading"     =>__('Name Size', 'zipprich-core'),
        "param_name"  => "name_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Profession Size', 'zipprich-core'),
        "param_name"  => "profession_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),

      ), // Params
    ) );
  }
}