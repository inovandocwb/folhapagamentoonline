<?php
/* Team */
if ( !function_exists('ziph_team_function')) {
  function ziph_team_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'class'  => '',

      // Listing
      'team_id'  => '',
      'team_limit'  => '',
      'team_order'  => '',
      'team_orderby'  => '',
      // Color & Style
      'name_color'  => '',
      'profession_color'  => '',
      'name_size'  => '',
      'profession_size'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    // Name Color
    if ( $name_color || $name_size ) {
      $inline_style .= '.ziph-team_area-'. $e_uniqid .' .team-name {';
      $inline_style .= ( $name_color ) ? 'color:'. $name_color .';' : '';
      $inline_style .= ( $name_size ) ? 'font-size:'. $name_size .';' : '';
      $inline_style .= '}';
    }
    // Profession Color
    if ( $profession_color || $profession_size ) {
      $inline_style .= '.ziph-team_area-'. $e_uniqid .' .team-pro {';
      $inline_style .= ( $profession_color ) ? 'color:'. $profession_color .';' : '';
      $inline_style .= ( $profession_size ) ? 'font-size:'. $profession_size .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-team_area-'. $e_uniqid;

    // Turn output buffer on
    ob_start();

    // Query Starts Here
    // Pagination
    global $paged;
    if( get_query_var( 'paged' ) )
      $my_page = get_query_var( 'paged' );
    else {
      if( get_query_var( 'page' ) )
        $my_page = get_query_var( 'page' );
      else
        $my_page = 1;
      set_query_var( 'paged', $my_page );
      $paged = $my_page;
    }

    $args = array(
      'paged' => $my_page,
      'post_type' => 'team',
      'posts_per_page' => (int)$team_limit,
      'order' => $team_order
    );

    if(!empty($testimonial_id)){
      $integerIDs = array_map('intval', explode(',', $testimonial_id));
      $args['post__in'] = $integerIDs;
      $args['orderby'] = 'post__in';
    } else {
      $args['orderby'] = $team_orderby;
    }

    $ziph_team = new WP_Query( $args );

    if ($ziph_team->have_posts()) :
    ?>

    <div class="ziph-team_area <?php echo esc_attr( $styled_class .' '. $class ); ?>"> <!-- Testimonial Starts -->
      <div class="row">
        <?php
          while ($ziph_team->have_posts()) : $ziph_team->the_post();

          // Get Meta Box Options - zipprich_framework_active()
          $team_options = get_post_meta( get_the_ID(), 'team_options', true );
          $team_pro  = $team_options['team_job_position'];
          $team_social  = $team_options['team_social'];
          $open_link  = $team_options['open_link'];

          // Featured Image
          $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
          $large_image = $large_image[0];
          $large_image_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);

          if(class_exists('Aq_Resize')) {
            $testi_img = aq_resize( $large_image, '270', '280', true );
          } else {$testi_img = $large_image;}
          if($large_image) {
            if($testi_img) {
              $actual_image = '<img src="'. esc_url($testi_img) .'" alt="'.esc_attr( $large_image_alt ).'">';
            } else {
              $actual_image = '<img src="'. esc_url($large_image) .'" alt="'.esc_attr( $large_image_alt ).'">';
            }
          } else {
            $actual_image = '';
          }

          $open_link = $open_link ? ' target="_blank"' : '';
        ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class('col-md-3 col-sm-6'); ?>>
          <div class="ziph-team_single">
            <!-- image and social start\-->
            <div class="ziph-fix ziph-team_media">
                <?php echo $actual_image; ?>
                <div class="text-center  ziph-team_socail">
                    <ul class="list-inline">
                      <?php if(isset($team_social)){ foreach ($team_social as $social) {
                        echo '<li><a href="'.esc_url($social['link']).'" '.$open_link.'><i class="'.esc_attr($social['icon']).'"></i></a></li>';
                      } } ?>
                    </ul>
                </div>
            </div> <!--/end-->

            <!-- info text start\-->
            <div class="text-center  ziph-team_info">
                <h5 class="ziph-tm_name"><span class="team-name"><?php the_title(); ?></span></h5>
                <h5 class="ziph-tm_intro"><span class="team-pro"><?php echo esc_attr($team_pro); ?></span></h5>
            </div> <!--/end-->
          </div>
        </div>
        <?php
          endwhile;
          wp_reset_postdata();
        ?>
      </div>
    </div> <!-- Testimonial End -->

<?php
    endif;

    // Return outbut buffer
    return ob_get_clean();

  }
}
add_shortcode( 'ziph_team', 'ziph_team_function' );