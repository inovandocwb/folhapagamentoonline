<?php
/* Testimonial Carousel */
if ( !function_exists('ziph_testimonial_carousel_function')) {
  function ziph_testimonial_carousel_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'testimonial_id'  => '',
      'class'  => '',

      // Listing
      'testimonial_limit'  => '',
      'testimonial_order'  => '',
      'testimonial_orderby'  => '',

      // Carousel
      'carousel_loop'  => '',
      'carousel_items'  => '',
      'carousel_margin'  => '',
      'carousel_dots'  => '',
      'carousel_nav'  => '',
      'carousel_autoplay_timeout'  => '',
      'carousel_autoplay'  => '',
      'carousel_animate_out'  => '',
      'carousel_mousedrag'  => '',
      'carousel_autowidth'  => '',
      'carousel_autoheight'  => '',
      'carousel_tablet'  => '',
      'carousel_mobile'  => '',
      'carousel_small_mobile'  => '',

      // Color & Style
      'content_color'  => '',
      'name_color'  => '',
      'profession_color'  => '',
      'content_size'  => '',
      'name_size'  => '',
      'profession_size'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    // Content Color
    if ( $content_color || $content_size ) {
      $inline_style .= '.ziph-single_tetimnal-'. $e_uniqid .' .ziph-tetimnal_txt p {';
      $inline_style .= ( $content_color ) ? 'color:'. $content_color .';' : '';
      $inline_style .= ( $content_size ) ? 'font-size:'. $content_size .';' : '';
      $inline_style .= '}';
    }
    // Name Color
    if ( $name_color || $name_size ) {
      $inline_style .= '.ziph-single_tetimnal-'. $e_uniqid .' .ziph-tetimnal_txt .testi-name {';
      $inline_style .= ( $name_color ) ? 'color:'. $name_color .';' : '';
      $inline_style .= ( $name_size ) ? 'font-size:'. $name_size .';' : '';
      $inline_style .= '}';
    }
    // Profession Color
    if ( $profession_color || $profession_size ) {
      $inline_style .= '.ziph-single_tetimnal-'. $e_uniqid .' .ziph-tetimnal_txt .testi-pro {';
      $inline_style .= ( $profession_color ) ? 'color:'. $profession_color .';' : '';
      $inline_style .= ( $profession_size ) ? 'font-size:'. $profession_size .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-single_tetimnal-'. $e_uniqid;

    // Carousel Data's
    $carousel_loop = $carousel_loop !== 'true' ? 'data-loop="true"' : 'data-loop="false"';
    $carousel_items = $carousel_items ? 'data-items="'. $carousel_items .'"' : 'data-items="1"';
    $carousel_margin = $carousel_margin ? 'data-margin="'. $carousel_margin .'"' : 'data-margin="0"';
    $carousel_dots = $carousel_dots ? 'data-dots="'. $carousel_dots .'"' : 'data-dots="true"';
    $carousel_nav = $carousel_nav ? 'data-nav="'. $carousel_nav .'"' : 'data-nav="false"';
    $carousel_autoplay_timeout = $carousel_autoplay_timeout ? ' data-autoplay-timeout="'. $carousel_autoplay_timeout .'"' : '';
    $carousel_autoplay = $carousel_autoplay ? ' data-autoplay="'. $carousel_autoplay .'"' : '';
    $carousel_animate_out = $carousel_animate_out ? 'data-animateout="'. $carousel_animate_out .'"' : '';
    $carousel_mousedrag = $carousel_mousedrag !== 'true' ? 'data-mouse-drag="true"' : 'data-mouse-drag="false"';
    $carousel_autowidth = $carousel_autowidth ? 'data-auto-width="'. $carousel_autowidth .'"' : '';
    $carousel_autoheight = $carousel_autoheight ? 'data-auto-height="'. $carousel_autoheight .'"' : '';
    $carousel_tablet = $carousel_tablet ? 'data-items-tablet="'. $carousel_tablet .'"' : '';
    $carousel_mobile = $carousel_mobile ? 'data-items-mobile-landscape="'. $carousel_mobile .'"' : '';
    $carousel_small_mobile = $carousel_small_mobile ? 'data-items-mobile-portrait="'. $carousel_small_mobile .'"' : '';

    // Turn output buffer on
    ob_start();

    // Query Starts Here
    // Pagination
    global $paged;
    if( get_query_var( 'paged' ) )
      $my_page = get_query_var( 'paged' );
    else {
      if( get_query_var( 'page' ) )
        $my_page = get_query_var( 'page' );
      else
        $my_page = 1;
      set_query_var( 'paged', $my_page );
      $paged = $my_page;
    }

    $args = array(
      'paged' => $my_page,
      'post_type' => 'testimonial',
      'posts_per_page' => (int)$testimonial_limit,
      'order' => $testimonial_order
    );

    if(!empty($testimonial_id)){
      $integerIDs = array_map('intval', explode(',', $testimonial_id));
      $args['post__in'] = $integerIDs;
      $args['orderby'] = 'post__in';
    } else {
      $args['orderby'] = $testimonial_orderby;
    }

    $ziph_testi = new WP_Query( $args );

    if ($ziph_testi->have_posts()) :
    ?>
    <section class="ziph-testimonial_area <?php echo esc_attr( $styled_class . ' ' . $class ); ?>">
      <div class="ziph-fix ziph-testimonial_warp">
          <div class="owl-carousel ziph-testimonial_carousel" <?php echo $carousel_loop .' '. $carousel_items .' '. $carousel_margin .' '. $carousel_dots .' '. $carousel_nav .' '. $carousel_autoplay_timeout .' '. $carousel_autoplay .' '. $carousel_animate_out .' '. $carousel_mousedrag .' '. $carousel_autowidth .' '. $carousel_autoheight .' '. $carousel_tablet .' '. $carousel_mobile .' '. $carousel_small_mobile; ?>> <!-- Testimonial Starts -->

          <?php
          while ($ziph_testi->have_posts()) : $ziph_testi->the_post();

          // Get Meta Box Options - zipprich_framework_active()
          $testimonial_options = get_post_meta( get_the_ID(), 'testimonial_options', true );
          $client_name = $testimonial_options['testi_name'];
          $name_link = $testimonial_options['testi_name_link'];
          $client_pro = $testimonial_options['testi_pro'];
          $pro_link = $testimonial_options['testi_pro_link'];

          $name_tag = $name_link ? 'a' : 'span';
          $name_link = $name_link ? 'href="'. esc_url($name_link) .'" target="_blank"' : '';
          $client_name = $client_name ? '<'. $name_tag .' '. $name_link .' class="testi-name">'. esc_attr($client_name) .'</'. $name_tag .'>' : '';

          $pro_tag = $pro_link ? 'a' : 'span';
          $pro_link = $pro_link ? 'href="'. esc_url($pro_link) .'" target="_blank"' : '';
          $client_pro = $client_pro ? '<'. $pro_tag .' '. $pro_link .' class="testi-pro">'. esc_attr($client_pro) .'</'. $pro_tag .'>' : '';

          // Featured Image
          $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
          $large_image = $large_image[0];
          $large_image_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);

          if(class_exists('Aq_Resize')) {
            $testi_img = aq_resize( $large_image, '155', '155', true );
          } else {$testi_img = $large_image;}

          $actual_image = $large_image ? '<'. $name_tag .' '. $name_link .'><img src="'. esc_url($testi_img) .'" alt="'.esc_attr( $large_image_alt ).'"></'. $name_tag .'>' : '';
          ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class('ziph-single_tetimnal'); ?>>
              <div class="ziph-fix ziph-tetimnal_warp">
                <div class="ziph-fix ziph-flt_left ziph-tetimnal_avatr">
                    <?php echo $actual_image; ?>
                </div>
                <div class="ziph-fix ziph-tetimnal_txt">
                    <?php the_content(); ?>
                    <h4 class="ziph-tstimnl_name"><?php echo $client_name; ?></h4>
                    <h5 class="ziph-tstimnl_bio"><?php echo $client_pro; ?></h5>
                </div>
              </div>
            </div>
          <?php
          endwhile;
          wp_reset_postdata();
          ?>

      </div>
    </div> <!-- Testimonial End -->
  </section> <!-- Testimonial End -->

<?php
  endif;

    // Return outbut buffer
    return ob_get_clean();

  }
}
add_shortcode( 'ziph_testimonial_carousel', 'ziph_testimonial_carousel_function' );