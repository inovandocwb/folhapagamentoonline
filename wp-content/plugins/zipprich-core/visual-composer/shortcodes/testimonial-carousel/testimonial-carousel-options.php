<?php
/**
 * Testimonial Carousel - Shortcode Options
 */
add_action( 'init', 'testimonial_carousel_vc_map' );
if ( ! function_exists( 'testimonial_carousel_vc_map' ) ) {
  function testimonial_carousel_vc_map() {
    vc_map( array(
    "name" => __( "Testimonial Carousel", 'zipprich-core'),
    "base" => "ziph_testimonial_carousel",
    "description" => __( "Carousel Style Testimonial", 'zipprich-core'),
    "icon" => "fa fa-comments color-green",
    "category" => ZipprichLib::ziph_cat_name(),
    "params" => array(

      array(
        "type"        =>'textfield',
        "heading"     =>__('Show from Specific IDs', 'zipprich-core'),
        "param_name"  => "testimonial_id",
        "value"       => "",
        "description" => __( "Enter comma seperated testimonial ids to show.", 'zipprich-core'),
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Limit', 'zipprich-core'),
        "param_name"  => "testimonial_limit",
        "value"       => "",
        'admin_label' => true,
        "description" => __( "Enter the number of items to show.", 'zipprich-core'),
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Order', 'zipprich-core' ),
        'value' => array(
          __( 'Select Testimonial Order', 'zipprich-core' ) => '',
          __('Asending', 'zipprich-core') => 'ASC',
          __('Desending', 'zipprich-core') => 'DESC',
        ),
        'param_name' => 'testimonial_order',
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Order By', 'zipprich-core' ),
        'value' => array(
          __('None', 'zipprich-core') => 'none',
          __('ID', 'zipprich-core') => 'ID',
          __('Author', 'zipprich-core') => 'author',
          __('Title', 'zipprich-core') => 'title',
          __('Date', 'zipprich-core') => 'date',
        ),
        'param_name' => 'testimonial_orderby',
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      ZipprichLib::vt_class_option(),

      // Carousel
      ZipprichLib::vt_notice_field(__( "Basic Options", 'zipprich-core' ),'bsic_opt','cs-warning', 'Carousel'), // Notice
      ZipprichLib::vt_carousel_loop(), // Loop
      ZipprichLib::vt_carousel_items(), // Items
      ZipprichLib::vt_carousel_margin(), // Margin
      ZipprichLib::vt_carousel_dots(), // Dots
      ZipprichLib::vt_carousel_nav(), // Nav
      ZipprichLib::vt_notice_field(__( "Auto Play & Interaction", 'zipprich-core' ),'apyi_opt','cs-warning', 'Carousel'), // Notice
      ZipprichLib::vt_carousel_autoplay_timeout(), // Autoplay Timeout
      ZipprichLib::vt_carousel_autoplay(), // Autoplay
      ZipprichLib::vt_carousel_animateout(), // Animate Out
      ZipprichLib::vt_carousel_mousedrag(), // Mouse Drag
      ZipprichLib::vt_notice_field(__( "Width & Height", 'zipprich-core' ),'wah_opt','cs-warning', 'Carousel'), // Notice
      ZipprichLib::vt_carousel_autowidth(), // Auto Width
      ZipprichLib::vt_carousel_autoheight(), // Auto Height
      ZipprichLib::vt_notice_field('Responsive Options','res_opt','cs-warning', 'Carousel'), // Notice
      ZipprichLib::vt_carousel_tablet(), // Tablet
      ZipprichLib::vt_carousel_mobile(), // Mobile
      ZipprichLib::vt_carousel_small_mobile(), // Small Mobile

      // Style
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Content Color', 'zipprich-core'),
        "param_name"  => "content_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
      ),
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Name Color', 'zipprich-core'),
        "param_name"  => "name_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
      ),
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Profession Color', 'zipprich-core'),
        "param_name"  => "profession_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
      ),
      // Size
      array(
        "type"        =>'textfield',
        "heading"     =>__('Content Size', 'zipprich-core'),
        "param_name"  => "content_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Name Size', 'zipprich-core'),
        "param_name"  => "name_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Profession Size', 'zipprich-core'),
        "param_name"  => "profession_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
      ),

      ), // Params
    ) );
  }
}