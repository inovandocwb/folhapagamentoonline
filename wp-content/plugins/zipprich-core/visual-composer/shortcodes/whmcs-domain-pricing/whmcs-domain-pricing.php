<?php
/* Domain Pricing */
if ( !function_exists('ziph_whmcs_domain_pricing_function')) {
  function ziph_whmcs_domain_pricing_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'class'  => '',
    ), $atts));

    if(function_exists('cc_whmcs_bridge_mainpage')){
      $whmcs_page_link = get_permalink(cc_whmcs_bridge_mainpage());
    } else {
      $whmcs_page_link = '';
    }

    // Output
    $output   = '<div class="whmpress whmpress_price_matrix_domain">';
    $output   .= '<form method="get" action="'.$whmcs_page_link.'"><label>'.esc_html__( 'Search', 'zipprich-core' ).'</label><input type="hidden" name="ccce" value="cart"><input type="hidden" name="a" value="add"><input type="hidden" name="domain" value="register"><input type="hidden" name="systpl" value="portal"><input style="width:50%" type="search" id="whmcs_domain_search" name="query" placeholder="'. esc_html__( 'Type Extension to search a domain', 'zipprich-core' ).'"></form>';
    $output   .= '<div class="table-responsive ziph-affilte_table ziph-affilte_table_whmcs '. esc_attr($class) .'">';
    $output  .= do_shortcode('[ziph_whmcs_domain_pricing_info_wrap]' . $content . '[/ziph_whmcs_domain_pricing_info_wrap]');
    $output  .= '</div></div>';
    return $output;

  }
}
add_shortcode( 'ziph_whmcs_domain_pricing', 'ziph_whmcs_domain_pricing_function' );

/* Domain Pricing Info */
if ( !class_exists('Ziph_Whmcs_Domain_Pricing_Info')) {
  class Ziph_Whmcs_Domain_Pricing_Info{

    public function __construct(){
      add_shortcode( 'ziph_whmcs_domain_pricing_info_wrap', array( $this, 'ziph_whmcs_domain_pricing_info_wrap_function') );
      add_shortcode( 'ziph_whmcs_domain_pricing_title', array( $this, 'ziph_whmcs_domain_pricing_title_function') );
      add_shortcode( 'ziph_whmcs_domain_pricing_info', array( $this, 'ziph_whmcs_domain_pricing_info_function') );
    }

    public function ziph_whmcs_domain_pricing_info_wrap_function( $atts, $content = true ) {

      // Output
      $output   = '<table class="table table-bordered">';
      $output  .= do_shortcode($content);
      $output  .= '</table>';
      return $output;

    }

    public function ziph_whmcs_domain_pricing_title_function( $atts, $content = true ) {

      extract(shortcode_atts(array(
        'title'  => '',
        'plan_titles'  => '',
        'class'  => '',
        // Style
        'title_color'  => '',
        'title_size'  => '',
      ), $atts));

      // Shortcode Style CSS
      $e_uniqid        = uniqid();
      $inline_style  = '';

      if ( $title_color || $title_size ) {
        $inline_style .= '.ziph-table-title-'. $e_uniqid .' > tr > th {';
        $inline_style .= ( $title_color ) ? 'color:'. $title_color .' !important;' : '';
        $inline_style .= ( $title_size ) ? 'font-size:'. zipprich_core_check_px($title_size) .' !important;' : '';
        $inline_style .= '}';
      }

      // add inline style
      zipprich_add_inline_style( $inline_style );
      $styled_class  = ' ziph-table-title-'. $e_uniqid;

      // Group Field
      $plan_titles = (array) vc_param_group_parse_atts( $plan_titles );
      $get_each_list = array();
      foreach ( $plan_titles as $plan_title ) {
        $each_list = $plan_title;
        $each_list['title'] = isset( $plan_title['title'] ) ? $plan_title['title'] : '';
        $get_each_list[] = $each_list;
      }

      // Output
      $output  = '<thead class="'.esc_attr( $class . ' ' . $styled_class ).'"><tr>';
      $output  .= '<th>'.$title.'</th> ';
      foreach ($get_each_list as $each_list) {
        $output  .= '<th>'.esc_attr($each_list['title']).'</th> ';
      }
      $output  .= '</tr></thead>';
      return  $output;

    }

    public function ziph_whmcs_domain_pricing_info_function( $atts, $content = true ) {

      extract(shortcode_atts(array(
        'title'  => '',
        'plan_prices'  => '',
      ), $atts));

      // Group Field
      $plan_prices = (array) vc_param_group_parse_atts( $plan_prices );
      $get_each_list = array();
      foreach ( $plan_prices as $plan_price ) {
        $each_list = $plan_price;
        $each_list['price'] = isset( $plan_price['price'] ) ? $plan_price['price'] : '';
        $get_each_list[] = $each_list;
      }

      // Output
      $output  = '<tbody><tr>';
      $output  .= '<td><h4>'.esc_attr($title).'</h4></td>';
      foreach ($get_each_list as $each_list) {
        $output  .= '<td>'.esc_attr($each_list['price']).'</td>';
      }
      $output  .= '</tr></tbody>';
      return  $output;

    }

  }
}
new Ziph_Whmcs_Domain_Pricing_Info();