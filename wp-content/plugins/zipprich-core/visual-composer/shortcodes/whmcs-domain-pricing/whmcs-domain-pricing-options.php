<?php
/**
 * WHMCS Domain Pricing - Shortcode Options
 */
add_action( 'init', 'ziph_whmcs_domain_pricing_vc_map' );
if ( ! function_exists( 'ziph_whmcs_domain_pricing_vc_map' ) ) {
 function ziph_whmcs_domain_pricing_vc_map() {
   vc_map( array(
     "name" => __( "WHMCS Domain Pricing Chart", 'zipprich-core'),
     "base" => "ziph_whmcs_domain_pricing",
     "description" => __( "WHMCS Domain Chart Group", 'zipprich-core'),
     "as_parent" => array('only' => 'ziph_whmcs_domain_pricing_title,ziph_whmcs_domain_pricing_info'),
     "content_element" => true,
     "show_settings_on_create" => false,
     "is_container" => true,
     "icon" => "fa fa-globe color-brown",
     "category" => ZipprichLib::ziph_cat_name(),
     "params" => array(

        ZipprichLib::vt_class_option(),

     ),
     "js_view" => 'VcColumnView'
   ) );
 }
}

// Call to Action List
add_action( 'init', 'ziph_whmcs_domain_pricing_title_vc_map' );
if ( ! function_exists( 'ziph_whmcs_domain_pricing_title_vc_map' ) ) {
  function ziph_whmcs_domain_pricing_title_vc_map() {
    vc_map( array(
      "name" => __( "WHMCS Domain Chart - Titles", 'zipprich-core'),
      "base" => "ziph_whmcs_domain_pricing_title",
      "description" => __( "WHMCS Domain Chart Titles Setup", 'zipprich-core'),
      "icon" => "fa fa-arrows-h color-blue",
      "as_child" => array('only' => 'ziph_whmcs_domain_pricing'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Table Chart Title', 'zipprich-core' ),
          'param_name' => 'title',
          'admin_label' => true,
        ),    
        
        // List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'WHMCS Domain Titles', 'zipprich-core' ),
          'param_name' => 'plan_titles',
          // Note params is mapped inside param-group:
          'params' => array(
            
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Title', 'zipprich-core' ),
              'param_name' => 'title',
              'admin_label' => true
            ),

          )
        ),
        ZipprichLib::vt_class_option(),


        // Style
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Text Color', 'zipprich-core' ),
          'param_name' => 'title_color',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Text Size', 'zipprich-core' ),
          'param_name' => 'title_size',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),    
      )
    ) );
  }
}

// Call to Action List
add_action( 'init', 'ziph_whmcs_domain_pricing_info_vc_map' );
if ( ! function_exists( 'ziph_whmcs_domain_pricing_info_vc_map' ) ) {
  function ziph_whmcs_domain_pricing_info_vc_map() {
    vc_map( array(
      "name" => __( "WHMCS Domain Chart - Infos", 'zipprich-core'),
      "base" => "ziph_whmcs_domain_pricing_info",
      "description" => __( "WHMCS Domain Chart Details", 'zipprich-core'),
      "icon" => "fa fa-arrows-v color-green",
      "as_child" => array('only' => 'ziph_whmcs_domain_pricing'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'WHMCS Domain Title', 'zipprich-core' ),
          'param_name' => 'title',
          'admin_label' => true
        ),
        
        // List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'WHMCS Domain Prices', 'zipprich-core' ),
          'param_name' => 'plan_prices',
          // Note params is mapped inside param-group:
          'params' => array(
            
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Price/Info', 'zipprich-core' ),
              'param_name' => 'price',
              'admin_label' => true
            ),

          )
        ),
        ZipprichLib::vt_class_option(),

      )
    ) );
  }
}