<?php
/* Video Text */
if ( !function_exists('ziph_video_text_function')) {
  function ziph_video_text_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title_color'  => '',
      'text_color'  => '',
      'class'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Text Color
    if ( $title_color ) {
      $inline_style .= '.ziph-vds_text-'. $e_uniqid .' h1, .ziph-vds_text-'. $e_uniqid .' h2, .ziph-vds_text-'. $e_uniqid .' h3 {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= '}';
    }
    if ( $text_color ) {
      $inline_style .= '.ziph-vds_text-'. $e_uniqid .' p {';
      $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-vds_text-'. $e_uniqid;

    // Output
    $output   = '<div class="ziph-vds_text '. $class . $styled_class .'">';
    $output  .= do_shortcode($content);
    $output  .= '</div>';
    return $output;

  }
}
add_shortcode( 'ziph_video_text', 'ziph_video_text_function' );