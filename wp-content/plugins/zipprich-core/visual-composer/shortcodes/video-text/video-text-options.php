<?php
/**
 * Video Text - Shortcode Options
 */
add_action( 'init', 'ziph_video_text_vc_map' );
if ( ! function_exists( 'ziph_video_text_vc_map' ) ) {
 function ziph_video_text_vc_map() {
   vc_map( array(
     "name" => __( "Video Text", 'zipprich-core'),
     "base" => "ziph_video_text",
     "description" => __( "Video Text Section", 'zipprich-core'),
     "as_parent" => array('only' => 'vc_column_text,ziph_button'),
     "content_element" => true,
     "show_settings_on_create" => false,
     "is_container" => true,
     "icon" => "fa fa-file-video-o color-green",
     "category" => ZipprichLib::ziph_cat_name(),
     "params" => array(
        array(
          "type" => "colorpicker",
          "heading" => __( "Title Color", 'zipprich-core' ),
          "param_name" => "title_color",
          'value' => '',
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Paragraph Color", 'zipprich-core' ),
          "param_name" => "text_color",
          'value' => '',
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        ZipprichLib::vt_class_option(),

     ),
     "js_view" => 'VcColumnView'
   ) );
 }
}