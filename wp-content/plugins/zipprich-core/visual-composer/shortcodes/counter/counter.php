<?php
/* ==========================================================
  Partner
=========================================================== */
if ( !function_exists('ziph_counter_function')) {
  function ziph_counter_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'counter_title'  => '',
      'counter_value'  => '',
      'counter_value_in'  => '',
      'class'  => '',

      // Style
      'counter_title_color'  => '',
      'counter_value_color'  => '',
      'counter_value_in_color'  => '',
      'counter_title_size'  => '',
      'counter_value_size'  => '',
      'counter_value_in_size'  => '',
    ), $atts));

    // Style
    $counter_title_color = $counter_title_color ? 'color:'. $counter_title_color .';' : '';
    $counter_value_color = $counter_value_color ? 'color:'. $counter_value_color .';' : '';
    $counter_value_in_color = $counter_value_in_color ? 'color:'. $counter_value_in_color .';' : '';
    // Size
    $counter_title_size = $counter_title_size ? 'font-size:'. $counter_title_size .';' : '';
    $counter_value_size = $counter_value_size ? 'font-size:'. $counter_value_size .';' : '';
    $counter_value_in_size = $counter_value_in_size ? 'font-size:'. $counter_value_in_size .';' : '';

    // Counter Title
    $counter_title = $counter_title ? '<span style="'. $counter_title_color . $counter_title_size .'">'. $counter_title .'</span>' : '';

    // Value
    $counter_value = $counter_value ? '<span style="'. $counter_value_color . $counter_value_size .'">'. $counter_value .'</span>' : '';

    // Value In
    $counter_value_in = $counter_value_in ? '<span style="'. $counter_value_in_color . $counter_value_in_size .'">'. $counter_value_in .'</span>' : '';

    // Counters
      $output = '<div class="text-center ziph-counter_single '. $class .'"><h2><span class="ziph-counter">'. $counter_value .'</span>' . $counter_value_in .'</h2><p>'. $counter_title .'</p></div>';

    // Output
    return $output;

  }
}
add_shortcode( 'ziph_counter', 'ziph_counter_function' );
