<?php
/**
 * Counter - Shortcode Options
 */
add_action( 'init', 'ziph_counter_vc_map' );
if ( ! function_exists( 'ziph_counter_vc_map' ) ) {
  function ziph_counter_vc_map() {
    vc_map( array(
      "name" => __( "Counter", 'zipprich-core'),
      "base" => "ziph_counter",
      "description" => __( "Counter Styles", 'zipprich-core'),
      "icon" => "fa fa-sort-numeric-asc color-blue",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          "type"        =>'textfield',
          "heading"     =>__('Title', 'zipprich-core'),
          "param_name"  => "counter_title",
          "value"       => "",
          "admin_label"  => true,
          "description" => __( "Enter your counter title.", 'zipprich-core')
        ),
        array(
          "type"        =>'textfield',
          "heading"     =>__('Counter Value', 'zipprich-core'),
          "param_name"  => "counter_value",
          "value"       => "",
          "admin_label"  => true,
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          "description" => __( "Enter numeric value to count. Ex : 20", 'zipprich-core')
        ),
        array(
          "type"        =>'textfield',
          "heading"     =>__('Value In', 'zipprich-core'),
          "param_name"  => "counter_value_in",
          "value"       => "",
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          "description" => __( "Enter value in symbol or text. Eg : +", 'zipprich-core')
        ),
        ZipprichLib::vt_class_option(),

        // Stylings
        array(
          "type"        => 'colorpicker',
          "heading"     => __('Title Color', 'zipprich-core'),
          "param_name"  => "counter_title_color",
          "value"       => "",
          'edit_field_class'   => 'vc_col-md-4 vt_field_space',
          "group"       => __('Style', 'zipprich-core'),
        ),
        array(
          "type"        => 'colorpicker',
          "heading"     => __('Counter Color', 'zipprich-core'),
          "param_name"  => "counter_value_color",
          "value"       => "",
          'edit_field_class'   => 'vc_col-md-4 vt_field_space',
          "group"       => __('Style', 'zipprich-core'),
        ),
        array(
          "type"        => 'colorpicker',
          "heading"     => __('Value In Color', 'zipprich-core'),
          "param_name"  => "counter_value_in_color",
          "value"       => "",
          'edit_field_class'   => 'vc_col-md-4 vt_field_space',
          "group"       => __('Style', 'zipprich-core'),
        ),
        // Size
        array(
          "type"        => 'textfield',
          "heading"     => __('Title Size', 'zipprich-core'),
          "param_name"  => "counter_title_size",
          "value"       => "",
          'edit_field_class'   => 'vc_col-md-4 vt_field_space',
          "group"       => __('Style', 'zipprich-core'),
          "description" => __( "Enter font size in px.", 'zipprich-core')
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Counter Size', 'zipprich-core'),
          "param_name"  => "counter_value_size",
          "value"       => "",
          'edit_field_class'   => 'vc_col-md-4 vt_field_space',
          "group"       => __('Style', 'zipprich-core'),
          "description" => __( "Enter font size in px.", 'zipprich-core')
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Value In Size', 'zipprich-core'),
          "param_name"  => "counter_value_in_size",
          "value"       => "",
          'edit_field_class'   => 'vc_col-md-4 vt_field_space',
          "group"       => __('Style', 'zipprich-core'),
          "description" => __( "Enter font size in px.", 'zipprich-core')
        ),

      )
    ) );
  }
}
