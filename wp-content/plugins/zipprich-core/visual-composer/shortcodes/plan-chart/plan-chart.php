<?php
/* Plan Chart */
if ( !function_exists('ziph_plan_chart_function')) {
  function ziph_plan_chart_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'class'  => '',
    ), $atts));

    // Output
    $output   = '<section class="ziph-affiltearnup_area '. $class .'"><div class="table-responsive ziph-affilte_table">';
    $output  .= do_shortcode('[ziph_plan_chart_info_wrap]' . $content . '[/ziph_plan_chart_info_wrap]');
    $output  .= '</div></section>';
    return $output;

  }
}
add_shortcode( 'ziph_plan_chart', 'ziph_plan_chart_function' );

/* Call to Action */
if ( !class_exists('Ziph_Plan_Chart_Info')) {
  class Ziph_Plan_Chart_Info{

    public function __construct(){
      add_shortcode( 'ziph_plan_chart_info_wrap', array( $this, 'ziph_plan_chart_info_wrap_function') );
      add_shortcode( 'ziph_plan_chart_title', array( $this, 'ziph_plan_chart_title_function') );
      add_shortcode( 'ziph_plan_chart_info', array( $this, 'ziph_plan_chart_info_function') );
    }

    public function ziph_plan_chart_info_wrap_function( $atts, $content = true ) {

      // Output
      $output   = '<table class="table table-bordered">';
      $output  .= do_shortcode($content);
      $output  .= '</table>';
      return $output;

    }

    public function ziph_plan_chart_title_function( $atts, $content = true ) {

      extract(shortcode_atts(array(
        'title'  => '',
        'plan_titles'  => '',
        'class'  => '',
        // Style
        'title_color'  => '',
        'title_size'  => '',
        'head_bg_color'  => '',
      ), $atts));

      // Shortcode Style CSS
      $e_uniqid        = uniqid();
      $inline_style  = '';

      if ( $title_color || $title_size || $head_bg_color ) {
        $inline_style .= '.ziph-table-title-'. $e_uniqid .' > tr > th {';
        $inline_style .= ( $title_color ) ? 'color:'. $title_color .' !important;' : '';
        $inline_style .= ( $head_bg_color ) ? 'background-color:'. $head_bg_color .' !important;' : '';
        $inline_style .= ( $title_size ) ? 'font-size:'. zipprich_core_check_px($title_size) .' !important;' : '';
        $inline_style .= '}';
      }

      // add inline style
      zipprich_add_inline_style( $inline_style );
      $styled_class  = ' ziph-table-title-'. $e_uniqid;

      // Group Field
      $plan_titles = (array) vc_param_group_parse_atts( $plan_titles );
      $get_each_list = array();
      foreach ( $plan_titles as $plan_title ) {
        $each_list = $plan_title;
        $each_list['title'] = isset( $plan_title['title'] ) ? $plan_title['title'] : '';
        $get_each_list[] = $each_list;
      }

      // Output
      $output  = '<thead class="'.esc_attr( $class . ' ' . $styled_class ).'"><tr>';
      $output  .= '<th>'.$title.'</th> ';
      foreach ($get_each_list as $each_list) {
        $output  .= '<th>'.$each_list['title'].'</th> ';
      }
      $output  .= '</tr></thead>';
      return  $output;

    }

    public function ziph_plan_chart_info_function( $atts, $content = true ) {

      extract(shortcode_atts(array(
        'title'  => '',
        'subtitle'  => '',
        'plan_prices'  => '',
      ), $atts));

      // Group Field
      $plan_prices = (array) vc_param_group_parse_atts( $plan_prices );
      $get_each_list = array();
      foreach ( $plan_prices as $plan_price ) {
        $each_list = $plan_price;
        $each_list['price'] = isset( $plan_price['price'] ) ? $plan_price['price'] : '';
        $get_each_list[] = $each_list;
      }

      // Output
      $output  = '<tbody><tr>';
      $output  .= '<td><h4>'.$title.'</h4><p>'.$subtitle.'</p></td> ';
      foreach ($get_each_list as $each_list) {
        $output  .= '<td>'.$each_list['price'].'</td> ';
      }
      $output  .= '</tr></tbody>';
      return  $output;

    }

  }
}
new Ziph_Plan_Chart_Info();