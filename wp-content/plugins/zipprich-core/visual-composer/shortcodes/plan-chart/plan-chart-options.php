<?php
/**
 * Plan Chart - Shortcode Options
 */
add_action( 'init', 'ziph_plan_chart_vc_map' );
if ( ! function_exists( 'ziph_plan_chart_vc_map' ) ) {
 function ziph_plan_chart_vc_map() {
   vc_map( array(
     "name" => __( "Plan Chart - Affiliate ", 'zipprich-core'),
     "base" => "ziph_plan_chart",
     "description" => __( "Affiliate Plan Chart Group", 'zipprich-core'),
     "as_parent" => array('only' => 'ziph_plan_chart_title,ziph_plan_chart_info'),
     "content_element" => true,
     "show_settings_on_create" => false,
     "is_container" => true,
     "icon" => "fa fa-outdent color-brown",
     "category" => ZipprichLib::ziph_cat_name(),
     "params" => array(

        ZipprichLib::vt_class_option(),

     ),
     "js_view" => 'VcColumnView'
   ) );
 }
}

// Call to Action List
add_action( 'init', 'ziph_plan_chart_title_vc_map' );
if ( ! function_exists( 'ziph_plan_chart_title_vc_map' ) ) {
  function ziph_plan_chart_title_vc_map() {
    vc_map( array(
      "name" => __( "Plan Chart - Titles", 'zipprich-core'),
      "base" => "ziph_plan_chart_title",
      "description" => __( "Plan Chart Titles Setup", 'zipprich-core'),
      "icon" => "fa fa-arrows-h color-blue",
      "as_child" => array('only' => 'ziph_plan_chart'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Table Chart Title', 'zipprich-core' ),
          'param_name' => 'title',
          'admin_label' => true,
        ),    
        
        // List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Plan Titles', 'zipprich-core' ),
          'param_name' => 'plan_titles',
          // Note params is mapped inside param-group:
          'params' => array(
            
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Title', 'zipprich-core' ),
              'param_name' => 'title',
              'admin_label' => true
            ),

          )
        ),
        ZipprichLib::vt_class_option(),


        // Style
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Text Color', 'zipprich-core' ),
          'param_name' => 'title_color',
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Background Color', 'zipprich-core' ),
          'param_name' => 'head_bg_color',
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Text Size', 'zipprich-core' ),
          'param_name' => 'title_size',
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),    
      )
    ) );
  }
}

// Call to Action List
add_action( 'init', 'ziph_plan_chart_info_vc_map' );
if ( ! function_exists( 'ziph_plan_chart_info_vc_map' ) ) {
  function ziph_plan_chart_info_vc_map() {
    vc_map( array(
      "name" => __( "Plan Chart - Infos", 'zipprich-core'),
      "base" => "ziph_plan_chart_info",
      "description" => __( "Plan Chart Details", 'zipprich-core'),
      "icon" => "fa fa-arrows-v color-green",
      "as_child" => array('only' => 'ziph_plan_chart'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Offer Title', 'zipprich-core' ),
          'param_name' => 'title',
          'admin_label' => true
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Offer Subtitle', 'zipprich-core' ),
          'param_name' => 'subtitle'
        ),
        
        // List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Plan Prices', 'zipprich-core' ),
          'param_name' => 'plan_prices',
          // Note params is mapped inside param-group:
          'params' => array(
            
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Price', 'zipprich-core' ),
              'param_name' => 'price',
              'admin_label' => true
            ),

          )
        ),
        ZipprichLib::vt_class_option(),

      )
    ) );
  }
}