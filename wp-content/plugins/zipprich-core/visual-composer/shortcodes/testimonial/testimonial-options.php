<?php
/**
 * Testimonial - Shortcode Options
 */
add_action( 'init', 'testimonial_vc_map' );
if ( ! function_exists( 'testimonial_vc_map' ) ) {
  function testimonial_vc_map() {
    vc_map( array(
    "name" => __( "Testimonial", 'zipprich-core'),
    "base" => "ziph_testimonial",
    "description" => __( "Testimonial Style", 'zipprich-core'),
    "icon" => "fa fa-comment color-grey",
    "category" => ZipprichLib::ziph_cat_name(),
    "params" => array(
      
      array(
        "type"        => "notice",
        "heading"     => __( "This addon to show single testimonial from sprecific id. You may use multiple", 'zipprich-core' ),
        "param_name"  => 'lsng_opt',
        'class'       => 'cs-warning',
        'value'       => '',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Title', 'zipprich-core'),
        "param_name"  => "title",
        "value"       => "",
        "admin_label" => true,
        "description" => __( "Enter section title.", 'zipprich-core'),
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Testimonial ID', 'zipprich-core'),
        "param_name"  => "testimonial_id",
        "value"       => "",
        "description" => __( "Enter single[Recommnded]/comma seperated testimonial id to show.", 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Limit', 'zipprich-core'),
        "param_name"  => "testimonial_limit",
        "value"       => "",
        "description" => __( "Enter the number of items to show.", 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      
      ZipprichLib::vt_class_option(),

      // Style
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Title Color', 'zipprich-core'),
        "param_name"  => "title_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Content Color', 'zipprich-core'),
        "param_name"  => "content_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Name Color', 'zipprich-core'),
        "param_name"  => "name_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'colorpicker',
        "heading"     =>__('Profession Color', 'zipprich-core'),
        "param_name"  => "profession_color",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      // Size
      array(
        "type"        =>'textfield',
        "heading"     =>__('Title Size', 'zipprich-core'),
        "param_name"  => "title_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Content Size', 'zipprich-core'),
        "param_name"  => "content_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Name Size', 'zipprich-core'),
        "param_name"  => "name_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),
      array(
        "type"        =>'textfield',
        "heading"     =>__('Profession Size', 'zipprich-core'),
        "param_name"  => "profession_size",
        "value"       => "",
        "group"       => __('Style', 'zipprich-core'),
        'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
      ),

      ), // Params
    ) );
  }
}