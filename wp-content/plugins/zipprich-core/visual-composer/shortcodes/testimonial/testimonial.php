<?php
/* Testimonial */
if ( !function_exists('ziph_testimonial_function')) {
  function ziph_testimonial_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'class'  => '',

      // Listing
      'testimonial_id'  => '',
      'testimonial_limit'  => '',

      // Color & Style
      'title_color'  => '',
      'content_color'  => '',
      'name_color'  => '',
      'profession_color'  => '',
      'title_size'  => '',
      'content_size'  => '',
      'name_size'  => '',
      'profession_size'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    // Title Color
    if ( $title_color || $title_size ) {
      $inline_style .= '.ziph-rev_testimonial_area-'. $e_uniqid .' .ziph-revtestimonial_title {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'. $title_size .';' : '';
      $inline_style .= '}';
    }
    // Content Color
    if ( $content_color || $content_size ) {
      $inline_style .= '.ziph-rev_testimonial_area-'. $e_uniqid .' .ziph-revtestimonial-single p {';
      $inline_style .= ( $content_color ) ? 'color:'. $content_color .';' : '';
      $inline_style .= ( $content_size ) ? 'font-size:'. $content_size .';' : '';
      $inline_style .= '}';
    }
    // Name Color
    if ( $name_color || $name_size ) {
      $inline_style .= '.ziph-rev_testimonial_area-'. $e_uniqid .' .ziph-revtestimonial_info .testi-name {';
      $inline_style .= ( $name_color ) ? 'color:'. $name_color .';' : '';
      $inline_style .= ( $name_size ) ? 'font-size:'. $name_size .';' : '';
      $inline_style .= '}';
    }
    // Profession Color
    if ( $profession_color || $profession_size ) {
      $inline_style .= '.ziph-rev_testimonial_area-'. $e_uniqid .' .ziph-revtestimonial_info .testi-pro {';
      $inline_style .= ( $profession_color ) ? 'color:'. $profession_color .';' : '';
      $inline_style .= ( $profession_size ) ? 'font-size:'. $profession_size .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-rev_testimonial_area-'. $e_uniqid;

    // Turn output buffer on
    ob_start();

    // Query Starts Here
    // Pagination
    global $paged;
    if( get_query_var( 'paged' ) )
      $my_page = get_query_var( 'paged' );
    else {
      if( get_query_var( 'page' ) )
        $my_page = get_query_var( 'page' );
      else
        $my_page = 1;
      set_query_var( 'paged', $my_page );
      $paged = $my_page;
    }

    $args = array(
      'paged' => $my_page,
      'post_type' => 'testimonial',
      'posts_per_page' => (int)$testimonial_limit,
      'orderby' => 'post__in'
    );

    if(!empty($testimonial_id)){
      $integerIDs = array_map('intval', explode(',', $testimonial_id));
      $args['post__in'] = $integerIDs;
    }

    $ziph_testi = new WP_Query( $args );

    if ($ziph_testi->have_posts()) :
    ?>

    <div class="ziph-rev_testimonial_area <?php echo esc_attr( $styled_class .' '. $class ); ?>"> <!-- Testimonial Starts -->
      <div class="text-center ziph-revtestimonial_warp">
        <h3 class="ziph-revtestimonial_title"><?php echo esc_attr( $title ); ?></h3>
        <?php
          while ($ziph_testi->have_posts()) : $ziph_testi->the_post();

          // Get Meta Box Options - zipprich_framework_active()
          $testimonial_options = get_post_meta( get_the_ID(), 'testimonial_options', true );
          $client_name = $testimonial_options['testi_name'];
          $name_link = $testimonial_options['testi_name_link'];
          $client_pro = $testimonial_options['testi_pro'];
          $pro_link = $testimonial_options['testi_pro_link'];

          $name_tag = $name_link ? 'a' : 'span';
          $name_link = $name_link ? 'href="'. esc_url($name_link) .'" target="_blank"' : '';
          $client_name = $client_name ? '<'. $name_tag .' '. $name_link .' class="testi-name">'. esc_attr($client_name) .'</'. $name_tag .'>' : '';

          $pro_tag = $pro_link ? 'a' : 'span';
          $pro_link = $pro_link ? 'href="'. esc_url($pro_link) .'" target="_blank"' : '';
          $client_pro = $client_pro ? '<'. $pro_tag .' '. $pro_link .' class="testi-pro">, '. esc_attr($client_pro) .'</'. $pro_tag .'>' : '';

          // Featured Image
          $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
          $large_image = $large_image[0];
          $large_image_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);

          if(class_exists('Aq_Resize')) {
            $testi_img = aq_resize( $large_image, '170', '170', true );
            $testi_img = ( $testi_img ) ? $testi_img : $large_image;
          } else {$testi_img = $large_image;}

          $actual_image = $large_image ? '<'. $name_tag .' '. $name_link .'><img src="'. esc_url($testi_img) .'" alt="'.esc_attr( $large_image_alt ).'"></'. $name_tag .'>' : '';
        ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class('ziph-revtestimonial-single'); ?>>
          <div class="ziph-revtestimonial_thumb">
            <?php echo $actual_image; ?>
          </div>
          <?php the_content(); ?>
          <div class="ziph-revtestimonial_info">
            <?php echo $client_name . $client_pro; ?>
          </div>
        </div>
        <?php
          endwhile;
          wp_reset_postdata();
        ?>

      </div>
    </div> <!-- Testimonial End -->

<?php
    endif;

    // Return outbut buffer
    return ob_get_clean();

  }
}
add_shortcode( 'ziph_testimonial', 'ziph_testimonial_function' );