<?php
/**
 * Callout - Shortcode Options
 */
add_action( 'init', 'ziph_callout_vc_map' );
if ( ! function_exists( 'ziph_callout_vc_map' ) ) {
  function ziph_callout_vc_map() {
    vc_map( array(
      "name" => __( "Callout Info", 'zipprich-core'),
      "base" => "ziph_callout",
      "description" => __( "Callout Info", 'zipprich-core'),
      "icon" => "fa fa-font color-slate-blue",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        array(
          "type"        => 'textfield',
          "heading"     => __('Title', 'zipprich-core'),
          "param_name"  => "title",
          "value"       => "",
          "admin_label" => true,
          "description" => __( "Enter the title of callout section.", 'zipprich-core')
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Pricing Text', 'zipprich-core'),
          "param_name"  => "subtitle",
          "value"       => "",
          "description" => __( "Enter the subtitle of callout section.", 'zipprich-core')
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Price', 'zipprich-core'),
          "param_name"  => "price",
          "value"       => "",
          "admin_label" => true,
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
          "description" => __( "Enter the price.", 'zipprich-core')
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Time', 'zipprich-core'),
          "param_name"  => "time",
          "value"       => "",
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
          "description" => __( "Enter time for callout price.", 'zipprich-core')
        ),        
        array(
          "type" => "textfield",
          "heading" => __( "Button Text", 'zipprich-core' ),
          "param_name" => "button_text",
          'value' => '',
          'admin_label' => true,
          "description" => __( "Enter your button text.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "switcher",
          "heading" => __( "Open New Tab?", 'zipprich-core' ),
          "param_name" => "open_link",
          "std" => false,
          'value' => '',
          "on_text" => __( "Yes", 'zipprich-core' ),
          "off_text" => __( "No", 'zipprich-core' ),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "textfield",
          "heading" => __( "Button Link", 'zipprich-core' ),
          "param_name" => "button_link",
          'value' => '',
          "description" => __( "Enter your button link.", 'zipprich-core'),
        ),
        ZipprichLib::vt_class_option(),
        
        // Styling
        array(
          "type" => "colorpicker",
          "heading" => __( "Text Color", 'zipprich-core' ),
          "param_name" => "text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Text Hover Color", 'zipprich-core' ),
          "param_name" => "text_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Background Color", 'zipprich-core' ),
          "param_name" => "btn_bg_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Background Hover Color", 'zipprich-core' ),
          "param_name" => "bg_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Border Color", 'zipprich-core' ),
          "param_name" => "border_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Border Hover Color", 'zipprich-core' ),
          "param_name" => "border_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
      )
    ) );
  }
}