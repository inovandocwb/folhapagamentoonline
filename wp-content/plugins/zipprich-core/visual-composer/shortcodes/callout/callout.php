<?php
/* Callout */
if ( !function_exists('ziph_callout_function')) {
  function ziph_callout_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'subtitle'  => '',
      'price'  => '',
      'time'  => '',
      'button_text'  => '',
      'button_link'  => '',
      'open_link'  => '',
      'class'  => '',
      // Styling
      'text_color'  => '',
      'text_hover_color'  => '',
      'btn_bg_color'  => '',
      'border_color'  => '',
      'bg_hover_color'  => '',
      'border_hover_color'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Button Text Color
    if ( $text_color ) {
      $inline_style .= '.btn.ziph-btn-'. $e_uniqid .' {';
      $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
      $inline_style .= '}';
    }

    // Button Text Hover Color
    if ( $text_hover_color ) {
      $inline_style .= '.btn.ziph-btn-'. $e_uniqid .':hover, .btn.ziph-btn-'. $e_uniqid .':focus, .btn.ziph-btn-'. $e_uniqid .':active {';
      $inline_style .= ( $text_hover_color ) ? 'color:'. $text_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button BG Color
    if ( $btn_bg_color || $border_color ) {
      $inline_style .= '.btn.ziph-btn-'. $e_uniqid .' {';
      $inline_style .= ( $btn_bg_color ) ? 'background:'. $btn_bg_color .' !important;' : '';
      $inline_style .= ( $border_color ) ? 'border: 2px solid '. $border_color .' !important;' : '';
      $inline_style .= '}';
    }

    // Button Hover Color
    if ( $bg_hover_color || $border_hover_color ) {
      $inline_style .= '.btn.ziph-btn-'. $e_uniqid .':hover, .btn.ziph-btn-'. $e_uniqid .':focus, .btn.ziph-btn-'. $e_uniqid .':active {';
      $inline_style .= ( $bg_hover_color ) ? 'background:'. $bg_hover_color .' !important;' : '';
      $inline_style .= ( $border_hover_color ) ? 'border: 2px solid '. $border_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-btn-'. $e_uniqid;

    // Styling
    $button_text = $button_text ? $button_text : '';
    $button_link = $button_link ? 'href="'. esc_url( $button_link ) .'"' : '';
    $open_link = $open_link ? ' target="_blank"' : '';

    // Output
    $output = '<div class="ziph-callout_area '. esc_attr( $class ) .'"><div class="ziph-flxvr-mdl row">';
      $output .= '<div class="col-md-6"><h3 class="ziph-callout_title">'. esc_attr($title) .'</h3></div>';
      $output .= '<div class="col-md-6"><div class="text-right ziph-callout_price">';
        $output .= '<h4>';
        if(!empty($subtitle) || !empty($price)) {
          $time = $time ? '<sub>/ '. esc_attr($time) .'</sub>' : '';
          $output .= esc_attr($subtitle) .'<span>'. esc_attr($price) . $time .'</span>';
        }
        $output .= '<a class="btn ziph-btn '. esc_attr( $styled_class . $class ) .'" '. $button_link . $open_link .'>'. $button_text .'</a>';
      $output .= '</h4></div></div>';
    $output .= '</div></div>';

    return $output;

  }
}
add_shortcode( 'ziph_callout', 'ziph_callout_function' );