<?php
/**
 * WHMCS Knowledgebase - Shortcode Options
 */
add_action( 'init', 'ziph_whmcs_knowledgebase_vc_map' );
if ( ! function_exists( 'ziph_whmcs_knowledgebase_vc_map' ) ) {
  function ziph_whmcs_knowledgebase_vc_map() {
    vc_map( array(
      "name" => __( "WHMCS - Knowledgebase", 'zipprich-core'),
      "base" => "ziph_whmcs_knowledgebase",
      "description" => __( "WHMCS Knowledgebase Styles", 'zipprich-core'),
      "icon" => "fa fa-globe color-pink",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(  
        array(
          'type' => 'notice',
          'value' => '',
          'class' => 'cs-warning',
          'heading' => __( 'This addons is for WHMCS Knowledgebase section. May add link into input box so user can read full arcile', 'zipprich-core' ),
          'param_name' => 'vt_notice',
        ),  
        array(
          'type' => 'textfield',
          'value' => '',
          'admin_label' => true,
          'heading' => __( 'Title', 'zipprich-core' ),
          'param_name' => 'title',
        ),
        array(
          'type' => 'attach_image',
          'value' => '',
          'heading' => __( 'Upload Image', 'zipprich-core' ),
          'param_name' => 'img',
        ),
        array(
          'type' => 'href',
          'value' => '',
          'heading' => __( 'Link', 'zipprich-core' ),
          'param_name' => 'link',
        ),
        array(
          'type' => 'textarea',
          'value' => '',
          'heading' => __( 'Description', 'zipprich-core' ),
          'param_name' => 'desc',
        ),
        ZipprichLib::vt_class_option(),
      )
    ) );
  }
}