<?php
/* ==========================================================
  WHMCS Knowledgebase
=========================================================== */
if ( !function_exists('ziph_whmcs_knowledgebase_function')) {
  function ziph_whmcs_knowledgebase_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'desc'  => '',
      'img'  => '',
      'link'  => '',
      'class'  => '',
    ), $atts));

    if($title) {
      if($link) {
        $title = '<h3><a href="'.$link.'">'.$title.'</a></h3>';
      } else {
        $title = '<h3>'.$title.'</h3>';
      }
    } else {
      $title = '';
    }

    $desc = ($desc) ? "<p>".$desc."</p>" : '';

    $zipprich_large_image =  wp_get_attachment_image_src( $img, 'fullsize', false, '' );
    $zipprich_large_image = $zipprich_large_image[0];
    $zipprich_image_alt = get_post_meta($img, '_wp_attachment_image_alt', true);

    if(class_exists('Aq_Resize')) {
      $image = aq_resize( $zipprich_large_image, '370', '250', true );
    } else {$image = $zipprich_large_image;}
    $image = ($image) ? $image : $zipprich_large_image;
    if (!empty($image)) {
      $image = '<div class="ziph-whmpressKnowl_meadia"><img src="'.esc_url( $image ).'" alt="'.$zipprich_image_alt.'"></div>';
    } else {
      $image = '';
    }

    $output = '<div class="ziph-whmpressKnowl_single '. $class .'">'. $image . $title . $desc .'</div>';

    return $output;
    
  }
}
add_shortcode( 'ziph_whmcs_knowledgebase', 'ziph_whmcs_knowledgebase_function' );