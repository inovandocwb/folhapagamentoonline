<?php
/* Info Grid */
if ( !function_exists('ziph_info_grids_function')) {
  function ziph_info_grids_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'text_color'  => '',
      'class'  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid     = uniqid();
    $inline_style = '';

    // Text Color
    if ( $text_color ) {
      $inline_style .= '.ziph-choose-text-'. $e_uniqid .' a.ziph-bdr_btn, .ziph-choose-text-'. $e_uniqid .' h3, .ziph-choose-text-'. $e_uniqid .' p, .ziph-choose-text-'. $e_uniqid .' .ziph-choose-list ul li {';
      $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-choose-text-'. $e_uniqid;

    // Output
    $output   = '<div class="ziph-fix ziph-choose-text ' . $class . $styled_class .'">';
    $output  .= do_shortcode($content);
    $output  .= '</div>';
    return $output;

  }
}
add_shortcode( 'ziph_info_grids', 'ziph_info_grids_function' );

/* Title */
if ( !function_exists('ziph_info_grid_title_function')) {
  function ziph_info_grid_title_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'class'  => '',
    ), $atts));

    // Output
    return '<h3 class="'. $class .'">'. $title .'</h3>';

  }
}
add_shortcode( 'ziph_info_grid_title', 'ziph_info_grid_title_function' );

/* Call to Action */
if ( !function_exists('ziph_info_grid_content_function')) {
  function ziph_info_grid_content_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'class'  => '',
    ), $atts));

    // fix unclosed/unwanted paragraph tags in $content
    $content = wpb_js_remove_wpautop($content, true);
    $content = $content ? do_shortcode($content) : '';

    // Output
    return '<div class="'. $class .'">'. $content .'</div>';

  }
}
add_shortcode( 'ziph_info_grid_content', 'ziph_info_grid_content_function' );

/* List Info */
if ( !function_exists('ziph_info_grid_list_function')) {
  function ziph_info_grid_list_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'list_style'  => '',
      'list_items'  => '',
      'class'  => '',
    ), $atts));

    // Group Field
    $list_items = (array) vc_param_group_parse_atts( $list_items );
    $get_each_list = array();
    foreach ( $list_items as $list_item ) {
      $each_list = $list_item;
      $each_list['info'] = isset( $list_item['info'] ) ? $list_item['info'] : '';
      $get_each_list[] = $each_list;
    }

    $list_style = ($list_style == 'style-inline') ? 'ziph-inline' : 'ziph-list';

    $output = '<div class="ziph-fix ziph-choose-list '. $class .'"><ul class="list-unstyled ziph-flt_left '.$list_style.'">';
    foreach ( $get_each_list as $each_list ) {
      $output .= '<li>'.$each_list['info'].'</li>';
    }
    $output .= '</ul></div>';
    // Output
    return $output;

  }
}
add_shortcode( 'ziph_info_grid_list', 'ziph_info_grid_list_function' );