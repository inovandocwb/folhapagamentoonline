<?php
/**
 * Call to Action - Shortcode Options
 */
add_action( 'init', 'ziph_info_grid_vc_map' );
if ( ! function_exists( 'ziph_info_grid_vc_map' ) ) {
 function ziph_info_grid_vc_map() {
   vc_map( array(
     "name" => __( "Features Info", 'zipprich-core'),
     "base" => "ziph_info_grids",
     "description" => __( "Features Info Area", 'zipprich-core'),
     "as_parent" => array('only' => 'ziph_info_grid_title, ziph_info_grid_content, ziph_info_grid_list, ziph_button'),
     "content_element" => true,
     "show_settings_on_create" => false,
     "is_container" => true,
     "icon" => "fa fa-clipboard color-orange",
     "category" => ZipprichLib::ziph_cat_name(),
     "params" => array(
        
        array(
          "type" => "colorpicker",
          "heading" => __( "Text Color", 'zipprich-core' ),
          "param_name" => "text_color",
          'value' => '',
        ),

        ZipprichLib::vt_class_option(),

     ),
     "js_view" => 'VcColumnView'
   ) );
 }
}

// Title
add_action( 'init', 'ziph_info_grid_title_vc_map' );
if ( ! function_exists( 'ziph_info_grid_title_vc_map' ) ) {
  function ziph_info_grid_title_vc_map() {
    vc_map( array(
      "name" => __( "Title", 'zipprich-core'),
      "base" => "ziph_info_grid_title",
      "description" => __( "Title of section", 'zipprich-core'),
      "icon" => "fa fa-arrows-h color-blue",
      "as_child" => array('only' => 'ziph_info_grids'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          "type"        => 'textfield',
          "heading"     => __('Title', 'zipprich-core'),
          "param_name"  => "title",
          "value"       => "",
          "admin_label" => true,
          "description" => __( "Title of your offer.", 'zipprich-core')
        ),
        ZipprichLib::vt_class_option(),

      )
    ) );
  }
}

// Description
add_action( 'init', 'ziph_info_grid_content_vc_map' );
if ( ! function_exists( 'ziph_info_grid_content_vc_map' ) ) {
  function ziph_info_grid_content_vc_map() {
    vc_map( array(
      "name" => __( "Description", 'zipprich-core'),
      "base" => "ziph_info_grid_content",
      "description" => __( "Offer Content", 'zipprich-core'),
      "icon" => "fa fa-exchange color-brown",
      "as_child" => array('only' => 'ziph_info_grids'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          "type"        => 'textarea_html',
          "heading"     => __('Content', 'zipprich-core'),
          "param_name"  => "content",
          "value"       => "",
          "admin_label" => true,
          "description" => __( "Explain about your offer.", 'zipprich-core')
        ),
        ZipprichLib::vt_class_option(),

      )
    ) );
  }
}

// Lists
add_action( 'init', 'ziph_info_grid_list_vc_map' );
if ( ! function_exists( 'ziph_info_grid_list_vc_map' ) ) {
  function ziph_info_grid_list_vc_map() {
    vc_map( array(
      "name" => __( "List of info", 'zipprich-core'),
      "base" => "ziph_info_grid_list",
      "description" => __( "Offer Main Points", 'zipprich-core'),
      "icon" => "fa fa-font color-slate-blue",
      "as_child" => array('only' => 'ziph_info_grids'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Main Points', 'zipprich-core' ),
          'param_name' => 'list_items',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              "type"        => 'textfield',
              "heading"     => __('Info', 'zipprich-core'),
              "param_name"  => "info",
              "value"       => "",
            ),
          ),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __( 'List Style', 'zipprich-core' ),
          'value' => array(
            __( 'Select List Style', 'zipprich-core' ) => '',
            __( 'List', 'zipprich-core' ) => 'style-list',
            __( 'Inline', 'zipprich-core' ) => 'style-inline',
          ),
          'admin_label' => true,
          'param_name' => 'list_style',
          'description' => __( 'Select list style', 'zipprich-core' ),
        ),

        ZipprichLib::vt_class_option(),

      )
    ) );
  }
}