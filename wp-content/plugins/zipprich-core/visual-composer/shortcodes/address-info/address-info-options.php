<?php
/**
 * Title - Shortcode Options
 */
add_action( 'init', 'ziph_address_info_vc_map' );
if ( ! function_exists( 'ziph_address_info_vc_map' ) ) {
	function ziph_address_info_vc_map() {

		vc_map( array(
			"name" => __( "Address Info", 'zipprich-core'),
			"base" => "ziph_contact_address_info",
			"description" => __( "Contact address info area", 'zipprich-core'),
			"icon" => "fa fa-file-text color-green",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(

				array(
					'type' => 'textfield',
					'value' => '',
					'heading' => __( 'Title', 'zipprich-core' ),
					'description' => __( 'Input a title', 'zipprich-core' ),
					'param_name' => 'title',
					'admin_label' => true,
					),
		        array(
					'type' => 'dropdown',
					'heading' => __( 'Image Type', 'zipprich-core' ),
					'value' => array(
						__( 'Select Image Type', 'zipprich-core' ) => '',
						__( 'Image', 'zipprich-core' ) => 'address_img',
						__( 'Icon', 'zipprich-core' ) => 'address_ico',
						),
					'param_name' => 'img_type',
		        	),				
				array(
					'type' => 'attach_image',
					'value' => '',
					'heading' => __( 'Image', 'zipprich-core' ),
					'description' => __( 'Upload Image', 'zipprich-core' ),
					'param_name' => 'image',              
					'dependency' => array(
		                'element' => 'img_type',
		                'value' => 'address_img',
		            	),
					),
				array(
					'type' => 'vt_icon',
					'value' => '',
					'heading' => __( 'Icon', 'zipprich-core' ),
					'description' => __( 'Insert Icon', 'zipprich-core' ),
					'param_name' => 'icon',              
					'dependency' => array(
		                'element' => 'img_type',
		                'value' => 'address_ico',
		            	),
					),
				array(
					'type' => 'textarea',
					'value' => '',
					'heading' => __( 'Info', 'zipprich-core' ),
					'description' => __( 'Insert Info', 'zipprich-core' ),
					'param_name' => 'info',
					),
				array(
					"type" => "textfield",
					"heading" => __( "Link", 'zipprich-core' ),
					"param_name" => "link",
					'value' => '',
					"description" => __( "Enter your link.", 'zipprich-core'),
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),
				array(
					"type" => "switcher",
					"heading" => __( "Open New Tab?", 'zipprich-core' ),
					"param_name" => "open_link",
					"std" => false,
					'value' => '',
					"on_text" => __( "Yes", 'zipprich-core' ),
					"off_text" => __( "No", 'zipprich-core' ),
					'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
					),

				ZipprichLib::vt_class_option(),
      			), // Params
			)
		);
	}
}