<?php
/* ==========================================================
    Address Info
=========================================================== */
if ( !function_exists('ziph_contact_address_info_function')) {
  function ziph_contact_address_info_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'img_type'  => '',
      'image'  => '',
      'icon'  => '',
      'info'  => '',
      'link'  => '',
      'open_link'  => '',
      'class'  => '',
    ), $atts));

    $open_link = $open_link ? ' target="_blank"' : ''; 
    
    if($link){
    	$info = $info ? '<a href="'. esc_url($link) .'" '.$open_link.'>'. $info .'</a>' : '';
    } else {
    	$info = $info ? $info : '';
    }

    if($img_type === 'address_img'){
		$large_image =  wp_get_attachment_image_src( $image, 'fullsize', false, '' );
    $large_image = $large_image[0];
    $large_image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
		
		if(class_exists('Aq_Resize')) {
			$address_img = aq_resize( $large_image, '21', '24', true );
		} else {$address_img = $large_image;} 

		$image_icon = ($address_img) ? '<img src="'. esc_url($address_img) .'" alt="'. esc_attr($large_image_alt) .'">' : '';
    } elseif ($img_type === 'address_ico'){
    	$image_icon = ($icon) ? '<i class="ziph-brand-color '. esc_attr($icon) .'"></i>' : '';
    }

    $output = '<li class="ziph-fix ziph-contactCSingle_info '. $class .'">';
    $output .= '<div class="ziph-flt_left ziph-contactCI_icon">'. $image_icon .'</div>';
    $output .= '<div class="ziph-fix ziph-contactCI_txt">';
    $output .= '<strong class="ziph-contactCI_label">'. $title .':</strong><p>'. $info .'</p>';
    $output .= '</div>';
    $output .= '</li>';
    return $output;

  }
}
add_shortcode( 'ziph_contact_address_info', 'ziph_contact_address_info_function' );