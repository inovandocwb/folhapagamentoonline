<?php
/* Hosting Pricing */
if ( !function_exists('ziph_hosting_pricing_function')) {
  function ziph_hosting_pricing_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'class'  => '',
    ), $atts));

    // Output
    $output   = '<div class="table-responsive ziph-shost_table '. esc_attr($class) .'">';
    $output  .= do_shortcode('[ziph_hosting_pricing_info_wrap]' . $content . '[ziph_hosting_pricing_footer][/ziph_hosting_pricing_info_wrap]');
    $output  .= '</div>';
    return $output;

  }
}
add_shortcode( 'ziph_hosting_pricing', 'ziph_hosting_pricing_function' );

/* Domain Hosting Info */
if ( !class_exists('Ziph_Hosting_Pricing_Info')) {
  class Ziph_Hosting_Pricing_Info{

    public $hosting_price = array();
    public $styled_class_btn = '';

    public function __construct(){
      add_shortcode( 'ziph_hosting_pricing_info_wrap', array( $this, 'ziph_hosting_pricing_info_wrap_function') );
      add_shortcode( 'ziph_hosting_pricing_title', array( $this, 'ziph_hosting_pricing_title_function') );
      add_shortcode( 'ziph_hosting_pricing_info', array( $this, 'ziph_hosting_pricing_info_function') );
      add_shortcode( 'ziph_hosting_pricing_footer', array( $this, 'ziph_hosting_pricing_footer_function') );
    }

    public function ziph_hosting_pricing_info_wrap_function( $atts, $content = true ) {

      // Output
      $output   = '<table class="table table-bordered">';
      $output  .= do_shortcode($content);
      $output  .= '</table>';
      return $output;

    }

    public function ziph_hosting_pricing_title_function( $atts, $content = true ) {

      extract(shortcode_atts(array(
        'title'  => '',
        'plan_titles'  => '',
        'class'  => '',
        // Style
        'title_color'  => '',
        'title_size'  => '',
        // Button
        'btn_text_color'  => '',
        'btn_text_hover_color'  => '',
        'btn_bg_color'  => '',
        'btn_bg_hover_color'  => '',
      ), $atts));

      // Shortcode Style CSS
      $e_uniqid        = uniqid();
      $inline_style  = '';

      if ( $title_color || $title_size ) {
        $inline_style .= '.ziph-table-title-'. $e_uniqid .' > tr > th {';
        $inline_style .= ( $title_color ) ? 'color:'. $title_color .' !important;' : '';
        $inline_style .= ( $title_size ) ? 'font-size:'. zipprich_core_check_px($title_size) .' !important;' : '';
        $inline_style .= '}';
      }

      // Button Styles
      if ( $btn_text_color || $btn_bg_color) {
        $inline_style .= '.ziph-shost_table .table > tfoot > tr > td .btn.ziph-btn-'. $e_uniqid .' {';
        $inline_style .= ( $btn_text_color ) ? 'color:'. $btn_text_color .';' : '';
        $inline_style .= ( $btn_bg_color ) ? 'background-color:'. $btn_bg_color .';' : '';
        $inline_style .= '}';
      }
      if ( $btn_text_hover_color || $btn_bg_hover_color ) {
        $inline_style .= '.ziph-shost_table .table > tfoot > tr > td .btn.ziph-btn-'. $e_uniqid .':hover {';
        $inline_style .= ( $btn_text_hover_color ) ? 'color:'. $btn_text_hover_color .';' : '';
        $inline_style .= ( $btn_bg_hover_color ) ? 'background-color:'. $btn_bg_hover_color .'!important;' : '';
        $inline_style .= '}';
      }

      // add inline style
      zipprich_add_inline_style( $inline_style );
      $styled_class  = ' ziph-table-title-'. $e_uniqid;
      $this->styled_class_btn  = ' ziph-btn-'. $e_uniqid;

      // Group Field
      $plan_titles = (array) vc_param_group_parse_atts( $plan_titles );
      $get_each_list = $this->hosting_price = array();
      foreach ( $plan_titles as $plan_title ) {
        $each_list = $plan_title;
        $each_list['title'] = isset( $plan_title['title'] ) ? $plan_title['title'] : '';
        $get_each_list[] = $this->hosting_price[] = $each_list;
      }

      // Output
      $output  = '<thead class="ziph-sht_plan '.esc_attr( $class . ' ' . $styled_class ).'"><tr>';
      $output  .= '<th class="ziph-sht_plan">'.$title.'</th> ';
      foreach ($get_each_list as $each_list) {
        $output  .= '<th>'.esc_attr($each_list['title']).'</th> ';
      }
      $output  .= '</tr></thead>';
      return  $output;

    }

    public function ziph_hosting_pricing_info_function( $atts, $content = true ) {

      extract(shortcode_atts(array(
        'title'  => '',
        'plan_prices'  => '',
      ), $atts));

      // Group Field
      $plan_prices = (array) vc_param_group_parse_atts( $plan_prices );
      $get_each_list = array();
      foreach ( $plan_prices as $plan_price ) {
        $each_list = $plan_price;
        $each_list['info_type']     = isset( $plan_price['info_type'] ) ? $plan_price['info_type'] : '';
        $each_list['icon']          = isset( $plan_price['icon'] ) ? $plan_price['icon'] : '';
        $each_list['icon_color']    = isset( $plan_price['icon_color'] ) ? $plan_price['icon_color'] : '';
        $each_list['price']         = isset( $plan_price['price'] ) ? $plan_price['price'] : '';
        $get_each_list[]            = $each_list;
      }

      // Output
      $output  = '<tbody><tr>';
      $output  .= '<td class="ziph-sht_title">'.esc_attr($title).'</td> ';
      foreach ($get_each_list as $each_list) {
        if (!empty($each_list['price']) && $each_list['info_type'] == 'text') {
          $output  .= '<td><span>'.esc_attr($each_list['price']).'</span></td>';
        }
        if (!empty($each_list['icon']) && $each_list['info_type'] == 'icon') {
          $output  .= '<td><i style="color: '.esc_attr($each_list['icon_color']).'" class="'.esc_attr($each_list['icon']).'"></i></td>';
        }
      }
      $output  .= '</tr></tbody>';

      return  $output;

    }

    public function ziph_hosting_pricing_footer_function( $atts, $content = true ) {

      $output  = '<tfoot><tr><td class="empty-cells"></td>';
      foreach ($this->hosting_price as $hosting_pricing) {
        $output  .= '<td>';

        if(isset($hosting_pricing['pricing_time'])){
          $pricing_time  = '<sub>/'.esc_attr($hosting_pricing['pricing_time']).'</sub>';
        } else {
          $pricing_time  = '';
        }

        if(isset($hosting_pricing['pricing_price'])){
          $output  .= '<h2>'.esc_attr($hosting_pricing['pricing_price']).$pricing_time.'</h2>';
        }

        if(isset($hosting_pricing['pricing_desc'])){
          $output  .= '<q>'.esc_attr($hosting_pricing['pricing_desc']).'</q>';
        }

        if(isset($hosting_pricing['pricing_btn_text'])){
          $output  .= '<a class="btn ziph-btn '.esc_attr( $this->styled_class_btn ).'" href="'.esc_url($hosting_pricing['pricing_btn_link']).'">'.esc_attr($hosting_pricing['pricing_btn_text']).'</a>';
        }

        $output  .= '</td>';
      }
      $output  .= '</tr></tfoot>';

      return  $output;

    }

  }
}
new Ziph_Hosting_Pricing_Info();