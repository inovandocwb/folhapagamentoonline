<?php
/**
 * Domain Pricing - Shortcode Options
 */
add_action( 'init', 'ziph_hosting_pricing_vc_map' );
if ( ! function_exists( 'ziph_hosting_pricing_vc_map' ) ) {
 function ziph_hosting_pricing_vc_map() {
   vc_map( array(
     "name" => __( "Hosting Pricing Chart", 'zipprich-core'),
     "base" => "ziph_hosting_pricing",
     "description" => __( "Hosting Chart Group", 'zipprich-core'),
     "as_parent" => array('only' => 'ziph_hosting_pricing_title,ziph_hosting_pricing_info'),
     "content_element" => true,
     "show_settings_on_create" => false,
     "is_container" => true,
     "icon" => "fa fa-bar-chart color-grey",
     "category" => ZipprichLib::ziph_cat_name(),
     "params" => array(

        ZipprichLib::vt_class_option(),

     ),
     "js_view" => 'VcColumnView'
   ) );
 }
}

// Titles
add_action( 'init', 'ziph_hosting_pricing_title_vc_map' );
if ( ! function_exists( 'ziph_hosting_pricing_title_vc_map' ) ) {
  function ziph_hosting_pricing_title_vc_map() {
    vc_map( array(
      "name" => __( "Hosting Chart - Titles", 'zipprich-core'),
      "base" => "ziph_hosting_pricing_title",
      "description" => __( "Hosting Chart Titles Setup", 'zipprich-core'),
      "icon" => "fa fa-arrows-h color-blue",
      "as_child" => array('only' => 'ziph_hosting_pricing'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Table Chart Title', 'zipprich-core' ),
          'param_name' => 'title',
          'admin_label' => true,
        ),    
        
        // List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Hosting Titles', 'zipprich-core' ),
          'param_name' => 'plan_titles',
          // Note params is mapped inside param-group:
          'params' => array(
            
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Title', 'zipprich-core' ),
              'param_name' => 'title',
            ),

            array(
              "type" => "textarea",
              "heading" => __( "Description", 'zipprich-core' ),
              "param_name" => "pricing_desc",
              'value' => '',
              "description" => __( "Enter table description.", 'zipprich-core'),
            ),

            array(
              "type" => "textfield",
              "heading" => __( "Price", 'zipprich-core' ),
              "param_name" => "pricing_price",
              'value' => '',
              "description" => __( "Enter table price.", 'zipprich-core'),
              'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
            ),

            array(
              "type" => "textfield",
              "heading" => __( "Time", 'zipprich-core' ),
              "param_name" => "pricing_time",
              'value' => '',
              "description" => __( "Enter table time.", 'zipprich-core'),
              'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
            ),

            array(
              "type" => "textfield",
              "heading" => __( "Button Text", 'zipprich-core' ),
              "param_name" => "pricing_btn_text",
              'value' => '',
              "description" => __( "Enter button text.", 'zipprich-core'),
              'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
            ),

            array(
              "type" => "textfield",
              "heading" => __( "Button Link", 'zipprich-core' ),
              "param_name" => "pricing_btn_link",
              'value' => '',
              "description" => __( "Enter button url.", 'zipprich-core'),
              'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
            ),

          )
        ),
        ZipprichLib::vt_class_option(),


        // Style
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Text Color', 'zipprich-core' ),
          'param_name' => 'title_color',
          'group' => __( 'Style', 'zipprich-core' ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Text Size', 'zipprich-core' ),
          'param_name' => 'title_size',
          'group' => __( 'Style', 'zipprich-core' ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),  
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Color", 'zipprich-core' ),
          "param_name" => "btn_text_color",
          'value' => '',
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Hover Color", 'zipprich-core' ),
          "param_name" => "btn_text_hover_color",
          'value' => '',
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Color", 'zipprich-core' ),
          "param_name" => "btn_bg_color",
          'value' => '',
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Hover Color", 'zipprich-core' ),
          "param_name" => "btn_bg_hover_color",
          'value' => '',
          "group" => __( "Style", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),          
      )
    ) );
  }
}

// Call to Action List
add_action( 'init', 'ziph_hosting_pricing_info_vc_map' );
if ( ! function_exists( 'ziph_hosting_pricing_info_vc_map' ) ) {
  function ziph_hosting_pricing_info_vc_map() {
    vc_map( array(
      "name" => __( "Hosting Chart - Infos", 'zipprich-core'),
      "base" => "ziph_hosting_pricing_info",
      "description" => __( "Hosting Chart Details", 'zipprich-core'),
      "icon" => "fa fa-arrows-v color-green",
      "as_child" => array('only' => 'ziph_hosting_pricing'),
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Hosting Title', 'zipprich-core' ),
          'param_name' => 'title',
          'admin_label' => true
        ),
        
        // List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Hosting Info/Prices', 'zipprich-core' ),
          'param_name' => 'plan_prices',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              'type' => 'dropdown',
              'heading' => __( 'Info Type', 'zipprich-core' ),
              'value' => array(
                __( 'Select Info Type', 'zipprich-core' ) => '',
                __( 'Text', 'zipprich-core' ) => 'text',
                __( 'Icon', 'zipprich-core' ) => 'icon',
              ),
              'param_name' => 'info_type',
              'admin_label' => true,
              'description' => __( 'Select button size', 'zipprich-core' ),
            ),                
            array(
              'type' => 'vt_icon',
              'value' => '',
              'heading' => __( 'Select Icon', 'zipprich-core' ),
              'param_name' => 'icon',         
              'dependency' => array(
                'element' => 'info_type',
                'value' => 'icon',
              ),
            ),                
            array(
              'type' => 'colorpicker',
              'value' => '',
              'heading' => __( 'Icon Color', 'zipprich-core' ),
              'param_name' => 'icon_color',         
              'dependency' => array(
                'element' => 'info_type',
                'value' => 'icon',
              ),
            ),         
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Info/Price', 'zipprich-core' ),
              'param_name' => 'price',         
              'dependency' => array(
                'element' => 'info_type',
                'value' => 'text',
              ),
            ),

          )
        ),
        ZipprichLib::vt_class_option(),

      )
    ) );
  }
}