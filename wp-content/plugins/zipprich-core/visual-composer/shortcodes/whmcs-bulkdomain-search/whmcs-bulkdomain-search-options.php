<?php
/**
 * Balk Domain Search - Shortcode Options
 */
add_action( 'init', 'ziph_whmch_bulkdomain_search_vc_map' );
if ( ! function_exists( 'ziph_whmch_bulkdomain_search_vc_map' ) ) {
  function ziph_whmch_bulkdomain_search_vc_map() {
    vc_map( array(
      "name" => __( "WHMCS - Bulk Domain Search", 'zipprich-core'),
      "base" => "ziph_whmch_bulkdomain_search",
      "description" => __( "Domain Search Styles", 'zipprich-core'),
      "icon" => "fa fa-globe color-blue",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        array(
          'type' => 'notice',
          'value' => '',
          'class' => 'cs-warning',
          'heading' => __( 'This addons is associated with WHMCS Bridge plugins', 'zipprich-core' ),
          'param_name' => 'vt_notice',
        ), 
        array(
          'type' => 'textfield',
          'value' => '',
          'admin_label' => true,
          'heading' => __( 'Section Title', 'zipprich-core' ),
          'param_name' => 'title',
        ),
        array(
          'type' => 'textarea',
          'value' => '',
          'heading' => __( 'Section Note', 'zipprich-core' ),
          'param_name' => 'desc',
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Button Text', 'zipprich-core' ),
          'param_name' => 'btn_text',
        ),
        ZipprichLib::vt_class_option(),

        // Styling
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Font Color', 'zipprich-core' ),
          'param_name' => 'text_color',
          'group' => __( 'Styling', 'zipprich-core' ),
        ),        
        array(
          'type' => 'notice',
          'value' => '',
          'class' => 'cs-info',
          'heading' => __( 'Button Styling', 'zipprich-core' ),
          'param_name' => 'vt_notice',
          "group" => __( "Styling", 'zipprich-core'),
        ),        
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Color", 'zipprich-core' ),
          "param_name" => "btn_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Hover Color", 'zipprich-core' ),
          "param_name" => "btn_text_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Color", 'zipprich-core' ),
          "param_name" => "btn_bg_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Hover Color", 'zipprich-core' ),
          "param_name" => "btn_bg_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Border Color", 'zipprich-core' ),
          "param_name" => "btn_border_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Border Hover Color", 'zipprich-core' ),
          "param_name" => "btn_border_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
      )
    ) );
  }
}