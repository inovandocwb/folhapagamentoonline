<?php
/* ==========================================================
  Bulk Domain Search
=========================================================== */
if ( !function_exists('ziph_whmch_bulkdomain_search_function')) {
  function ziph_whmch_bulkdomain_search_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'title'  => '',
      'desc'  => '',
      'btn_text'  => '',
      'class'  => '',
      // Style
      'text_color'  => '',
      'btn_text_color'  => '',
      'btn_text_hover_color'  => '',
      'btn_bg_color'  => '',
      'btn_bg_hover_color'  => '',
      'btn_border_color'  => '',
      'btn_border_hover_color'  => '',
      ''  => '',
    ), $atts));

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    if ( $text_color ) {
      $inline_style .= '.ziph-bulkDomains_area-'. $e_uniqid .' h2.ziph-bulkDomains_title, .ziph-bulkDomains_area-'. $e_uniqid .' .bulk-options p, .ziph-bulkDomains_area-'. $e_uniqid .' .search-button button.search_btn {';
      $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
      $inline_style .= '}';
    }
    // Button Text Color
    if ( $btn_text_color || $btn_bg_color) {
      $inline_style .= '.ziph-bulkDomains_area-'. $e_uniqid .' .search-button button.search_btn {';
      $inline_style .= ( $btn_text_color ) ? 'color:'. $btn_text_color .';' : '';
      $inline_style .= ( $btn_bg_color ) ? 'background-color:'. $btn_bg_color .';' : '';
      $inline_style .= '}';
    }

    // Button Text Hover Color
    if ( $btn_bg_hover_color || $btn_text_hover_color ) {
      $inline_style .= '.ziph-bulkDomains_area-'. $e_uniqid .' .search-button button.search_btn:hover, .ziph-bulkDomains_area-'. $e_uniqid .' .search-button button.search_btn:focus {';
      $inline_style .= ( $btn_text_hover_color ) ? 'color:'. $btn_text_hover_color .' !important;' : '';
      $inline_style .= ( $btn_bg_hover_color ) ? 'background-color:'. $btn_bg_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    if ( $btn_border_color ) {
      $inline_style .= '.ziph-bulkDomains_area-'. $e_uniqid .' .search-button button.search_btn {';
      $inline_style .= ( $btn_border_color ) ? 'border-color:'. $btn_border_color .' !important;' : '';
      $inline_style .= '}';
    }

    if ( $btn_border_hover_color ) {
      $inline_style .= '.ziph-bulkDomains_area-'. $e_uniqid .' .search-button button.search_btn:hover, .ziph-bulkDomains_area-'. $e_uniqid .' .search-button button.search_btn:focus {';
      $inline_style .= ( $btn_border_hover_color ) ? 'border-color:'. $btn_border_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-bulkDomains_area-'. $e_uniqid;

    $btn_text = $btn_text ? $btn_text : esc_html__( 'Search', 'zipprich-core' );

    ob_start();
    ?>
    <div style="clear:both"></div>
    <div class="ziph-bulkDomains_area <?php echo esc_attr($styled_class . ' ' . $class ); ?>">
      <?php if($title){ ?>
        <h2 class="ziph-bulkDomains_title"><?php echo $title; ?></h2>
      <?php } ?>
      <!-- WHMPress -->
      <div class='whmpress whmpress_domain_search_bulk'>
      <div>
        <?php
          if(function_exists('cc_whmcs_bridge_mainpage')){
            $whmcs_page_link = get_permalink(cc_whmcs_bridge_mainpage());
          } else {
            $whmcs_page_link = '';
          }
        ?>
        <form method="get" action="<?php echo $whmcs_page_link; ?>">
          <input type="hidden" name="m" value="bulk_domain_checker">
          <div class="bulk-domains">
             <textarea required="required" class="" placeholder="" name="search_bulk_domain"></textarea>
          </div>
          <div class="bulk-options">
            <?php if($desc){ ?>
              <p><?php echo $desc; ?></p>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="search-button">
              <button class="search_btn "><?php echo $btn_text; ?></button>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>
    <div style="clear:both"></div>
    <?php
    $output = ob_get_clean();

    return $output;
  }
}
add_shortcode( 'ziph_whmch_bulkdomain_search', 'ziph_whmch_bulkdomain_search_function' );