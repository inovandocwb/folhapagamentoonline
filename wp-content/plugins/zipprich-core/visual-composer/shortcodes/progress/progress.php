<?php
/* ==========================================================
  Progress
=========================================================== */
if ( !function_exists('ziph_progress_function')) {
  function ziph_progress_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'list_style'  => '',
      'list_items'  => '',
      'class'  => '',
      // Style
      'badge_color'  => '',
      'badge_bg_color'  => '',
      'icon_color'  => '',
      'icon_size'  => '',
      'title_color'  => '',
      'title_size'  => '',
    ), $atts));

    // Group Field
    $list_items = (array) vc_param_group_parse_atts( $list_items );
    $get_each_list = array();
    foreach ( $list_items as $list_item ) {
      $each_list = $list_item;
      $each_list['icon_image'] = isset( $list_item['icon_image'] ) ? $list_item['icon_image'] : '';
      $each_list['select_icon'] = isset( $list_item['select_icon'] ) ? $list_item['select_icon'] : '';
      $each_list['select_image'] = isset( $list_item['select_image'] ) ? $list_item['select_image'] : '';
      $each_list['list_title'] = isset( $list_item['list_title'] ) ? $list_item['list_title'] : '';
      $get_each_list[] = $each_list;
    }

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    if ( $badge_color || $badge_bg_color ) {
      $inline_style .= '.ziph-process_warp-'. $e_uniqid .' .ziph-process_single .ziph-badge {';
      $inline_style .= ( $badge_color ) ? 'color:'. $badge_color .';' : '';
      $inline_style .= ( $badge_bg_color ) ? 'background-color:'. $badge_bg_color .';' : '';
      $inline_style .= '}';
    }
    if ( $title_size || $title_color ) {
      $inline_style .= '.ziph-process_warp-'. $e_uniqid .' .ziph-process_single h4 {';
      $inline_style .= ( $title_color ) ? 'color:'. $title_color .';' : '';
      $inline_style .= ( $title_size ) ? 'font-size:'. zipprich_core_check_px($title_size) .';' : '';
      $inline_style .= '}';
    }
    if ( $icon_color || $icon_size ) {
      $inline_style .= '.ziph-process_warp-'. $e_uniqid .' .ziph-process_icon i {';
      $inline_style .= ( $icon_color ) ? 'color:'. $icon_color .';' : '';
      $inline_style .= ( $icon_size ) ? 'font-size:'. zipprich_core_check_px($icon_size) .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-process_warp-'. $e_uniqid;

    // Group Param Output
    $output = '<div class="ziph-fix ziph-process_warp '.esc_attr($styled_class . ' ' . $class ).'">';

    foreach ( $get_each_list as $key => $each_list ) {

      $output .= '<div class="text-center col-md-4 col-sm-6 ziph-process_single">';
      $list_title = $each_list['list_title'] ? '<h4>'. esc_attr($each_list['list_title']) .'</h4>' : '';

      if (!empty($each_list['select_image'])) {
        $zipprich_large_image = wp_get_attachment_image_src( $each_list['select_image'], 'full' );
        $alt = get_post_meta($each_list['select_image'], '_wp_attachment_image_alt', true);
        $zipprich_large_image = $zipprich_large_image[0];

        if(class_exists('Aq_Resize')) {
          $image_url = aq_resize( $zipprich_large_image, '54', '54', false );
          $image_url = $image_url ? $image_url : $zipprich_large_image;
        } else {$image_url = $zipprich_large_image;}

        $output .= '<div class="ziph-process_icon"><img src="'. esc_url($image_url) .'" alt="'.esc_attr($alt).'"><span class="ziph-badge">'. esc_attr(($key + 1)) .'</span></div>'. $list_title;
      } else {
        $output .= '<div class="ziph-process_icon"><i class="'. esc_attr($each_list['select_icon']) .'"></i><span class="ziph-badge">'. esc_attr(($key + 1)) .'</span></div>'. $list_title;
      }
      $output .= '</div>';
    }

    $output .= '</div>';

    return $output;
  }
}
add_shortcode( 'ziph_progress', 'ziph_progress_function' );