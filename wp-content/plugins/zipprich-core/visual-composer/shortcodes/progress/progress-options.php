<?php
/**
 * Progress - Shortcode Options
 */
add_action( 'init', 'ziph_progress_vc_map' );
if ( ! function_exists( 'ziph_progress_vc_map' ) ) {
  function ziph_progress_vc_map() {
    vc_map( array(
      "name" => __( "Progress", 'zipprich-core'),
      "base" => "ziph_progress",
      "description" => __( "Progress Styles", 'zipprich-core'),
      "icon" => "fa fa-list color-red",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        // Progress List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Progress Lists', 'zipprich-core' ),
          'param_name' => 'list_items',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              'type' => 'dropdown',
              'value' => array(
                __( 'Select an option', 'zipprich-core' ) => '',
                __( 'Image', 'zipprich-core' ) => 'list_image',
                __( 'Icon', 'zipprich-core' ) => 'list_icon',
              ),
              'heading' => __( 'Icon or Image', 'zipprich-core' ),
              'param_name' => 'icon_image',
            ),
            array(
              'type' => 'vt_icon',
              'value' => '',
              'heading' => __( 'Select Icon', 'zipprich-core' ),
              'param_name' => 'select_icon',
              'dependency' => array(
                'element' => 'icon_image',
                'value' => 'list_icon',
              ),
            ),
            array(
              'type' => 'attach_image',
              'value' => '',
              'heading' => __( 'Upload Icon Image', 'zipprich-core' ),
              'param_name' => 'select_image',
              'dependency' => array(
                'element' => 'icon_image',
                'value' => 'list_image',
              ),
            ),
            array(
              'type' => 'textfield',
              'value' => '',
              'admin_label' => true,
              'heading' => __( 'Title', 'zipprich-core' ),
              'param_name' => 'list_title',
            ),
          )
        ),
        ZipprichLib::vt_class_option(),

        // Style
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Badge Color', 'zipprich-core' ),
          'param_name' => 'badge_color',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Badge Background Color', 'zipprich-core' ),
          'param_name' => 'badge_bg_color',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Icon Color', 'zipprich-core' ),
          'param_name' => 'icon_color',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Icon Size', 'zipprich-core' ),
          'param_name' => 'icon_size',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Title Color', 'zipprich-core' ),
          'param_name' => 'title_color',
          'group' => __( 'Style', 'zipprich-core' ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Title Size', 'zipprich-core' ),
          'param_name' => 'title_size',
          'group' => __( 'Style', 'zipprich-core' ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),

      )
    ) );
  }
}
