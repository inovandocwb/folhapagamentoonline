<?php
/* ==========================================================
  Blog
=========================================================== */
if ( !function_exists('ziph_blog_function')) {
  function ziph_blog_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'blog_style'  => '',
      'blog_column'  => '',
      'blog_limit'  => '',
      'ignore_sticky_posts'  => '',
      // Enable & Disable
      'blog_category'  => '',
      'blog_date'  => '',
      'blog_author'  => '',
      'blog_comments'  => '',
      'blog_pagination'  => '',
      // Listing
      'blog_order'  => '',
      'blog_orderby'  => '',
      'blog_show_category'  => '',
      'short_content'  => '',
      'class'  => '',
      // Read More Text
      'read_more_txt'  => '',
    ), $atts));

    // Column
    if ($blog_style === 'ziph-blog-two') {
      $blog_column_class = $blog_column ? $blog_column . ' ziph-fix col-sm-6 ziph-blog_post masonry-entry' : 'ziph-fix col-sm-6 ziph-blog_post masonry-entry';
    } elseif ($blog_style === 'ziph-blog-three') {
      $blog_column_class = 'ziph-fix  ziph-hmlblog_single';
    } else {
      $blog_column_class = 'ziph-blogList_post';
    }

    // Excerpt
    if (zipprich_framework_active()) {
      $excerpt_length = cs_get_option('theme_blog_excerpt');
      $excerpt_length = $excerpt_length ? $excerpt_length : '25';
      if ($short_content) {
        $short_content = $short_content;
      } else {
        $short_content = $excerpt_length;
      }
    } else {
      $short_content = '25';
    }

    // Style
    if ($blog_style === 'ziph-blog-two') {
      $blog_style_class = 'row ziph-fix ziph-blogGrid_warp masonry-loop';
    } elseif ($blog_style === 'ziph-blog-three') {
      $blog_style_class = 'ziph-hmlblog_warp';
    } else {
      $blog_style_class = 'ziph-blogList_warp';
    }

    // Read More Text
    if (zipprich_framework_active()) {
      $read_more_to = cs_get_option('read_more_text');
      if ($read_more_txt) {
        $read_more_txt = $read_more_txt;
      } elseif($read_more_to) {
        $read_more_txt = $read_more_to;
      } else {
        $read_more_txt = esc_html__( 'Read More', 'zipprich-core' );
      }
    } else {
      $read_more_txt = $read_more_txt ? $read_more_txt : esc_html__( 'Read More', 'zipprich-core' );
    }

    // Turn output buffer on
    ob_start();

    // Pagination
    global $paged;
    if( get_query_var( 'paged' ) )
      $my_page = get_query_var( 'paged' );
    else {
      if( get_query_var( 'page' ) )
        $my_page = get_query_var( 'page' );
      else
        $my_page = 1;
      set_query_var( 'paged', $my_page );
      $paged = $my_page;
    }

    $args = array(
      // other query params here,
      'paged' => $my_page,
      'post_type' => 'post',
      'posts_per_page' => (int)$blog_limit,
      'category_name' => esc_attr($blog_show_category),
      'orderby' => $blog_orderby,
      'order' => $blog_order
    );

    if($ignore_sticky_posts){
      $args['ignore_sticky_posts'] = true;
    }

    $ziph_post = new WP_Query( $args ); ?>

    <!-- Blog Start -->
    <div class="<?php echo esc_attr($blog_style_class .' '. $class); ?>">
      <?php if ($ziph_post->have_posts()) : while ($ziph_post->have_posts()) : $ziph_post->the_post();

        $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
        $large_image = $large_image[0];
        $large_image_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);
        if ($blog_style === 'ziph-blog-two') {
          if(class_exists('Aq_Resize')) {
            $post_img = aq_resize( $large_image, '570', '354', true );
          } else {$post_img = $large_image;}
        } else if ($blog_style === 'ziph-blog-three') {
          if(class_exists('Aq_Resize')) {
            $post_img = aq_resize( $large_image, '150', '150', true );
          } else {$post_img = $large_image;}
        } else {
          if(class_exists('Aq_Resize')) {
            $post_img = aq_resize( $large_image, '845', '460', true );
          } else {$post_img = $large_image;}
        }
        $post_type = get_post_meta( get_the_ID(), 'post_type_metabox', true );
      ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class($blog_column_class); ?>>
        <?php if($blog_style === 'ziph-blog-three'){ ?>
          <?php if ($post_img) { ?>
          <!--image start\-->
          <div class="ziph-flt_left ziph-hmlblog_img">
            <img src="<?php echo esc_url( $post_img ); ?>" alt="<?php echo esc_attr( $large_image_alt ); ?>">
          </div><!--/end-->
          <?php } ?>

          <!--Text  start\-->
          <div class="ziph-fix  ziph-hmlblog_txt">
            <div class="ziph-hmlblog_date">
            <?php if ( !$blog_date ) { the_time(get_option('date_format')); } // Date Hide

              if ( !$blog_category ) { // Category Hide
                if ( get_post_type() === 'post') {
                  $category_list = get_the_category_list( ', ' );
                  if ( $category_list ) {
              ?>
               &nbsp;<span class="ziph-crshap"></span>&nbsp;
               <?php echo esc_html__( 'in', 'zipprich-core' ) . ' ' . $category_list; ?>
             <?php }
              }
            } // Category Hides ?>
            </div>
            <h4>
              <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
              </a>
            </h4>
            <p><?php
            if (zipprich_framework_active()) {
              zipprich_excerpt($short_content);
            } else {
              the_excerpt();
            }
            if ( function_exists( 'zipprich_wp_link_pages' ) ) {
              echo zipprich_wp_link_pages();
            }
            ?></p>
            <a  class="ziph-hmlblog_link" href="<?php the_permalink(); ?>"><?php echo esc_attr($read_more_txt); ?></a>
          </div><!--/end-->
        <?php } else { ?>
        <!--Post header start\-->
        <header class="ziph-post_header">
          <!-- Featured Image -->
          <?php if ($post_img) { ?>
          <div class="ziph-post_media">
            <img src="<?php echo esc_url( $post_img ); ?>" alt="<?php echo esc_attr( $large_image_alt ); ?>">
          </div>
          <?php } // Featured Image ?>
          <div class="ziph-post_date">
            <?php if ( !$blog_date ) { the_time(get_option('date_format')); } // Date Hide

              if ( !$blog_category ) { // Category Hide
                if ( get_post_type() === 'post') {
                  $category_list = get_the_category_list( ', ' );
                  if ( $category_list ) {
              ?>
               &nbsp;<span class="ziph-crshap"></span>&nbsp;
               <?php echo esc_html__( 'in', 'zipprich-core' ) . ' ' . $category_list; ?>
             <?php }
              }
            } // Category Hides ?>
          </div>
          <h4 class="ziph-post_title">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h4>
        </header><!--Post header end\-->
        <!--Post excerpt start\-->
        <div class="ziph-post_excerpt">
          <p><?php
          if (zipprich_framework_active()) {
            zipprich_excerpt($short_content);
          } else {
            the_excerpt();
          }
          if ( function_exists( 'zipprich_wp_link_pages' ) ) {
            echo zipprich_wp_link_pages();
          }
          ?></p>
        </div><!--Post excerpt end\-->
        <!--Post footer start\-->
        <footer class="ziph-post_footer">
          <div class="row">
            <div class="text-left col-xs-6">
              <a href="<?php the_permalink(); ?>" class="ziph-readmore_link">
                <?php echo esc_attr($read_more_txt); ?><i class="fa fa-angle-right"></i>
              </a>
            </div>
            <div class="text-right col-xs-6">
              <?php if ( !$blog_comments ) { // Comments Hide
                echo '<span class="ziph-commnt_link">';
                comments_popup_link( esc_html__( 'No Comments', 'zipprich-core' ), esc_html__( 'One Comment', 'zipprich-core' ), esc_html__( '% Comments', 'zipprich-core' ), '', '' );
                echo '</span>';
              } // Comments Hide ?>
            </div>
          </div>
        </footer><!--Post footer end\-->
        <?php } ?>
      </article><!-- #post-## -->

      <?php
      endwhile;
      endif;
      wp_reset_postdata(); ?>
    </div>
    <!-- Blog End -->

    <?php
    if ($blog_pagination) {
      if ($blog_style === 'ziph-blog-two') {
        $blog_nav_style = 'm-24 ';
      } else { $blog_nav_style = 'ziph-nav-m-10 '; }
      echo '<div class="' . esc_attr( $blog_nav_style ) . ' ziph-clear text-center ziph-postPagination_warp"><nav class="navigation pagination"><div class="nav-links">';
      if ( function_exists('wp_pagenavi')) {
        wp_pagenavi(array( 'query' => $ziph_post ) );
        wp_reset_postdata();  // avoid errors further down the page
      } else {
        $older_post = cs_get_option('older_post');
        $newer_post = cs_get_option('newer_post');
        $older_post = $older_post ? $older_post : esc_html__( 'PREV', 'zipprich-core' );
        $newer_post = $newer_post ? $newer_post : esc_html__( 'NEXT', 'zipprich-core' );

        $big = 999999999;
        if($ziph_post->max_num_pages == '1' ) {} else {echo '';}
        echo paginate_links( array(
          'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
          'format' => '?paged=%#%',
          'prev_text' => '<i class="fa fa-angle-left"></i>' . $older_post,
          'next_text' => $newer_post . '<i class="fa fa-angle-right"></i>',
          'current' => max( 1, get_query_var('paged') ),
          'total' => $ziph_post->max_num_pages,
          'type' => 'plain'
        ));
        if($ziph_post->max_num_pages == '1' ) {} else {echo '';}
      }
      echo '</div></nav></div>';
    }

    // Return outbut buffer
    return ob_get_clean();

  }
}
add_shortcode( 'ziph_blog', 'ziph_blog_function' );