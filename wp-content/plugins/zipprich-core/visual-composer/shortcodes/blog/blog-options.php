<?php
/**
 * Blog - Shortcode Options
 */
add_action( 'init', 'ziph_blog_vc_map' );
if ( ! function_exists( 'ziph_blog_vc_map' ) ) {
  function ziph_blog_vc_map() {
    vc_map( array(
      "name" => __( "Blog", 'zipprich-core'),
      "base" => "ziph_blog",
      "description" => __( "Blog Styles", 'zipprich-core'),
      "icon" => "fa fa-newspaper-o color-red",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          'type' => 'dropdown',
          'heading' => __( 'Blog Style', 'zipprich-core' ),
          'value' => array(
            __( 'Select Blog Style', 'zipprich-core' ) => '',
            __( 'Style One (List)', 'zipprich-core' ) => 'ziph-blog-one',
            __( 'Style Two (Grid)', 'zipprich-core' ) => 'ziph-blog-two',
            __( 'Style Three (List Small for Section Block)', 'zipprich-core' ) => 'ziph-blog-three',
          ),
          'admin_label' => true,
          'param_name' => 'blog_style',
          'description' => __( 'Select your blog style.', 'zipprich-core' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Columns', 'zipprich-core' ),
          'value' => array(
            __( 'Select Blog Columns', 'zipprich-core' ) => '',
            __( 'Column Two', 'zipprich-core' ) => 'col-md-6',
            __( 'Column Three', 'zipprich-core' ) => 'col-md-4',
          ),
          'admin_label' => true,
          'param_name' => 'blog_column',
          'description' => __( 'Select your blog column.', 'zipprich-core' ),
          'dependency' => array(
            'element' => 'blog_style',
            'value' => array('ziph-blog-two'),
          ),
        ),
        array(
          "type"        =>'textfield',
          "heading"     =>__('Limit', 'zipprich-core'),
          "param_name"  => "blog_limit",
          "value"       => "",
          'admin_label' => true,
          'edit_field_class' => 'vc_col-md-6 vc_column vt_field_space',
          "description" => __( "Enter the number of items to show.", 'zipprich-core'),
        ),
        array(
          "type"        =>'switcher',
          "heading"     =>__('Ignore Sticky Posts', 'zipprich-core'),
          "param_name"  => "ignore_sticky_posts",
          "value"       => "",
          "std"         => false,
          'edit_field_class' => 'vc_col-md-6 vc_column vt_field_space',
          "description" => __( "Enable if you want to ignore sticky post.", 'zipprich-core'),
        ),

        array(
    			"type"        => "notice",
    			"heading"     => __( "Meta's to Hide", 'zipprich-core' ),
    			"param_name"  => 'mts_opt',
    			'class'       => 'cs-warning',
    			'value'       => '',
    		),
        array(
          "type"        => 'switcher',
          "heading"     => __('Category', 'zipprich-core'),
          "param_name"  => "blog_category",
          "value"       => "",
          "std"         => false,
          'edit_field_class' => 'vc_col-md-4 vc_column vt_field_space',
        ),
        array(
          "type"        =>'switcher',
          "heading"     =>__('Date', 'zipprich-core'),
          "param_name"  => "blog_date",
          "value"       => "",
          "std"         => false,
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
        ),
        array(
          "type"        =>'switcher',
          "heading"     =>__('Comments', 'zipprich-core'),
          "param_name"  => "blog_comments",
          "value"       => "",
          "std"         => false,
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
        ),

        array(
    			"type"        => "notice",
    			"heading"     => __( "Listing", 'zipprich-core' ),
    			"param_name"  => 'lsng_opt',
    			'class'       => 'cs-warning',
    			'value'       => '',
    		),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Order', 'zipprich-core' ),
          'value' => array(
            __( 'Select Blog Order', 'zipprich-core' ) => '',
            __('Asending', 'zipprich-core') => 'ASC',
            __('Desending', 'zipprich-core') => 'DESC',
          ),
          'param_name' => 'blog_order',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Order By', 'zipprich-core' ),
          'value' => array(
            __('None', 'zipprich-core') => 'none',
            __('ID', 'zipprich-core') => 'ID',
            __('Author', 'zipprich-core') => 'author',
            __('Title', 'zipprich-core') => 'title',
            __('Date', 'zipprich-core') => 'date',
          ),
          'param_name' => 'blog_orderby',
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Show only certain categories?', 'zipprich-core'),
          "param_name"  => "blog_show_category",
          "value"       => "",
          "description" => __( "Enter category SLUGS (comma separated) you want to display.", 'zipprich-core')
        ),
        array(
          "type"        => 'textfield',
          "heading"     => __('Short Content (Excerpt Length)', 'zipprich-core'),
          "param_name"  => "short_content",
          "value"       => "",
          "description" => __( "Enter the numeric value of, how many words you want in short content paragraph.", 'zipprich-core')
        ),
        array(
          "type"        =>'switcher',
          "heading"     =>__('Pagination', 'zipprich-core'),
          "param_name"  => "blog_pagination",
          "value"       => "",
          "std"         => true,
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"        =>'textfield',
          "heading"     =>__('Read More Button Text', 'zipprich-core'),
          "param_name"  => "read_more_txt",
          "value"       => "",
          "description" => __( "Enter read more button text.", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        ZipprichLib::vt_class_option(),

      )
    ) );
  }
}
