<?php
/**
 * Video - Shortcode Options
 */
add_action( 'init', 'ziph_video_vc_map' );
if ( ! function_exists( 'ziph_video_vc_map' ) ) {
	function ziph_video_vc_map() {

		vc_map( array(
			"name" => __( "Video", 'zipprich-core'),
			"base" => "ziph_video",
			"description" => __( "Video Style", 'zipprich-core'),
			"icon" => "fa fa-video-camera color-brown",
			"category" => ZipprichLib::ziph_cat_name(),
			"params" => array(

				array(
					'type' => 'attach_image',
					'value' => '',
					'heading' => __( 'Video Image', 'zipprich-core' ),
					'param_name' => 'video_img',
					),

				array(
					'type' => 'href',
					'value' => '',
					'heading' => __( 'Video Player Link', 'zipprich-core' ),
					'description' => __( 'Input video player iframe url here.', 'zipprich-core' ),
					'param_name' => 'video_link',
					),

				ZipprichLib::vt_class_option(),
      			), // Params
			));
	}
}