<?php
/* ==========================================================
    Video
=========================================================== */
if ( !function_exists('ziph_video_function')) {
  function ziph_video_function( $atts, $content = true ) {

    extract(shortcode_atts(array(
      'video_img'  => '',
      'video_link'  => '',
      'class'  => '',
    ), $atts));
    
    $zipprich_large_image =  wp_get_attachment_image_src( $video_img, 'fullsize', false, '' );
    $zipprich_large_image = $zipprich_large_image[0];
    $zipprich_large_image_alt = get_post_meta($video_img, '_wp_attachment_image_alt', true);

	if(class_exists('Aq_Resize')) {
		$post_img = aq_resize( $zipprich_large_image, '570', '260', true );
	} else {$post_img = $zipprich_large_image;}

    $output = '<div class="ziph-vds_warp '.esc_attr( $class ).'">';
    if($post_img){
    	$output .= '<div class="ziph-vds_poster"><img src="'.esc_url( $post_img ).'" alt="'.esc_attr( $zipprich_large_image_alt ).'"><a href="#" class="ziph-vd_play"></a></div>';
    }
    $output .= '<div class="ziph-vds"><iframe id="ziph-video" src="'.esc_url( $video_link ).'" width="462" height="260"></iframe></div>';
    $output .= '</div>';
    return $output;

  }
}
add_shortcode( 'ziph_video', 'ziph_video_function' );