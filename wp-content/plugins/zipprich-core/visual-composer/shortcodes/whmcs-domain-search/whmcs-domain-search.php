<?php
/* ==========================================================
  WHMCS Domain Search
=========================================================== */
if ( !function_exists('ziph_whmcs_domain_search_function')) {
  function ziph_whmcs_domain_search_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'domain_style'  => '',
      'title'  => '',
      'subtitle'  => '',
      'note'  => '',
      'link_list_items'  => '',
      'price_list_items'  => '',
      'class'  => '',
      // Style
      'text_color'  => '',
    ), $atts));

    // Group Field
    $link_list_items = (array) vc_param_group_parse_atts( $link_list_items );
    $get_link_each_list = array();
    foreach ( $link_list_items as $link_list_item ) {
      $link_each_list = $link_list_item;
      $link_each_list['title'] = isset( $link_list_item['title'] ) ? $link_list_item['title'] : '';
      $link_each_list['link'] = isset( $link_list_item['link'] ) ? $link_list_item['link'] : '';
      $get_link_each_list[] = $link_each_list;
    }

    // Group Field
    $price_list_items = (array) vc_param_group_parse_atts( $price_list_items );
    $get_price_each_list = array();
    foreach ( $price_list_items as $price_list_item ) {
      $price_each_list = $price_list_item;
      $price_each_list['title'] = isset( $price_list_item['title'] ) ? $price_list_item['title'] : '';
      $price_each_list['price'] = isset( $price_list_item['price'] ) ? $price_list_item['price'] : '';
      $get_price_each_list[] = $price_each_list;
    }

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    if ( $text_color ) {
      $inline_style .= '.ziph-domainsearch_area-'. $e_uniqid .' h2.ziph-domainMainSrch_title, .ziph-domainsearch_area-'. $e_uniqid .' h3.ziph-domainsrch_title, .ziph-domainsearch_area-'. $e_uniqid .' .ziph-domainsrch_links a, .ziph-domainsearch_area-'. $e_uniqid .' .ziph-domainsrch_price .ziph-dsp_col, .ziph-domainsearch_area-'. $e_uniqid .' p.whmp-domain-required {';
      $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
      $inline_style .= '}';
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    $styled_class  = ' ziph-domainsearch_area-'. $e_uniqid;

    if($domain_style == 'style-two') {
      $section_class = 'ziph-domainMainSearch_area';
      $serach_class = 'ziph-domainMainSearch';
      $wrap_class = 'ziph-domainMain_warp';
      $form_class = '';
      $form_price_class = 'text-right';
    } elseif($domain_style == 'style-three') {
      $section_class = 'ziph-hm2domainsearch_area';
      $serach_class = 'ziph-domainsearch ziph-hm2domainsearch';
      $wrap_class = 'ziph-domainsrch_warp';
      $form_class = 'center-block';
      $form_price_class = 'text-center';
    } else {
      $section_class = 'ziph-domainsearch_area';
      $serach_class = 'ziph-domainsearch';
      $wrap_class = 'ziph-domainsrch_warp';
      $form_class = 'ziph-flt_right';
      $form_price_class = 'text-right';
    }

    ob_start();
    ?>
    <div class="<?php echo esc_attr($section_class . ' ' . $styled_class . ' ' . $class ); ?>">
      <div class="<?php echo esc_attr($serach_class); ?>">
        <div class="ziph-fix <?php echo esc_attr($wrap_class); ?>">
          <?php if($domain_style == 'style-two') { ?>
            <div class="text-center  ziph-domainMainSrch_txt">
              <h2 class="ziph-domainMainSrch_title"><?php echo esc_attr( $title ); ?></h2>
              <?php if($subtitle) { ?>
              <p><?php echo esc_attr( $subtitle ); ?></p>
              <?php } ?>
            </div>
          <?php } elseif($domain_style == 'style-three') { ?>
            <h3 class="ziph-domainsrch_title"><?php echo esc_attr( $title ); ?></h3>
          <?php } else { ?>
            <h3 class="ziph-flt_left ziph-domainsrch_title"><?php echo esc_attr( $title ); ?></h3>
          <?php } ?>
          <div class="<?php echo esc_attr($form_class); ?> ziph-domainsrch_form">
            <?php
              if(function_exists('cc_whmcs_bridge_mainpage')){
                $whmcs_page_link = get_permalink(cc_whmcs_bridge_mainpage());
              } else {
                $whmcs_page_link = '';
              }
            ?>
            <form method="get" action="<?php echo $whmcs_page_link; ?>">
              <input type="hidden" name="ccce" value="cart">
              <input type="hidden" name="a" value="add">
              <input type="hidden" name="domain" value="register">
              <input type="hidden" name="systpl" value="portal">
              <input type="search" id="whmcs_domain_search" name="query" placeholder="<?php echo esc_html__( 'Enter your domain here', 'zipprich-core' ); ?>">
              <input id="domsrch_btn" value="<?php echo esc_html__( 'Search', 'zipprich-core' ); ?>" type="submit">
            </form>
            <?php if(zipprich_empty_array($get_link_each_list) == false) { ?>
            <div class="<?php echo esc_attr($form_price_class); ?>  ziph-domainsrch_links">
              <?php foreach ( $get_link_each_list as $key => $each_link_list ) {  ?>
                <a href="<?php echo esc_url( $each_link_list['link'] ); ?>"><?php echo esc_attr( $each_link_list['title'] ); ?></a>
              <?php } ?>
            </div>
            <?php } ?>
          </div>
        </div>

        <?php if(zipprich_empty_array($get_price_each_list) == false) { ?>
        <div class="<?php echo esc_attr($form_price_class); ?>  ziph-domainsrch_price">
          <?php foreach ( $get_price_each_list as $key => $each_price_list ) {  ?>
          <span class="ziph-dsp_col">
            <strong><?php echo esc_attr( $each_price_list['title'] ); ?></strong>
            <?php echo esc_attr( $each_price_list['price'] ); ?>
          </span>
          <?php } ?>
        </div>
        <?php } ?>

        <?php if($note) { ?>
        <div class="text-center">
          <p class="whmp-domain-required"><?php echo esc_attr( $note ); ?></p>
        </div>
        <?php } ?>
      </div>
    </div>

    <?php
    $output = ob_get_clean();

    return $output;
  }
}
add_shortcode( 'ziph_whmcs_domain_search', 'ziph_whmcs_domain_search_function' );