<?php
/**
 * WHMCS Domain Search - Shortcode Options
 */
add_action( 'init', 'ziph_whmcs_domain_search_vc_map' );
if ( ! function_exists( 'ziph_whmcs_domain_search_vc_map' ) ) {
  function ziph_whmcs_domain_search_vc_map() {
    vc_map( array(
      "name" => __( "WHMCS - Domain Search", 'zipprich-core'),
      "base" => "ziph_whmcs_domain_search",
      "description" => __( "Domain Search Styles", 'zipprich-core'),
      "icon" => "fa fa-globe color-pink",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(
        array(
          'type' => 'notice',
          'value' => '',
          'class' => 'cs-warning',
          'heading' => __( 'This addons is associated with WHMCS Bridge plugins', 'zipprich-core' ),
          'param_name' => 'vt_notice',
        ),              
        array(
          'type'        => 'dropdown',
          'heading'     => __( "Domain Search Area Style", 'zipprich-core'),
          'param_name'  => 'domain_style',
          'value'       => array(
            __( "Select Style", 'zipprich-core') => '',
            __( "Style One (Left Title + Right Search Box)", 'zipprich-core')   => 'style-one',
            __( "Style Two (Centered Large Title + Search Box)", 'zipprich-core')   => 'style-two',
            __( "Style Three (Centered Title + Search Box)", 'zipprich-core')   => 'style-three',
          ),
          'description' => __( "Select Domain Search Area Style", 'zipprich-core'),
        ),
        array(
          'type' => 'textfield',
          'value' => '',
          'admin_label' => true,
          'heading' => __( 'Section Title', 'zipprich-core' ),
          'param_name' => 'title',
        ),  
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Section Sub Title', 'zipprich-core' ),
          'param_name' => 'subtitle',
        ),    
        array(
          'type' => 'textfield',
          'value' => '',
          'heading' => __( 'Domain Section Note', 'zipprich-core' ),
          'param_name' => 'note',
        ),  
        // Domain Search Link List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Domain Area Link Lists', 'zipprich-core' ),
          'param_name' => 'link_list_items',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Title', 'zipprich-core' ),
              'param_name' => 'title',
            ),
            array(
              'type' => 'href',
              'value' => '',
              'heading' => __( 'Link', 'zipprich-core' ),
              'param_name' => 'link',
            ),
          )
        ),
        // Domain Search Link List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Domain Area Pricing Lists', 'zipprich-core' ),
          'param_name' => 'price_list_items',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Title', 'zipprich-core' ),
              'param_name' => 'title',
            ),
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Price', 'zipprich-core' ),
              'param_name' => 'price',
            ),
          )
        ),
        ZipprichLib::vt_class_option(),

        // Style
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Badge Color', 'zipprich-core' ),
          'param_name' => 'text_color',
          'group' => __( 'Style', 'zipprich-core' ),
        ),
      )
    ) );
  }
}