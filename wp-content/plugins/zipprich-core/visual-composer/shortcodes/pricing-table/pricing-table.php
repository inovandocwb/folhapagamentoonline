<?php
/* ==========================================================
  Pricing Table
=========================================================== */
if ( !function_exists('ziph_pricing_table_function')) {
  function ziph_pricing_table_function( $atts, $content = NULL ) {

    extract(shortcode_atts(array(
      'pricing_table_style'  => '',
      'pricing_table_title'  => '',
      'pricing_table_desc'  => '',
      'pricing_table_price'  => '',
      'pricing_table_time'  => '',
      'pricing_table_image'  => '',
      'info_list_items'  => '',
      'pricing_table_button_text'  => '',
      'pricing_table_button_link'  => '',
      'class'  => '',
      // Style
      ''  => '',
      'box_bg_color'  => '',
      'box_text_color'  => '',
      'btn_text_color'  => '',
      'btn_text_hover_color'  => '',
      'btn_bg_color'  => '',
      'btn_bg_hover_color'  => '',
      'btn_border_color'  => '',
      'btn_border_hover_color'  => ''
    ), $atts));

    // Group Field
    $info_list_items = (array) vc_param_group_parse_atts( $info_list_items );
    $get_each_list = array();
    foreach ( $info_list_items as $list_item ) {
      $each_list = $list_item;
      $each_list['pricing_table_list'] = isset( $list_item['pricing_table_list'] ) ? $list_item['pricing_table_list'] : '';
      $get_each_list[] = $each_list;
    }

    // Shortcode Style CSS
    $e_uniqid        = uniqid();
    $inline_style  = '';

    // BG Color
    if ( $pricing_table_style == 'style-one' ) {
      if ( $box_bg_color ) {
        $inline_style .= '.ziph-pricing_single-'. $e_uniqid .' .ziph-tblpricing {';
        $inline_style .= ( $box_bg_color ) ? 'background-color:'. $box_bg_color .';' : '';
        $inline_style .= '}';
      }

      if ( $box_text_color ) {
        $inline_style .= '.ziph-pricing_single-'. $e_uniqid .' .ziph-tblpricing, .ziph-pricing_single-'. $e_uniqid .' .ziph-tblpricing q {';
        $inline_style .= ( $box_text_color ) ? 'color:'. $box_text_color .';' : '';
        $inline_style .= '}';
      }
   }

   if ( $pricing_table_style == 'style-two' ) {
	    if ( $box_bg_color ) {
	      $inline_style .= '.ziph-hostpltfrm_single-'. $e_uniqid .' {';
	      $inline_style .= ( $box_bg_color ) ? 'background-color:'. $box_bg_color .';' : '';
	      $inline_style .= '}';
	    }

	    if ( $box_text_color ) {
	      $inline_style .= '.ziph-hostpltfrm_single-'. $e_uniqid .' h3, .ziph-hostpltfrm_single-'. $e_uniqid .' h4, .ziph-hostpltfrm_single-'. $e_uniqid .' h5, .ziph-hostpltfrm_single-'. $e_uniqid .' p {';
	      $inline_style .= ( $box_text_color ) ? 'color:'. $box_text_color .';' : '';
	      $inline_style .= '}';
	    }
	 }

    // Button Text Color
    if ( $btn_text_color || $btn_bg_color) {
      $inline_style .= '.ziph-pricing_single-'. $e_uniqid .' .btn.ziph-btn, .ziph-hostpltfrm_single-'. $e_uniqid .' .ziph-bdr_btn {';
      $inline_style .= ( $btn_text_color ) ? 'color:'. $btn_text_color .';' : '';
      $inline_style .= ( $btn_bg_color ) ? 'background-color:'. $btn_bg_color .';' : '';
      $inline_style .= '}';
    }

    // Button Text Hover Color
    if ( $btn_bg_hover_color || $btn_text_hover_color ) {
      $inline_style .= '.ziph-pricing_single-'. $e_uniqid .' .btn.ziph-btn:hover, .ziph-pricing_single-'. $e_uniqid .' .btn.ziph-btn:focus, .ziph-hostpltfrm_single-'. $e_uniqid .' .ziph-bdr_btn:hover, .ziph-hostpltfrm_single-'. $e_uniqid .' .ziph-bdr_btn:focus {';
      $inline_style .= ( $btn_text_hover_color ) ? 'color:'. $btn_text_hover_color .' !important;' : '';
      $inline_style .= ( $btn_bg_hover_color ) ? 'background-color:'. $btn_bg_hover_color .' !important;' : '';
      $inline_style .= '}';
    }

    if ( $pricing_table_style == 'style-two' ) {
	    if ( $btn_border_color ) {
	      $inline_style .= '.ziph-hostpltfrm_single-'. $e_uniqid .' .ziph-bdr_btn {';
	      $inline_style .= ( $btn_border_color ) ? 'border-color:'. $btn_border_color .' !important;' : '';
	      $inline_style .= '}';
	    }
	    if ( $btn_border_hover_color ) {
	      $inline_style .= '.ziph-hostpltfrm_single-'. $e_uniqid .' .ziph-bdr_btn:hover, .ziph-hostpltfrm_single-'. $e_uniqid .' .ziph-bdr_btn:focus {';
	      $inline_style .= ( $btn_border_hover_color ) ? 'border-color:'. $btn_border_hover_color .' !important;' : '';
	      $inline_style .= '}';
	    }
    }

    // add inline style
    zipprich_add_inline_style( $inline_style );
    if ($pricing_table_style == 'style-two') {
    	$styled_class  = ' ziph-hostpltfrm_single-'. $e_uniqid;
    } else {
    	$styled_class  = ' ziph-pricing_single-'. $e_uniqid;
    }


    // Group Param Output
    $output = '';

    ob_start();?>

    	<?php if($pricing_table_style == 'style-two') { ?>
	        <div class="text-center ziph-hostpltfrm_single <?php echo esc_attr( $class . ' '. $styled_class) ?>">
	            <h4><?php echo esc_attr( $pricing_table_title ); ?></h4>
      				<?php
      					$zipprich_large_image =  wp_get_attachment_image_src( $pricing_table_image, 'fullsize', false, '' );
      					$zipprich_large_image = $zipprich_large_image[0];
      					$zipprich_large_image_alt = get_post_meta($pricing_table_image, '_wp_attachment_image_alt', true);

                if(class_exists('Aq_Resize')) {
                  $zipprich_pricing_img = aq_resize( $zipprich_large_image, '79', '74', true );
                  $zipprich_pricing_img = $zipprich_pricing_img ? $zipprich_pricing_img : $zipprich_large_image;
                } else {$zipprich_pricing_img = $zipprich_large_image;}

      				  if (!empty($zipprich_pricing_img)) {
      				?>
	            <div class="ziph-htplt_icon">
	                <img src="<?php echo esc_url( $zipprich_pricing_img ); ?>" alt="<?php echo esc_attr( $zipprich_large_image_alt ); ?>">
	            </div>
	            <?php } ?>
	            <p><?php echo do_shortcode( wp_kses($pricing_table_desc, array('br' => array())) ); ?></p>
	            <div class="ziph-htplt_priceform">
	                <h5><?php echo esc_html__( 'PRICES FROM', 'zipprich-core' ); ?></h5>
	                <h3><?php echo esc_attr( $pricing_table_price ); ?> <span>/</span> <sub><?php echo esc_attr( $pricing_table_time ); ?></sub></h3>
	                <a class="ziph-bdr_btn" href="<?php echo esc_url( $pricing_table_button_link ); ?>"><?php echo esc_attr( $pricing_table_button_text ); ?></a>
	            </div>
	        </div>
    	<?php } else { ?>
	        <div class="text-center ziph-pricing_single <?php echo esc_attr( $class . ' '. $styled_class) ?>">
	            <div class="ziph-tblpric_hdr">
	                <?php echo esc_attr( $pricing_table_title ); ?>
	            </div>
	            <div class="ziph-tblpricing">
	                <div class="ziph-tblpric"><?php echo esc_attr( $pricing_table_price ); ?><sub>/<?php echo esc_attr( $pricing_table_time ); ?></sub></div>
	                <q><?php echo do_shortcode( wp_kses($pricing_table_desc, array('br' => array())) ); ?></q>
	                <a href="<?php echo esc_attr( $pricing_table_button_link ); ?>" class="btn ziph-btn"><?php echo esc_attr( $pricing_table_button_text ); ?></a>
	            </div>
	            <div class="ziph-tblpric_list">
	                <ul class="list-unstyled">
		                <?php
        							foreach ( $get_each_list as $each_list ) {
        								echo '<li>'.$each_list['pricing_table_list'].'</li>';
        							}
      		  				?>
	                </ul>
	            </div>
	        </div>
        <?php } ?>

    <?php
    $output .= ob_get_clean();
    return $output;
  }
}
add_shortcode( 'ziph_pricing_table', 'ziph_pricing_table_function' );