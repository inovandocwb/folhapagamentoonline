<?php
/**
 * Pricing Table Options - Shortcode Options
 */
add_action( 'init', 'ziph_pricing_table_vc_map' );
if ( ! function_exists( 'ziph_pricing_table_vc_map' ) ) {
  function ziph_pricing_table_vc_map() {
    vc_map( array(
      "name" => __( "Pricing Table", 'zipprich-core'),
      "base" => "ziph_pricing_table",
      "description" => __( "Pricing Table Styles", 'zipprich-core'),
      "icon" => "fa fa-star color-blue",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          'type' => 'dropdown',
          'heading' => __( 'Pricing Table Style', 'zipprich-core' ),
          'value' => array(
            __( 'Select Pricing Table Style', 'zipprich-core' ) => '',
            __( 'Style One', 'zipprich-core' ) => 'style-one',
            __( 'Style Two', 'zipprich-core' ) => 'style-two',
          ),
          'admin_label' => true,
          'param_name' => 'pricing_table_style',
          'description' => __( 'Select pricing table style', 'zipprich-core' ),
        ),

        array(
          "type" => "textfield",
          "heading" => __( "Title", 'zipprich-core' ),
          "param_name" => "pricing_table_title",
          'value' => '',
          'admin_label' => true,
          "description" => __( "Enter pricing table title.", 'zipprich-core'),
        ),

        array(
          "type" => "textarea",
          "heading" => __( "Description", 'zipprich-core' ),
          "param_name" => "pricing_table_desc",
          'value' => '',
          "description" => __( "Enter table description.", 'zipprich-core'),
        ),

        array(
          "type" => "textfield",
          "heading" => __( "Price", 'zipprich-core' ),
          "param_name" => "pricing_table_price",
          'value' => '',
          "description" => __( "Enter table price.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),

        array(
          "type" => "textfield",
          "heading" => __( "Time", 'zipprich-core' ),
          "param_name" => "pricing_table_time",
          'value' => '',
          "description" => __( "Enter table time.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),       

        // List
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Pricing Info', 'zipprich-core' ),
          'param_name' => 'info_list_items',
          // Note params is mapped inside param-group:
          'params' => array(
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Info', 'zipprich-core' ),
              'param_name' => 'pricing_table_list',
              'admin_label' => true
            ),
          ),
          'dependency' => array(
            'element' => 'pricing_table_style',
            'value' => 'style-one',
          ),          
        ),

        array(
          "type" => "attach_image",
          "heading" => __( "Image", 'zipprich-core' ),
          "param_name" => "pricing_table_image",
          'value' => '',
          "description" => __( "Upload icon/image.", 'zipprich-core'),
          'dependency' => array(
            'element' => 'pricing_table_style',
            'value' => 'style-two',
          ),   
        ),

        array(
          "type" => "textfield",
          "heading" => __( "Button Text", 'zipprich-core' ),
          "param_name" => "pricing_table_button_text",
          'value' => '',
          "description" => __( "Enter your button text.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),

        array(
          "type" => "textfield",
          "heading" => __( "Button Link", 'zipprich-core' ),
          "param_name" => "pricing_table_button_link",
          'value' => '',
          "description" => __( "Enter your button link.", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space',
        ),

        ZipprichLib::vt_class_option(),

        // Style
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Price Box Background Color', 'zipprich-core' ),
          'param_name' => 'box_bg_color',
          'group' => __( 'Styling', 'zipprich-core' ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          'type' => 'colorpicker',
          'value' => '',
          'heading' => __( 'Price Box Text Color', 'zipprich-core' ),
          'param_name' => 'box_text_color',
          'group' => __( 'Styling', 'zipprich-core' ),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',  
        ),        
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Color", 'zipprich-core' ),
          "param_name" => "btn_text_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Text Hover Color", 'zipprich-core' ),
          "param_name" => "btn_text_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Color", 'zipprich-core' ),
          "param_name" => "btn_bg_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Background Hover Color", 'zipprich-core' ),
          "param_name" => "btn_bg_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Border Color", 'zipprich-core' ),
          "param_name" => "btn_border_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'dependency' => array(
            'element' => 'pricing_table_style',
            'value' => 'style-two',
          ),   
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),
        array(
          "type" => "colorpicker",
          "heading" => __( "Button Border Hover Color", 'zipprich-core' ),
          "param_name" => "btn_border_hover_color",
          'value' => '',
          "group" => __( "Styling", 'zipprich-core'),
          'dependency' => array(
            'element' => 'pricing_table_style',
            'value' => 'style-two',
          ),   
          'edit_field_class'  => 'vc_col-md-6 vc_column vt_field_space'
        ),

      )
    ) );
  }
}
