<?php
/**
 * Gmap - Shortcode Options
 */
add_action( 'init', 'ziph_gmap_vc_map' );
if ( ! function_exists( 'ziph_gmap_vc_map' ) ) {
  function ziph_gmap_vc_map() {
    vc_map( array(
      "name" => __( "Google Map", 'zipprich-core'),
      "base" => "ziph_gmap",
      "description" => __( "Google Map Styles", 'zipprich-core'),
      "icon" => "fa fa-map color-cadetblue",
      "category" => ZipprichLib::ziph_cat_name(),
      "params" => array(

        array(
          "type"        => "notice",
          "heading"     => __( "API KEY", 'zipprich-core' ),
          "param_name"  => 'api_key',
          'class'       => 'cs-info',
          'value'       => '',
        ),
        array(
          "type"        =>'textfield',
          "heading"     =>__('Enter Map ID', 'zipprich-core'),
          "param_name"  => "gmap_id",
          "value"       => "",
          "description" => __( 'Enter google map ID. If you\'re using this in <strong>Map Tab</strong> shortcode, enter unique ID for each map tabs. Else leave it as blank. <strong>Note : This should same as Tab ID in Map Tabs shortcode.</strong>', 'zipprich-core'),
          'admin_label' => true,
        ),
        array(
          "type"        =>'textfield',
          "heading"     =>__('Enter your Google Map API Key', 'zipprich-core'),
          "param_name"  => "gmap_api",
          "value"       => "",
          "description" => __( 'New Google Maps usage policy dictates that everyone using the maps should register for a free API key. <br />Please create a key for "Google Static Maps API" and "Google Maps Embed API" using the <a href="https://console.developers.google.com/project" target="_blank">Google Developers Console</a>.<br /> Or follow this step links : <br /><a href="https://console.developers.google.com/flows/enableapi?apiid=maps_embed_backend&keyType=CLIENT_SIDE&reusekey=true" target="_blank">1. Step One</a> <br /><a href="https://console.developers.google.com/flows/enableapi?apiid=static_maps_backend&keyType=CLIENT_SIDE&reusekey=true" target="_blank">2. Step Two</a><br /> If you still receive errors, please check following link : <a href="https://churchthemes.com/2016/07/15/page-didnt-load-google-maps-correctly/" target="_blank">How to Fix?</a>', 'zipprich-core'),
        ),

        array(
          "type"        => "notice",
          "heading"     => __( "Map Settings", 'zipprich-core' ),
          "param_name"  => 'map_settings',
          'class'       => 'cs-info',
          'value'       => '',
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Google Map Type', 'zipprich-core' ),
          'value' => array(
            __( 'Select Type', 'zipprich-core' ) => '',
            __( 'ROADMAP', 'zipprich-core' ) => 'ROADMAP',
            __( 'SATELLITE', 'zipprich-core' ) => 'SATELLITE',
            __( 'HYBRID', 'zipprich-core' ) => 'HYBRID',
            __( 'TERRAIN', 'zipprich-core' ) => 'TERRAIN',
          ),
          'admin_label' => true,
          'param_name' => 'gmap_type',
          'description' => __( 'Select your google map type.', 'zipprich-core' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Google Map Style', 'zipprich-core' ),
          'value' => array(
            __( 'Select Style', 'zipprich-core' ) => '',
            __( 'Gray Scale', 'zipprich-core' ) => "gray-scale",
            __( 'Mid Night', 'zipprich-core' ) => "mid-night",
            __( 'Blue Water', 'zipprich-core' ) => 'blue-water',
            __( 'Light Dream', 'zipprich-core' ) => 'light-dream',
            __( 'Pale Dawn', 'zipprich-core' ) => 'pale-dawn',
            __( 'Apple Maps-esque', 'zipprich-core' ) => 'apple-maps',
            __( 'Blue Essence', 'zipprich-core' ) => 'blue-essence',
            __( 'Unsaturated Browns', 'zipprich-core' ) => 'unsaturated-browns',
            __( 'Paper', 'zipprich-core' ) => 'paper',
            __( 'Midnight Commander', 'zipprich-core' ) => 'midnight-commander',
            __( 'Light Monochrome', 'zipprich-core' ) => 'light-monochrome',
            __( 'Flat Map', 'zipprich-core' ) => 'flat-map',
            __( 'Retro', 'zipprich-core' ) => 'retro',
            __( 'becomeadinosaur', 'zipprich-core' ) => 'becomeadinosaur',
            __( 'Neutral Blue', 'zipprich-core' ) => 'neutral-blue',
            __( 'Subtle Grayscale', 'zipprich-core' ) => 'subtle-grayscale',
            __( 'Ultra Light with Labels', 'zipprich-core' ) => 'ultra-light-labels',
            __( 'Shades of Grey', 'zipprich-core' ) => 'shades-grey',
          ),
          'admin_label' => true,
          'param_name' => 'gmap_style',
          'description' => __( 'Select your google map style.', 'zipprich-core' ),
          'dependency' => array(
            'element' => 'gmap_type',
            'value' => 'ROADMAP',
          ),
        ),
        array(
          "type"        =>'textfield',
          "heading"     =>__('Height', 'zipprich-core'),
          "param_name"  => "gmap_height",
          "value"       => "",
          "description" => __( "Enter the px value for map height. This will not work if you add this shortcode into the Map Tab shortcode.", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),
        array(
          "type"        =>'attach_image',
          "heading"     =>__('Common Marker', 'zipprich-core'),
          "param_name"  => "gmap_common_marker",
          "value"       => "",
          "description" => __( "Upload your custom marker.", 'zipprich-core'),
          'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
        ),

        array(
          "type"        => "notice",
          "heading"     => __( "Enable & Disable", 'zipprich-core' ),
          "param_name"  => 'enb_disb',
          'class'       => 'cs-info',
          'value'       => '',
        ),
        array(
          "type"        =>'switcher',
          "heading"     =>__('Scroll Wheel', 'zipprich-core'),
          "param_name"  => "gmap_scroll_wheel",
          "value"       => "",
          "std"         => false,
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
        ),
        array(
          "type"        =>'switcher',
          "heading"     =>__('Street View Control', 'zipprich-core'),
          "param_name"  => "gmap_street_view",
          "value"       => "",
          "std"         => false,
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
        ),
        array(
          "type"        =>'switcher',
          "heading"     =>__('Map Type Control', 'zipprich-core'),
          "param_name"  => "gmap_maptype_control",
          "value"       => "",
          "std"         => false,
          'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
        ),

        // Map Markers
        array(
          "type"        => "notice",
          "heading"     => __( "Map Pins", 'zipprich-core' ),
          "param_name"  => 'map_pins',
          'class'       => 'cs-info',
          'value'       => '',
        ),
        array(
          'type' => 'param_group',
          'value' => '',
          'heading' => __( 'Map Locations', 'zipprich-core' ),
          'param_name' => 'locations',
          'params' => array(

            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Latitude', 'zipprich-core' ),
              'param_name' => 'latitude',
              'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
              'admin_label' => true,
              'description' => __( 'Find Latitude : <a href="http://www.latlong.net/" target="_blank">latlong.net</a>', 'zipprich-core' ),
            ),
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Longitude', 'zipprich-core' ),
              'param_name' => 'longitude',
              'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
              'admin_label' => true,
              'description' => __( 'Find Longitude : <a href="http://www.latlong.net/" target="_blank">latlong.net</a>', 'zipprich-core' ),
            ),
            array(
              'type' => 'attach_image',
              'value' => '',
              'heading' => __( 'Custom Marker', 'zipprich-core' ),
              'param_name' => 'custom_marker',
              "description" => __( "Upload your unique map marker if your want to differentiate from others.", 'zipprich-core'),
            ),
            array(
              'type' => 'textfield',
              'value' => '',
              'heading' => __( 'Heading', 'zipprich-core' ),
              'param_name' => 'location_heading',
              'admin_label' => true,
            ),
            array(
              'type' => 'textarea',
              'value' => '',
              'heading' => __( 'Content', 'zipprich-core' ),
              'param_name' => 'location_text',
            ),

          )
        ),

        ZipprichLib::vt_class_option(),

        // Design Tab
        array(
          "type" => "css_editor",
          "heading" => __( "Text Size", 'zipprich-core' ),
          "param_name" => "css",
          "group" => __( "Design", 'zipprich-core'),
        ),

      )
    ) );
  }
}
