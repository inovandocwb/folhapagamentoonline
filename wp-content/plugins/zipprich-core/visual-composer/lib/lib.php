<?php
/**
 * Visual Composer Library
 * Common Fields
 */
class ZipprichLib {

	// Get Theme Name
	public static function ziph_cat_name() {
		return __( "by VictorThemes", 'zipprich-core' );
	}

	// Notice
	public static function vt_notice_field($heading, $param, $class, $group) {
		return array(
			"type"        => "notice",
			"heading"     => $heading,
			"param_name"  => $param,
			'class'       => $class,
			'value'       => '',
			"group"       => $group,
		);
	}

	// Extra Class
	public static function vt_class_option() {
		return array(
		  "type" => "textfield",
		  "heading" => __( "Extra class name", 'zipprich-core' ),
		  "param_name" => "class",
		  'value' => '',
		  "description" => __( "Custom styled class name.", 'zipprich-core')
		);
	}

	// ID
	public static function vt_id_option() {
		return array(
		  "type" => "textfield",
		  "heading" => __( "Element ID", 'zipprich-core' ),
		  "param_name" => "id",
		  'value' => '',
		  "description" => __( "Enter your ID for this element. If you want.", 'zipprich-core')
		);
	}

	// Open Link in New Tab
	public static function vt_open_link_tab() {
		return array(
			"type" => "switcher",
			"heading" => __( "Open New Tab? (Links)", 'zipprich-core' ),
			"param_name" => "open_link",
			"std" => true,
			'value' => '',
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
		);
	}

	/**
	 * Carousel Default Options
	 */

	// Loop
	public static function vt_carousel_loop() {
		return array(
			"type" => "switcher",
			"heading" => __( "Disable Loop?", 'zipprich-core' ),
			"group" => __( "Carousel", 'zipprich-core' ),
			"param_name" => "carousel_loop",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			"description" => __( "Continuously moving carousel, if enabled.", 'zipprich-core')
		);
	}
	// Items
	public static function vt_carousel_items() {
		return array(
		  "type" => "textfield",
			"heading" => __( "Items", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_items",
		  'value' => '',
			'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
		  "description" => __( "Enter the numeric value of how many items you want in per slide.", 'zipprich-core')
		);
	}
	// Margin
	public static function vt_carousel_margin() {
		return array(
		  "type" => "textfield",
			"heading" => __( "Margin", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_margin",
		  'value' => '',
			'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
		  "description" => __( "Enter the numeric value of how much space you want between each carousel item.", 'zipprich-core')
		);
	}
	// Dots
	public static function vt_carousel_dots() {
		return array(
		  "type" => "switcher",
			"heading" => __( "Dots", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_dots",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
		  "description" => __( "If you want Carousel Dots, enable it.", 'zipprich-core')
		);
	}
	// Nav
	public static function vt_carousel_nav() {
		return array(
		  "type" => "switcher",
			"heading" => __( "Navigation", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_nav",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
		  "description" => __( "If you want Carousel Navigation, enable it.", 'zipprich-core')
		);
	}
	// Autoplay Timeout
	public static function vt_carousel_autoplay_timeout() {
		return array(
		  "type" => "textfield",
			"heading" => __( "Autoplay Timeout", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_autoplay_timeout",
		  'value' => '',
		  "description" => __( "Change carousel Autoplay timing value. Default : 5000. Means 5 seconds.", 'zipprich-core')
		);
	}
	// Autoplay
	public static function vt_carousel_autoplay() {
		return array(
		  "type" => "switcher",
			"heading" => __( "Autoplay", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_autoplay",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
		  "description" => __( "If you want to start Carousel automatically, enable it.", 'zipprich-core')
		);
	}
	// Animate Out
	public static function vt_carousel_animateout() {
		return array(
		  "type" => "switcher",
			"heading" => __( "Animate Out", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_animate_out",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
		  "description" => __( "CSS3 animation out.", 'zipprich-core')
		);
	}
	// Mouse Drag
	public static function vt_carousel_mousedrag() {
		return array(
		  "type" => "switcher",
			"heading" => __( "Disable Mouse Drag?", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_mousedrag",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
		  "description" => __( "If you want to disable Mouse Drag, check it.", 'zipprich-core')
		);
	}
	// Auto Width
	public static function vt_carousel_autowidth() {
		return array(
		  "type" => "switcher",
			"heading" => __( "Auto Width", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_autowidth",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
		  "description" => __( "Adjust Auto Width automatically for each carousel items.", 'zipprich-core')
		);
	}
	// Auto Height
	public static function vt_carousel_autoheight() {
		return array(
		  "type" => "switcher",
			"heading" => __( "Auto Height", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_autoheight",
			"on_text" => __( "Yes", 'zipprich-core' ),
			"off_text" => __( "No", 'zipprich-core' ),
			"value" => '',
			'edit_field_class'   => 'vc_col-md-6 vc_column vt_field_space',
		  "description" => __( "Adjust Auto Height automatically for each carousel items.", 'zipprich-core')
		);
	}
	// Tablet
	public static function vt_carousel_tablet() {
		return array(
		  "type" => "textfield",
			"heading" => __( "Tablet", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_tablet",
		  'value' => '',
			'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
		  "description" => __( "Enter number of items to show in tablet.", 'zipprich-core')
		);
	}
	// Mobile
	public static function vt_carousel_mobile() {
		return array(
		  "type" => "textfield",
			"heading" => __( "Mobile", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_mobile",
		  'value' => '',
			'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
		  "description" => __( "Enter number of items to show in mobile.", 'zipprich-core')
		);
	}
	// Small Mobile
	public static function vt_carousel_small_mobile() {
		return array(
		  "type" => "textfield",
			"heading" => __( "Small Mobile", 'zipprich-core' ),
		  "group" => __( "Carousel", 'zipprich-core' ),
		  "param_name" => "carousel_small_mobile",
		  'value' => '',
			'edit_field_class'   => 'vc_col-md-4 vc_column vt_field_space',
		  "description" => __( "Enter number of items to show in small mobile.", 'zipprich-core')
		);
	}

}

/* Shortcode Extends */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
  class WPBakeryShortCode_Ziph_Histories extends WPBakeryShortCodesContainer {
  }
  class WPBakeryShortCode_Ziph_Map_Tabs extends WPBakeryShortCodesContainer {
  }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_Ziph_History extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Map_Tab extends WPBakeryShortCode {}
}

// Group VC Addons
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
  class WPBakeryShortCode_Ziph_Info_Grids extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_Plan_Chart extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_Domain_Pricing extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_Whmcs_Domain_Pricing extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_Hosting_Pricing extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_Domain_Affiliate extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_Contact_Members extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_About extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Ziph_Video_Text extends WPBakeryShortCodesContainer {}
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_Ziph_Info_Grid_Title extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Info_Grid_Content extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Info_Grid_List extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Plan_Chart_Title extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Plan_Chart_Info extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Domain_Pricing_Title extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Domain_Pricing_Info extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Whmcs_Domain_Pricing_Title extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Whmcs_Domain_Pricing_Info extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Hosting_Pricing_Title extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Hosting_Pricing_Info extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Domain_Affiliate_Content extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Domain_Affiliate_List extends WPBakeryShortCode {}
  class WPBakeryShortCode_Ziph_Contact_Member extends WPBakeryShortCode {}
}

/*
 * Load All Shortcodes within a directory of visual-composer/shortcodes
 */
function ziph_all_shortcodes() {
	$dirs = glob( ZIPPRICH_SHORTCODE_PATH . '*', GLOB_ONLYDIR );
	if ( !$dirs ) return;
	foreach ($dirs as $dir) {
		$dirname = basename( $dir );

		/* Include all shortcodes backend options file */
		$options_file = $dir . DS . $dirname . '-options.php';
		$options = array();
		if ( file_exists( $options_file ) ) {
			include_once( $options_file );
		} else {
			continue;
		}

		/* Include all shortcodes frondend options file */
		$shortcode_class_file = $dir . DS . $dirname .'.php';
		if ( file_exists( $shortcode_class_file ) ) {
			include_once( $shortcode_class_file );
		}
	}
}
ziph_all_shortcodes();

if( ! function_exists( 'vc_add_shortcode_param' ) && function_exists( 'add_shortcode_param' ) ) {
  function vc_add_shortcode_param( $name, $form_field_callback, $script_url = null ) {
    return add_shortcode_param( $name, $form_field_callback, $script_url );
  }
}

/* Inline Style */
global $zipprich_all_inline_styles;
$zipprich_all_inline_styles = array();
if( ! function_exists( 'zipprich_add_inline_style' ) ) {
  function zipprich_add_inline_style( $style ) {
    global $zipprich_all_inline_styles;
    array_push( $zipprich_all_inline_styles, $style );
  }
}

/* Enqueue Inline Styles */
if ( ! function_exists( 'zipprich_enqueue_inline_styles' ) ) {
  function zipprich_enqueue_inline_styles() {

    global $zipprich_all_inline_styles;

    if ( ! empty( $zipprich_all_inline_styles ) ) {
      echo '<style id="zipprich-inline-style" type="text/css">'. zipprich_compress_css_lines( join( '', $zipprich_all_inline_styles ) ) .'</style>';
    }

  }
  add_action( 'wp_footer', 'zipprich_enqueue_inline_styles' );
}

/* Validate px entered in field */
if( ! function_exists( 'zipprich_core_check_px' ) ) {
  function zipprich_core_check_px( $num ) {
    return ( is_numeric( $num ) ) ? $num . 'px' : $num;
  }
}

/* Tabs Added Via ziph_vt_tabs_function */
if( function_exists( 'ziph_vt_tabs_function' ) ) {
  add_shortcode( 'vc_tabs', 'ziph_vt_tabs_function' );
  add_shortcode( 'vc_tab', 'ziph_vt_tab_function' );
}