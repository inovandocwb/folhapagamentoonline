<?php
/*
 * Get a Contact Widget
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

class zipprich_contactform_widget extends WP_Widget {

  /**
   * Specifies the widget name, description, class name and instatiates it
   */
  public function __construct() {
    parent::__construct(
      'ziph-get-contactform',
      VTHEME_NAME_P . __( ': Contact Form', 'zipprich-core' ),
      array(
        'classname'   => 'ziph-get-contactform',
        'description' => VTHEME_NAME_P . __( ' widget that displays get a contact form.', 'zipprich-core' )
      )
    );
  }

  public function ziph_contact_form() {
    $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );
    $contact_forms = array();
    if ( $cf7 ) {
      foreach ( $cf7 as $cform ) {
        $contact_forms[ $cform->ID ] = $cform->post_title;
      }
    } else {
      $contact_forms[ __( 'No contact forms found', 'zipprich-core' ) ] = 0;
    }

    return $contact_forms;
  }

  /**
   * Generates the back-end layout for the widget
   */
  public function form( $instance ) {
    // Default Values
    $instance   = wp_parse_args( $instance, array(
      'title'    => '',
      'contact_form_id' => '',
    ));

    // Title
    $title_value = esc_attr( $instance['title'] );
    $title_field = array(
      'id'    => $this->get_field_name('title'),
      'name'  => $this->get_field_name('title'),
      'type'  => 'text',
      'title' => __( 'Title :', 'zipprich-core' ),
      'wrap_class' => 'vt-cs-widget-fields',
    );
    echo cs_add_element( $title_field, $title_value );

    // Forms
    $contact_form_id_value = esc_attr( $instance['contact_form_id'] );
    $contact_form_id_field = array(
      'id'    => $this->get_field_name('contact_form_id'),
      'name'  => $this->get_field_name('contact_form_id'),
      'type'  => 'select',
      'options' => $this->ziph_contact_form(),
      'default_option' => __( 'Select contact form', 'zipprich-core' ),
      'title' => __( 'Contact Form 7', 'zipprich-core' ),
      'wrap_class' => 'vt-cs-widget-fields',
    );
    echo cs_add_element( $contact_form_id_field, $contact_form_id_value );

  }

  /**
   * Processes the widget's values
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;

    // Update values
    $instance['title']      = strip_tags( stripslashes( $new_instance['title'] ) );
    $instance['contact_form_id']   = strip_tags( stripslashes( $new_instance['contact_form_id'] ) );

    return $instance;
  }

  /**
   * Output the contents of the widget
   */
  public function widget( $args, $instance ) {
    // Extract the arguments
    extract( $args );

    $title      = apply_filters( 'widget_title', $instance['title'] );
    $contact_form_id   = $instance['contact_form_id'];

    // Display the markup before the widget
    echo $before_widget;
    echo '<div class="ziph-footer_conform">';

    if ( $title ) {
      echo $before_title . $title . $after_title;
    }

    echo '<div class="ziph-ftrform_warp">';
    echo zipprich_set_wpautop(do_shortcode( '[contact-form-7 id="'. $contact_form_id .'"]' ));
    echo '</div>';

    // Display the markup after the widget
    echo '</div>';
    echo $after_widget;
  }
}

// Register the widget using an annonymous function
add_action( 'widgets_init', create_function( '', 'register_widget( "zipprich_contactform_widget" );' ) );