<?php
/*
 * Text Widget
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

class zipprich_link_widget extends WP_Widget {

  /**
   * Specifies the widget name, description, class name and instatiates it
   */
  public function __construct() {
    parent::__construct(
      'zipprich-link-widget',
      VTHEME_NAME_P . __( ': Link Widget', 'zipprich-core' ),
      array(
        'classname'   => 'zipprich-link-widget',
        'description' => VTHEME_NAME_P . __( ' widget that displays link.', 'zipprich-core' )
      )
    );
  }

  /**
   * Generates the back-end layout for the widget
   */
  public function form( $instance ) {
    // Default Values
    $instance   = wp_parse_args( $instance, array(
      'title'       => '',
      'link_types'  => '',
      'page_ids'    => array(),
      'post_ids'    => array(),
      'link_group'  => array(),
    ));

    // Title
    $title_value = esc_attr( $instance['title'] );
    $title_field = array(
      'id'    => $this->get_field_name('title'),
      'name'  => $this->get_field_name('title'),
      'type'  => 'text',
      'title' => __( 'Title :', 'zipprich-core' ),
      'wrap_class' => 'vt-cs-widget-fields',
    );
    echo cs_add_element( $title_field, $title_value );

    // Types
    $link_types_value = esc_attr( $instance['link_types'] );
    $link_types_field = array(
      'id'    => $this->get_field_name('link_types'),
      'name'  => $this->get_field_name('link_types'),
      'type'  => 'select',
      'class'  => 'widefat',
      'options' => array(
        'page'   => __( 'Links from pages', 'zipprich-core' ),
        'post'   => __( 'Links from posts', 'zipprich-core' ),
        'custom' => __( 'Custom Links', 'zipprich-core' ),
      ),
      'default_option' => __( 'Select link type', 'zipprich-core' ),
      'title' => __( 'Link types :', 'zipprich-core' ),
      'wrap_class' => 'vt-cs-widget-fields',
    );
    echo cs_add_element( $link_types_field, $link_types_value );

    // Page IDs
    echo sprintf('<div class="ziph-select-area" data-controller="widget-'.esc_attr( $this->id_base ).'['.esc_attr( $this->number ).'][link_types]" data-condition="==" data-value="page"><label for="%1$s[]">%2$s:</label>', 
      esc_attr( $this->get_field_id( 'page_ids' ) ),
      esc_html__('Pages', 'zipprich-core')
    );

    echo '<select type="text" id="'.esc_attr( $this->get_field_id( 'page_ids' ) ).'[]" class="widefat ziph-select" name="'.esc_attr( $this->get_field_name( 'page_ids' ) ).'[]" multiple>';
      $pages = get_pages(array('posts_per_page' => -1));
      foreach ($pages as $key => $page) {
        if(isset($instance['page_ids'])){
          $selected = ( in_array($page->ID, $instance['page_ids']) ) ? 'selected="selected"' : '';
        } else {
          $selected = '';
        }
        echo '<option '.$selected.' value="' . $page->ID . '">' . $page->post_title . '</option>';
      }  
    echo '</select></div>';

    // Post IDs
    echo sprintf('<div class="ziph-select-area" data-controller="widget-'.esc_attr( $this->id_base ).'['.esc_attr( $this->number ).'][link_types]" data-condition="==" data-value="post"><label for="%1$s[]">%2$s:</label>', 
      esc_attr( $this->get_field_id( 'post_ids' ) ),
      esc_html__('Posts', 'zipprich-core')
    );

    echo '<select type="text" id="'.esc_attr( $this->get_field_id( 'post_ids' ) ).'[]" class="widefat ziph-select" name="'.esc_attr( $this->get_field_name( 'post_ids' ) ).'[]" multiple>';
      $posts = get_posts(array('posts_per_page' => -1));
      foreach ($posts as $key => $post) {
        if(isset($instance['post_ids'])){
          $selected = ( in_array($post->ID, $instance['post_ids']) ) ? 'selected="selected"' : '';
        } else {
          $selected = '';
        }
        echo '<option '.$selected.' value="' . $post->ID . '">' . $post->post_title . '</option>';
      }  
    echo '</select></div>';
    
    // Custom Link
    echo '<div class="ziph-repeat" data-controller="widget-'.esc_attr( $this->id_base ).'['.esc_attr( $this->number ).'][link_types]" data-condition="==" data-value="custom"><div class="wrapper">';
      
      echo '<div class="container">';
        // Template Raw
        echo '<div class="ziph-widget widget open template row">';
          
          echo '<div class="widget-top"><div class="widget-title-action"><button type="button" class="widget-action hide-if-no-js" aria-expanded="true"><span class="toggle-indicator" aria-hidden="true"></span></button></div><div class="widget-title ui-sortable-handle"><h3>'.esc_html__( 'Add Link', 'zipprich-core' ).'<span class="in-widget-title"></span></h3><span class="move"></span></div></div>';     
          
          echo '<div class="widget-inside">';     
            
            // Inputs
            echo sprintf('<p><label for="%1$s[{{row-count-placeholder}}][text]">%2$s:</label>', 
              esc_attr( $this->get_field_id( 'link_group' ) ),
              esc_html__('Text', 'zipprich-core')
            );

            echo sprintf('<input type="text" id="%2$s[{{row-count-placeholder}}][text]" class="widefat ziph-text" name="%1$s[{{row-count-placeholder}}][text]" value=""></p>',
              esc_attr( $this->get_field_name( 'link_group' ) ),
              esc_attr( $this->get_field_id( 'link_group' ) )
            );

            // Inputs
            echo sprintf('<p><label for="%1$s[{{row-count-placeholder}}][url]">%2$s:</label>', 
              esc_attr( $this->get_field_id( 'link_group' ) ),
              esc_html__('URL', 'zipprich-core')
            );

            echo sprintf('<input type="text" id="%2$s[{{row-count-placeholder}}][url]" class="widefat" name="%1$s[{{row-count-placeholder}}][url]" value=""></p>', 
              esc_attr( $this->get_field_name( 'link_group' ) ),
              esc_attr( $this->get_field_id( 'link_group' ) )
            );
          
            echo '<span class="remove button cs-warning-primary">'.esc_html__( 'Remove', 'zipprich-core' ).'</span>';
          echo '</div>';

        echo '</div>';      

        // Repeated values (Results)
        if(isset($instance['link_group'])){
          foreach ($instance['link_group'] as $key => $value) {
            echo '<div class="ziph-widget widget row">';     
              echo '<div class="widget-top"><div class="widget-title-action"><button type="button" class="widget-action hide-if-no-js" aria-expanded="true"><span class="toggle-indicator" aria-hidden="true"></span></button></div><div class="widget-title ui-sortable-handle"><h3>'.esc_attr($value['text']).'<span class="in-widget-title"></span><span class="move"></span></h3></div></div>';     
              
                echo '<div class="widget-inside">';               
                  
                  // Inputs
                  echo sprintf('<p><label for="%1$s[%2$s][text]">%3$s:</label>', 
                    esc_attr( $this->get_field_id( 'link_group' ) ),
                    esc_attr($key),
                    esc_html__('Text', 'zipprich-core')
                  );

                  echo sprintf('<input type="text" id="%4$s[%2$s][text]" class="widefat ziph-text" name="%1$s[%2$s][text]" value="%3$s"></p>', 
                    esc_attr( $this->get_field_name( 'link_group' ) ),
                    esc_attr($key),
                    esc_attr($value['text']),
                    esc_attr( $this->get_field_id( 'link_group' ) )
                  );

                  // Inputs
                  echo sprintf('<p><label for="%1$s[%2$s][url]">%3$s:</label>', 
                    esc_attr( $this->get_field_id( 'link_group' ) ),
                    esc_attr($key),
                    esc_html__('URL', 'zipprich-core')
                  );

                  echo sprintf('<input type="text" id="%4$s[%2$s][url]" class="widefat" name="%1$s[%2$s][url]" value="%3$s"></p>', 
                    esc_attr( $this->get_field_name( 'link_group' ) ),
                    esc_attr($key),
                    esc_url($value['url']),
                    esc_attr( $this->get_field_id( 'link_group' ) )
                  );
              
                echo '<span class="remove button cs-warning-primary">'.esc_html__( 'Remove', 'zipprich-core' ).'</span>';
              echo '</div>';
            echo '</div>';
          }
        }
      echo '</div>';
      echo '<span class="add button button-primary">'.esc_html__( 'Add New', 'zipprich-core' ).'</span><br>';
    echo '</div></div>';
  }

  /**
   * Processes the widget's values
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;

    // Update values
    $instance['title']      = strip_tags( stripslashes( $new_instance['title'] ) );
    $instance['link_types'] = strip_tags( stripslashes( $new_instance['link_types'] ) );
    $instance['page_ids']   = array();
    $instance['post_ids']   = array();
    $instance['link_group'] = array();

    if ( isset ( $new_instance['page_ids'] ) ) {
      foreach ( $new_instance['page_ids'] as $value ) {
        if ( '' !== trim( $value ) ) {
          $instance['page_ids'][] = strip_tags( stripslashes($value) );
        }
      }
    }

    if ( isset ( $new_instance['post_ids'] ) ) {
      foreach ( $new_instance['post_ids'] as $value ) {
        if ( '' !== trim( $value ) ) {
          $instance['post_ids'][] = strip_tags( stripslashes($value) );
        }
      }
    }

    if ( isset ( $new_instance['link_group'] ) ) {
      foreach ( $new_instance['link_group'] as $value ) {
        if ( '' !== trim( $value ) ) {
          $instance['link_group'][] = $value;
        }
      }
    }
    return $instance;
  }

  /**
   * Output the contents of the widget
   */
  public function widget( $args, $instance ) {
    // Extract the arguments
    extract( $args );

    $title        = apply_filters( 'widget_title', $instance['title'] );
    $link_types   = $instance['link_types'];
    $page_ids     = $instance['page_ids'];
    $post_ids     = $instance['post_ids'];
    $link_group   = $instance['link_group'];

    // Display the markup before the widget
    echo $before_widget;

    if ( $title ) {
      echo $before_title . $title . $after_title;
    }

    echo '<ul>';

    if(!empty($link_types)){
      if($link_types == 'custom'){
        foreach ($link_group as $value) {
          echo '<li><a href="'.esc_url($value['url']).'">'.esc_attr($value['text']).'</a></li>';
        }
      } else {
        if($link_types == 'page'){ $ids = array_map('intval', $page_ids); }
        if($link_types == 'post'){ $ids = array_map('intval', $post_ids); }
        foreach ($ids as $id) {
          echo '<li><a href="'.get_permalink( $id ).'">'.get_the_title( $id ).'</a></li>';
        }      
      }
    }

    echo '</ul>';

    // Display the markup after the widget
    echo $after_widget;
  }
}

// Register the widget using an annonymous function
add_action( 'widgets_init', create_function( '', 'register_widget( "zipprich_link_widget" );' ) );