<?php
/*
 * Text Widget
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

class zipprich_social_widget extends WP_Widget {

  /**
   * Specifies the widget name, description, class name and instatiates it
   */
  public function __construct() {
    parent::__construct(
      'zipprich-social-widget',
      VTHEME_NAME_P . __( ': Social Widget', 'zipprich-core' ),
      array(
        'classname'   => 'zipprich-social-widget',
        'description' => VTHEME_NAME_P . __( ' widget that displays social icons.', 'zipprich-core' )
      )
    );
  }

  /**
   * Generates the back-end layout for the widget
   */
  public function form( $instance ) {
    // Default Values
    $instance   = wp_parse_args( $instance, array(
      'title'       => '',
      'open_link'       => '',
      'social_group'  => array(),
    ));

    // Title
    $title_value = esc_attr( $instance['title'] );
    $title_field = array(
      'id'    => $this->get_field_name('title'),
      'name'  => $this->get_field_name('title'),
      'type'  => 'text',
      'title' => __( 'Title :', 'zipprich-core' ),
      'wrap_class' => 'vt-cs-widget-fields',
    );
    echo cs_add_element( $title_field, $title_value );
    
    // Custom Link
    echo '<div class="ziph-repeat"><div class="wrapper">';
      
      echo '<div class="container">';
        // Template Raw
        echo '<div class="ziph-widget widget open template row">';
          
          echo '<div class="widget-top"><div class="widget-title-action"><button type="button" class="widget-action hide-if-no-js" aria-expanded="true"><span class="toggle-indicator" aria-hidden="true"></span></button></div><div class="widget-title ui-sortable-handle"><h3>'.esc_html__( 'Add Social', 'zipprich-core' ).'<span class="in-widget-title"></span></h3><span class="move"></span></div></div>';     
          
          echo '<div class="widget-inside">';     
            
            // Text
            echo sprintf('<p><label for="%1$s[{{row-count-placeholder}}][text]">%2$s:</label>', 
              esc_attr( $this->get_field_id( 'social_group' ) ),
              esc_html__('Text', 'zipprich-core')
            );

            echo sprintf('<input type="text" id="%2$s[{{row-count-placeholder}}][text]" class="widefat ziph-text" name="%1$s[{{row-count-placeholder}}][text]" value=""></p>',
              esc_attr( $this->get_field_name( 'social_group' ) ),
              esc_attr( $this->get_field_id( 'social_group' ) )
            );

            // Icon
            echo '<div class="cs-element-icon cs-field-icon"><div class="cs-title"><h4>'.esc_html__('Icon', 'zipprich-core').' :</h4></div><div class="cs-fieldset"><div class="cs-icon-select"><span class="cs-icon-preview hidden"><i class=""></i></span><a href="#" class="button button-primary cs-icon-add">'.esc_html__('Add', 'zipprich-core').'</a><a href="#" class="button cs-warning-primary cs-icon-remove hidden">'.esc_html__('Remove', 'zipprich-core').'</a>';
            echo sprintf('<input type="text" name="%1$s[{{row-count-placeholder}}][icon]" value="" class="cs-icon-value">',
              esc_attr( $this->get_field_name( 'social_group' ) ),
              esc_attr( $this->get_field_id( 'social_group' ) )
            );
            echo '</div></div></div>';

            // URL
            echo sprintf('<p><label for="%1$s[{{row-count-placeholder}}][url]">%2$s:</label>', 
              esc_attr( $this->get_field_id( 'social_group' ) ),
              esc_html__('URL', 'zipprich-core')
            );

            echo sprintf('<input type="text" id="%2$s[{{row-count-placeholder}}][url]" class="widefat" name="%1$s[{{row-count-placeholder}}][url]" value=""></p>', 
              esc_attr( $this->get_field_name( 'social_group' ) ),
              esc_attr( $this->get_field_id( 'social_group' ) )
            );
          
            echo '<span class="remove button cs-warning-primary">'.esc_html__( 'Remove', 'zipprich-core' ).'</span>';
          echo '</div>';

        echo '</div>';      

        // Repeated values (Results)
        if(isset($instance['social_group'])){
          foreach ($instance['social_group'] as $key => $value) {
            echo '<div class="ziph-widget widget row">';     
              echo '<div class="widget-top"><div class="widget-title-action"><button type="button" class="widget-action hide-if-no-js" aria-expanded="true"><span class="toggle-indicator" aria-hidden="true"></span></button></div><div class="widget-title ui-sortable-handle"><h3>'.esc_attr($value['text']).'<span class="in-widget-title"></span><span class="move"></span></h3></div></div>';     
              
                echo '<div class="widget-inside">';               
                  
                  // Inputs
                  echo sprintf('<p><label for="%1$s[%2$s][text]">%3$s:</label>', 
                    esc_attr( $this->get_field_id( 'social_group' ) ),
                    esc_attr($key),
                    esc_html__('Text', 'zipprich-core')
                  );

                  echo sprintf('<input type="text" id="%4$s[%2$s][text]" class="widefat ziph-text" name="%1$s[%2$s][text]" value="%3$s"></p>', 
                    esc_attr( $this->get_field_name( 'social_group' ) ),
                    esc_attr($key),
                    esc_attr($value['text']),
                    esc_attr( $this->get_field_id( 'social_group' ) )
                  );          

                  // Icon
                  echo '<div class="cs-element-icon cs-field-icon"><div class="cs-title"><h4>'.esc_html__('Icon', 'zipprich-core').' :</h4></div><div class="cs-fieldset"><div class="cs-icon-select"><span class="cs-icon-preview hidden"><i class="'.esc_attr($value['icon']).'"></i></span><a href="#" class="button button-primary cs-icon-add">'.esc_html__('Add', 'zipprich-core').'</a><a href="#" class="button cs-warning-primary cs-icon-remove hidden">'.esc_html__('Remove', 'zipprich-core').'</a>';                  
                  echo sprintf('<input type="text" id="%1$s[%2$s][icon]" name="%3$s[%2$s][icon]" value="%4$s" class="cs-icon-value">',
                    esc_attr( $this->get_field_id( 'social_group' ) ),
                    esc_attr($key),
                    esc_attr( $this->get_field_name( 'social_group' ) ),
                    esc_attr($value['icon'])
                  );
                  echo '</div></div></div>';

                  // Inputs
                  echo sprintf('<p><label for="%1$s[%2$s][url]">%3$s:</label>', 
                    esc_attr( $this->get_field_id( 'social_group' ) ),
                    esc_attr($key),
                    esc_html__('URL', 'zipprich-core')
                  );

                  echo sprintf('<input type="text" id="%4$s[%2$s][url]" class="widefat" name="%1$s[%2$s][url]" value="%3$s"></p>', 
                    esc_attr( $this->get_field_name( 'social_group' ) ),
                    esc_attr($key),
                    esc_url($value['url']),
                    esc_attr( $this->get_field_id( 'social_group' ) )
                  );
              
                echo '<span class="remove button cs-warning-primary">'.esc_html__( 'Remove', 'zipprich-core' ).'</span>';
              echo '</div>';
            echo '</div>';
          }
        }
      echo '</div>';
      echo '<span class="add button button-primary">'.esc_html__( 'Add New', 'zipprich-core' ).'</span><br>';
    echo '</div></div>';

    // New Tab
    $open_link_value = esc_attr( $instance['open_link'] );
    $open_link_field = array(
      'id'    => $this->get_field_name('open_link'),
      'name'  => $this->get_field_name('open_link'),
      'type'  => 'switcher',
      'default' => false,
      'title' => __( 'Open in new tab?', 'zipprich-core' ),
      'wrap_class' => 'vt-cs-widget-fields',
    );
    echo cs_add_element( $open_link_field, $open_link_value );    
  }

  /**
   * Processes the widget's values
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;

    // Update values
    $instance['title']      = strip_tags( stripslashes( $new_instance['title'] ) );
    $instance['open_link']      = strip_tags( stripslashes( $new_instance['open_link'] ) );
    $instance['social_group'] = array();

    if ( isset ( $new_instance['social_group'] ) ) {
      foreach ( $new_instance['social_group'] as $value ) {
        if ( '' !== trim( $value ) ) {
          $instance['social_group'][] = $value;
        }
      }
    }
    return $instance;
  }

  /**
   * Output the contents of the widget
   */
  public function widget( $args, $instance ) {
    // Extract the arguments
    extract( $args );

    $title        = apply_filters( 'widget_title', $instance['title'] );
    $open_link    = $instance['open_link'];
    $social_group   = $instance['social_group'];

    // Display the markup before the widget
    echo $before_widget;

    if ( $title ) {
      echo $before_title . $title . $after_title;
    }

    echo '<ul class="textwidget list-inline ziph-footrwidget_social">';

    $open_link = $open_link ? ' target="_blank"' : '';

    if(!empty($social_group)){
        foreach ($social_group as $value) {
          echo '<li><a href="'.esc_url($value['url']).'" '.$open_link.'><i class="'.esc_attr($value['icon']).'"></i></a></li>';
        }
    }

    echo '</ul>';

    // Display the markup after the widget
    echo $after_widget;
  }
}

// Register the widget using an annonymous function
add_action( 'widgets_init', create_function( '', 'register_widget( "zipprich_social_widget" );' ) );