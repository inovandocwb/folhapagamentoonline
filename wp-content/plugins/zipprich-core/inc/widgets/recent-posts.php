<?php
/*
 * Recent Post Widget
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

class zipprich_recent_posts extends WP_Widget {

  /**
   * Specifies the widget name, description, class name and instatiates it
   */
  public function __construct() {
    parent::__construct(
      'ziph-recent-blog',
      VTHEME_NAME_P . __( ': Recent Posts', 'zipprich-core' ),
      array(
        'classname'   => 'ziph-recent-blog',
        'description' => VTHEME_NAME_P . __( ' widget that displays recent posts.', 'zipprich-core' )
      )
    );
  }

  /**
   * Generates the back-end layout for the widget
   */
  public function form( $instance ) {
    // Default Values
    $instance   = wp_parse_args( $instance, array(
      'title'    => __( 'Recent Posts', 'zipprich-core' ),
      'ptypes'   => 'post',
      'limit'    => '3',
      'date'     => true,
      'category' => '',
      'order' => '',
      'orderby' => '',
    ));

    // Title
    $title_value = esc_attr( $instance['title'] );
    $title_field = array(
      'id'    => $this->get_field_name('title'),
      'name'  => $this->get_field_name('title'),
      'type'  => 'text',
      'title' => __( 'Title :', 'zipprich-core' ),
      'wrap_class' => 'vt-cs-widget-fields',
    );
    echo cs_add_element( $title_field, $title_value );

    // Post Type
    $ptypes_value = esc_attr( $instance['ptypes'] );
    $ptypes_field = array(
      'id'    => $this->get_field_name('ptypes'),
      'name'  => $this->get_field_name('ptypes'),
      'type' => 'select',
      'options' => 'post_types',
      'default_option' => __( 'Select Post Type', 'zipprich-core' ),
      'title' => __( 'Post Type :', 'zipprich-core' ),
    );
    echo cs_add_element( $ptypes_field, $ptypes_value );

    // Limit
    $limit_value = esc_attr( $instance['limit'] );
    $limit_field = array(
      'id'    => $this->get_field_name('limit'),
      'name'  => $this->get_field_name('limit'),
      'type'  => 'text',
      'title' => __( 'Limit :', 'zipprich-core' ),
      'help' => __( 'How many posts want to show?', 'zipprich-core' ),
    );
    echo cs_add_element( $limit_field, $limit_value );

    // Date
    $date_value = esc_attr( $instance['date'] );
    $date_field = array(
      'id'    => $this->get_field_name('date'),
      'name'  => $this->get_field_name('date'),
      'type'  => 'switcher',
      'on_text'  => __( 'Yes', 'zipprich-core' ),
      'off_text'  => __( 'No', 'zipprich-core' ),
      'title' => __( 'Display Date :', 'zipprich-core' ),
    );
    echo cs_add_element( $date_field, $date_value );

    // Category
    $category_value = esc_attr( $instance['category'] );
    $category_field = array(
      'id'    => $this->get_field_name('category'),
      'name'  => $this->get_field_name('category'),
      'type'  => 'text',
      'title' => __( 'Category :', 'zipprich-core' ),
      'help' => __( 'Enter category slugs with comma(,) for multiple items', 'zipprich-core' ),
    );
    echo cs_add_element( $category_field, $category_value );

    // Order
    $order_value = esc_attr( $instance['order'] );
    $order_field = array(
      'id'    => $this->get_field_name('order'),
      'name'  => $this->get_field_name('order'),
      'type' => 'select',
      'options'   => array(
        'ASC' => 'Ascending',
        'DESC' => 'Descending',
      ),
      'default_option' => __( 'Select Order', 'zipprich-core' ),
      'title' => __( 'Order :', 'zipprich-core' ),
    );
    echo cs_add_element( $order_field, $order_value );

    // Orderby
    $orderby_value = esc_attr( $instance['orderby'] );
    $orderby_field = array(
      'id'    => $this->get_field_name('orderby'),
      'name'  => $this->get_field_name('orderby'),
      'type' => 'select',
      'options'   => array(
        'none' => __('None', 'zipprich-core'),
        'ID' => __('ID', 'zipprich-core'),
        'author' => __('Author', 'zipprich-core'),
        'title' => __('Title', 'zipprich-core'),
        'name' => __('Name', 'zipprich-core'),
        'type' => __('Type', 'zipprich-core'),
        'date' => __('Date', 'zipprich-core'),
        'modified' => __('Modified', 'zipprich-core'),
        'rand' => __('Random', 'zipprich-core'),
      ),
      'default_option' => __( 'Select OrderBy', 'zipprich-core' ),
      'title' => __( 'OrderBy :', 'zipprich-core' ),
    );
    echo cs_add_element( $orderby_field, $orderby_value );

  }

  /**
   * Processes the widget's values
   */
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;

    // Update values
    $instance['title']        = strip_tags( stripslashes( $new_instance['title'] ) );
    $instance['ptypes']       = strip_tags( stripslashes( $new_instance['ptypes'] ) );
    $instance['limit']        = strip_tags( stripslashes( $new_instance['limit'] ) );
    $instance['date']         = strip_tags( stripslashes( $new_instance['date'] ) );
    $instance['category']     = strip_tags( stripslashes( $new_instance['category'] ) );
    $instance['order']        = strip_tags( stripslashes( $new_instance['order'] ) );
    $instance['orderby']      = strip_tags( stripslashes( $new_instance['orderby'] ) );

    return $instance;
  }

  /**
   * Output the contents of the widget
   */
  public function widget( $args, $instance ) {
    // Extract the arguments
    extract( $args );

    $title          = apply_filters( 'widget_title', $instance['title'] );
    $ptypes         = $instance['ptypes'];
    $limit          = $instance['limit'];
    $display_date   = $instance['date'];
    $category       = $instance['category'];
    $order          = $instance['order'];
    $orderby        = $instance['orderby'];

    $args = array(
      // other query params here,
      'post_type' => esc_attr($ptypes),
      'posts_per_page' => (int)$limit,
      'orderby' => esc_attr($orderby),
      'order' => esc_attr($order),
      'category_name' => esc_attr($category),
      'ignore_sticky_posts' => 1,
     );

     $zipprich_rpw = new WP_Query( $args );
     global $post;

    // Display the markup before the widget
    echo $before_widget;

    if ( $title ) {
      echo $before_title . $title . $after_title;
    }

    echo '<div class="ziph-wigtResent_posts">';

    if ($zipprich_rpw->have_posts()) : while ($zipprich_rpw->have_posts()) : $zipprich_rpw->the_post();
  ?>

  <!--Single post start\-->
  <div class="ziph-wigtResentPost_single">
    <?php
      if(class_exists('Aq_Resize')) {
        $large_image  =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
        $large_image  = $large_image[0];
        $post_img     = aq_resize( $large_image, '80', '80', true );
        $post_img_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);
      } else {
        $post_img     = '';
        $post_img_alt = the_title();
      }
    ?>

    <?php if(!empty($post_img)) { ?>
    <!--Post img start\-->
    <div class="ziph-wigtRp_img">
      <a href="<?php the_permalink(); ?>">
         <img src="<?php echo esc_url( $post_img ); ?>" alt="<?php echo esc_attr( $post_img_alt ); ?>">
      </a>
    </div><!--/Post img end-->
    <?php } ?>

    <!--Single Post start\-->
    <div class="ziph-fix ziph-wigtRp_text">
      <a class="ziph-wigtRp_title" href="<?php esc_url(the_permalink()) ?>"><?php the_title(); ?></a>
      <?php if ($display_date === '1') { ?>
      <div class="ziph-wigtRp_date">
        <?php echo get_the_date('M d Y'); ?>
      </div>
      <?php } ?>
    </div>
  </div>

  <?php
    endwhile; endif;
    echo '</div>';
    wp_reset_postdata();
    // Display the markup after the widget
    echo $after_widget;
  }
}

// Register the widget using an annonymous function
add_action( 'widgets_init', create_function( '', 'register_widget( "zipprich_recent_posts" );' ) );