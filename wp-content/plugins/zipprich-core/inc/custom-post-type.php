<?php

/**
 * Initialize Custom Post Type - Zipprich Theme
 */

function zipprich_custom_post_type() {

	// Testimonials - Start
	$testimonial_slug = 'testimonial';
	$testimonials = __('Testimonials', 'zipprich-core');

	// Register custom post type - Testimonials
	register_post_type('testimonial',
		array(
			'labels' => array(
				'name' => $testimonials,
				'singular_name' => sprintf(esc_html__('%s Post', 'zipprich-core' ), $testimonials),
				'all_items' => sprintf(esc_html__('%s', 'zipprich-core' ), $testimonials),
				'add_new' => esc_html__('Add New', 'zipprich-core') ,
				'add_new_item' => sprintf(esc_html__('Add New %s', 'zipprich-core' ), $testimonials),
				'edit' => esc_html__('Edit', 'zipprich-core') ,
				'edit_item' => sprintf(esc_html__('Edit %s', 'zipprich-core' ), $testimonials),
				'new_item' => sprintf(esc_html__('New %s', 'zipprich-core' ), $testimonials),
				'view_item' => sprintf(esc_html__('View %s', 'zipprich-core' ), $testimonials),
				'search_items' => sprintf(esc_html__('Search %s', 'zipprich-core' ), $testimonials),
				'not_found' => esc_html__('Nothing found in the Database.', 'zipprich-core') ,
				'not_found_in_trash' => esc_html__('Nothing found in Trash', 'zipprich-core') ,
				'parent_item_colon' => ''
			) ,
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			// 'menu_position' => 10,
			'show_in_menu' => 'zipprich_post_type',
			'menu_icon' => 'dashicons-groups',
			'rewrite' => array(
				'slug' => $testimonial_slug,
				'with_front' => false
			),
			'has_archive' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'revisions',
				'sticky',
			)
		)
	);
	// Testimonials - End

	// Team - Start
	$team_slug = 'team';
	$teams = __('Teams', 'zipprich-core');

	// Register custom post type - Team
	register_post_type('team',
		array(
			'labels' => array(
				'name' => $teams,
				'singular_name' => sprintf(esc_html__('%s Post', 'zipprich-core' ), $teams),
				'all_items' => sprintf(esc_html__('%s', 'zipprich-core' ), $teams),
				'add_new' => esc_html__('Add New', 'zipprich-core') ,
				'add_new_item' => sprintf(esc_html__('Add New %s', 'zipprich-core' ), $teams),
				'edit' => esc_html__('Edit', 'zipprich-core') ,
				'edit_item' => sprintf(esc_html__('Edit %s', 'zipprich-core' ), $teams),
				'new_item' => sprintf(esc_html__('New %s', 'zipprich-core' ), $teams),
				'view_item' => sprintf(esc_html__('View %s', 'zipprich-core' ), $teams),
				'search_items' => sprintf(esc_html__('Search %s', 'zipprich-core' ), $teams),
				'not_found' => esc_html__('Nothing found in the Database.', 'zipprich-core') ,
				'not_found_in_trash' => esc_html__('Nothing found in Trash', 'zipprich-core') ,
				'parent_item_colon' => ''
			) ,
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			// 'menu_position' => 10,
			'show_in_menu' => 'zipprich_post_type',
			'menu_icon' => 'dashicons-businessman',
			'rewrite' => array(
				'slug' => $team_slug,
				'with_front' => false
			),
			'has_archive' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt',
				'revisions',
				'sticky',
			)
		)
	);
	// Team - End

}

// Post Type - Menu
if( ! function_exists( 'zipprich_post_type_menu' ) ) {
  function zipprich_post_type_menu(){
    if ( current_user_can( 'edit_theme_options' ) ) {
			add_menu_page( __('Company', 'zipprich-core'), __('Company', 'zipprich-core'), 'edit_theme_options', 'zipprich_post_type', 'vt_welcome_page', 'dashicons-groups', 10 );
    }
  }
}
add_action( 'admin_menu', 'zipprich_post_type_menu' );

// After Theme Setup
function zipprich_custom_flush_rules() {
	// Enter post type function, so rewrite work within this function
	zipprich_custom_post_type();
	// Flush it
	flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'zipprich_custom_flush_rules');
add_action('init', 'zipprich_custom_post_type');

/* ---------------------------------------------------------------------------
 * Custom columns - Testimonial
 * --------------------------------------------------------------------------- */
add_filter("manage_edit-testimonial_columns", "zipprich_testimonial_edit_columns");
function zipprich_testimonial_edit_columns($columns) {
  $new_columns['cb'] = '<input type="checkbox" />';
  $new_columns['title'] = __('Title', 'zipprich-core' );
  $new_columns['thumbnail'] = __('Image', 'zipprich-core' );
  $new_columns['name'] = __('Client Name', 'zipprich-core' );
  $new_columns['date'] = __('Date', 'zipprich-core' );

  return $new_columns;
}

add_action('manage_testimonial_posts_custom_column', 'zipprich_manage_testimonial_columns', 10, 2);
function zipprich_manage_testimonial_columns( $column_name ) {
  global $post;

  switch ($column_name) {

    /* If displaying the 'Image' column. */
    case 'thumbnail':
      echo get_the_post_thumbnail( $post->ID, array( 100, 100) );
    break;

    case "name":
    	$testimonial_options = get_post_meta( get_the_ID(), 'testimonial_options', true );
      echo $testimonial_options['testi_name'];
    break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
    break;

  }
}

/* ---------------------------------------------------------------------------
 * Custom columns - Team
 * --------------------------------------------------------------------------- */
add_filter("manage_edit-team_columns", "zipprich_team_edit_columns");
function zipprich_team_edit_columns($columns) {
  $new_columns['cb'] = '<input type="checkbox" />';
  $new_columns['title'] = __('Title', 'zipprich-core' );
  $new_columns['thumbnail'] = __('Image', 'zipprich-core' );
  $new_columns['name'] = __('Job Position', 'zipprich-core' );
  $new_columns['date'] = __('Date', 'zipprich-core' );

  return $new_columns;
}

add_action('manage_team_posts_custom_column', 'zipprich_manage_team_columns', 10, 2);
function zipprich_manage_team_columns( $column_name ) {
  global $post;

  switch ($column_name) {

    /* If displaying the 'Image' column. */
    case 'thumbnail':
      echo get_the_post_thumbnail( $post->ID, array( 100, 100) );
    break;

    case "name":
    	$team_options = get_post_meta( get_the_ID(), 'team_options', true );
      echo $team_options['team_job_position'];
    break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
    break;

  }
}
