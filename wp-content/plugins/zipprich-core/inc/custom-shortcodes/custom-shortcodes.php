<?php
/* Spacer */
function ziph_spacer_function($atts, $content = true) {
  extract(shortcode_atts(array(
    "height" => '',
  ), $atts));

  $result = do_shortcode('[vc_empty_space height="'. $height .'"]');
  return $result;

}
add_shortcode("ziph_spacer", "ziph_spacer_function");

/* Break */
function ziph_break_function($atts, $content = true) {
  $result = '<br>';
  return $result;
}
add_shortcode("ziph_break", "ziph_break_function");

/* Brand Color */
function ziph_brand_function($atts, $content = true) {
  extract(shortcode_atts(array(
    "color" => '',
  ), $atts));

  $result = '<span class="ziph-brand-color" style="color: ' . $color . '">' . $content . '</span>';
  return $result;
}
add_shortcode("ziph_brand", "ziph_brand_function");

/* Social Icons */
function ziph_socials_function($atts, $content = true) {
  extract(shortcode_atts(array(
    "custom_class" => '',
    // Colors
    "icon_color" => '',
    "icon_hover_color" => '',
    "bg_color" => '',
    "bg_hover_color" => '',
    "border_color" => '',
    "icon_size" => '',
  ), $atts));

  // Shortcode Style CSS
  $e_uniqid       = uniqid();
  $inline_style   = '';

  // Colors & Size
  if ( $icon_color || $icon_size ) {
    $inline_style .= '.ziph-headV_social-'. $e_uniqid .' ul li a, .ziph-headV_social-'. $e_uniqid .' ul li a i {';
    $inline_style .= ( $icon_color ) ? 'color:'. $icon_color .';' : '';
    $inline_style .= ( $icon_size ) ? 'font-size:'. zipprich_core_check_px($icon_size) .';' : '';
    $inline_style .= '}';
  }

  if ( $icon_hover_color ) {
    $inline_style .= '.ziph-headV_social-'. $e_uniqid .' ul li a i:hover {';
    $inline_style .= ( $icon_hover_color ) ? 'color:'. $icon_hover_color .';' : '';
    $inline_style .= '}';
  }

  if ( $bg_color ) {
    $inline_style .= '.ziph-headV_social-'. $e_uniqid .' ul li a {';
    $inline_style .= ( $bg_color ) ? 'background-color:'. $bg_color .';' : '';
    $inline_style .= '}';
  }

  if ( $bg_hover_color ) {
    $inline_style .= '.ziph-headV_social-'. $e_uniqid .' ul li a:hover {';
    $inline_style .= ( $bg_hover_color ) ? 'background-color:'. $bg_hover_color .';' : '';
    $inline_style .= '}';
  }

  // add inline style
  zipprich_add_inline_style( $inline_style );
  $styled_class  = ' ziph-headV_social-'. $e_uniqid;

  $result = '<div class="ziph-headV_social '. $custom_class . $styled_class .'"><ul class="list-inline">'. do_shortcode($content) .'</ul></div>';
  return $result;

}
add_shortcode("ziph_socials", "ziph_socials_function");

/* Social Icon */
function ziph_social_function($atts, $content = NULL) {
   extract(shortcode_atts(array(
      "social_link" => '',
      "social_icon" => '',
      "target_tab" => ''
   ), $atts));

   $social_link = ( isset( $social_link ) ) ? 'href="'. esc_url($social_link) . '"' : '';
   $target_tab = ( $target_tab === '1' ) ? ' target="_blank"' : '';

   $result = '<li><a '. $social_link . $target_tab .' class="'. str_replace('fa ', 'icon-', $social_icon) .'"><i class="'. $social_icon .'"></i></a></li>';
   return $result;

}
add_shortcode("ziph_social", "ziph_social_function");

/* Topbar Links */
function ziph_top_links_function($atts, $content = true) {
  extract(shortcode_atts(array(
    "custom_class" => '',
    "text_align" => '',
    // Colors
    "text_color" => '',
    "text_hover_color" => '',
    "text_size" => '',
  ), $atts));

  // Shortcode Style CSS
  $e_uniqid       = uniqid();
  $inline_style   = '';

  // Colors & Size
  if ( $text_color || $text_size ) {
    $inline_style .= '.ziph-head_info-'. $e_uniqid .' a, .ziph-head_info-'. $e_uniqid .' a i {';
    $inline_style .= ( $text_color ) ? 'color:'. $text_color .';' : '';
    $inline_style .= ( $text_size ) ? 'font-size:'. zipprich_core_check_px($text_size) .';' : '';
    $inline_style .= '}';
  }

  if ( $text_hover_color ) {
    $inline_style .= '.ziph-head_info-'. $e_uniqid .' a:hover, .ziph-head_info-'. $e_uniqid .' a i:hover {';
    $inline_style .= ( $text_hover_color ) ? 'color:'. $text_hover_color .';' : '';
    $inline_style .= '}';
  }

  // add inline style
  zipprich_add_inline_style( $inline_style );
  $styled_class  = ' ziph-head_info-'. $e_uniqid;

  $text_align = ($text_align == 'right') ? 'ziph-flt_right ' : '';

  $result = '<div class="ziph-head_info '. $text_align. $custom_class . $styled_class .'">'. do_shortcode($content) .'</div>';
  return $result;

}
add_shortcode("ziph_top_links", "ziph_top_links_function");

/* Topbar Link */
function ziph_top_link_function($atts, $content = NULL) {
   extract(shortcode_atts(array(
      "topbar_text" => '',
      "topbar_link" => '',
      "topbar_icon" => '',
      "target_tab" => ''
   ), $atts));

   $topbar_link = ( isset( $topbar_link ) ) ? 'href="'. esc_url($topbar_link) . '"' : '';
   $target_tab = ( $target_tab === '1' ) ? ' target="_blank"' : '';

   $result = '<a '. $topbar_link . $target_tab .' class="'. str_replace('fa ', 'icon-', $topbar_icon) .'"><i class="'. $topbar_icon .'"></i>'. $topbar_text .'</a>';
   return $result;

}
add_shortcode("ziph_top_link", "ziph_top_link_function");

/* Topbar Links */
function ziph_top_buttons_function($atts, $content = true) {
  extract(shortcode_atts(array(
    "custom_class" => '',
    // Colors
    "text_color" => '',
    "text_hover_color" => '',
    "bg_color" => '',
    "bg_hover_color" => '',
    "text_size" => '',
  ), $atts));

  // Shortcode Style CSS
  $e_uniqid       = uniqid();
  $inline_style   = '';

  // Colors & Size
  if ( $text_color || $text_size ) {
    $inline_style .= '.ziph-headlogin_btn-'. $e_uniqid .' a {';
    $inline_style .= ( $text_color ) ? 'color:'. $text_color .' !important;' : '';
    $inline_style .= ( $text_size ) ? 'font-size:'. zipprich_core_check_px($text_size) .' !important;' : '';
    $inline_style .= '}';
  }

  if ( $text_hover_color ) {
    $inline_style .= '.ziph-headlogin_btn-'. $e_uniqid .' a:hover {';
    $inline_style .= ( $text_hover_color ) ? 'color:'. $text_hover_color .' !important;' : '';
    $inline_style .= '}';
  }

  if ( $bg_color ) {
    $inline_style .= '.ziph-headlogin_btn-'. $e_uniqid .' a {';
    $inline_style .= ( $bg_color ) ? 'background-color:'. $bg_color .' !important;' : '';
    $inline_style .= '}';
  }

  if ( $bg_hover_color ) {
    $inline_style .= '.ziph-headlogin_btn-'. $e_uniqid .' a:hover {';
    $inline_style .= ( $bg_hover_color ) ? 'background-color:'. $bg_hover_color .' !important;' : '';
    $inline_style .= '}';
  }

  // add inline style
  zipprich_add_inline_style( $inline_style );
  $styled_class  = ' ziph-headlogin_btn-'. $e_uniqid;

  $result = '<div class="ziph-flt_right ziph-headlogin_btn '. $custom_class . $styled_class .'">'. do_shortcode($content) .'</div>';
  return $result;

}
add_shortcode("ziph_top_buttons", "ziph_top_buttons_function");

/* Topbar Button */
function ziph_top_button_function($atts, $content = NULL) {
   extract(shortcode_atts(array(
      "topbar_text" => '',
      "topbar_link" => '',
      "target_tab" => ''
   ), $atts));

   $topbar_link = ( isset( $topbar_link ) ) ? 'href="'. esc_url($topbar_link) . '"' : '';
   $target_tab = ( $target_tab === '1' ) ? ' target="_blank"' : '';

   $result = '<a '. $topbar_link . $target_tab .' class="btn btn-info">'. $topbar_text .'</a>';
   return $result;

}
add_shortcode("ziph_top_button", "ziph_top_button_function");

/* Topbar WPML */
function ziph_topbar_wpml_function($atts, $content = NULL) {
  extract(shortcode_atts(array(
    "custom_class" => ''
  ), $atts));

  $output   = '';

  if ( is_wpml_activated() ) {
    global $sitepress;
    $sitepress_settings = $sitepress->get_settings();
    $icl_get_languages  = icl_get_languages();

    if ( ! empty( $icl_get_languages ) ) {

      $output .= '<div class="ziph-tr-element '. $custom_class .'"><div class="ziph-top-dropdown ziph-wpml-dropdown">';

      // current language
      $output .= '<a href="#" class="ziph-top-active">';
      foreach ( $icl_get_languages as $id => $current_lang ) {
        if ( $current_lang['active'] ) {
          $output .= ( ( $sitepress_settings['icl_lso_native_lang'] ) ? $current_lang['code'] : $current_lang['translated_name'] );
          $output .= '<i class="fa fa-angle-down"></i>';
        }
      }
      $output .= '</a>';

      // list languages
      $output .= '<ul class="ziph-topdd-content">';
      foreach ( $icl_get_languages as $id => $language ) {
        if ( ! $language['active'] ) {
          $output .= '<li>';
          $output .= '<a href="'. $language['url'] .'">';
          $output .= ( $sitepress_settings['icl_lso_native_lang'] && $sitepress_settings['icl_lso_display_lang'] ) ? $language['code'] : '';
          $output .= ( ! $sitepress_settings['icl_lso_native_lang'] && $sitepress_settings['icl_lso_display_lang'] ) ? $language['translated_name'] : '';
          $output .= ( $sitepress_settings['icl_lso_native_lang'] && ! $sitepress_settings['icl_lso_display_lang'] ) ? $language['code'] : '';
          $output .= '</a>';
          $output .= '</li>';
        }
        // print_r($language);
      }
      $output .= '</ul>';
      $output .= '</div>';
      $output .= '</div>';
    }

  } else {
    $output .= '<p class="wpml-not-active">Please Activate WPML Plugin</p>';
  }

  return $output;

}
add_shortcode("ziph_topbar_wpml", "ziph_topbar_wpml_function");

/* Address Infos */
function ziph_address_infos_function($atts, $content = true) {
   extract(shortcode_atts(array(
      "custom_class" => ''
   ), $atts));

   $result = '<div class="ziph-hm2hdrcontact_info '. $custom_class .'"><ul class="list-inline">'. do_shortcode($content) .'</ul></div>';
   return $result;

}
add_shortcode("ziph_address_infos", "ziph_address_infos_function");

/* Address Info */
function ziph_address_info_function($atts, $content = NULL) {
   extract(shortcode_atts(array(
      "info_icon" => '',
      "info_icon_color" => '',
      "info_main_text" => '',
      "info_main_text_color" => '',
      "info_sec_text" => '',
      "info_sec_text_link" => '',
      "info_sec_text_color" => '',
      "target_tab" => ''
   ), $atts));

   // Color
   $info_icon_color = $info_icon_color ? 'color:'. $info_icon_color .';' : '';
   $info_main_text_color = $info_main_text_color ? 'color:'. $info_main_text_color .';' : '';
   $info_sec_text_color = $info_sec_text_color ? 'color:'. $info_sec_text_color .';' : '';

   $target_tab = ( $target_tab === '1' ) ? 'target="_blank"' : '';
   $info_icon = ( isset( $info_icon ) ) ? '<div class="ziph-flt_left ziph-hm2hdrcontct_icon"><i class="'. $info_icon .'" style="'. $info_icon_color .'"></i></div>' : '';

   if (isset( $info_main_text ) ) {
      $info_main_text = '<h6 style="'. $info_main_text_color .'">'. $info_main_text .'</h6>';
   } else {
      $info_main_text = '';
   }
   if (isset( $info_sec_text ) && !$info_sec_text_link ) {
      $info_sec_text = '<h5 style="'. $info_sec_text_color .'">'. $info_sec_text .'</h5>';
   } elseif (isset( $info_sec_text ) && isset( $info_sec_text_link )) {
      $info_sec_text = '<h5><a href="'. $info_sec_text_link .'" '. $target_tab .' style="'. $info_sec_text_color .'">'. $info_sec_text .'</a></h5>';
   } else {
      $info_sec_text = '';
   }

   $result = '<li><div class="ziph-fix text-left ziph-hm2hdrcontct_single">'. $info_icon .'<div class="ziph-fix ziph-flt_left ziph-hm2hdrcontct_txt">'. $info_main_text . $info_sec_text .'</div></div></li>';
   return $result;

}
add_shortcode("ziph_address_info", "ziph_address_info_function");

/* Useful Link */
function ziph_useful_link_function($atts, $content = NULL) {
   extract(shortcode_atts(array(
      "custom_class" => '',
      "target_tab" => '',
      "title_link" => '',
      "link_title" => ''
   ), $atts));

   $title_link = ( isset( $title_link ) ) ? 'href="'. $title_link . '"' : '';
   $target_tab = ( $target_tab === '1' ) ? 'target="_blank"' : '';

   $result = '<a class="' . $custom_class .'" '. $title_link . $target_tab .'>'. $link_title .'</a>';
   return $result;

}
add_shortcode("ziph_useful_link", "ziph_useful_link_function");

/* Current Year - Shortcode */
if( ! function_exists( 'ziph_current_year' ) ) {
  function ziph_current_year() {
    return date('Y');
  }
  add_shortcode( 'ziph_current_year', 'ziph_current_year' );
}

/* Get Home Page URL - Via Shortcode */
if( ! function_exists( 'ziph_home_url' ) ) {
  function ziph_home_url() {
    return esc_url( home_url( '/' ) );
  }
  add_shortcode( 'ziph_home_url', 'ziph_home_url' );
}

/* Form Field Wrap */
if( ! function_exists( 'ziph_contact_form_field_row_function' ) ) {
  function ziph_contact_form_field_row_function($atts, $content = true) {
    extract(shortcode_atts(array(
      "row_col" => '',
      "custom_class" => ''
    ), $atts));
    if ($row_col == true){
      $row_col_class = 'row ziph-input_group ziph-m-0';
    } else {
      $row_col_class = 'ziph-input_single ziph-m-0';
    }
    $output  = '<div class="'.$row_col_class.''. $custom_class .'">'. do_shortcode( $content ) .'</div>';
    return $output;
  }
  add_shortcode( 'ziph_field_row', 'ziph_contact_form_field_row_function' );
}

/* Form Field Wrap */
if( ! function_exists( 'ziph_contact_form_field_col_function' ) ) {
  function ziph_contact_form_field_col_function($atts, $content = true) {
    extract(shortcode_atts(array(
      "col" => '',
      "custom_class" => ''
    ), $atts));

    if ($col == 2){
      $row_col_class = 'col-md-6';
    } elseif ($col == 3){
      $row_col_class = 'col-md-4';
    } elseif ($col == 4){
      $row_col_class = 'col-md-3';
    } elseif ($col == 5){
      $row_col_class = 'col-md-2';
    } else {
      $row_col_class = 'col-md-12';
    }

    $output  = '<div class="'. $row_col_class. ' '. $custom_class .'">'. do_shortcode( $content ) .'</div>';
    return $output;

  }
  add_shortcode( 'ziph_field_col', 'ziph_contact_form_field_col_function' );
}

/* Footer Logo */
if(!function_exists('ziph_footer_logo_function')){
  function ziph_footer_logo_function($atts, $content = true) {
    extract(shortcode_atts(array(
      "custom_class" => '',
      "logo" => '',
      "width" => '',
      "height" => '',
    ), $atts));

    $result = '<div class="ziph-footer_logo"><a href="'. home_url( '/' ) .'"><img src="'. wp_get_attachment_url($logo) .'" width="'. $width .'" height="'. $height .'" alt="logo"></a></div>';
    return $result;

  }
  add_shortcode("ziph_footer_logo", "ziph_footer_logo_function");
}