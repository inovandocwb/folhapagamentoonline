<?php
/*
 * All Custom Shortcode for [theme_name] theme.
 * Author & Copyright: VictorThemes
 * URL: http://themeforest.net/user/VictorThemes
 */

if( ! function_exists( 'zipprich_ziph_shortcodes' ) ) {
  function zipprich_ziph_shortcodes( $options ) {

    $options       = array();

    /* Topbar Shortcodes */
    $options[]     = array(
      'title'      => __('Topbar Shortcodes', 'zipprich-core'),
      'shortcodes' => array(

        // WPML
        array(
          'name'          => 'ziph_topbar_wpml',
          'title'         => __('WPML Language Dropdown', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => __('Custom Class', 'zipprich-core'),
            ),

          ),

        ),
        // WPML

      ),
    );

    /* Top Bar Shortcodes */
    $options[]     = array(
      'title'      => __('Top Bar Shortcodes', 'zipprich-core'),
      'shortcodes' => array(

        // Social Icons
        array(
          'name'          => 'ziph_socials',
          'title'         => __('Social Icons', 'zipprich-core'),
          'view'          => 'clone',
          'clone_id'      => 'ziph_social',
          'clone_title'   => __('Add New', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => __('Custom Class', 'zipprich-core'),
            ),

            // Colors
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => __('Colors', 'zipprich-core')
            ),
            array(
              'id'        => 'icon_color',
              'type'      => 'color_picker',
              'title'     => __('Icon Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'icon_hover_color',
              'type'      => 'color_picker',
              'title'     => __('Icon Hover Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'bg_color',
              'type'      => 'color_picker',
              'title'     => __('Backrgound Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'bg_hover_color',
              'type'      => 'color_picker',
              'title'     => __('Backrgound Hover Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),

            // Icon Size
            array(
              'id'        => 'icon_size',
              'type'      => 'text',
              'title'     => __('Icon Size', 'zipprich-core'),
              'wrap_class' => 'column_full',
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'social_link',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
              'title'     => __('Link', 'zipprich-core')
            ),
            array(
              'id'        => 'social_icon',
              'type'      => 'icon',
              'title'     => __('Social Icon', 'zipprich-core')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => __('Open New Tab?', 'zipprich-core'),
              'on_text'     => __('Yes', 'zipprich-core'),
              'off_text'     => __('No', 'zipprich-core'),
            ),

          ),

        ),
        // Social Icons

        // Topbar Links
        array(
          'name'          => 'ziph_top_links',
          'title'         => __('Topbar Links', 'zipprich-core'),
          'view'          => 'clone',
          'clone_id'      => 'ziph_top_link',
          'clone_title'   => __('Add New', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => __('Custom Class', 'zipprich-core'),
            ),
            array(
              'id'        => 'text_align',
              'type'      => 'select',
              'title'     => __('Text Alignment', 'zipprich-core'),
              'options'   => array(
                'left'    => __('Left', 'zipprich-core'),
                'right'   => __('Right', 'zipprich-core'),
              ),
              'default_option'  => __('Select text alignment', 'zipprich-core'),
            ),

            // Colors
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => __('Colors', 'zipprich-core')
            ),
            array(
              'id'        => 'text_color',
              'type'      => 'color_picker',
              'title'     => __('Text Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'text_hover_color',
              'type'      => 'color_picker',
              'title'     => __('Text Hover Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            // Text Size
            array(
              'id'        => 'text_size',
              'type'      => 'text',
              'title'     => __('Text Size', 'zipprich-core'),
              'wrap_class' => 'column_full',
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'topbar_text',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'Web Mail',
              ),
              'title'     => __('Text', 'zipprich-core')
            ),
            array(
              'id'        => 'topbar_link',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
              'title'     => __('Link', 'zipprich-core')
            ),
            array(
              'id'        => 'topbar_icon',
              'type'      => 'icon',
              'title'     => __('Icon', 'zipprich-core')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => __('Open New Tab?', 'zipprich-core'),
              'on_text'     => __('Yes', 'zipprich-core'),
              'off_text'     => __('No', 'zipprich-core'),
            ),

          ),

        ),
        // Topbar Icons

        // Topbar Buttons
        array(
          'name'          => 'ziph_top_buttons',
          'title'         => __('Topbar Buttons', 'zipprich-core'),
          'view'          => 'clone',
          'clone_id'      => 'ziph_top_button',
          'clone_title'   => __('Add New', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => __('Custom Class', 'zipprich-core'),
            ),

            // Colors
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => __('Colors', 'zipprich-core')
            ),
            array(
              'id'        => 'text_color',
              'type'      => 'color_picker',
              'title'     => __('Text Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'text_hover_color',
              'type'      => 'color_picker',
              'title'     => __('Text Hover Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'bg_color',
              'type'      => 'color_picker',
              'title'     => __('Backrgound Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'bg_hover_color',
              'type'      => 'color_picker',
              'title'     => __('Backrgound Hover Color', 'zipprich-core'),
              'wrap_class' => 'column_third',
            ),
            // Text Size
            array(
              'id'        => 'text_size',
              'type'      => 'text',
              'title'     => __('Text Size', 'zipprich-core'),
              'wrap_class' => 'column_full',
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'topbar_text',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'Web Mail',
              ),
              'title'     => __('Text', 'zipprich-core')
            ),
            array(
              'id'        => 'topbar_link',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
              'title'     => __('Link', 'zipprich-core')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => __('Open New Tab?', 'zipprich-core'),
              'on_text'     => __('Yes', 'zipprich-core'),
              'off_text'     => __('No', 'zipprich-core'),
            ),

          ),

        ),
        // Topbar Buttons

      ),
    );

    /* Header Shortcodes */
    $options[]     = array(
      'title'      => __('Header Shortcodes', 'zipprich-core'),
      'shortcodes' => array(

        // Address Info
        array(
          'name'          => 'ziph_address_infos',
          'title'         => __('Address Info', 'zipprich-core'),
          'view'          => 'clone',
          'clone_id'      => 'ziph_address_info',
          'clone_title'   => __('Add New', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => __('Custom Class', 'zipprich-core'),
            ),

          ),
          'clone_fields'  => array(
            array(
              'id'        => 'info_icon',
              'type'      => 'icon',
              'title'     => __('Info Icon', 'zipprich-core')
            ),
            array(
              'id'        => 'info_icon_color',
              'type'      => 'color_picker',
              'title'     => __('Info Icon Color', 'zipprich-core'),
            ),
            array(
              'id'        => 'info_main_text',
              'type'      => 'text',
              'title'     => __('Main Text', 'zipprich-core')
            ),
            array(
              'id'        => 'info_main_text_color',
              'type'      => 'color_picker',
              'title'     => __('Main Text Color', 'zipprich-core'),
            ),
            array(
              'id'        => 'info_sec_text',
              'type'      => 'text',
              'title'     => __('Secondary Text', 'zipprich-core'),
            ),
            array(
              'id'        => 'info_sec_text_link',
              'type'      => 'text',
              'title'     => __('Secondary Text Link', 'zipprich-core'),
            ),
            array(
              'id'        => 'info_sec_text_color',
              'type'      => 'color_picker',
              'title'     => __('Secondary Text Color', 'zipprich-core'),
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => __('Open New Tab?', 'zipprich-core'),
              'on_text'     => __('Yes', 'zipprich-core'),
              'off_text'     => __('No', 'zipprich-core'),
            ),

          ),

        ),
        // Address Info

      ),
    );

    /* Content Shortcodes */
    $options[]     = array(
      'title'      => __('Content Shortcodes', 'zipprich-core'),
      'shortcodes' => array(

        // Spacer
        array(
          'name'          => 'vc_empty_space',
          'title'         => __('Spacer', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'height',
              'type'      => 'text',
              'title'     => __('Height', 'zipprich-core'),
              'attributes' => array(
                'placeholder'     => '20px',
              ),
            ),

          ),
        ),
        // Spacer

        // Useful Links
        array(
          'name'          => 'ziph_useful_link',
          'title'         => __('Useful Link', 'trainer'),
          'fields'        => array(
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => __('Custom Class', 'trainer'),
            ),
            array(
              'id'        => 'title_link',
              'type'      => 'text',
              'title'     => __('Link', 'trainer')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => __('Open New Tab?', 'trainer'),
              'on_text'     => __('Yes', 'trainer'),
              'off_text'     => __('No', 'trainer'),
            ),
            array(
              'id'        => 'link_title',
              'type'      => 'text',
              'title'     => __('Title', 'trainer')
            ),
          ),
        ),
        // Useful Links        

        // Brand Color
        array(
          'name'          => 'ziph_brand',
          'title'         => __('Brand Color', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'content',
              'type'      => 'text',
              'title'     => __('Text', 'zipprich-core'),
            ),

            array(
              'id'        => 'color',
              'type'      => 'color_picker',
              'title'     => __('Text Color', 'zipprich-core'),
            ),

          ),
        ),
        // Brand Color

      ),
    );

    /* Footer Shortcodes */
    $options[]     = array(
      'title'      => __('Footer Shortcodes', 'zipprich-core'),
      'shortcodes' => array(

        // Footer Logo
        array(
          'name'          => 'ziph_footer_logo',
          'title'         => __('Footer logo', 'zipprich-core'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => __('Custom Class', 'zipprich-core'),
            ),

            array(
              'id'        => 'logo',
              'type'      => 'image',
              'title'     => __('Upload Logo', 'zipprich-core'),
            ),

            array(
              'id'        => 'width',
              'type'      => 'text',
              'title'     => __('Custom Width', 'zipprich-core'),
              'attributes'         => array(
                'placeholder' => '100',
              ),
            ),

            array(
              'id'        => 'height',
              'type'      => 'text',
              'title'     => __('Custom Height', 'zipprich-core'),
              'attributes'         => array(
                'placeholder' => '100',
              ),
            ),

          ),

        ),
        // Footer Logo

      ),
    );

  return $options;

  }
  add_filter( 'cs_shortcode_options', 'zipprich_ziph_shortcodes' );
}