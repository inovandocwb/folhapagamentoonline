<?php
/*
Plugin Name: Zipprich Core
Plugin URI: https://victorthemes.com/themes/zipprich
Description: Plugin to contain shortcodes and custom post types of the zipprich theme.
Author: VictorThemes
Author URI: https://victorthemes.com/
Version: 1.3
Text Domain: zipprich-core
*/

if( ! function_exists( 'zipprich_block_direct_access' ) ) {
	function zipprich_block_direct_access() {
		if( ! defined( 'ABSPATH' ) ) {
			exit( 'Forbidden' );
		}
	}
}

// Plugin URL
define( 'ZIPPRICH_PLUGIN_URL', plugins_url( '/', __FILE__ ) );

// Plugin PATH
define( 'ZIPPRICH_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'ZIPPRICH_PLUGIN_ASTS', ZIPPRICH_PLUGIN_URL . 'assets' );
define( 'ZIPPRICH_PLUGIN_IMGS', ZIPPRICH_PLUGIN_ASTS . '/images' );
define( 'ZIPPRICH_PLUGIN_INC', ZIPPRICH_PLUGIN_PATH . 'inc' );

// DIRECTORY SEPARATOR
define ( 'DS' , DIRECTORY_SEPARATOR );

// Zipprich Shortcode Path
define( 'ZIPPRICH_SHORTCODE_BASE_PATH', ZIPPRICH_PLUGIN_PATH . 'visual-composer/' );
define( 'ZIPPRICH_SHORTCODE_PATH', ZIPPRICH_SHORTCODE_BASE_PATH . 'shortcodes/' );

/**
 * Check if Codestar Framework is Active or Not!
 */
function zipprich_framework_active() {
  return ( defined( 'CS_VERSION' ) ) ? true : false;
}

/* VTHEME_NAME_P */
define('VTHEME_NAME_P', 'Zipprich', true);

// Initial File
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (is_plugin_active('zipprich-core/zipprich-core.php')) {

	// Custom Post Type
	require_once( ZIPPRICH_PLUGIN_INC . '/custom-post-type.php' );

  // Shortcodes
  require_once( ZIPPRICH_SHORTCODE_BASE_PATH . '/vc-setup.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/custom-shortcodes/theme-shortcodes.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/custom-shortcodes/custom-shortcodes.php' );

  // Widgets
  require_once( ZIPPRICH_PLUGIN_INC . '/widgets/contactform-widget.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/widgets/link-widget.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/widgets/nav-widget.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/widgets/recent-posts.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/widgets/social-widget.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/widgets/text-widget.php' );
  require_once( ZIPPRICH_PLUGIN_INC . '/widgets/widget-extra-fields.php' );

}

/**
 * Plugin language
 */
function zipprich_plugin_language_setup() {
  load_plugin_textdomain( 'zipprich-core', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'init', 'zipprich_plugin_language_setup' );

/* WPAUTOP for shortcode output */
if( ! function_exists( 'zipprich_set_wpautop' ) ) {
  function zipprich_set_wpautop( $content, $force = true ) {
    if ( $force ) {
      $content = wpautop( preg_replace( '/<\/?p\>/', "\n", $content ) . "\n" );
    }
    return do_shortcode( shortcode_unautop( $content ) );
  }
}

/* Use shortcodes in text widgets */
add_filter('widget_text', 'do_shortcode');

/* Shortcodes enable in the_excerpt */
add_filter('the_excerpt', 'do_shortcode');

/* Remove p tag and add by our self in the_excerpt */
remove_filter('the_excerpt', 'wpautop');

/* Add Extra Social Fields in Admin User Profile */
function zipprich_add_twitter_facebook( $contactmethods ) {
  $contactmethods['facebook']   = 'Facebook';
  $contactmethods['twitter']    = 'Twitter';
  $contactmethods['google_plus']  = 'Google Plus';
  $contactmethods['linkedin']   = 'Linkedin';
  return $contactmethods;
}
add_filter('user_contactmethods','zipprich_add_twitter_facebook',10,1);

/* Share Options */
if ( ! function_exists( 'zipprich_wp_share_option' ) ) {
  function zipprich_wp_share_option() {

    global $post;
    $page_url = get_permalink($post->ID );
    $title = $post->post_title;
    $share_text = cs_get_option('share_text');
    $share_text = $share_text ? $share_text : esc_html__( 'SHARE THIS POST', 'zipprich-core' );
    $share_on_text = cs_get_option('share_on_text');
    $share_on_text = $share_on_text ? $share_on_text : esc_html__( 'Share On', 'zipprich-core' );
    ?>
    <div class="ziph-fix  ziph-siglShare_warp">
      <div class="text-left text-uppercase col-sm-6 ziph-siglShare_txt"><?php echo esc_attr($share_text); ?></div>
      <div class="text-right  col-sm-6 ziph-siglShare">
        <ul class="list-inline">
          <li>
            <a href="//twitter.com/home?status=<?php print(urlencode($title)); ?>+<?php print(urlencode($page_url)); ?>" class="icon-fa-twitter" data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr( $share_on_text .' '); echo esc_attr('Twitter', 'zipprich-core'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
          </li>
          <li>
            <a href="//www.facebook.com/sharer/sharer.php?u=<?php print(urlencode($page_url)); ?>&amp;t=<?php print(urlencode($title)); ?>" class="icon-fa-facebook" data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr( $share_on_text .' '); echo esc_attr('Facebook', 'zipprich-core'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
          </li>
          <li>
            <a href="//www.linkedin.com/shareArticle?mini=true&amp;url=<?php print(urlencode($page_url)); ?>&amp;title=<?php print(urlencode($title)); ?>" class="icon-fa-linkedin" data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr( $share_on_text .' '); echo esc_attr('Linkedin', 'zipprich-core'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
          </li>
          <li>
            <a href="//plus.google.com/share?url=<?php print(urlencode($page_url)); ?>" class="icon-fa-google-plus" data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr( $share_on_text .' '); echo esc_attr('Google+', 'zipprich-core'); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
          </li>
        </ul>
      </div>
    </div>
<?php
  }
}

/**
 * Strip Single Tag
 */
if ( ! function_exists( 'zipprich_strip_single_tag' ) ) {
  function zipprich_strip_single_tag( $str, $tag ) {
    $str1 = preg_replace('/<\/'.$tag.'>/i', '', $str);
    if( $str1 != $str ) {
      $str=preg_replace('/<'.$tag.'[^>]*>/i', '', $str1);
    }
    return $str;
  }
}

/**
 * Strip Single p Tag
 */
if ( ! function_exists( 'zipprich_strip_p_tag' ) ) {
  function zipprich_strip_p_tag( $str ) {
    $str1 = preg_replace('/<\/p>/i', '', $str);
    if( $str1 != $str ) {
      $str = preg_replace( '/<p[^>]*>/i', '', $str1 );
    }
    return $str;
  }
}

/**
 * Contact Form 7
 */
function zipprich_wpcf7_form_elements( $form ) {
$form = do_shortcode( $form );
  return $form;
}
add_filter( 'wpcf7_form_elements', 'zipprich_wpcf7_form_elements' );

/* Custom WordPress admin login logo */
if( ! function_exists( 'zipprich_theme_login_logo' ) ) {
  function zipprich_theme_login_logo() {
    $login_logo = cs_get_option('brand_logo_wp');
    if($login_logo) {
      $login_logo_url = wp_get_attachment_url($login_logo);
    } else {
      $login_logo_url = ZIPPRICH_IMAGES . '/logo.png';
    }
    if($login_logo) {
    echo "
      <style>
        body.login #login h1 a {
        background: url('$login_logo_url') no-repeat scroll center bottom transparent;
        height: 100px;
        width: 100%;
        margin-bottom:0px;
        }
      </style>";
    }
  }
  add_action('login_head', 'zipprich_theme_login_logo');
}

/* WordPress admin login logo link */
if( ! function_exists( 'zipprich_login_url' ) ) {
  function zipprich_login_url() {
    return site_url();
  }
  add_filter( 'login_headerurl', 'zipprich_login_url', 10, 4 );
}

/* WordPress admin login logo link */
if( ! function_exists( 'zipprich_login_title' ) ) {
  function zipprich_login_title() {
    return get_bloginfo('name');
  }
  add_filter('login_headertitle', 'zipprich_login_title');
}

/* Zipprich empty array checking */
function zipprich_empty_array($array) {
  $empty = true;
  if (is_array($array)) {
    foreach ($array as $value) {
      if (!zipprich_empty_array($value)) {
        $empty = false;
      }
    }
  } elseif (!empty($array)) {
    $empty = false;
  }
  return $empty;
}

/**
 *
 * Encode string for backup options
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'cs_encode_string' ) ) {
  function cs_encode_string( $string ) {
    return rtrim( strtr( call_user_func( 'base'. '64' .'_encode', addslashes( gzcompress( serialize( $string ), 9 ) ) ), '+/', '-_' ), '=' );
  }
}

/**
 *
 * Decode string for backup options
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'cs_decode_string' ) ) {
  function cs_decode_string( $string ) {
    return unserialize( gzuncompress( stripslashes( call_user_func( 'base'. '64' .'_decode', rtrim( strtr( $string, '-_', '+/' ), '=' ) ) ) ) );
  }
}

/**
 * One Click Install
 * @return Import Demos - Needed Import Demo's
 */
function zipprich_import_files() {
  return array(
    array(
      'import_file_name'           => 'Zipprich',
      'import_file_url'            => trailingslashit( ZIPPRICH_PLUGIN_URL ) . 'inc/import/content.xml',
      'import_widget_file_url'     => trailingslashit( ZIPPRICH_PLUGIN_URL ) . 'inc/import/widget.wie',
      'local_import_csf'           => array(
        array(
          'file_path'   => trailingslashit( ZIPPRICH_PLUGIN_URL ) . 'inc/import/theme-options.json',
          'option_name' => '_cs_options',
        ),
      ),
      'import_notice'              => __( 'Import process may take 1-2 minutes, please be patient. It\'s really based on your network speed.', 'zipprich-core' ),
      'preview_url'                => 'http://victorthemes.com/themes/zipprich',
    ),
  );
}
add_filter( 'pt-ocdi/import_files', 'zipprich_import_files' );

/**
 * One Click Import Function for CodeStar Framework
 */
if ( ! function_exists( 'csf_after_content_import_execution' ) ) {
  function csf_after_content_import_execution( $selected_import_files, $import_files, $selected_index ) {

    $downloader = new OCDI\Downloader();

    if( ! empty( $import_files[$selected_index]['import_csf'] ) ) {

      foreach( $import_files[$selected_index]['import_csf'] as $index => $import ) {
        $file_path = $downloader->download_file( $import['file_url'], 'demo-csf-import-file-'. $index . '-'. date( 'Y-m-d__H-i-s' ) .'.json' );
        $file_raw  = OCDI\Helpers::data_from_file( $file_path );
        update_option( $import['option_name'], json_decode( $file_raw, true ) );
      }

    } else if( ! empty( $import_files[$selected_index]['local_import_csf'] ) ) {

      foreach( $import_files[$selected_index]['local_import_csf'] as $index => $import ) {
        $file_path = $import['file_path'];
        $file_raw  = OCDI\Helpers::data_from_file( $file_path );
        update_option( $import['option_name'], json_decode( $file_raw, true ) );
      }

    }

    // Put info to log file.
    $ocdi       = OCDI\OneClickDemoImport::get_instance();
    $log_path   = $ocdi->get_log_file_path();

    OCDI\Helpers::append_to_file( 'Codestar Framework files loaded.'. $logs, $log_path );

  }
  add_action('pt-ocdi/after_content_import_execution', 'csf_after_content_import_execution', 3, 99 );
}

/**
 * [zipprich_after_import_setup]
 * @return Front Page, Post Page & Menu Set
 */
function zipprich_after_import_setup() {

  // Assign menus to their locations.
  $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

  set_theme_mod( 'nav_menu_locations', array(
      'primary' => $main_menu->term_id,
    )
  );

  // Assign front page and posts page (blog page).
  $front_page_id = get_page_by_title( 'Home One' );
  $blog_page_id  = get_page_by_title( 'Blog List' );

  update_option( 'show_on_front', 'page' );
  update_option( 'page_on_front', $front_page_id->ID );
  update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'zipprich_after_import_setup' );

// Install Demos Menu - Menu Edited
function zipprich_core_one_click_page( $default_settings ) {
  $default_settings['parent_slug'] = 'themes.php';
  $default_settings['page_title']  = esc_html__( 'Install Demos', 'zipprich-core' );
  $default_settings['menu_title']  = esc_html__( 'Install Demos', 'zipprich-core' );
  $default_settings['capability']  = 'import';
  $default_settings['menu_slug']   = 'install_demos';

  return $default_settings;
}
add_filter( 'pt-ocdi/plugin_page_setup', 'zipprich_core_one_click_page' );

// Model Popup - Width Increased
function zipprich_ocdi_confirmation_dialog_options ( $options ) {
  return array_merge( $options, array(
    'width'       => 600,
    'dialogClass' => 'wp-dialog',
    'resizable'   => false,
    'height'      => 'auto',
    'modal'       => true,
  ) );
}
add_filter( 'pt-ocdi/confirmation_dialog_options', 'zipprich_ocdi_confirmation_dialog_options', 10, 1 );

// Disable the branding notice - ProteusThemes
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

function ocdi_plugin_intro_text( $default_text ) {
    $auto_install = admin_url('themes.php?page=install_demos');
    $manual_install = admin_url('themes.php?page=install_demos&import-mode=manual');
    $default_text .= '<h1>Install Demos</h1>
    <div class="vtheme-core_intro-text vtdemo-one-click">
    <div id="poststuff">

      <div class="postbox important-notes">
        <h3><span>Important notes:</span></h3>
        <div class="inside">
          <ol>
            <li>Please note, this import process will take time. So, please be patient.</li>
            <li>Please make sure you\'ve installed recommended plugins before you import this content.</li>
            <li>All images are demo purposes only. So, images may repeat in your site content.</li>
          </ol>
        </div>
      </div>

      <div class="postbox vt-support-box vt-error-box">
        <h3><span>Don\'t Edit Parent Theme Files:</span></h3>
        <div class="inside">
          <p>Don\'t edit any files from parent theme! Use only a <strong>Child Theme</strong> files for your customizations!</p>
          <p>If you get future updates from our theme, you\'ll lose edited customization from your parent theme.</p>
        </div>
      </div>

      <div class="postbox vt-support-box">
        <h3><span>Need Support?</span> <a href="https://www.youtube.com/watch?v=hZyRPSpUvsM" target="_blank" class="cs-section-video"><i class="fa fa-youtube-play"></i> <span>How to?</span></a></h3>
        <div class="inside">
          <p>Have any doubts regarding this installation or any other issues? Please feel free to open a ticket in our support center.</p>
          <a href="http://victorthemes.com/docs/zipprich" class="button-primary" target="_blank">Docs</a>
          <a href="https://victorthemes.com/my-account/support/" class="button-primary" target="_blank">Support</a>
          <a href="https://themeforest.net/item/zipprich-web-hosting-whmcs-wordpress-theme/20702916?ref=VictorThemes" class="button-primary" target="_blank">Item Page</a>
        </div>
      </div>
      <div class="nav-tab-wrapper vt-nav-tab">
        <a href="'. $auto_install .'" class="nav-tab vt-mode-switch vt-auto-mode nav-tab-active">Auto Import</a>
        <a href="'. $manual_install .'" class="nav-tab vt-mode-switch vt-manual-mode">Manual Import</a>
      </div>

    </div>
  </div>';

  return $default_text;
}
add_filter( 'pt-ocdi/plugin_intro_text', 'ocdi_plugin_intro_text' );
